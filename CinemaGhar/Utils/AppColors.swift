//
//  AppColors.swift
//  CinemaGhar
//
//  Created by Midas on 12/03/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import Foundation
import UIKit
class AppColors {
    static let yellowColor = UIColor.init(hex: "f3bc31")
    static let darkColor = UIColor.init(hex: "252B38")
    //static let yellowsecondColor = UIColor.init(hex: "#F1B830")
    static let orangeColor = UIColor.init(hex: "#E59827")
    static let lightDarkColor = UIColor.init(hex: "#585E6B")
    static let redColor = UIColor.init(hex: "#B92C4C")
    static let unSelectedColor = UIColor.init(hex: "#424141")
    static let downloadedProgresscolor = UIColor.init(hex: "76E4BA")
    static let downloadingProgressColor = UIColor.init(hex: "73E6F0")
    static let completedLblColor = UIColor.init(hex: "4F7C8D")
    static let darkRedColor = UIColor.init(hex: "C72B28")
    static let parrotColor = UIColor.init(hex: "EABE51")
    static let darkblueColor = UIColor.init(hex: "39285D")
    static let darkBrinjalColor = UIColor.init(hex: "1C2331")
    static let lightGray = UIColor.init(hex: "CBCBCB")
    static let earnedTickColor = UIColor.init(hex: "9DF072")
    static let darkbackgound = UIColor.init(hex: "#0E1525")
    static let newDark = UIColor.init(hex: "2A303E")
    static let inactive = UIColor.init(hex: "353C4D")
   
    
    
}
struct SCREEN {
    static let WIDTH = UIScreen.main.bounds.width
    static let HEIGHT = UIScreen.main.bounds.height
    static let statusBarHeight = UIApplication.shared.statusBarFrame.height
    @available(iOS 11.0, *)
    static let safeAreaBottom = UIApplication.shared.keyWindow!.safeAreaInsets.bottom
        
//    verticalSafeAreaInset = self.view.safeAreaInsets.bottom + self.view.safeAreaInsets.top

}
