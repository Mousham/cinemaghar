
import Foundation
import Alamofire

class Util {
    
    class func setBackgroundImage(view: UIView, imageName: String){
        UIGraphicsBeginImageContext(view.frame.size)
        UIImage(named: imageName)?.draw(in: view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        view.backgroundColor = UIColor(patternImage: image)
    }
    
    
 class   func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    
    class func addShadow(view: UIView){
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 0.0
        view.layer.masksToBounds = false
    }
    
    class func dropShadow(view: UIView, color: UIColor, opacity: Float, offSet: CGSize, radius: CGFloat, scale: Bool) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOpacity = opacity
        view.layer.shadowOffset = offSet
        view.layer.shadowRadius = radius
        
        //        view.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        view.layer.shouldRasterize = true
        view.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    class func handleImageMotion(withView view: UIView, scaleX: CGFloat, scaleY: CGFloat){
        UIView.animate(withDuration: 10.0, delay: 0, options: [.repeat, .autoreverse], animations: {
            view.transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
        }, completion: nil)
    }
    
    class func showAlert(title:String, message:String, view:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        view.present(alert, animated: true, completion: nil)
    }
    
    class func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    class func marqueeLabel(myLabel: UILabel){
        UIView.animate(withDuration: 12.0, delay: 5, options: ([.curveLinear, .repeat]), animations: {() -> Void in
            myLabel.center = CGPoint(x: myLabel.frame.origin.x - myLabel.bounds.size.width / 2, y: myLabel.center.y)
        }, completion: nil)
    }
    
//    class func resetAsAppRunningFirstTime(){
//        if(UserDefaults.standard.bool(forKey: "HasLaunchedOnce")){
//            // app already launched
//        }else{
//            // This is the first launch ever
//            UserDefaults.standard.set(true, forKey: "HasLaunchedOnce")
//            UserDefaults.standard.synchronize()
//        }
//    }
    
    class func isAppRunningForTheFirstTime() -> Bool{
        var flag: Bool = false
        var result: Bool?
        if !flag{
            if(UserDefaults.standard.bool(forKey: "HasLaunchedOnce")){
                // app already launched
                result = false
            }else{
                // This is the first launch ever
                UserDefaults.standard.set(true, forKey: "HasLaunchedOnce")
                UserDefaults.standard.synchronize()
                result = true
            }
            flag = true
        }
        return result!
    }
    
   
    
   
    
    
    
    class func getCurrentHour() -> Int{
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        let date = picker.date
        let components = Calendar.current.dateComponents([.hour], from: date)
        let hour = components.hour!
        return hour
    }
    
    class func getCurrentMinute() -> Int{
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        let date = picker.date
        let components = Calendar.current.dateComponents([.minute], from: date)
        let minute = components.minute!
        return minute
    }
    
    class func millisecondToDateFormatter(millisecond: Int64) -> String{
        let dateVar = Date.init(timeIntervalSince1970: TimeInterval(millisecond)/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm"
        return dateFormatter.string(from: dateVar)
    }
    
    class func millisecondToDateFormatterWithoutTime(millisecond: Int64) -> String{
        let dateVar = Date.init(timeIntervalSince1970: TimeInterval(millisecond)/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: dateVar)
    }
    
    class func animateTableView(view: UITableView){
        view.reloadData()
        let cells = view.visibleCells
        let collectionNumberHeight = view.bounds.size.height
        for cell in cells{
            cell.transform = CGAffineTransform(translationX: 0, y: collectionNumberHeight)
        }
        var delayCounter = 0
        for cell in cells{
            UIView.animate(withDuration: 1.75, delay: Double(delayCounter) * 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = .identity
            })
            delayCounter += 1
        }
    }
    
    class func stringToDate(specificDate: String) -> Int64{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let date = formatter.date(from: specificDate)
        if let seconds = date?.timeIntervalSince1970{
            let milliseconds = seconds * 1000
            return Int64(milliseconds)
        }
        return 0
    }
    
    class func dateToString(specificDate: Int64) -> String{
        let dateVar = Date.init(timeIntervalSince1970: TimeInterval(specificDate)/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: dateVar)
    }
    
    class func getCurrentDate() -> String{
        let date = Date()
        let calendar = Calendar.current
        let weekDay = calendar.component(.weekday, from: date)
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd yyyy"
        let today = formatter.string(from: date)
        return String(describing: today)
    }
    
        
    
    
}

