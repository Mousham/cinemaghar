import UIKit










class Constant {
    static let testbannerKey = "3940256099942544/2934735716"
    static let testrewardKey = "ca-app-pub-3940256099942544/1712485313"
    static let livebannerKey = "ca-app-pub-4373633293802764/6257681114"
    static let liveRewardKey = "ca-app-pub-4373633293802764/4725107596"
    
    //static let googleAdKey = "ca-app-pub-3940256099942544/2934735716"
    static let googleRewardKey = liveRewardKey
    //static let googleRewardKey = "ca-app-pub-4373633293802764/4725107596"
    static let googleBannerKey = livebannerKey
    //static let googleBannerKey = "ca-app-pub-4373633293802764/6257681114"
    static let downloadsKey = "vid_loader_example_items"
    static let totalItem: CGFloat = 20
    static let column: CGFloat = 3
    static let minLineSpacing: CGFloat = 1.0
    static let minItemSpacing: CGFloat = 1.0
    static let offset: CGFloat = 1.0 // TODO: for each side, define its offset
    static func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        // totalCellWidth = (collectionview width or tableview width) - (left offset + right offset) - (total space x space width)
        let totalWidth = boundWidth - (offset + offset) - ((column - 1) * minItemSpacing)
        return totalWidth / column        
    }
    static let userProgress = "userprogress"
    static let disableOneDay = "disableOneDay"
}
