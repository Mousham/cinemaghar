

import UIKit

class SessionManager {

    
    let defaults = UserDefaults.standard
    
    func isLoggedIn() -> Bool {
    return defaults.bool(forKey: "isLoggedIn")
    }
  
    
    
    func setLogin(loginStatus:Bool){
        defaults.set(loginStatus, forKey: "isLoggedIn")
        defaults.synchronize()
    }
    
    func setAccessToken(token: String){
        defaults.set(token, forKey: "accessToken")
        defaults.synchronize()
    }
    
    func getAccessToken() -> String? {
        return defaults.string(forKey: "accessToken")
    }
    
    
    func isOnBoardingFinished() -> Bool {
        
        return defaults.bool(forKey: "isOnBoardingFinished")
    }
    
    
    func setOnBoardingFinished(status: Bool) {
        
        defaults.set(status, forKey: "isOnBoardingFinished")
        defaults.synchronize()
        
    }
    
    func isTutorialFinished() -> Bool {
        
        return defaults.bool(forKey: "isTutorialFinished")
    }
    
    
    func setIsTutorialFinished(status: Bool) {
        print("sawsank: setIsTutorialFinished = \(status)")
        defaults.set(status, forKey: "isTutorialFinished")
        defaults.synchronize()
        
    }
    
    func setCurrentPlayingTime(id: String, duration: Float64){
        defaults.set(duration, forKey:id)
        defaults.synchronize()
    }
    
    func getCurrentPlayingTime(id: String) -> Float {
        return defaults.float(forKey: id)
    }
    
    func setSessionID(uid: String){
        defaults.set(uid, forKey: "sessionID")
        defaults.synchronize()
    }
    
    func getSessionID() -> String? {
        return defaults.string(forKey: "sessionID")
    }
}

