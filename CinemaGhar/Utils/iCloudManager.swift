//
//  iCloudManager.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 2/24/21.
//  Copyright © 2021 sunBi. All rights reserved.
//

import Foundation
import CloudKit


class iCloudManager {
    
    static let shared = iCloudManager()
    
    let database = CKContainer.init(identifier: "iCloud.com.sagarkharel.cinemagharhd.iCloudContainer").publicCloudDatabase
    func saveToCloud(userID: String){
        print("sawsank: Saving to cloud....")
        let record = CKRecord(recordType: "UserLogin")
       
        record.setValue("Value", forKey: "KeyTest1")
        
        database.save(record) { (record, err) in
            guard let savedRecord = record else {
                return
            }
            print("sawsank: Saved Record \(String(describing: savedRecord.object(forKey: "KeyTest1")))")
        }
        
        
    }
    
    func checkIfUserIDExists(userID: String) -> Bool{
        let record = CKRecord(recordType: "UserLogin")
        let query = CKQuery(recordType: "UserLogin", predicate: NSPredicate(value: true))
        var doesExist = false
        database.perform(query, inZoneWith: nil) { (records, err) in
            guard let records = records else{
                return
            }
            let recordUserID = records.first?.value(forKey: "KeyTest1") as! String
            print("sawsank: recordUserID-> \(recordUserID)")
            if  recordUserID == userID {
                print("sawsank: Already exists")
                doesExist = true
            }else{
                print("sawsank: Doesn't exists")
                doesExist = false
            }
        }
        
        return doesExist
        
    }
}
