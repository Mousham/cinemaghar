//
//  Profile.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 4/17/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import Foundation
import SwiftyJSON

class Profile: NSObject, NSCoding{
    
    var id : String!
    var name : String!
    var image : String!
    var walletBalance : String!
    var walletBalanceRaw: Int!
    var phoneNumber: String!
    var isSubscribed: Bool!
    
 
    init(fromJson json: JSON!){
        if json.isEmpty{
        return
        }
        id = json["id"].stringValue
        name = json["name"].stringValue
        image = json["image"].stringValue
        walletBalance = json["walletBalance"].stringValue
        walletBalanceRaw = json["walletBalanceRaw"].intValue
        phoneNumber = json["phoneNumber"].stringValue
        isSubscribed = json["isSubscribed"].boolValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
        dictionary["id"] = id
        }
        if name != nil{
        dictionary["name"] = name
        }
        if image != nil{
        dictionary["image"] = image
        }
        if walletBalance != nil{
        dictionary["walletBalance"] = walletBalance
        }
        if walletBalanceRaw != nil{
        dictionary["walletBalanceRaw"] = walletBalanceRaw
        }
        if phoneNumber != nil{
        dictionary["phoneNumber"] = phoneNumber
        }
        
        if isSubscribed != nil {
            dictionary["isSubscribed"] = isSubscribed
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
        walletBalance = aDecoder.decodeObject(forKey: "walletBalance") as? String
        walletBalanceRaw = aDecoder.decodeObject(forKey: "walletBalanceRaw") as? Int
        phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as? String
        isSubscribed = aDecoder.decodeObject(forKey: "isSubscribed") as? Bool
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
        aCoder.encode(id, forKey: "id")
        }
        if name != nil{
        aCoder.encode(name, forKey: "name")
        }
        if image != nil{
        aCoder.encode(image, forKey: "image")
        }
        if walletBalance != nil{
        aCoder.encode(walletBalance, forKey: "walletBalance")
        }
        if walletBalanceRaw != nil{
        aCoder.encode(walletBalanceRaw, forKey: "walletBalanceRaw")
        }
        if phoneNumber != nil{
        aCoder.encode(phoneNumber, forKey: "phoneNumber")
        }
        if isSubscribed != nil {
            aCoder.encode(isSubscribed, forKey: "isSubscribed")
        }
    }
    
}
