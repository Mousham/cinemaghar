//
//	Data.swift
//
//	Create by Sunil Gurung on 26/6/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SeriesSeason : NSObject, NSCoding{

	var episodes : [Episode]!
	var season : Int!
    var isUnderSubscription: Bool!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    init(fromJson json: JSON!, isUnderSubscription: Bool!){
		if json.isEmpty{
			return
		}
		episodes = [Episode]()
		let episodesArray = json["episodes"].arrayValue
		for episodesJson in episodesArray{
			let value = Episode(fromJson: episodesJson)
			episodes.append(value)
		}
		season = json["season"].intValue
        self.isUnderSubscription = isUnderSubscription
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if episodes != nil{
			var dictionaryElements = [[String:Any]]()
			for episodesElement in episodes {
				dictionaryElements.append(episodesElement.toDictionary())
			}
			dictionary["episodes"] = dictionaryElements
		}
		if season != nil{
			dictionary["season"] = season
		}
        if isUnderSubscription != nil {
            dictionary["isUnderSubscription"] = isUnderSubscription
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         episodes = aDecoder.decodeObject(forKey: "episodes") as? [Episode]
         season = aDecoder.decodeObject(forKey: "season") as? Int
        isUnderSubscription = aDecoder.decodeObject(forKey: "isUnderSubscription") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if episodes != nil{
			aCoder.encode(episodes, forKey: "episodes")
		}
		if season != nil{
			aCoder.encode(season, forKey: "season")
		}
        if isUnderSubscription != nil {
            aCoder.encode(isUnderSubscription, forKey: "isUnderSubscription")
        }

	}

}
