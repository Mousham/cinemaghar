//
//	Data.swift
//
//	Create by Sunil Gurung on 21/5/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class MovieSlider : NSObject, NSCoding{

	var content : Bool!
	var id : Int!
	var image : String!
	var coverImage : String!
	var title : String!
	var url : String!
    var descriptionField : String!
    var productId: String!
    var type: String!
    
    var movie : Movie!

    


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		content = json["content"].boolValue
		id = json["id"].intValue
		image = json["image"].stringValue
		coverImage = json["coverImage"].stringValue
		title = json["title"].stringValue
		url = json["url"].stringValue
        descriptionField = json["description"].stringValue
        productId = "com.sagarkharel.cinemagharhd."+(String(id))
        type = json["type"].stringValue

        let movieDetailJSON = json["contentDetail"]
        if !movieDetailJSON.isEmpty{
            movie = Movie(fromJson: movieDetailJSON)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if content != nil{
			dictionary["content"] = content
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if coverImage != nil{
			dictionary["coverImage"] = coverImage
		}
		if title != nil{
			dictionary["title"] = title
		}
		if url != nil{
			dictionary["url"] = url
		}
        
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        
        if type != nil {
            dictionary["type"] = type
        }
        
        if movie != nil{
            dictionary["contentDetail"] = movie.toDictionary()
        }
        
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         content = aDecoder.decodeObject(forKey: "content") as? Bool
         id = aDecoder.decodeObject(forKey: "id") as? Int
         image = aDecoder.decodeObject(forKey: "image") as? String
         coverImage = aDecoder.decodeObject(forKey: "coverImage") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         url = aDecoder.decodeObject(forKey: "url") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        movie = aDecoder.decodeObject(forKey: "contentDetail") as? Movie
        descriptionField = aDecoder.decodeObject(forKey: "description") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if content != nil{
			aCoder.encode(content, forKey: "content")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if coverImage != nil{
			aCoder.encode(coverImage, forKey: "coverImage")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if url != nil{
			aCoder.encode(url, forKey: "url")
		}
        
        if type != nil {
            aCoder.encode(type, forKey: "type")
        }
        
        if descriptionField != nil {
            aCoder.encode(descriptionField, forKey: "description")
        }
        
        if movie != nil{
            aCoder.encode(movie, forKey: "contentDetail")
        }

	}

}
