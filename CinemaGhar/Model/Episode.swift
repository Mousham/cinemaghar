//
//	Episode.swift
//
//	Create by Sunil Gurung on 26/6/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Episode : NSObject, NSCoding{

	var descriptionField : String!
	var duration : String!
	var exclusive : Bool!
	var id : Int!
	var poster : String!
	var title : String!
	var video : String!
	var views : Int!
    var videoIsFromYoutube: Bool!
    var paid: Bool!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		
		descriptionField = json["description"].stringValue
		duration = json["duration"].stringValue
		exclusive = json["exclusive"].boolValue
		id = json["id"].intValue
		poster = json["poster"].stringValue
		title = json["title"].stringValue
		video = json["video"].stringValue
		views = json["views"].intValue
        videoIsFromYoutube = json["videoIsFromYoutube"].boolValue
        paid = json["paid"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if duration != nil{
			dictionary["duration"] = duration
		}
		if exclusive != nil{
			dictionary["exclusive"] = exclusive
		}
		if id != nil{
			dictionary["id"] = id
		}
		if poster != nil{
			dictionary["poster"] = poster
		}
		if title != nil{
			dictionary["title"] = title
		}
		if video != nil{
			dictionary["video"] = video
		}
		if views != nil{
			dictionary["views"] = views
		}
        
        if videoIsFromYoutube != nil {
            dictionary["videoIsFromYoutube"] = videoIsFromYoutube
        }
        
        if paid != nil {
            dictionary["paid"] = paid
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         duration = aDecoder.decodeObject(forKey: "duration") as? String
         exclusive = aDecoder.decodeObject(forKey: "exclusive") as? Bool
         id = aDecoder.decodeObject(forKey: "id") as? Int
         poster = aDecoder.decodeObject(forKey: "poster") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         video = aDecoder.decodeObject(forKey: "video") as? String
         views = aDecoder.decodeObject(forKey: "views") as? Int
        videoIsFromYoutube = aDecoder.decodeObject(forKey: "videoIsFromYoutube") as? Bool
        paid = aDecoder.decodeObject(forKey: "paid") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if duration != nil{
			aCoder.encode(duration, forKey: "duration")
		}
		if exclusive != nil{
			aCoder.encode(exclusive, forKey: "exclusive")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if poster != nil{
			aCoder.encode(poster, forKey: "poster")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if video != nil{
			aCoder.encode(video, forKey: "video")
		}
		if views != nil{
			aCoder.encode(views, forKey: "views")
		}
        if videoIsFromYoutube != nil {
            aCoder.encode(videoIsFromYoutube, forKey: "videoIsFromYoutube")
        }
        if paid != nil {
            aCoder.encode(paid, forKey: "paid")
        }

	}

}
