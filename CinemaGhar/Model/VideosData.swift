//
//  VideosData.swift
//  CinemaGhar
//
//  Created by Midas on 02/06/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import Foundation
import VidLoader
struct VideoData: Codable{
    var identifier: String
    var title: String
    var imageName: String
    var state: DownloadState
    var stringURL: String
    var location: URL?
    var coverImage: String?
    var duration: String?
    var year: String?

    init(identifier: String, title: String, imageName: String = "watermark",
         state: DownloadState = .unknown, stringURL: String, location: URL? = nil, coverImage: String,duration: String,year: String) {
        self.identifier = identifier
        self.title = title
        self.imageName = imageName
        self.state = state
        self.stringURL = stringURL
        self.location = location
        self.coverImage = coverImage
        self.duration = duration
        self.year = year
    }
}

