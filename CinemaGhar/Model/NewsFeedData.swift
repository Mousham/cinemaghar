//
//	Data.swift
//
//	Create by Sunil Gurung on 6/2/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class NewsFeedData : NSObject, NSCoding{

    var createdAt : String!
    var descriptionField : String!
    var image : String!
    var images : [String]!
    var shortDescription : String!
    var title : String!
    var type : String!
    var updatedAt : String!
    var video : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(json: JSON!){
        if json.isEmpty{
            return
        }
        createdAt = json["createdAt"].stringValue
        descriptionField = json["description"].stringValue
        image = json["image"].stringValue
        images = [String]()
        let imagesArray = json["images"].arrayValue
        for imagesJson in imagesArray{
            images.append(imagesJson.stringValue)
        }
        shortDescription = json["short_description"].stringValue
        title = json["title"].stringValue
        type = json["type"].stringValue
        updatedAt = json["updatedAt"].stringValue
        video = json["video"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["createdAt"] = createdAt
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if image != nil{
            dictionary["image"] = image
        }
        if images != nil{
            dictionary["images"] = images
        }
        if shortDescription != nil{
            dictionary["short_description"] = shortDescription
        }
        if title != nil{
            dictionary["title"] = title
        }
        if type != nil{
            dictionary["type"] = type
        }
        if updatedAt != nil{
            dictionary["updatedAt"] = updatedAt
        }
        if video != nil{
            dictionary["video"] = video
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
        descriptionField = aDecoder.decodeObject(forKey: "description") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
        images = aDecoder.decodeObject(forKey: "images") as? [String]
        shortDescription = aDecoder.decodeObject(forKey: "short_description") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updatedAt") as? String
        video = aDecoder.decodeObject(forKey: "video") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "createdAt")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if images != nil{
            aCoder.encode(images, forKey: "images")
        }
        if shortDescription != nil{
            aCoder.encode(shortDescription, forKey: "short_description")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updatedAt")
        }
        if video != nil{
            aCoder.encode(video, forKey: "video")
        }
        
    }
    
}
