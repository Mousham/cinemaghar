//
//  VotingPrices.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 3/25/21.
//  Copyright © 2021 sunBi. All rights reserved.
//

import Foundation
import SwiftyJSON


class VotingPrices {
    var productId: String!
    var name: String!
    var amount: String!

    
    init(productId: String, name: String, amount: String){
        self.productId = productId
        self.name = name
        self.amount = amount
    }
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        productId = json["productId"].stringValue
        name = json["name"].stringValue
        amount = json["amount"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
       
        if productId != nil{
            dictionary["productId"] = productId
        }
        if name != nil {
            
            dictionary["name"] = name
        }
        
        if amount != nil{
            dictionary["amount"] = amount
        }
       
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        productId = aDecoder.decodeObject(forKey: "productId") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        amount = aDecoder.decodeObject(forKey: "amount") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
       
        if productId != nil{
            aCoder.encode(productId, forKey: "productId")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if amount != nil{
            aCoder.encode(amount, forKey: "amount")
        }
        
    }
}
