//
//  Event.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 3/24/21.
//  Copyright © 2021 Cinemaghar. All rights reserved.
//

import Foundation
import SwiftyJSON

class Event {
    var imageUrl: String!
    var title: String!
    var votingClose: String!

    
    init(imageUrl: String, title: String, votingClose: String){
        self.imageUrl = imageUrl
        self.title = title
        self.votingClose = votingClose
    }
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        imageUrl = json["imageUrl"].stringValue
        title = json["title"].stringValue
        votingClose = json["votingClose"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
       
        if imageUrl != nil{
            dictionary["imageUrl"] = imageUrl
        }
        if title != nil {
            
            dictionary["title"] = title
        }
        
        if votingClose != nil{
            dictionary["votingClose"] = votingClose
        }
       
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        imageUrl = aDecoder.decodeObject(forKey: "imageUrl") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        votingClose = aDecoder.decodeObject(forKey: "votingClose") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
       
        if imageUrl != nil{
            aCoder.encode(imageUrl, forKey: "imageUrl")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if votingClose != nil{
            aCoder.encode(votingClose, forKey: "votingClose")
        }
        
    }
    
}
