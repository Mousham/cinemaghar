//
//  Genre.swift
//  CinemaGhar
//
//  Created by Sunil on 5/29/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import Foundation
import  SwiftyJSON
class Genre : NSObject, NSCoding{
    
    var movies : [Movie]!
    var icon : String!
    var id : Int!
    var name : String!
    var nextPageURL: String!
    
    
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        movies = [Movie]()
        let contentsArray = json["contents"]["data"].arrayValue
        for contentsJson in contentsArray{
            let value = Movie(fromJson: contentsJson)
            movies.append(value)
        }
        icon = json["icon"].stringValue
        id = json["id"].intValue
        name = json["name"].stringValue

//        nextPageURL = checkForNull(value: json["contents"]["links"]["next"])
        if json["contents"]["links"]["next"] == JSON.null {
            
            nextPageURL = ""
            
        }
        
        else {
            nextPageURL = json["contents"]["links"]["next"].stringValue

        }
        
    }
    
   
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if movies != nil{
            var dictionaryElements = [[String:Any]]()
            for movieElement in movies {
                dictionaryElements.append(movieElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        if icon != nil{
            dictionary["icon"] = icon
        }
        if id != nil{
            dictionary["id"] = id
        }
        if name != nil{
            dictionary["name"] = name
        }
        if nextPageURL != nil{
            dictionary["nextPageURL"] = nextPageURL
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        movies = aDecoder.decodeObject(forKey: "data") as? [Movie]
        icon = aDecoder.decodeObject(forKey: "icon") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
        nextPageURL = aDecoder.decodeObject(forKey: "nextPageURL") as? String

        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if movies != nil{
            aCoder.encode(movies, forKey: "data")
        }
        if icon != nil{
            aCoder.encode(icon, forKey: "icon")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if nextPageURL != nil {
            
            aCoder.encode(nextPageURL, forKey: "nextPageURL")
        }
        
    }
    
}
