//
//  Movie.swift
//  CinemaGhar
//
//  Created by Sunil Gurung on 5/15/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import Foundation
import SwiftyJSON


class Trending : NSObject, NSCoding{
    
    var cast : String!
    var category : String!
    var coverImage : String!
    var descriptionField : String!
    var duration : String!
    var exclusive : Bool!
    var expiryDate : String!
    var genre : String!
    var id : Int!
    var paid : Bool!
    var poster : String!
    var price : Int!
    var rating : Double!
    var releaseYear : String!
    var title : String!
    var trailerUrl : String!
    var video : String!
    var views : Int!
    var BackGroundImage: String!
    var priceDollar: Int!
    var PriceIAP: String!
    var isAddedToWishList: Bool!
    var isBought: Bool!
    var isUnderSubscription: Bool!
    var detailPageUrl: String!
    
    var videoIsFromYoutube: Bool!
    var slug: String!


    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        cast = json["cast"].stringValue
        category = json["category"].stringValue
        coverImage = json["cover_image"].stringValue
        descriptionField = json["description"].stringValue
        duration = json["duration"].stringValue
        exclusive = json["exclusive"].boolValue
        expiryDate = json["expiryDate"].stringValue
        genre = json["genre"].stringValue
        id = json["id"].intValue
        paid = json["paid"].boolValue
        poster = json["poster"].stringValue
        price = json["price"].intValue
        priceDollar = json["priceDollar"].intValue
        rating = json["rating"].doubleValue
        releaseYear = json["releaseYear"].stringValue
        title = json["title"].stringValue
        trailerUrl = json["trailerUrl"].stringValue
        video = json["video"].stringValue
        views = json["views"].intValue
        BackGroundImage = json["background_image"].stringValue
        isAddedToWishList = json["isWishlisted"].boolValue
        isBought = json["isBought"].boolValue
        isUnderSubscription = json["isUnderSubscription"].boolValue
        detailPageUrl = json["detailPageUrl"].stringValue
        
        videoIsFromYoutube = json["videoIsFromYoutube"].boolValue
        slug = json["slug"].stringValue

        
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
       
        if cast != nil{
            dictionary["cast"] = cast
        }
        if priceDollar != nil {
            
            dictionary["priceDollar"] = priceDollar
        }
        
        if category != nil{
            dictionary["category"] = category
        }
        if coverImage != nil{
            dictionary["cover_image"] = coverImage
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if duration != nil{
            dictionary["duration"] = duration
        }
        if exclusive != nil{
            dictionary["exclusive"] = exclusive
        }
        if expiryDate != nil{
            dictionary["expiryDate"] = expiryDate
        }
        if genre != nil{
            dictionary["genre"] = genre
        }
        if id != nil{
            dictionary["id"] = id
        }
        if paid != nil{
            dictionary["paid"] = paid
        }
        if poster != nil{
            dictionary["poster"] = poster
        }
        if price != nil{
            dictionary["price"] = price
        }
        if rating != nil{
            dictionary["rating"] = rating
        }
        if releaseYear != nil{
            dictionary["releaseYear"] = releaseYear
        }
        if title != nil{
            dictionary["title"] = title
        }
        if trailerUrl != nil{
            dictionary["trailerUrl"] = trailerUrl
        }
        if video != nil{
            dictionary["video"] = video
        }
        if views != nil{
            dictionary["views"] = views
        }
        
        if BackGroundImage != nil {
            dictionary["backgroundImage"] = BackGroundImage
        }
        
        if isAddedToWishList != nil {
            dictionary["isWishlisted"] = isAddedToWishList
        }
        
        if isBought != nil {
            dictionary["isBought"] = isBought
        }
        if isUnderSubscription != nil {
            dictionary["isUnderSubscription"] = isUnderSubscription
        }
        if detailPageUrl != nil {
            dictionary["detailPageUrl"] = detailPageUrl
        }
        if videoIsFromYoutube != nil {
            dictionary["videoIsFromYoutube"] = videoIsFromYoutube
            
        }
        
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        cast = aDecoder.decodeObject(forKey: "cast") as? String
        category = aDecoder.decodeObject(forKey: "category") as? String
        coverImage = aDecoder.decodeObject(forKey: "cover_image") as? String
        descriptionField = aDecoder.decodeObject(forKey: "description") as? String
        duration = aDecoder.decodeObject(forKey: "duration") as? String
        exclusive = aDecoder.decodeObject(forKey: "exclusive") as? Bool
        expiryDate = aDecoder.decodeObject(forKey: "expiryDate") as? String
        genre = aDecoder.decodeObject(forKey: "genre") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        paid = aDecoder.decodeObject(forKey: "paid") as? Bool
        poster = aDecoder.decodeObject(forKey: "poster") as? String
        price = aDecoder.decodeObject(forKey: "price") as? Int
        rating = aDecoder.decodeObject(forKey: "rating") as? Double
        releaseYear = aDecoder.decodeObject(forKey: "releaseYear") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        trailerUrl = aDecoder.decodeObject(forKey: "trailerUrl") as? String
        video = aDecoder.decodeObject(forKey: "video") as? String
        views = aDecoder.decodeObject(forKey: "views") as? Int
        BackGroundImage = aDecoder.decodeObject(forKey: "backgroundImage") as? String
        priceDollar = aDecoder.decodeObject(forKey: "priceDollar") as? Int
        isAddedToWishList = aDecoder.decodeObject(forKey: "isWishlisted") as? Bool
        isBought = aDecoder.decodeObject(forKey: "isBought") as? Bool
        isUnderSubscription = aDecoder.decodeObject(forKey: "isUnderSubscription") as? Bool
        detailPageUrl = aDecoder.decodeObject(forKey: "detailPageUrl") as? String
        videoIsFromYoutube = aDecoder.decodeObject(forKey: "videoIsFromYoutube") as? Bool

        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
       
        if cast != nil{
            aCoder.encode(cast, forKey: "cast")
        }
        if category != nil{
            aCoder.encode(category, forKey: "category")
        }
        if coverImage != nil{
            aCoder.encode(coverImage, forKey: "cover_image")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if duration != nil{
            aCoder.encode(duration, forKey: "duration")
        }
        if exclusive != nil{
            aCoder.encode(exclusive, forKey: "exclusive")
        }
        if expiryDate != nil{
            aCoder.encode(expiryDate, forKey: "expiryDate")
        }
        if genre != nil{
            aCoder.encode(genre, forKey: "genre")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if paid != nil{
            aCoder.encode(paid, forKey: "paid")
        }
        if poster != nil{
            aCoder.encode(poster, forKey: "poster")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if rating != nil{
            aCoder.encode(rating, forKey: "rating")
        }
        if releaseYear != nil{
            aCoder.encode(releaseYear, forKey: "releaseYear")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if trailerUrl != nil{
            aCoder.encode(trailerUrl, forKey: "trailerUrl")
        }
        if video != nil{
            aCoder.encode(video, forKey: "video")
        }
        if views != nil{
            aCoder.encode(views, forKey: "views")
        }
        if BackGroundImage != nil{
            aCoder.encode(BackGroundImage, forKey: "backgroundImage")
        }
        
        if priceDollar != nil {
            aCoder.encode(priceDollar , forKey: "priceDollar")
        }
        
        if isAddedToWishList != nil{
            
            aCoder.encode(isAddedToWishList, forKey: "isWishlisted")
        }
        
        
        if isBought != nil{
            
            aCoder.encode(isBought, forKey: "isBought")

        }
        
        if isUnderSubscription != nil{
            
            aCoder.encode(isUnderSubscription, forKey: "isUnderSubscription")

        }
        
        if detailPageUrl != nil{
            
            aCoder.encode(detailPageUrl, forKey: "detailPageUrl")

        }
        if videoIsFromYoutube != nil{
            
            aCoder.encode(videoIsFromYoutube, forKey: "videoIsFromYoutube")
        }
        
        
    }
    
}
