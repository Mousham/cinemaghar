//
//  Advertisements.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 11/11/20.
//  Copyright © 2020 sunBi. All rights reserved.
//


import Foundation
import SwiftyJSON


class Advertisements : NSObject, NSCoding{
    
    var type : String!
    var video : String!
    var url : String!
    var image : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        type = json["type"].stringValue
        video = json["video"].stringValue
        url = json["url"].stringValue
        image = json["image"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
       
        if type != nil{
            dictionary["type"] = type
        }
        if video != nil {
            
            dictionary["video"] = video
        }
        
        if url != nil{
            dictionary["url"] = url
        }
        if image != nil{
            dictionary["image"] = image
        }

        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        type = aDecoder.decodeObject(forKey: "type") as? String
        video = aDecoder.decodeObject(forKey: "video") as? String
        url = aDecoder.decodeObject(forKey: "url") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
       
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
       
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if video != nil{
            aCoder.encode(video, forKey: "video")
        }
        if url != nil{
            aCoder.encode(url, forKey: "url")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
   
        
    }
    
}

