//
//	Data.swift
//
//	Create by Sunil Gurung on 25/5/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SignUpModel : NSObject, NSCoding{

	var address : String!
	var email : String!
	var id : Int!
	var name : String!
	var phoneNumber : String!
    var uniqueId: String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		address = json["address"].stringValue
		email = json["email"].stringValue
		id = json["id"].intValue
		name = json["name"].stringValue
		phoneNumber = json["phoneNumber"].stringValue
        uniqueId = json["uniqueId"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if address != nil{
			dictionary["address"] = address
		}
		if email != nil{
			dictionary["email"] = email
		}
		if id != nil{
			dictionary["id"] = id
		}
		if name != nil{
			dictionary["name"] = name
		}
		if phoneNumber != nil{
			dictionary["phoneNumber"] = phoneNumber
		}
        if uniqueId != nil{
            dictionary["uniqueId"] = uniqueId
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         name = aDecoder.decodeObject(forKey: "name") as? String
         phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as? String
         uniqueId = aDecoder.decodeObject(forKey: "uniqueId") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if phoneNumber != nil{
			aCoder.encode(phoneNumber, forKey: "phoneNumber")
		}
        if uniqueId != nil{
            aCoder.encode(uniqueId, forKey: "uniqueId")
        }

	}

}
