//
//  TestStatementModel.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 4/16/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import Foundation
import SwiftyJSON

class Statement {
    var mediaTitle: String!
    var createdAt: String!
    var price: String!
    var type: String!
    var paymentGateway: String!
    var mediaType: String!
    var withCoupon: String!
    var voucherCode: String!
    
    init(mediaTitle: String, createdAt: String, price: String, type: String, paymentGateway: String, mediaType: String, withCoupon: String, voucherCode: String){
        self.mediaTitle = mediaTitle
        self.createdAt = createdAt
        self.price = price
        self.type = type
        self.paymentGateway = paymentGateway
        self.mediaType = mediaType
        self.withCoupon = withCoupon
        self.voucherCode = voucherCode
    }
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        mediaTitle = json["mediaTitle"].stringValue
        createdAt = json["createdAt"].stringValue
        price = json["price"].stringValue
        type = json["type"].stringValue
        paymentGateway = json["paymentGateway"].stringValue
        mediaType = json["mediaType"].stringValue
        withCoupon = json["withCoupon"].stringValue
        voucherCode = json["voucherCode"].stringValue
        
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
       
        if mediaTitle != nil{
            dictionary["mediaTitle"] = mediaTitle
        }
        if createdAt != nil {
            
            dictionary["createdAt"] = createdAt
        }
        
        if price != nil{
            dictionary["price"] = price
        }
        if type != nil{
            dictionary["type"] = type
        }
        if paymentGateway != nil{
            dictionary["paymentGateway"] = paymentGateway
        }
        
        if mediaType != nil{
            dictionary["mediaType"] = mediaType
        }
        
        if withCoupon != nil{
            dictionary["withCoupon"] = withCoupon
        }
        
        if voucherCode != nil{
            dictionary["voucherCode"] = voucherCode
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        mediaTitle = aDecoder.decodeObject(forKey: "mediaTitle") as? String
        createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
        price = aDecoder.decodeObject(forKey: "price") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        paymentGateway = aDecoder.decodeObject(forKey: "paymentGateway") as? String
        mediaType = aDecoder.decodeObject(forKey: "mediaType") as? String
        withCoupon = aDecoder.decodeObject(forKey: "withCoupon") as? String
        voucherCode = aDecoder.decodeObject(forKey: "voucherCode") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
       
        if mediaTitle != nil{
            aCoder.encode(mediaTitle, forKey: "mediaTitle")
        }
        if createdAt != nil{
            aCoder.encode(time, forKey: "createdAt")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if paymentGateway != nil{
            aCoder.encode(paymentGateway, forKey: "paymentGateway")
        }
        if mediaType != nil{
            aCoder.encode(mediaType, forKey: "mediaType")
        }
        if withCoupon != nil{
            aCoder.encode(withCoupon, forKey: "withCoupon")
        }
        if voucherCode != nil{
            aCoder.encode(voucherCode, forKey: "voucherCode")
        }
        
    }
    
}
