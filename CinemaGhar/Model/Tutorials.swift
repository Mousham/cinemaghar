//
//  Tutorials.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 12/26/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import Foundation
import SwiftyJSON

class Tutorials: NSObject, NSCoding{
    
    var id : String!
    var title : String!
    var desc : String!
    var links : [JSON]!

    
 
    init(fromJson json: JSON!){
        if json.isEmpty{
        return
        }
        id = json["id"].stringValue
        title = json["title"].stringValue
        desc = json["description"].stringValue
        links = json["links"].array
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
        dictionary["id"] = id
        }
        if title != nil{
        dictionary["title"] = title
        }
        if desc != nil{
        dictionary["description"] = desc
        }
        if links != nil{
        dictionary["links"] = links
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        desc = aDecoder.decodeObject(forKey: "description") as? String
        links = aDecoder.decodeObject(forKey: "links") as? Array
       
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
        aCoder.encode(id, forKey: "id")
        }
        if title != nil{
        aCoder.encode(title, forKey: "title")
        }
        if desc != nil{
        aCoder.encode(desc, forKey: "description")
        }
        if links != nil{
        aCoder.encode(links, forKey: "links")
        }
    }
    
}
struct VotingModel {
    var entity: String?
    var id: Int?
    var title: String?
    var description: String?
    var coverImage: String?
   
    init(json: JSON) {
        self.entity = json["entity"].string ?? ""
        self.id = json["id"].intValue 
        self.title = json["title"].string ?? ""
        self.description = json["description"].string ?? ""
        self.coverImage = json["coverImage"].string ?? ""
       
    }
}
struct VotingDetailsModel {
    var coverImage: String?
    var id: Int?
    var entity: String?
    var description: String?
    var title: String?
   
    init(json: JSON) {
        self.coverImage = json["coverImage"].string ?? ""
        self.id = json["id"].intValue
        self.entity = json["entity"].string ?? ""
        self.description = json["description"].string ?? ""
        self.title = json["title"].string ?? ""
       
    }
}
struct QuestionModel {
    var id: Int?
    var question: String?
    var ans1: String?
    var ans2: String?
    var ans3: String?
    var ans4: String?
    var write_answer: Int?
    var created_at: String?
    var updated_at: String?
   
    init(json: JSON) {
        self.id = json["id"].int ?? 0
        self.question = json["question"].string
        self.ans1 = json["ans1"].string ?? ""
        self.ans2 = json["ans2"].string ?? ""
        self.ans3 = json["ans3"].string ?? ""
        self.ans4 = json["ans4"].string
        self.write_answer = json["write_answer"].int ?? 0
        self.created_at = json["created_at"].string ?? ""
        self.updated_at = json["updated_at"].string ?? ""
       
    }
}
struct VotingPriceModel {
    var votes: Int?
    var id: Int?
    var coins: Int?
    var isSelected: Bool = false
    var status: Bool
    var message: String
    var token = [String]()
    var code: Int?
   
   
    init(json: JSON) {
        self.votes = json["votes"].intValue ?? 0
        self.id = json["id"].intValue
        self.coins = json["coins"].intValue ?? 0
        self.status = json["status"].boolValue ?? false
        self.message = json["message"].stringValue ?? ""
        self.token = json["token"].arrayValue as? [String] ?? []
        self.code = json["code"].intValue ?? 0
       
       
    }
}
struct DefaultResponse {
    var code: Int?
    var message: String?
    var status: Bool?
    init(json: JSON) {
        self.code = json["code"].intValue
        self.message = json["message"].stringValue
        self.status = json["status"].boolValue
       
       
    }
}
