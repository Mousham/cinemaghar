//
//  Notifications.swift
//  CinemaGhar
//
//  Created by Sunil on 11/23/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import Foundation
import SwiftyJSON


class Notifications : NSObject, NSCoding{
    
    var body : String!
    var createdAt : String!
    var title : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        body = json["body"].stringValue
        createdAt = json["createdAt"].stringValue
        title = json["title"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if body != nil{
            dictionary["body"] = body
        }
        if createdAt != nil{
            dictionary["createdAt"] = createdAt
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        body = aDecoder.decodeObject(forKey: "body") as? String
        createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if body != nil{
            aCoder.encode(body, forKey: "body")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "createdAt")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        
    }
    
}

