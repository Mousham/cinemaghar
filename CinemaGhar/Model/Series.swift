
import Foundation
import SwiftyJSON


class Series : NSObject, NSCoding{
    
    var id : Int!
    var image : String!
    var name : String!
    var paid : Bool!
    var price : Int!
    var rating: Int!
    var priceDollar: Int!
    var casts: String!
    var desc: String!
    var releaseYear: String!
    var isBought: Bool!
    var isAddedToWishList: Bool!
    var videoIsFromYoutube: Bool!
    var isUnderSubscription: Bool!
    var slug:String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        image = json["image"].stringValue
        name = json["name"].stringValue
        paid = json["paid"].boolValue
        price = json["price"].intValue
        rating = json["rating"].intValue
        priceDollar = json["priceDollar"].intValue
        casts = json["cast"].stringValue
        desc = json["description"].stringValue
        releaseYear = json["releaseYear"].stringValue
        isBought = json["isBought"].boolValue
        isAddedToWishList = json["isWishlisted"].boolValue
        videoIsFromYoutube = json["videoIsFromYoutube"].boolValue
        slug = json["slug"].stringValue
        isUnderSubscription = json["isUnderSubscription"].bool

    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
       
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if name != nil{
            dictionary["name"] = name
        }
        if paid != nil{
            dictionary["paid"] = paid
        }
        if price != nil{
            dictionary["price"] = price
        }
        if rating != nil {
            dictionary["rating"] = rating
        }
            
        if priceDollar != nil {
            dictionary["priceDollar"] = priceDollar
        }
        if casts != nil {
            dictionary["cast"] = casts
        }
        if desc != nil {
            dictionary["description"] = desc
        }
        if releaseYear != nil {
            dictionary["releaseYear"] = releaseYear
        }
        if isBought != nil {
            dictionary["isBought"] = isBought
        }
        if isAddedToWishList != nil {
            dictionary["isWishlisted"] = isAddedToWishList
        }
        
        if videoIsFromYoutube != nil {
            dictionary["videoIsFromYoutube"] = videoIsFromYoutube

            
        }
        if slug != nil {
            dictionary["slug"] = slug

        }
        if isUnderSubscription != nil {
            dictionary["isUnderSubscription"] = isUnderSubscription
        }
        
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        image = aDecoder.decodeObject(forKey: "image") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        paid = aDecoder.decodeObject(forKey: "paid") as? Bool
        price = aDecoder.decodeObject(forKey: "price") as? Int
        rating = aDecoder.decodeObject(forKey: "rating") as? Int
        priceDollar = aDecoder.decodeObject(forKey: "priceDollar") as? Int
        casts = aDecoder.decodeObject(forKey: "cast") as? String
        desc = aDecoder.decodeObject(forKey: "description") as? String
        releaseYear = aDecoder.decodeObject(forKey: "releaseYear") as? String
        isBought = aDecoder.decodeObject(forKey: "isBought") as? Bool
        isAddedToWishList = aDecoder.decodeObject(forKey: "isWishlisted") as? Bool
        videoIsFromYoutube = aDecoder.decodeObject(forKey: "videoIsFromYoutube") as? Bool
        slug = aDecoder.decodeObject(forKey: "slug") as? String
        isUnderSubscription = aDecoder.decodeObject(forKey: "isUnderSubscription") as? Bool
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if paid != nil{
            aCoder.encode(paid, forKey: "paid")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        
        if rating != nil {
            aCoder.encode(rating, forKey : "rating")
        }
        if priceDollar != nil {
            aCoder.encode(priceDollar, forKey: "priceDollar")
        }
        if casts != nil {
            aCoder.encode(casts , forKey: "cast")
        }
        if desc != nil {
            aCoder.encode(desc, forKey: "description")
        }
        if releaseYear != nil {
            aCoder.encode(releaseYear, forKey: "releaseYear")
        }
        
        if isBought != nil{
            
            aCoder.encode(isBought, forKey: "isBought")
            
        }
        if isAddedToWishList != nil{
            
            aCoder.encode(isAddedToWishList, forKey: "isWishlisted")
        }
        
        if videoIsFromYoutube != nil{
            
            aCoder.encode(videoIsFromYoutube, forKey: "videoIsFromYoutube")
        }
        
        if slug != nil {
            aCoder.encode(slug, forKey: "slug")

        }
        if isUnderSubscription != nil {
            aCoder.encode(isUnderSubscription, forKey: "isUnderSubscription")
        }
        
    }
    
}
