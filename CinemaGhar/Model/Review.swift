//
//	Data.swift
//
//	Create by Sunil Gurung on 4/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Review : NSObject, NSCoding{

	var descriptionField : String!
	var image : String!
	var rating : Int!
	var reviewedBy : String!
	var title : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		descriptionField = json["description"].stringValue
		image = json["image"].stringValue
		rating = json["rating"].intValue
		reviewedBy = json["reviewedBy"].stringValue
		title = json["title"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if image != nil{
			dictionary["image"] = image
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if reviewedBy != nil{
			dictionary["reviewedBy"] = reviewedBy
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         rating = aDecoder.decodeObject(forKey: "rating") as? Int
         reviewedBy = aDecoder.decodeObject(forKey: "reviewedBy") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if reviewedBy != nil{
			aCoder.encode(reviewedBy, forKey: "reviewedBy")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}
