
//
//  Created by Sunil Gurung on 8/1/17.
//  Copyright © 2017 Sunil Gurung. All rights reserved.
//

import Foundation
import UIKit



struct CinemaGharAPI {
    //live url
    static let baseURL = "https://cinema-ghar.com"
    //static let baseURL = "https://stg.cinema-ghar.com"
    
    //dev url
   // static let baseURL = "https://staging.cinema-ghar.com"
    
    static let baseURLString = baseURL + "/api/"

    //Home
    static let homeURL = baseURL + "/home"
    
    //Auth
    static let registerationURL = baseURLString + "auth/register"
    static let passCodeVerificationURL = baseURLString + "auth/verify-registration-code"
    static let loginURL = baseURLString + "auth/login"
    static let forgotPassword = baseURLString + "auth/forgot-password"
    static let resendPassword = baseURLString + "auth/resend-registration-code"
    static let changePassword = baseURLString + "auth/change-password"
    
    //Profile
    static let profileURL = baseURLString + "profile"
    static let statements = baseURLString + "statements"
    static let profileUpdateURL = baseURLString + "profile/update"
    
    static let featuredMovieURL = baseURLString + "contents?"
    static let comedySeriesURL = baseURLString + "comedy-series"
    
    static let newsFeedURL = baseURLString + "news-feed"
    static let registerFCMToken = baseURLString + "save-device-token"
    
    static let notificationsURL = baseURLString + "notifications"
    static let webSeriesURL = baseURLString + "series"
    static let movieDetailURL = baseURLString + "content"
    
    //Wallet Flow
    static let walletVerifyPin = baseURLString + "wallet/verify-pin"
    static let verifyPayment = baseURLString + "process-payment"
    
    //Load Wallet
    static let loadWallet = baseURLString + "wallet/load"
    static let walletMaxCoinReached = baseURLString + "wallet/max-coins-reached"
    
    //Content
    static let genreURL = baseURLString + "genres"
    static let getAllPurchases = baseURLString + "my-orders"
    
    static let verifyPromoCode = baseURLString + "verify-coupon"
    static let searchURL = baseURLString + "search"
    static let addToWishListURL = baseURLString + "add-to-wishlists"
    static let removeFromWishListURL = baseURLString + "remove-from-wishlists"
    
    static let checkIfAppIsBlocked = baseURLString + "block-device?deviceType=ios"
    static let checkSettings = baseURLString + "settings"

    static let getShorts = baseURLString + "short-movies"
    
    static let tutorials = baseURLString + "tutorials"
    
    static let saveWatchedDuration = baseURLString + "save-watched-duration"

    static let tokenPayment = baseURLString + "subscribe-via-token"
    
    //Buy content via Token
    static let buyViaToken = baseURLString + "buy-movie-via-token"
    static let votingshows = baseURLString + "voting-shows"
    static let votingdetails = baseURLString + "voting-contestant/" + String(globalVotingId)
    static let votingPrice = baseURLString + "voting-prices/" + String(globalVotingId)
    static let postVote = baseURLString + "vote"
    static let loginChallenge = baseURLString + "wallet/recharge"
    static let loadWithReachargeCard = baseURLString + "wallet/recharge"
    
    
    //Prabhu Voting
    static let getVotingPrices = baseURLString + "voting/prices"
    static let voting = baseURL + "/voting"
    
    //Auto Login on Web
    static let authInitialization = baseURLString + "webview-login/initialize?scope=voting"
    
}


struct AppConstants {
    static let userName = "userName"
    static let userId = "userId"
    static let userPhoneNum = "userPhoneNum"
    static let userEmail = "userEmail"
    static let accessToken = "accessToken"
    static let uniqueId = "uniqueId"
}

