//
//  APIHandler.swift
//  CinemaGhar
//
//  Created by Sunil Gurung on 5/15/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import FirebaseMessaging
var globalVotingId = Int()

enum MovieType:String {
    case featured = "featured"
    case latest = "latest"
    case exclusive =  "exclusive"
    
    case paid = "paid"
    case  free = "free"
}

enum SignUpType:String {
    case facebook = "facebook"
    case google = "google"
    case normal = "normal"
}

enum paymentGateway:String {
    case eSewa = "esewa"
    case payPal = "paypal"
    case khalti = "khalti"
    case appleIAP = "appleInAppPurchase"
    case wallet = "wallet"
}

enum MediaType: String {
    case series = "series"
    case movie = "content"
    case subscription = "subscription"
}


class APIHandler: NSObject {
    
    static let shared = APIHandler()
    
    private override init() {
        
    }
    
    
    
//    let accessToken = SessionManager().getAccessToken()
//    let headers = ["Accept": "application/json"]
//    let headers = ["Accept": "application/json", "X-VERSION": "1", "X-DEVICE-TYPE": "ios", "X-DEVICE-ID": deviceID, "Authorization": SessionManager().getAccessToken() ?? ""]
    
    func createHeader() -> HTTPHeaders{
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let deviceName = UIDevice.current.name
        let headers: HTTPHeaders = ["Accept": "application/json", "X-VERSION": "1.1", "X-DEVICE-TYPE": "ios", "X-DEVICE-ID": deviceID, "X-DEVICE-MODEL": deviceName, "Authorization": SessionManager().getAccessToken() ?? ""]
        print(headers)
        return headers
    }
//    func headerForVoting() -> HTTPHeader {
//        let headers: HTTPHeaders = ["Accept": "application/json", "X-VERSION": "1.1", "X-DEVICE-TYPE": "ios", "X-DEVICE-ID": deviceID, "X-DEVICE-MODEL": deviceName, "Authorization": SessionManager().getAccessToken() ?? ""]
//    }

    func getMovies( pageId: Int = 1,  movieType: MovieType, success:@escaping (_ status:Bool, _ movies:[Movie], _ nextPageID: String) -> (), failure:@escaping (_ message: Error) -> ()){
        
        let url = CinemaGharAPI.baseURLString+"contents?"+movieType.rawValue+"&page="+String(pageId)
        print(url)
        
        
        AF.request(url, method: .get , parameters: nil, encoding:  JSONEncoding.default, headers: createHeader()).responseJSON { (response) in
            switch(response.result) {
                
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                                        print(json)
                    var movieArray = [Movie]()
                    if let movieJSONArray = json["data"].array{
                        if(movieJSONArray.count>0){
                            for i in 0..<movieJSONArray.count{
                                let movieObj = Movie(fromJson: movieJSONArray[i])
                                movieArray.append(movieObj)
                                
                            }
                            
                        }
                        
                        success(true, movieArray, json["links"]["next"].stringValue)
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                    
                    
                    
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
            
        }
        
        
    }
    
    func getWebSeries(success: @escaping (_ webseries: [Series]) -> (), failure: @escaping (_ error: Error)->()){
        
        let url = CinemaGharAPI.webSeriesURL
        
        
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{(response: AFDataResponse<Any>) in
            
            switch(response.result){
                
                
            case .success(_):
                
                if let value = response.value{
                    let json = JSON(value)
                                        print(json)
                    var webSeriesArray = [Series]()
                    if let webSeriesJSONArray = json["data"].array{
                        if(webSeriesJSONArray.count>0){
                            for i in 0..<webSeriesJSONArray.count{
                                let webSeriesObj = Series(fromJson: webSeriesJSONArray[i])
                                webSeriesArray.append(webSeriesObj)
                            }
                            
                        }
                        success( webSeriesArray)
                    }
                    
                    
                    
                }
                break
                
                
            case .failure(let error):
                
                failure(error)
                
                
                
            }
        }
        
        
    }
    
    
    
    
    func getComedyWebSeries(success: @escaping (_ webseries: [Series]) -> (), failure: @escaping (_ error: Error)->()){
        
        let url = CinemaGharAPI.comedySeriesURL
        
       
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{(response: AFDataResponse<Any>) in
            
            switch(response.result){
                
                
            case .success(_):
                
                if let value = response.value{
                    let json = JSON(value)
                                        print(json)
                    var webSeriesArray = [Series]()
                    if let webSeriesJSONArray = json["data"].array{
                        if(webSeriesJSONArray.count>0){
                            for i in 0..<webSeriesJSONArray.count{
                                let webSeriesObj = Series(fromJson: webSeriesJSONArray[i])
                                webSeriesArray.append(webSeriesObj)
                            }
                            
                        }
                        success( webSeriesArray)
                    }
                    
                    
                    
                }
                break
                
                
            case .failure(let error):
                
                failure(error)
                
                
                
            }
        }
        
        
    }
    
    
    func getAllPurchasedItems(success: @escaping(_ movies: [Movie], _ series: [Series])->(), failure: @escaping(_ error:Error) ->()){
        
        
        
        let url = CinemaGharAPI.getAllPurchases
        
        
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{(response: AFDataResponse<Any>) in
            
            switch response.result {
            case .success(_):
                if let value = response.value {
                    let json = JSON(value)
                    print(json)
                    var moviesArray = [Movie]()
                    var seriesArray = [Series]()
                    
                    if let moviesJSONArray = json["data"]["contents"].array{
                        for i in 0..<moviesJSONArray.count{
                            let movieObj = Movie(fromJson: moviesJSONArray[i])
                            moviesArray.append(movieObj)
                        }
                    }
                    
                    if let seriesJSOnArray = json["data"]["series"].array{
                        for i in 0..<seriesJSOnArray.count{
                            let seriesObj = Series(fromJson: seriesJSOnArray[i])
                            seriesArray.append(seriesObj)
                        }
                    }
                    
                    
                    
                    success(moviesArray, seriesArray)
                    
                    
                }
                
                
                
                
                break
                
            case .failure(let error):
                
                failure(error)
                
                
                break
                
            }
            
        }
        
    }
    
    
    
    
    func getSeriesDetail(seriesID:Int, success: @escaping(_ status: Bool, _ seriesDetail: [SeriesSeason]) -> (), failure: @escaping(_ error: Error) -> ()){
        let url = CinemaGharAPI.webSeriesURL + "/\(seriesID)/contents"
        
        AF.request(url, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    var seriesSeasonArray = [SeriesSeason]()
                    
                    if let seriesSeasonJSONArray = json["data"].array {
                        
                        if seriesSeasonJSONArray.count>0 {
                            
                            for i in 0..<seriesSeasonJSONArray.count{
                                let seriesSeasonObj = SeriesSeason(fromJson: seriesSeasonJSONArray[i], isUnderSubscription: json["isUnderSubscription"].bool)
                                seriesSeasonArray.append(seriesSeasonObj)
                            }
                            success(true, seriesSeasonArray)
                        }
                        
                    }
               }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
            
        }
   
    }
    
    
    func getMoviesDetail(contentID:String, success: @escaping(_ status: Bool, _ movieDetail: Movie) -> (), failure: @escaping(_ error: Error) -> ()){
        let url = CinemaGharAPI.movieDetailURL + "/\(contentID)"
        
        AF.request(url, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                print(response)
                print("Getting movie detail success")
                if let value = response.value{
                    print(value)
                    let json = JSON(value)
                    var movieDetail:Movie
                    print(json)
                    movieDetail = Movie.init(fromJson: json["data"])
                    success(true, movieDetail)
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
            
        }
        
    }
    
    func getSeriesDetail(seriesID:String, success: @escaping(_ status: Bool, _ seriesDetail: Series) -> (), failure: @escaping(_ error: Error) -> ()){
        let url = CinemaGharAPI.webSeriesURL + "/\(seriesID)"
        
        AF.request(url, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    var seriesDetail:Series
                    seriesDetail = Series.init(fromJson: json["data"])
                    success(true, seriesDetail)
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
            
        }
        
    }
    
    
    func forgotPassword(phoneNumber:String, success: @escaping(_ status: Bool, _ message: String) -> (), failure: @escaping (_ error : Error) -> ()){
        
        let params = ["phoneNumber": phoneNumber]
        
        
        print(CinemaGharAPI.forgotPassword,params)
        AF.request(CinemaGharAPI.forgotPassword, method:  .post , parameters: params, encoding: URLEncoding.httpBody, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    if json["status"].boolValue {
                        success(true, json["message"].stringValue)
                    }
                    else {
                        success(false, json["message"].stringValue)
                    }
                }
                break
                
            case .failure(let error):
                
                failure(error)
                break
            }
            
        }
    }
    
    func resendPassword(phoneNumber:String, success: @escaping(_ status: Bool, _ message: String) -> (), failure: @escaping (_ error : Error) -> ()){
        
        let params = ["phoneNumber": phoneNumber]
        
        
        
        AF.request(CinemaGharAPI.resendPassword, method:  .post , parameters: params, encoding: URLEncoding.httpBody, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    if json["status"].boolValue {
                        success(true, json["message"].stringValue)
                        
                    }
                    else {
                        
                        success(false, json["message"].stringValue)
                    }
                    
                    
                }
                break
                
            case .failure(let error):
                
                failure(error)
                break
            }
            
        }
    }
    
    func changePassword(oldPassword:String, newPassword:String, newPasswordConfirmation:String, success: @escaping(_ status: Bool, _ message: String) -> (), failure: @escaping (_ error: Error) -> ()){
        
        let params = ["oldPassword": oldPassword, "newPassword": newPassword, "newPasswordConfirmation": newPasswordConfirmation]
        
        AF.request(CinemaGharAPI.changePassword, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: createHeader()).responseJSON {
            (response: AFDataResponse<Any>) in
            switch(response.result){
                case .success(_):
                    if let value = response.value{
                        let json = JSON(value)
                        print("sawsank: \(json["message"])")
                        if json["status"].boolValue {
                            success(true, json["message"].stringValue)
                        }else{
                            success(false, String(describing: json["message"]))
                        }
                    }
                    break
                case .failure(let error):
                    failure(error)
                    break
            }
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    func getSliderData(success: @escaping (_ status:Bool, _ slider:[MovieSlider]) -> (), failure: @escaping (_ message: Error) -> ()){
        let url = CinemaGharAPI.baseURLString+"sliders"
        
        //        print(url)
        
        AF.request(url, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    var sliderArray = [MovieSlider]()
                    print(json)
                    if let sliderJSONArray = json["data"].array{
                        if(sliderJSONArray.count>0){
                            for i in 0..<sliderJSONArray.count{
                                let movieSliderObj = MovieSlider(fromJson: sliderJSONArray[i])
                                sliderArray.append(movieSliderObj)
                            }
                            success(true, sliderArray)
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                    }
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
            
        }
        
        
    }
    
    func getGoldSliderData(success: @escaping (_ status:Bool, _ slider:[MovieSlider]) -> (), failure: @escaping (_ message: Error) -> ()){
//        let url = CinemaGharAPI.baseURLString+"subscription-sliders"
        let url = CinemaGharAPI.baseURLString+"sliders"
        
        //        print(url)
        
        AF.request(url, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    var sliderArray = [MovieSlider]()
                    if let sliderJSONArray = json["data"].array{
                        if(sliderJSONArray.count>0){
                            for i in 0..<sliderJSONArray.count{
                                let movieSliderObj = MovieSlider(fromJson: sliderJSONArray[i])
                                sliderArray.append(movieSliderObj)
                            }
                            success(true, sliderArray)
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                    }
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
            
        }
        
        
    }
    func getQuestions(success: @escaping (_ status:Bool, _ slider:[QuestionModel]) -> (), failure: @escaping (_ message: Error) -> ()){
//        let url = CinemaGharAPI.baseURLString+"subscription-sliders"
        let url = CinemaGharAPI.baseURLString+"questions"
        
        //        print(url)
        
        AF.request(url, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    var sliderArray = [QuestionModel]()
                    if let sliderJSONArray = json["data"].array{
                        if(sliderJSONArray.count>0){
                            for i in 0..<sliderJSONArray.count{
                                let movieSliderObj = QuestionModel(json: sliderJSONArray[i])
                                sliderArray.append(movieSliderObj)
                            }
                            success(true, sliderArray)
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                    }
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
            
        }
        
        
    }
    
    func getSubscriptionData(pageId: Int = 1, success: @escaping (_ status:Bool, _ slider:[Movie], _ nextPageId: String) -> (), failure: @escaping (_ message: Error) -> ()){
//        let url = CinemaGharAPI.baseURLString+"contents/?content_type=movies"+"&page="+String(pageId)
        let url = CinemaGharAPI.baseURLString+"subscription-contents"+"?page="+String(pageId)
        print(url)
        AF.request(url, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            print(response)
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    var contentArray = [Movie]()
                    if let sliderJSONArray = json["data"].array{
                        if(sliderJSONArray.count>0){
                            for i in 0..<sliderJSONArray.count{
                                let movieSliderObj = Movie(fromJson: sliderJSONArray[i])
                                contentArray.append(movieSliderObj)
                            }
                            success(true, contentArray, json["links"]["next"].stringValue)
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }else{
                            success(false, [], "")
                        }
                    }
                } else {
                    print("hide reload")
                }
                break
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
        }
    }
    
    func getSubscriptionTrendingData(success: @escaping (_ status:Bool, _ slider:[Movie]) -> (), failure: @escaping (_ message: Error) -> ()){
        
        let url = CinemaGharAPI.baseURLString+"subscription-trending"
        
        //        print(url)
        
        AF.request(url, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    var contentArray = [Movie]()
                    print(json)
                    if let sliderJSONArray = json["data"].array{
                        if(sliderJSONArray.count>0){
                            for i in 0..<sliderJSONArray.count{
                                let movieSliderObj = Movie(fromJson: sliderJSONArray[i])
                                contentArray.append(movieSliderObj)
                            }
                            success(true, contentArray)
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                    }
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
            
        }
        
        
    }
    
    
    
    
    func getAllGenres(success:@escaping( _ movieGenres:[Genre])->(), failure: @escaping(_ error: Error)->()){
        AF.request(CinemaGharAPI.genreURL, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    var genreArray = [Genre]()
                    //                    print(json)
                    if let genreJSONArray = json["data"].array{
                        if(genreJSONArray.count>0){
                            for i in 0..<genreJSONArray.count{
                                let genre = Genre(fromJson: genreJSONArray[i])
                                genreArray.append(genre)
                            }
                            success(genreArray)
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                    }
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
            
        }
        
        
        
        
    }
    
    
    func getReviews(contentId: Int, success: @escaping(_ reviewArray: [Review]) -> (), failure: @escaping(_ error: Error) -> ()) {
        
        
        let url = CinemaGharAPI.baseURLString+"content/\(contentId)/reviews"
        
        
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            
            switch response.result{
            case .success(_):
                
                if let value = response.value {
                    
                    
                    let json = JSON(value)
                    //                    print(json)
                    
                    var reviewsArr = [Review]()
                    
                    if let reviewsJSONArr = json["data"].array{
                        
                        if reviewsJSONArr.count>0{
                            
                            for i in 0..<reviewsJSONArr.count{
                                let review = Review(fromJson: reviewsJSONArr[i])
                                reviewsArr.append(review)
                            }
                            
                        }
                        
                        success(reviewsArr)
                        
                    }
                    
                    
                    
                    
                }
                
                
                break
                
                
            case .failure(let err):
                
                failure(err)
                
                
                break
                
                
            }
            
            
        }
        
        
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    func rateMovie(rating:String, contentId: Int, success: @escaping(_ status: Bool) -> (), failure: @escaping(_ error: Error) -> ()) {
        
        let url = CinemaGharAPI.baseURLString+"content/\(contentId)/rate"
        
        
        let params = ["rating": rating]
        
        //        print(params)
        
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            
            switch response.result{
            case .success(_):
                
                if let value = response.value {
                    
                    
                    let json = JSON(value)
                    
                    
                    if json["status"].boolValue {
                        
                        success(true)
                        
                    }
                    else {
                        
                        success(false)
                    }
                    
                    
                }
                
                
                break
                
                
            case .failure(let err):
                
                failure(err)
                
                
                break
                
                
            }
            
            
        }
        
        
        
        
        
        
        
        
    }
    
    
    
    func signUp(signUpType: SignUpType,fullName: String,  emailAddress:String,  phoneNumber:String ,success: @escaping(_ status:Bool, _ signUp: SignUpModel) -> (), failure: @escaping(_ status:Bool, _ message: String)->(),  error: @escaping(_ error: Error) -> ()){
        
        let loginParams = ["fullName":fullName, "phoneNumber": phoneNumber, "email": emailAddress, "from": signUpType.rawValue, "validation": "string"]
        
        AF.request(CinemaGharAPI.registerationURL, method: .post, parameters: loginParams, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                        let signUpResponseModel = SignUpModel(fromJson: json["data"])
                        success(true, signUpResponseModel)
                        if let token = Messaging.messaging().fcmToken{
                            
                            APIHandler.shared.registerFCMToken(fcmToken: token, success: {(status) in
                                if status {
                                    UserDefaults.standard.set(true, forKey: "isFCMTokenSentToServer")

                                }

                            }, error: {(error) in
                                print("FCM TOKEN: \(error.localizedDescription)")
                            })
                        }
                    }
                        
                    else {
                        print("sawsank: failure sign up")
                       if json["message"].exists(){
                            print("sawsank: failure sign up message \(json["message"])")
                            failure(false, json["message"].stringValue)
                            
                        }
                        
                        
                    }
                    
                }
                
                break
            case .failure(let err):
                
                error(err)
                break
                
                
            }
            
        }
        
        
    }
    
    
    func verifyPassCode(phoneNumber: String, passCode:String, success: @escaping( _ loginModel: LoginModel)->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        
        let params = ["phoneNumber": phoneNumber, "verificationCode":passCode ]
        //TODO add headers
        
        AF.request(CinemaGharAPI.passCodeVerificationURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            print("verifying passcode")
            print(params)
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    
                    let json = JSON(value)
                    //                    print(json)
                    if json["status"].boolValue{
                        
                        let loginResponseModel = LoginModel(fromJson: json["data"])
                        
                        success(loginResponseModel)
                    }
                    else {
                        print("verifying passcode")
                        print(json)
                        failure( json["message"].stringValue)
                    }
                }
                
                break
            case .failure(let err):
                error(err)
                break
            }
        }
    }
    
    
    
    
    func search(searchText: String, mediaType: MediaType ,success: @escaping (_ movies: [Movie], _ series: [Series]) -> (), failure: @escaping(_ error: Error) -> ()) {
        
        let params = ["keyword":searchText , "type": mediaType.rawValue ]
        

        
        AF.request(CinemaGharAPI.searchURL, method: .get, parameters: params, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    
                    let json = JSON(value)
                    print(json)
                    if json["data"].arrayValue.count>0{
                        if mediaType == .movie {
                            var movies = [Movie]()

                            for i in 0..<json["data"].arrayValue.count{
                                movies.append(Movie(fromJson: json["data"][i]))
                            }
                            success(movies, [Series]())
                        }
                        else {
                            var series = [Series]()
                            for i in 0..<json["data"].arrayValue.count{
                                series.append(Series(fromJson: json["data"][i]))
                            }
                            success([Movie](), series)
                        }
                    }
                    else {
                   
                        success([Movie](), [Series]())
                    }
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
    
    
    func checkPromoCode(promoCode:String, success: @escaping(_ disCountPercen: Int)->(),  failure:  @escaping(_ failureMessage:String) -> (),  error: @escaping(_ error: Error)->()){
        
        let params = ["coupon":promoCode ]
        
        
        AF.request(CinemaGharAPI.verifyPromoCode, method: .post, parameters: params, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                        success(json["discountPercentage"].intValue)
                        
                    }
                    else {
                        failure(json["message"].stringValue)
                    }
                }
                
                break
            case .failure(let err):
                error(err)
                break
            }
        }
    }
    
    
    func checkIfVideoIsPurchased(contentType: MediaType, contentId:Int, episodeId:Int, success: @escaping(_ status:Bool, _ videoURL:String, _ hasUserBought:Bool,_ hasUserRated:Bool ,_ isComingSoon: Bool) -> (), failure: @escaping(_ error: Error) -> ()) {
        var url = ""
        
        if contentType == .movie{
            url = CinemaGharAPI.baseURLString+"content/\(contentId)/is-bought"
        }
        else {
            url = CinemaGharAPI.baseURLString+"series/\(contentId)/season/1/is-bought?episodeId=\(episodeId)"
            
        }
       
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value {
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                        success(true, json["videoUrl"].stringValue, json["isBought"].boolValue, json["isRated"].boolValue, json["isComingSoon"].boolValue)
                    }
                    else {
                        success(false, "", false, false, false)
                    }
                }
                break
            case .failure(let err):
                
                failure(err)
                
                break
            }
        }
    }
    
    
    func registerFCMToken(fcmToken: String, success:@escaping(_ status: Bool) -> (), error: @escaping( _ error: Error) -> () ){
        
        let params = ["token": fcmToken  , "from":"ios" ]

        AF.request(CinemaGharAPI.registerFCMToken, method: .post, parameters: params, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    
                    let json = JSON(value)
                       print(json)
                    if json["status"].boolValue{
                        
                        success(true)
                       
                    }
                        
                  
                }
                
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    
    func editWishList(url: String, contentID: String, type: MediaType, success: @escaping( _ status: Bool)->(), error: @escaping( _ error: Error) -> ()){
        
        
        let params = ["type": type.rawValue, "id": contentID]
        
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{(response: AFDataResponse<Any>) in
            switch response.result {
                
                case .success(_):
                    if let value = response.value {
                        
                        let json = JSON(value)
                        if json["status"].boolValue {
                            
                            success(true)
                        }
                        else {
                            success(false)
                        }
                    }
                case .failure(let err):
                    error(err)
            }
        }
    }
    
    
    
    func loginUser(mobileNumber: String, passCode:String, success: @escaping( _ loginModel: LoginModel)->(), failure: @escaping( _ message: String, _ customCode: String)->(), error: @escaping( _ error: Error) -> () ){
        
        let params = ["phoneNumber": mobileNumber  , "password":passCode ]
//        print(params)
        AF.request(CinemaGharAPI.loginURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    if json["status"].boolValue{
                        
                        let loginResponseModel = LoginModel(fromJson: json["data"])
                        success(loginResponseModel)
                        if let token = Messaging.messaging().fcmToken{
                            APIHandler.shared.registerFCMToken(fcmToken: token, success: {(status) in
                                if status {
                                    UserDefaults.standard.set(true, forKey: "isFCMTokenSentToServer")

                                }

                            }, error: {(error) in
                                print("FCM TOKEN: \(error.localizedDescription)")
                            })
                        }
                        
                    }
                    else {
                        failure( json["message"].stringValue, json["customCode"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    
    //Short Movies
    func getShortMovies(success: @escaping (_ status:Bool, _ shorts:[Movie]) -> (), failure: @escaping (_ message: Error) -> ()){
        
        let url = CinemaGharAPI.getShorts
        print(url)
        //        print(url)
        
        AF.request(url, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            print(response)
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    var contentArray = [Movie]()
                    print(json)
                    if let sliderJSONArray = json["data"].array{
                        if(sliderJSONArray.count>0){
                            for i in 0..<sliderJSONArray.count{
                                let movieSliderObj = Movie(fromJson: sliderJSONArray[i])
                                contentArray.append(movieSliderObj)
                            }
                            success(true, contentArray)
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                    }
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
            
        }
        
        
    }
    
    
    //Get Profile {GET}
    func getProfile(success: @escaping( _ profileModel: Profile)->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){

        AF.request(CinemaGharAPI.profileURL, method: .get, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                                        print(json)
                    if json["status"].boolValue{
                        
                        let profileResponseModel = Profile(fromJson: json["data"])
                        success(profileResponseModel)
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    
    //Edit profile <Start>
    
    //Edit Profile {Post} with Image
    func editProfileImage(name: String, image: UIImage, success: @escaping( _ status: Bool)->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        let imageData = UIImageJPEGRepresentation(image, 0.2)!
        let parameters = ["name": name]

        AF.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imageData, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    } //Optional for extra parameters
            },
        to: CinemaGharAPI.profileUpdateURL, headers: createHeader()).responseJSON { (result) in
            if let value = result.value{
                let json = JSON(value)
                print(json)
                if json["status"].boolValue{
                    success(true)
                }
                else {
                    failure( json["message"].stringValue)
                }
            }
        }
        
        
       
    }
    
    //Edit Profile {Post} without Image
    func editProfileName(name: String, success: @escaping( _ status: Bool)->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        let parameters = ["name": name]
        AF.upload(multipartFormData: { multipartFormData in
                for (key, value) in parameters {
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    } //Optional for extra parameters
            },
        to: CinemaGharAPI.profileUpdateURL, headers: createHeader()).responseJSON { (result) in
            if let value = result.value{
                let json = JSON(value)
                if json["status"].boolValue{
                    success(true)
                }
                else {
                    failure(String(describing: json["message"]))
                }
            }
        }
    }
    
    //Edit Profile <End>
    
    
    //Get Statements {GET}
    func getStatements(pageId: Int = 1, success: @escaping( _ statement: [Statement], _ nextPage: String)->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        
        AF.request(CinemaGharAPI.statements + "?page=" + String(pageId), method: .get, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    var statementArray = [Statement]()
                    if let statementJSONArray = json["data"].array{
                        if(statementJSONArray.count>0){
                            for i in 0..<statementJSONArray.count{
                                let statement = Statement(fromJson: statementJSONArray[i])
                                statementArray.append(statement)
                                
                            }
                            
                        }
                        let nextPageLink = json["links"]["next"].stringValue
                        success(statementArray, nextPageLink)
                        
                        
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }else{
                        failure(json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    
    //Edit profile <Start>
    
    
    //Wallet and Payment process <Start>
    
    func walletVerifyPin(pin: String, type: String, id: String, season: String?, status: @escaping(_ status: Bool,_ token: String) -> (), failure: @escaping(_ errorMessage: Error)-> ()){

        let url = CinemaGharAPI.walletVerifyPin
        
        let parameters: [String:Any] = ["pin": pin, "type": type, "id": id, "season": season ?? ""]
        
        AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response: AFDataResponse<Any>) in
            switch(response.result){
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    if json["token"].stringValue != "" {
                        status(true, json["token"].string!)
                        
                    }
                    else {
                        
                        status(false, json["message"].stringValue)
                    }
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
        }
    }
    
    func validatePayment( paymentGateway: paymentGateway, mediaType: MediaType, contentId: Int, episodeID: Int, paymentSuccessToken: String, transactionID: String, status: @escaping(_ status: Bool, _ videoURL: String , _ message: String )   -> (), failure: @escaping(_ errorMessaage: Error) -> ()){
        let url = CinemaGharAPI.verifyPayment
        
        let parameters: [String:Any]
//        parameters = ["from": paymentGateway.rawValue, "type": mediaType.rawValue, "token" : paymentSuccessToken, "id": contentId, "episodeId": episodeID, "validation": "string", "transactionId": transactionID]
        parameters = ["from": paymentGateway.rawValue, "type": mediaType.rawValue, "token" : paymentSuccessToken, "id": contentId]
        debugPrint(parameters,url)
        AF.request(url, method:  .post , parameters: parameters, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    
                    if json["status"].boolValue {
                        status(true, json["videoUrl"].stringValue, json["message"].stringValue)
                        
                    }
                    else {
                        
                        status(false, "An Error Occured While Verifying Your Payment.", json["message"].stringValue)
                    }
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
        }
    }
    func validatePaymentFromCineCoin( paymentGateway: paymentGateway, mediaType: MediaType, contentId: Int, episodeID: Int, paymentSuccessToken: String, transactionID: String, status: @escaping(_ status: Bool, _ videoURL: String , _ message: String )   -> (), failure: @escaping(_ errorMessaage: Error) -> ()){
        let url = CinemaGharAPI.verifyPayment
        
        let parameters: [String:Any]
//        parameters = ["from": paymentGateway.rawValue, "type": mediaType.rawValue, "token" : paymentSuccessToken, "id": contentId, "episodeId": episodeID, "validation": "string", "transactionId": transactionID]
        
        parameters = ["from": paymentGateway.rawValue, "type": mediaType.rawValue, "token" : paymentSuccessToken, "id": contentId]
        
        debugPrint(parameters,url)
        AF.request(url, method:  .post , parameters: parameters, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    
                    if json["status"].boolValue {
                        status(true, json["videoUrl"].stringValue, json["message"].stringValue)
                        
                    }
                    else {
                        
                        status(false, "An Error Occured While Verifying Your Payment.", json["message"].stringValue)
                    }
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
        }
    }
    //Wallet and Payment process <End>
    
    //Load Wallet
    func loadWallet( from: String, token: String, amount: String, phoneNumber: String, success: @escaping(_ message: String) -> (), failure: @escaping(_ message: String) -> (), error: @escaping(_ err: AFError) -> ()){
        let url = CinemaGharAPI.loadWallet

        let parameters = ["from": from, "token": token, "amount": amount, "phoneNumber": phoneNumber]
        
        AF.request(url, method:  .post , parameters: parameters, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if(json["status"].boolValue){
                        success(json["message"].stringValue)
                    }else{
                        failure(json["message"].stringValue)
                    }
                
                }
                break
                
            case .failure(let err):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                error(err)
                break
            }
        }
    }
    //
    
    //Load Wallet Max Coin Reached
    func walletMaxCoinReached( phone: String, coins: Int, success: @escaping(_ message: String) -> (), failure: @escaping(_ message: String) -> (), error: @escaping(_ err: AFError) -> ()){
        let url = CinemaGharAPI.walletMaxCoinReached

        let parameters = ["phone": phone, "coins": coins] as [String : Any]
        
        AF.request(url, method:  .post , parameters: parameters, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if(!json["maxCoinsReached"].boolValue){
                        success(json["message"].stringValue)
                    }else{
                        failure(json["message"].stringValue)
                    }
                
                }
                break
                
            case .failure(let err):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                error(err)
                break
            }
        }
    }
    //
    
    //Send watch duration
    func saveWatchedDuration( duration: Int, contentId: Int, status: @escaping(_ status: Bool)   -> (), failure: @escaping(_ errorMessaage: Error) -> ()){
        let url = CinemaGharAPI.saveWatchedDuration

        let parameters = ["mediaId": contentId, "watchedDuration": duration]
        
        AF.request(url, method:  .post , parameters: parameters, encoding: JSONEncoding.default, headers: createHeader()).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    
//                    if json["status"].boolValue {
//                        status(true, json["videoUrl"].stringValue, "")
//                        
//                    }
//                    else {
//                        
//                        status(false, "An Error Occured While Verifying Your Payment.", json["message"].stringValue)
//                    }
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure(error)
                break
            }
        }
    }

    
    //Check if app is blocked from server
    func checkIfAppIsBlocked(success: @escaping( _ status: Bool, _ message: String)->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
//        let parameters = ["deviceType": "ios"]
        AF.request(CinemaGharAPI.checkIfAppIsBlocked, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                                        print(json)
                    if json["status"].boolValue{
                        
                        let blockResponse = json["blockStatus"].boolValue
                        let message = json["message"].string
                        success(blockResponse, message!)
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    
    //Get Settings {GET}
    func getSettings(success: @escaping( _ result: JSON)->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        print(createHeader())
        //CinemaGharAPI.checkSettings
        AF.request(CinemaGharAPI.checkSettings, method: .get, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            print(response)
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                        
                        success(json)
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
        
    //Get Tutorials {GET}
    func getTutorials(success: @escaping( _ tutorials: [Tutorials])->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        var tutorialArr = [Tutorials]()
        AF.request(CinemaGharAPI.tutorials, method: .get, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                        if let tutorialsArray = json["data"].array{
                            for i in 0..<tutorialsArray.count{
                                let tutorials = Tutorials(fromJson: tutorialsArray[i])
                                tutorialArr.append(tutorials)
                            }
                            success(tutorialArr)
                        }
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    
    //Edit Tutorials <Start>
    
    //Buy content via token
    func buyViaToken(token: String, id: Int, success: @escaping(_ status: Bool, _ message: String)->(), failure: @escaping(_ status: Bool, _ message: String)->(), error: @escaping(_ error: Error)->()){
        let parameters: [String:Any] = ["token": token, "id": id]
        AF.request(CinemaGharAPI.buyViaToken, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                        let message = json["message"].string
                        success(true, message!)
                    }
                    else {
                        failure(false, json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    
    
    //Make payment with token
    func tokenPayment(token: String, success: @escaping( _ status: Bool, _ message: String)->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        
        let parameters: [String:Any] = ["token": token]
        AF.request(CinemaGharAPI.tokenPayment, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                                        print(json)
                    if json["status"].boolValue{
                        
                        let message = json["message"].string
                        success(true, message!)
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    
    
    func getVotingPrices(success: @escaping(_ status: Bool, _ prices: [VotingPrices], _ bannerImage: String) -> (), failure: @escaping(_ message: String)->(), error: @escaping(_ error: Error)->()){
        AF.request(CinemaGharAPI.getVotingPrices, method: .get, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response: AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    var votingPricesArray = [VotingPrices]()
                    if let votingPricesJSONArray = json["data"].array{
                        if(votingPricesJSONArray.count>0){
                            for i in 0..<votingPricesJSONArray.count{
                                let votingPrice = VotingPrices(fromJson: votingPricesJSONArray[i])
                                votingPricesArray.append(votingPrice)
                                
                            }
                        }
                        success(json["status"].boolValue, votingPricesArray, json["bannerImage"].stringValue)
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }else{
                        failure(json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                error(err)
                break
            }
        }
    }
    
    //Auto Login Initialization
    func getAutoLoginLink(success: @escaping( _ link: String)->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){

        AF.request(CinemaGharAPI.authInitialization, method: .get, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    if json["status"].boolValue{
                        success(json["message"].stringValue)
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    func getVotingShows(success: @escaping( _ tutorials: [VotingModel])->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        var tutorialArr = [VotingModel]()
        AF.request(CinemaGharAPI.votingshows, method: .get, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                        if let tutorialsArray = json["data"].array{
                            for i in 0..<tutorialsArray.count{
                                let tutorials = VotingModel(json: tutorialsArray[i])
                                tutorialArr.append(tutorials)
                            }
                            success(tutorialArr)
                        }
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    func getVotingDetails(success: @escaping( _ tutorials: [VotingDetailsModel])->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        var votingDetails = [VotingDetailsModel]()
       
        AF.request(CinemaGharAPI.votingdetails, method: .get ,encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                        if let tutorialsArray = json["data"].array{
                            for i in 0..<tutorialsArray.count{
                                let tutorials = VotingDetailsModel(json: tutorialsArray[i])
                                votingDetails.append(tutorials)
                            }
                            success(votingDetails)
                        }
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    func getVotingPrice(success: @escaping( _ tutorials: [VotingPriceModel])->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        var votingPriceDetails = [VotingPriceModel]()
        AF.request(CinemaGharAPI.votingPrice, method: .get, encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                        if let tutorialsArray = json["data"].array{
                            for i in 0..<tutorialsArray.count{
                                let votingPrice = VotingPriceModel(json: tutorialsArray[i])
                                votingPriceDetails.append(votingPrice)
                            }
                            success(votingPriceDetails)
                        }
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    func postVote(contestantID: Int,votes: Int,coins: Int,success: @escaping( _ tutorials: DefaultResponse)->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        var votingPriceDetails = DefaultResponse(json: JSON())
        let parameter: [String:Any] = ["show_id": globalVotingId, "id": contestantID,"votes":votes,"coins": coins]
        AF.request(CinemaGharAPI.postVote, method: .post,parameters: parameter,encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                      
                           
                                let votingPrice = DefaultResponse(json: json)
                                votingPriceDetails = votingPrice
                            
                            success(votingPriceDetails)
                        
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    func loadWalletWithReachargeCard(cardNo: String,success: @escaping( _ tutorials: [VotingPriceModel])->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        var votingPriceDetails = [VotingPriceModel]()
        let parameter: [String:Any] = ["from": "rechargeCard", "token": cardNo]
        AF.request(CinemaGharAPI.loadWithReachargeCard, method: .post,parameters: parameter,encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                       success(votingPriceDetails)
                    } else if json["code"].intValue == 422 {
                        let message = json["message"]["token"].arrayObject
                        var data = VotingPriceModel(json: JSON())
                        data.message = message?.first as? String ?? ""
                    print(data)
                    success([data])
                       
                    } else {
                        let error = VotingPriceModel(json: json)
                        success([error])
                        
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    func loadWalletWithReward(success: @escaping( _ tutorials: [VotingPriceModel])->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        var votingPriceDetails = [VotingPriceModel]()
        let parameter: [String:Any] = ["from": "rewardad", "token": "rewardad","amount": "10"]
        AF.request(CinemaGharAPI.loadWithReachargeCard, method: .post,parameters: parameter,encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                        
                        success(votingPriceDetails)
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    func loadWalletW(from: String,token: String,amount: String,success: @escaping( _ tutorials: [VotingPriceModel])->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        var votingPriceDetails = [VotingPriceModel]()
        let parameter: [String:Any] = ["from": from, "token": token,"amount": amount]
        AF.request(CinemaGharAPI.loadWithReachargeCard, method: .post,parameters: parameter,encoding: JSONEncoding.default, headers: createHeader()).responseJSON{
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                        var details = VotingPriceModel(json: JSON())
                        details.code = json["code"].intValue
                        details.status = json["status"].boolValue
                        details.message = json["message"].stringValue
                        votingPriceDetails.append(details)
                        success(votingPriceDetails)
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }
    
    func loginChallenge(success: @escaping( _ tutorials: DefaultResponse)->(), failure: @escaping( _ message: String)->(), error: @escaping( _ error: Error) -> () ){
        var votingPriceDetails = DefaultResponse(json: JSON())
        let parameter: [String:Any] = ["from": "login","token": "loginchallenge"]
        AF.request(CinemaGharAPI.loginChallenge, method: .post,parameters: parameter,encoding: JSONEncoding.default, headers: createHeader()).responseJSON {
            (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if json["status"].boolValue{
                      
                           
                                let votingPrice = DefaultResponse(json: json)
                                votingPriceDetails = votingPrice
                            
                            success(votingPriceDetails)
                        
                    }
                    else {
                        failure( json["message"].stringValue)
                    }
                }
                break
            case .failure(let err):
                
                error(err)
                break
            }
        }
    }


}


