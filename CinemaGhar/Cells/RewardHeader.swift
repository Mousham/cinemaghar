//
//  RewardHeader.swift
//  CinemaGhar
//
//  Created by Midas on 22/06/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit

class RewardHeader: UITableViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var headerTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uisetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
           super.layoutSubviews()
           // do your thing
        backView.roundCorners([.topLeft,.topRight], radius: 8)
       }
    func uisetup() {
        backView.backgroundColor = AppColors.darkbackgound
       
    }
    
}
