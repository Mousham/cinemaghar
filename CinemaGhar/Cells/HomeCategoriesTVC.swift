//
//  HomeCategoriesTVC.swift
//  CinemaGhar
//
//  Created by Midas on 03/08/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
enum HomeCategoriesTapType {
    case all
    case movies
    case shorts
    case series
}
protocol HomeCagegoresDelegate: class {
    func didselecCategories(type: HomeCategoriesTapType)
}

class HomeCategoriesTVC: UITableViewCell {
    @IBOutlet weak var shortsBackView: UIView!
    
   
    @IBOutlet weak var seriesBackView: UIView!
    @IBOutlet weak var moviesBackView: UIView!
    weak var delegate: HomeCagegoresDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uisetup()
        let movieTap = UITapGestureRecognizer(target: self, action: #selector(self.moviesTap(_:)))
        moviesBackView.addGestureRecognizer(movieTap)
        let shortTap = UITapGestureRecognizer(target: self, action: #selector(self.shortTap(_:)))
        shortsBackView.addGestureRecognizer(shortTap)
        let seriesTap = UITapGestureRecognizer(target: self, action: #selector(self.seriesTap(_:)))
        seriesBackView.addGestureRecognizer(seriesTap)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func uisetup() {
        moviesBackView.layer.cornerRadius = 12
        shortsBackView.layer.cornerRadius = 12
        seriesBackView.layer.cornerRadius = 12
    }
    @objc func moviesTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
       changeBackgroundColor(bview: moviesBackView)
        delegate?.didselecCategories(type: .movies)
    }
    @objc func shortTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
       changeBackgroundColor(bview: shortsBackView)
        delegate?.didselecCategories(type: .shorts)
    }
    @objc func seriesTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
       changeBackgroundColor(bview: seriesBackView)
        delegate?.didselecCategories(type: .series)
    }
    func changeBackgroundColor(bview: UIView) {
        self.moviesBackView.backgroundColor = AppColors.newDark
        self.shortsBackView.backgroundColor = AppColors.newDark
        self.seriesBackView.backgroundColor = AppColors.newDark
        bview.backgroundColor = AppColors.yellowColor
    }
    
}
