//
//  QuestionCVC.swift
//  CinemaGhar
//
//  Created by Midas on 07/07/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
protocol QuestionsDelegate: class {
    func submitAction()
    func moveToNextQuestion()
    func getOutOfQuiz(points: Int)
}
class QuestionCVC: UICollectionViewCell {
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var questionNumTitle: UILabel!
    @IBOutlet weak var questionTitle: UILabel!
    @IBOutlet weak var collectionviewheight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var submitBtn: AnimatedButton!
    var isrightAnswer = true
    var showAnswer: Bool? = false
    var isoptionSelected = false
    var goToNext: Bool?
    var selectedIndex: Int?
    var questionIndex: Int?
    var questionPoints = 0
    weak var delegate: QuestionsDelegate?
    var questionData = [QuestionModel]() {
        didSet {
            self.collectionView.reloadData()
        }
    }
    var numTitle = ["A: ","B: ","C: ","D: "]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uisetup()
    }
    func uisetup() {
        
        backView.layer.cornerRadius = 12
       backView.clipsToBounds = true
        backImage.layer.cornerRadius = 12
        backImage.clipsToBounds = true
        //backView.layer.masksToBounds = false
        collectionView.register(UINib(nibName: "OptionsCVC", bundle: nil), forCellWithReuseIdentifier: "OptionsCVC")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.reloadData()
        collectionviewheight.constant = 230
    }
    @IBAction func submitTap(_ sender: Any) {
        if isoptionSelected == true {
            isoptionSelected = false
            if showAnswer == true {
                showAnswer = false
            } else {
                showAnswer = true
            }
           
            collectionView.reloadData()
            if selectedIndex == ((questionData[questionIndex ?? 0].write_answer ?? 0) - 1) {
                questionPoints += 2
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                    self.selectedIndex = nil
                    self.delegate?.moveToNextQuestion()
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                    self.selectedIndex = nil
                    self.delegate?.getOutOfQuiz(points: self.questionPoints ?? 0)
                }
            
           // delegate?.submitAction()
        }
        } else {
           return
           // isoptionSelected = true
           
        }
       
  
       
    }
    
}
extension QuestionCVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionsCVC", for: indexPath) as! OptionsCVC
        cell.optionImage.image = UIImage(named: "iconPointedView")
        cell.leftView.backgroundColor = AppColors.inactive
        cell.rightView.backgroundColor = AppColors.inactive
       
        cell.numTitle.text = numTitle[indexPath.row]
        var options = [String]()
        options.removeAll()
        options.append(questionData[questionIndex ?? 0].ans1 ?? "")
        options.append(questionData[questionIndex ?? 0].ans2 ?? "")
        options.append(questionData[questionIndex ?? 0].ans3 ?? "")
        options.append(questionData[questionIndex ?? 0].ans4 ?? "")
        
        cell.optionTitle.text = options[indexPath.row]
        if selectedIndex == nil {
           // let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionsCVC", for: indexPath) as! OptionsCVC
//            cell.optionImage.image = UIImage(named: "iconPointedView")
//            print(numTitle[indexPath.row])
//            cell.numTitle.text = numTitle[indexPath.row]
//            var options = [String]()
//            options.append(questionData[indexPath.row].ans1 ?? "")
//            options.append(questionData[indexPath.row].ans2 ?? "")
//            options.append(questionData[indexPath.row].ans3 ?? "")
//            options.append(questionData[indexPath.row].ans4 ?? "")
//
//            cell.optionTitle.text = options[indexPath.row]
            return cell
        }  else if selectedIndex != nil && isoptionSelected == true{
            if indexPath.row == selectedIndex {
                cell.leftView.backgroundColor = .white
                cell.rightView.backgroundColor = .white
                cell.optionImage.image = UIImage(named: "iconSelectedOption")
                return cell
            } else {
                return cell
            }
        } else {
        if showAnswer == false {
            showAnswer = true
           // let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionsCVC", for: indexPath) as! OptionsCVC
            cell.optionImage.image = UIImage(named: "iconPointedView")
            return cell
        } else {
           
            if selectedIndex == ((questionData[questionIndex ?? 0].write_answer ?? 0) - 1) {
                if selectedIndex == indexPath.row {
                cell.optionImage.image = UIImage(named: "iconGreenOption")
                cell.leftView.backgroundColor = .white
                cell.rightView.backgroundColor = .white
                return cell
                } else {
                    return cell
                }
            } else {
                if indexPath.row == selectedIndex {
                    //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionsCVC", for: indexPath) as! OptionsCVC
                    cell.optionImage.image = UIImage(named: "iconRedOption")
                    cell.optionImage.shake()
                    cell.leftView.backgroundColor = .white
                    cell.rightView.backgroundColor = .white
                    cell.numTitle.textColor = AppColors.yellowColor
                   // return cell
                }
                if indexPath.row == (questionData[questionIndex ?? 0].write_answer ?? 0) - 1 {
                   // let cells = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionsCVC", for: indexPath) as! OptionsCVC
//                    UIView.animate(withDuration: 0.1,
//                               delay: 0.1,
//                               options: UIViewAnimationOptions.curveEaseIn,
//                               animations: { () -> Void in
//                        cell.optionImage.image = UIImage(named: "iconGreenOption")
//
//                                   self.superview?.layoutIfNeeded()
//                    }, completion: { (finished) -> Void in
//                    // ....
//                    })
                    cell.optionImage.image = UIImage(named: "iconGreenOption")
                    cell.leftView.backgroundColor = .white
                    cell.rightView.backgroundColor = .white
                    cell.numTitle.textColor = AppColors.yellowColor
                    //return cells
                }
                return cell
            }
        }
        }
        //return cell
    }
}
extension QuestionCVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 53)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        isoptionSelected = true
        selectedIndex = indexPath.row
        collectionView.reloadData()
    }
}
extension UIView {
    func shake(_ duration: Double? = 0.4) {
        self.transform = CGAffineTransform(translationX: 20, y: 0)
        UIView.animate(withDuration: duration ?? 0.4, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
}

