//
//  HomeMovieListTVC.swift
//  CinemaGhar
//
//  Created by Midas on 03/08/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
enum HomeCategoryType {
    case shorts
    case movies
    case series
}
protocol HomeMovieListDelegate: class {
    func didTap(type: HomeCategoryType, indexpath: Int?)
}
class HomeMovieListTVC: UITableViewCell {

    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var collectionview: UICollectionView!
    var type: HomeCategoryType?
    var trendingArray = [Movie]() {
        didSet {
            collectionview.reloadData()
        }
    }
    var subscriptionArray = [Movie]() {
        didSet {
            collectionview.reloadData()
        }
    }
    weak var delegate: HomeMovieListDelegate?
    var index: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uiseutp()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func uiseutp() {
        collectionview.register(UINib(nibName: "HomeMoviesCVC", bundle: nil), forCellWithReuseIdentifier: "HomeMoviesCVC")
        collectionview.dataSource = self
        collectionview.backgroundColor = .clear
        collectionview.delegate = self
        collectionview.reloadData()
        leftView.clipsToBounds = true
        leftView.layer.cornerRadius = 4
        leftView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        if type == .movies {
            self.titleLbl.text = "Featured"
        } else if type == .shorts {
            self.titleLbl.text = "Comedy"
        } else if type == .series {
            self.titleLbl.text = "Coming Soon"
            self.titleLbl.textAlignment = .center
        }
        //collectionview.isScrollEnabled = false
    }
    
}
//MARK: - COLLECTION VIEW DATASOURCE
extension HomeMovieListTVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if type == .movies {
            return subscriptionArray.count
        } else if type == .shorts{
            return trendingArray.count
        } else {
            return 0
        }
      
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "HomeMoviesCVC", for: indexPath) as! HomeMoviesCVC
        
        cell.contentView.backgroundColor = .clear
        if type == .movies {
            cell.movieImage.sd_setImage(with: URL(string: subscriptionArray[indexPath.row].poster), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
            cell.movieTitle.text = subscriptionArray[indexPath.row].title
        } else {
            cell.movieImage.sd_setImage(with: URL(string: trendingArray[indexPath.row].poster), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
        }
//        cell.movieImage.layer.cornerRadius = 12
//        cell.clipsToBounds = true
       // cell.movieImage.layer.masksToBounds = false
        cell.backVie.layer.masksToBounds = false
        cell.backVie.layer.cornerRadius = 8
        cell.backVie.clipsToBounds = true
        return cell
    }
    
}
//MARK: - COLLECTION VIEW DELEGATE
extension HomeMovieListTVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didTap(type: self.type!, indexpath: indexPath.row)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       // return CGSize(width: (collectionview.frame.size.width / 3 - 12), height: 140)
        return CGSize(width: (collectionview.frame.size.width / 3 - 8), height: 170)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
}
