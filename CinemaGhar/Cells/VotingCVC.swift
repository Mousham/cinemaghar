//
//  VotingCVC.swift
//  CinemaGhar
//
//  Created by Midas on 12/03/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit

class VotingCVC: UICollectionViewCell {

    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uisetup()
    }
    func uisetup() {
        backView.layer.cornerRadius = 20
//        backView.addDropShadow(offset: CGSize(width: 1, height: 0.3), color: .black, radius: 3, opacity: 0.5)
        iconImage.layer.cornerRadius = 12
        
        
    }

}
