//
//  EventTableViewCell.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 3/24/21.
//  Copyright © 2021 sunBi. All rights reserved.
//

import Foundation

class EventTableViewCell: UITableViewCell {
    @IBOutlet weak var votesView: UILabel!
    @IBOutlet weak var priceView: UILabel!
    @IBOutlet weak var descriptionView: UILabel!
    @IBOutlet weak var parentVoteView: UIView!
    
    func setVotingPrice(votingPrice: VotingPrices){
        let voteName = votingPrice.name.split(separator: " ")
        let vote = voteName[0]
        
        parentVoteView.layer.cornerRadius = 10
        
        votesView.text = "\(vote) \n Votes"
        priceView.text = "NRs. \(Double(votingPrice.amount) ?? 0.0000)"
        descriptionView.text = "Buy \(vote) votes for NRs. \(Double(votingPrice.amount) ?? 0.0000)"
    }
}
