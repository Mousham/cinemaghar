//
//  OptionsCVC.swift
//  CinemaGhar
//
//  Created by Midas on 21/09/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit

class OptionsCVC: UICollectionViewCell {
    @IBOutlet weak var numTitle: UILabel!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var optionTitle: UILabel!
    @IBOutlet weak var optionImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
