//
//  ExclusiveMovieCollectionViewCell.swift
//  CinemaGhar
//
//  Created by Sunil on 5/17/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit

class MovieGenreCell: UICollectionViewCell {
    
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var moviePosterImageView: UIImageView!
//    @IBOutlet weak var movieTitleLabel: UILabel!
    
    override func awakeFromNib() {
        cardView.layer.masksToBounds = true
    
        moviePosterImageView.layer.cornerRadius = 3
        addDropShadow(offset: CGSize(width: 1, height: 0.3), color: .black, radius: 3, opacity: 0.6)

    }
    
    func setMovieInfos(movie: Movie){
        
        
        if let posterImage = movie.poster{
            moviePosterImageView.sd_setImage(with: URL(string: posterImage), placeholderImage: UIImage(named: "placeholder_cell"), options: [.continueInBackground])
    

        }
        
//        if let isPaid = movie.paid {
//            
//            if isPaid{
//                if UserDefaults.standard.bool(forKey: movie.productId){
//                    
//                    moviePriceLabel.text = " Purchased "
//                    moviePriceLabel.backgroundColor = Util.hexStringToUIColor(hex: "1E90FF")
//                    
//                    
//                }
//                
//                else {
//                
//                
//                moviePriceLabel.text = "  Paid  "
//                moviePriceLabel.backgroundColor = .red
//                    
//                }
//            }
//            else {
//                moviePriceLabel.text = "  Free  "
//                moviePriceLabel.backgroundColor = Util.hexStringToUIColor(hex: "00803A")
//            }
//        }
      
     
        
        
    }
    
    
    
    
    
    
}
