////
////  VideoCell.swift
////  VidLoaderExample
////
////  Created by Petre on 18.10.19.
////  Copyright © 2019 Petre. All rights reserved.
////
//
//import UIKit
//import VidLoader
//import KDCircularProgress
//
//struct VideoCellActions {
//    let deleteVideo: () -> Void
//    let cancelDownload: () -> Void
//    let startDownload: () -> Void
//    let resumeFailedVideo: () -> Void
//    let showRunningActions: () -> Void
//    let showPausedActions: () -> Void
//}
//
//struct VideoCellModel {
//    let identifier: String
//    let title: String
//    let thumbnailName: String
//    let state: DownloadState
//    let actions: VideoCellActions
//    let coverImage: String
//    let delete: Bool
//    let duration: String
//    let year: String
//}
//
//final class VideoCell: UITableViewCell {
//    static let identifier = String(describing: VideoCell.self)
//    @IBOutlet weak var durationLbl: UILabel!
//    @IBOutlet weak var yearBtn: AnimatedButton!
//    @IBOutlet weak var removeBtn: AnimatedButton!
//    @IBOutlet weak var backView: UIView!
//    @IBOutlet weak var coverImage: UIImageView!
//    @IBOutlet weak var downloadStatusBtn: AnimatedButton!
//    @IBOutlet private var titleLabel: UILabel!
//
//    @IBOutlet weak var progressBar: KDCircularProgress!
//    @IBOutlet private var thumbnailImageView: UIImageView!
//
//    private var actions: VideoCellActions?
//    private var state: DownloadState = .unknown
//    private var observer: VidObserver?
//
//    private struct Colors {
//        static let limeGreen: UIColor = .init(red: 50 / 255.0, green: 205 / 255.0 , blue: 50 / 255.0, alpha: 1)
//        static let fireBrick: UIColor = .init(red: 178 / 255.0, green: 34 / 255.0 , blue: 34 / 255.0, alpha: 1)
//        static let gold: UIColor = .init(red: 255 / 255.0, green: 215 / 255.0 , blue: 0 / 255.0, alpha: 1)
//    }
//
//    override func prepareForReuse() {
//        super.prepareForReuse()
//
//        VidLoaderHandler.shared.loader.remove(observer: observer)
//        observer = nil
//    }
//
//    func setup(model: VideoCellModel) {
//        //uisetup
//        yearBtn.titleLabel?.font = UIFont.systemFont(ofSize: 10)
//        yearBtn.backgroundColor = AppColors.parrotColor
//        removeBtn.backgroundColor = AppColors.darkRedColor
//        removeBtn.titleLabel?.font = UIFont.systemFont(ofSize: 13)
//        progressBar.startAngle = -90
//        progressBar.progressThickness = 0.2
//        progressBar.trackThickness = 0.6
//        progressBar.clockwise = true
//        progressBar.gradientRotateSpeed = 2
//        progressBar.roundedCorners = false
//        //progressBar.glowMode = .forward
//        //progressBar.glowAmount = 0.9
//        progressBar.set(colors: UIColor.blue)
//        backView.layer.cornerRadius = 8
//        backView.backgroundColor = .clear
//        coverImage.layer.cornerRadius = 8
//        coverImage.clipsToBounds = true
//        coverImage.sd_setImage(with: URL(string: model.coverImage), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
//
//        // deleteBtn.isHidden = model.delete == true ? false : true
//
//        thumbnailImageView.tintColor = AppColors.yellowColor
//        contentView.backgroundColor = AppColors.darkColor
//        self.actions = model.actions
//        setup(state: model.state)
//        titleLabel.text = model.title
//        durationLbl.text = "Duration: " + model.duration
//        yearBtn.setTitle(model.year, for: .normal)
//        if model.state == .completed {
//            if #available(iOS 13.0, *) {
//                thumbnailImageView.image = UIImage(named: "iconVideo")?.withRenderingMode(.alwaysTemplate).withTintColor(AppColors.orangeColor)
//            } else {
//                // Fallback on earlier versions
//                thumbnailImageView.image = UIImage(named: "iconVideo")
//            }
//            downloadStatusBtn.setImage(UIImage(named: "iconDone"), for: .normal)
//            downloadStatusBtn.backgroundColor = .clear
//        }
//
//
//
//        observer = VidObserver(type: .single(model.identifier),
//                               stateChanged: { [weak self] item in self?.setup(state: item.state) })
//        VidLoaderHandler.shared.loader.observe(with: observer)
//    }
//
//    // MARK: - Private functions
//
//    private func setup(state: DownloadState) {
//        self.state = state
//        print("#### VideoCell: \(state)")
//        switch state {
//        case .keyLoaded:
//            DispatchQueue.main.async {
//                self.setupButton(title: "Asset loaded")
//                print("key has been loaded")
//            }
//
//        case .canceled, .unknown:
//
//            setupButton(title: "Download")
//
//            downloadStatusBtn.setImage(UIImage(named: "iconDownload1"), for: .normal)
//            downloadStatusBtn.backgroundColor = .clear
//            progressBar.isHidden = true
//        case .completed:
//            print("completed")
//            progressBar.isHidden = false
//            progressBar.progressColors = [AppColors.yellowColor]
//            progressBar.progress = 1.0
//            setupButton(title: "Downloaded")
//            DispatchQueue.main.async {
//                self.downloadStatusBtn.setImage(UIImage(named: "iconDone"), for: .normal)
//                self.downloadStatusBtn.backgroundColor = .clear
//            }
//
//        case .failed:
//            print("failed")
//            DispatchQueue.main.async {
//                self.downloadStatusBtn.setImage(UIImage(named: "iconError"), for: .normal)
//                self.downloadStatusBtn.backgroundColor = .clear
//
//            }
//            progressBar.isHidden = true
//            setupButton(title: "Failed", color: Colors.fireBrick)
//        case .prefetching:
//            DispatchQueue.main.async {
//                print("Prefetching")
//                self.progressBar.isHidden = false
//                self.setupButton(title: "Prefetchin")
//            }
//
//        case .running(let progress), .noConnection(let progress):
//            print("Mausam")
//            DispatchQueue.main.async {
//                self.downloadStatusBtn.setImage(UIImage(named: "iconPause"), for: .normal)
//
//                self.progressBar.isHidden = false
//                self.setupButton(title: "\(String(format: "%.0f", progress * 100)) %")
//                self.self.progressBar.progress = Double(progress)
//
//            }
//
//
//
//        case .paused(let progress):
//            DispatchQueue.main.async {
//
//                self.downloadStatusBtn.setImage(UIImage(named: "iconPlay"), for: .normal)
//                self.progressBar.isHidden = false
//                self.setupButton(title: "\(String(format: "%.0f", progress * 100)) %", color: Colors.gold)
//            }
//
//        case .waiting:
//            DispatchQueue.main.async {
//                print("waiting")
//                self.progressBar.isHidden = true
//                self.setupButton(title: "Waiting")
//            }
//
//        }
//    }
//
//    private func setupButton(title: String, color: UIColor = Colors.limeGreen) {
//        DispatchQueue.main.async {
//
//        }
//    }
//
//    // MARK: - Actions
//
//
//    @IBAction func BtnAction(_ sender: Any) {
//        switch state {
//        case .completed:
//            actions?.deleteVideo()
//        case .keyLoaded, .prefetching:
//            return
//        case .failed:
//            actions?.resumeFailedVideo()
//        case .canceled, .unknown:
//            actions?.startDownload()
//        case .waiting:
//            actions?.cancelDownload()
//        case .running, .noConnection:
//            actions?.showRunningActions()
//        case .paused:
//            actions?.showPausedActions()
//        }
//    }
//    @IBAction func deletetap(_ sender: Any) {
//        actions?.deleteVideo()
//    }
//}


//
//  VideoCell.swift
//  VidLoaderExample
//
//  Created by Petre on 18.10.19.
//  Copyright © 2019 Petre. All rights reserved.
//

import UIKit
import VidLoader
import KDCircularProgress

struct VideoCellActions {
    let deleteVideo: () -> Void
    let cancelDownload: () -> Void
    let startDownload: () -> Void
    let resumeFailedVideo: () -> Void
    let showRunningActions: () -> Void
    let showPausedActions: () -> Void
}

struct VideoCellModel {
    let identifier: String
    let title: String
    let thumbnailName: String
    let state: DownloadState
    let actions: VideoCellActions
    let coverImage: String
    let delete: Bool
    let duration: String
    let year: String
}

final class VideoCell: UITableViewCell {
    static let identifier = String(describing: VideoCell.self)
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var yearBtn: AnimatedButton!
    @IBOutlet weak var removeBtn: AnimatedButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var downloadStatusBtn: AnimatedButton!
    @IBOutlet private var titleLabel: UILabel!
    
    @IBOutlet weak var progressBar: KDCircularProgress!
    @IBOutlet private var thumbnailImageView: UIImageView!
    
    private var actions: VideoCellActions?
    private var state: DownloadState = .unknown
    private var observer: VidObserver?
    
    private struct Colors {
        static let limeGreen: UIColor = .init(red: 50 / 255.0, green: 205 / 255.0 , blue: 50 / 255.0, alpha: 1)
        static let fireBrick: UIColor = .init(red: 178 / 255.0, green: 34 / 255.0 , blue: 34 / 255.0, alpha: 1)
        static let gold: UIColor = .init(red: 255 / 255.0, green: 215 / 255.0 , blue: 0 / 255.0, alpha: 1)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        VidLoaderHandler.shared.loader.remove(observer: observer)
        observer = nil
    }
    
    func setup(model: VideoCellModel) {
        //uisetup
        yearBtn.titleLabel?.font = UIFont.systemFont(ofSize: 10)
        yearBtn.backgroundColor = AppColors.parrotColor
        removeBtn.backgroundColor = AppColors.darkRedColor
        removeBtn.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        progressBar.startAngle = -90
        progressBar.progressThickness = 0.2
        progressBar.trackThickness = 0.6
        progressBar.clockwise = true
        progressBar.gradientRotateSpeed = 2
        progressBar.roundedCorners = false
        //progressBar.glowMode = .forward
        //progressBar.glowAmount = 0.9
        progressBar.set(colors: UIColor.blue)
        backView.layer.cornerRadius = 8
        backView.backgroundColor = .clear
        coverImage.layer.cornerRadius = 8
        coverImage.clipsToBounds = true
        coverImage.sd_setImage(with: URL(string: model.coverImage), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
        
        // deleteBtn.isHidden = model.delete == true ? false : true
        
        thumbnailImageView.tintColor = AppColors.yellowColor
        contentView.backgroundColor = AppColors.darkColor
        self.actions = model.actions
        setup(state: model.state)
        titleLabel.text = model.title
        durationLbl.text = "Duration: " + model.duration
        yearBtn.setTitle(model.year, for: .normal)
        if model.state == .completed {
            if #available(iOS 13.0, *) {
                thumbnailImageView.image = UIImage(named: "iconVideo")?.withRenderingMode(.alwaysTemplate).withTintColor(AppColors.orangeColor)
            } else {
                // Fallback on earlier versions
                thumbnailImageView.image = UIImage(named: "iconVideo")
            }
            downloadStatusBtn.setImage(UIImage(named: "iconDone"), for: .normal)
            downloadStatusBtn.backgroundColor = .clear
        }
        observer = VidObserver(type: .single(model.identifier),
                               stateChanged: { [weak self] item in self?.setup(state: item.state) })
        VidLoaderHandler.shared.loader.observe(with: observer)
    }
    
    // MARK: - Private functions
    
    private func setup(state: DownloadState) {
        self.state = state
        print("#### VideoCell: \(state)")
        switch state {
        case .keyLoaded:
            DispatchQueue.main.async {
                self.setupButton(title: "Asset loaded")
                print("key has been loaded")
            }
            
        case .canceled, .unknown:
            
            setupButton(title: "Download")
            
            downloadStatusBtn.setImage(UIImage(named: "iconDownload1"), for: .normal)
            downloadStatusBtn.backgroundColor = .clear
            progressBar.isHidden = true
        case .completed:
            print("completed")
            progressBar.isHidden = false
            progressBar.progressColors = [AppColors.yellowColor]
            progressBar.progress = 1.0
            setupButton(title: "Downloaded")
            DispatchQueue.main.async {
                self.downloadStatusBtn.setImage(UIImage(named: "iconDone"), for: .normal)
                self.downloadStatusBtn.backgroundColor = .clear
            }
            
        case .failed:
            print("failed")
            DispatchQueue.main.async {
                self.downloadStatusBtn.setImage(UIImage(named: "iconError"), for: .normal)
                self.downloadStatusBtn.backgroundColor = .clear
                
            }
            progressBar.isHidden = true
            setupButton(title: "Failed", color: Colors.fireBrick)
        case .prefetching:
            DispatchQueue.main.async {
                print("Prefetching")
                self.progressBar.isHidden = false
                self.setupButton(title: "Prefetchin")
            }
            
        case .running(let progress), .noConnection(let progress):
            print("Mausam")
            DispatchQueue.main.async {
                self.downloadStatusBtn.setImage(UIImage(named: "iconPause"), for: .normal)
                
                self.progressBar.isHidden = false
                self.setupButton(title: "\(String(format: "%.0f", progress * 100)) %")
                self.self.progressBar.progress = Double(progress)
               
            }
            
            
            
        case .paused(let progress):
            DispatchQueue.main.async {
                
                self.downloadStatusBtn.setImage(UIImage(named: "iconPlay"), for: .normal)
                self.progressBar.isHidden = false
                self.setupButton(title: "\(String(format: "%.0f", progress * 100)) %", color: Colors.gold)
            }
            
        case .waiting:
            DispatchQueue.main.async {
                print("waiting")
                self.progressBar.isHidden = true
                self.setupButton(title: "Waiting")
            }
            
        }
    }
    
    private func setupButton(title: String, color: UIColor = Colors.limeGreen) {
        DispatchQueue.main.async {
            
        }
    }
    
    // MARK: - Actions
    
    
    @IBAction func BtnAction(_ sender: Any) {
        switch state {
        case .completed:
            actions?.deleteVideo()
        case .keyLoaded, .prefetching:
            return
        case .failed:
            actions?.resumeFailedVideo()
        case .canceled, .unknown:
            actions?.startDownload()
        case .waiting:
            actions?.cancelDownload()
        case .running, .noConnection:
            actions?.showRunningActions()
        case .paused:
            actions?.showPausedActions()
        }
    }
    @IBAction func deletetap(_ sender: Any) {
        actions?.deleteVideo()
    }
}
