//
//  ExclusiveMovieCollectionViewCell.swift
//  CinemaGhar
//
//  Created by Sunil on 5/17/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import  SDWebImage

class ComedySeriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var moviePosterImageView: UIImageView!
    
    
    func setWebSeriesInfo(webSeries:Series, labelColor: UIColor = UIColor.white){
        
        
     
        
        if let posterImage = webSeries.image {
            moviePosterImageView.sd_setImage(with: URL(string: posterImage), placeholderImage: UIImage(named: "placeholder_cell"), options: [ .progressiveLoad ])
        }
   

        
    }
    
    override func awakeFromNib() {
        cardView.layer.masksToBounds = false
        moviePosterImageView.layer.cornerRadius = 4
        addDropShadow(offset: CGSize(width: 1, height: 0.3), color: .black, radius: 3, opacity: 0.5)


    }
    
    
    
    
    
    
}
