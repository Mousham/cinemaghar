//
//  HomeVoteTVC.swift
//  CinemaGhar
//
//  Created by Midas on 03/08/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit

class HomeVoteTVC: UITableViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var coverImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uisetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func uisetup() {
        backView.layer.cornerRadius = 12
        backView.layer.masksToBounds = false
    }
    
}
