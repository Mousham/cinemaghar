//

//
//  Created by Sunil Gurung on 1/31/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit

class NewsFeedTableViewGalleryCell: UITableViewCell {

    @IBOutlet var galleryTitleLabel: UILabel!

    
    @IBOutlet var thirdView: UIView!
    @IBOutlet var galleryImageView1: UIImageView!
    @IBOutlet var galleryImageView2: UIImageView!
    
    @IBOutlet var galleryImageView3: UIImageView!
    
    @IBOutlet var gallerystackView: UIStackView!
    
    @IBOutlet var moreCountLabel: UILabel!
    
    @IBOutlet var overLayView: UIView!
    @IBOutlet var overLayViewLabel: UIView!
}
