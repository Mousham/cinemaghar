
//
//  Created by Sunil Gurung on 1/31/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit

class NewsFeedTableViewVideoCell: UITableViewCell {

    @IBOutlet var videoTitleLabel: UILabel!

    @IBOutlet var videoThumbnailImageView: UIImageView!
    
    
    @IBOutlet var youtubeLogoImg: UIImageView!
    
    @IBOutlet var videoPlayIcon: UIImageView!
    
    


}
