//
//  TutorialsCell.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 12/27/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import UIKit

class TutorialsCell: UITableViewCell {

    @IBOutlet var videoTitleLabel: UILabel!
    @IBOutlet var videoDescLabel: UILabel!

    @IBOutlet var videoThumbnailImageView: UIImageView!
    
    
    @IBOutlet var youtubeLogoImg: UIImageView!
    
    @IBOutlet var videoPlayIcon: UIImageView!
    
    


}
