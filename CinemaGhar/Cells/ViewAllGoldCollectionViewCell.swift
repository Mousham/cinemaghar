//
//  ExclusiveMovieCollectionViewCell.swift
//  CinemaGhar
//
//  Created by Sunil on 5/17/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit

class ViewAllGoldCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var moviePosterImageView: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    
    
    override func awakeFromNib() {
        layer.masksToBounds = false
        moviePosterImageView.layer.cornerRadius = 10
        addDropShadow(offset: CGSize(width: 1, height: 0.3), color: .black, radius: 3, opacity: 0.5)
        

    }
    
    func setMovieInfos(movie: Movie){
        
        if  let posterImage = movie.poster{
            moviePosterImageView.sd_setImage(with: URL(string: posterImage), placeholderImage: UIImage(named: "placeholder_cell"), options: [.continueInBackground, .progressiveLoad])
            
            movieTitle.text = movie.title
            

        }
    }
    
   
}
