//
//  CastCrewCVC.swift
//  CinemaGhar
//
//  Created by Midas on 21/06/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit

class CastCrewCVC: UICollectionViewCell {
    @IBOutlet weak var actorName: UILabel!
    @IBOutlet weak var actorImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uisetup()
    }
    func uisetup() {
        actorImage.layer.cornerRadius = 23.5
        actorImage.clipsToBounds = true
    }

}
