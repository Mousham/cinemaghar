//
//  ExclusiveMovieCollectionViewCell.swift
//  CinemaGhar
//
//  Created by Sunil on 5/17/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit

class ExclusiveMovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var moviePosterImageView: UIImageView!
//    @IBOutlet weak var movieTitleLabel: UILabel!
    
    override func awakeFromNib() {
layer.masksToBounds = false
        moviePosterImageView.layer.cornerRadius = 3
        addDropShadow(offset: CGSize(width: 1, height: 0.3), color: .black, radius: 3, opacity: 0.5)
        
//        let rectShape = CAShapeLayer()
//        rectShape.bounds = self.moviePosterImageView.frame
//        rectShape.position = self.moviePosterImageView.center
//        rectShape.path = UIBezierPath(roundedRect: self.moviePosterImageView.bounds,     byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 5, height: 5)).cgPath
//
//        
//        self.moviePosterImageView.layer.mask = rectShape

    }
    
    func setMovieInfos(movie: Movie){
        
        if  let posterImage = movie.poster{
//            movieTitleLabel.text = movieTitle
//            movieRatingLabel.text = "\(movieRating)"
            moviePosterImageView.sd_setImage(with: URL(string: posterImage), placeholderImage: UIImage(named: "placeholder_cell"), options: [.continueInBackground, .progressiveLoad])
            
            
//            if movieRating.truncatingRemainder(dividingBy: 1) == 0 {
//                formattedString.movieDetailLabelFormatter("\(Int(movieRating))/5",  11, UIFont.Weight.regular, .black )
//                    .movieDetailLabelFormatter("★", 13, .regular, Util.hexStringToUIColor(hex: "FF9300"))
//
//            }
//
//            else {
//                formattedString.movieDetailLabelFormatter("\(movieRating)/5",  11, UIFont.Weight.regular, .black )
//                    .movieDetailLabelFormatter("★", 13, .regular, Util.hexStringToUIColor(hex: "FF9300"))
//
//
//            }
//
//
//
//
//            self.movieRatingLabel.attributedText = formattedString

        }
        
//        if let isPaid = movie.paid {
//
//            if isPaid{
//
//                if UserDefaults.standard.bool(forKey: movie.productId){
//
//                     moviePriceLabel.text = " Purchased "
//                    moviePriceLabel.backgroundColor = Util.hexStringToUIColor(hex: "1E90FF")
//
//
//                }
//
//                else {
//                        moviePriceLabel.text = "  Paid  "
//                        moviePriceLabel.backgroundColor = .red
//
//                }
//
//
//
//
//            }
//            else {
//                moviePriceLabel.text = "  Free  "
//                moviePriceLabel.backgroundColor = Util.hexStringToUIColor(hex: "00803A")
//            }
//        }
      
     
        
        
    }
    
    
    
    
    
    
}
