//
//  GoalsTVC.swift
//  CinemaGhar
//
//  Created by Midas on 22/06/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
protocol GoalsTVCDelegate: class {
    func rewardbtnTap(type: RewardEarnType)
}
class GoalsTVC: UITableViewCell {

    
    @IBOutlet weak var pointsLbl: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var iconimage: UIImageView!
    var rewardChallengeType: RewardEarnType?
    weak var delegate: GoalsTVCDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func getItTap(_ sender: Any) {
        delegate?.rewardbtnTap(type: rewardChallengeType!)
    }
    
}
