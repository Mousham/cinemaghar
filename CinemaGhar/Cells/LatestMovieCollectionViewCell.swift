//
//  ExclusiveMovieCollectionViewCell.swift
//  CinemaGhar
//
//  Created by Sunil on 5/17/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import  SDWebImage

class LatestMovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var moviePosterImageView: UIImageView!
    
    
    func setMovieInfos(movie:Movie){
        
        
        
        if let posterImage = movie.poster {
            moviePosterImageView.sd_setImage(with: URL(string: posterImage), placeholderImage: UIImage(named: "placeholder_cell"), options: [.scaleDownLargeImages])
        }
        
  
        
        
    }
    
    override func awakeFromNib() {
        moviePosterImageView.layer.cornerRadius = 4
        addDropShadow(offset: CGSize(width: 1, height: 0.3), color: .black, radius: 3, opacity: 0.4)


    }
    
    
    
    
    
    
}
