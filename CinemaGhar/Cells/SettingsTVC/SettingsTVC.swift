//
//  SettingsTVC.swift
//  CinemaGhar
//
//  Created by Midas on 16/08/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit

class SettingsTVC: UITableViewCell {

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var iconNext: UIImageView!
    @IBOutlet weak var switchControl: UISwitch!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
