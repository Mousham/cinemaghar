//
//  AdTVC.swift
//  CinemaGhar
//
//  Created by Midas on 09/06/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import GoogleMobileAds


class AdTVC: UITableViewCell {

    @IBOutlet weak var googleAdView: UIView!
    lazy var adBannerView: GADBannerView = {
    let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
    adBannerView.adUnitID = "ca-app-pub-8501671653071605/1974659335"
//    adBannerView.delegate = self
//    adBannerView.rootViewController = self

    return adBannerView
    }()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       // googleAdViewSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func googleAdViewSetup() {
        let adRequest = GADRequest()
     
        adBannerView.load(GADRequest())
        googleAdView.addSubview(adBannerView)
    }
    
}
