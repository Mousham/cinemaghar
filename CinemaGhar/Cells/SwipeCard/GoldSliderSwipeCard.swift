//
//  FeaturedMovieSliderCell.swift
//  CinemaGhar
//
//  Created by Sunil on 5/16/18.
//  Copyright © 2018 sunBi. All rights reserved.
//


import UIKit
import SDWebImage
import Cosmos


class GoldSliderSwipeCard: UIView {
    @IBOutlet weak var movieCoverImage: UIImageView!
    
    private weak var shadowView: UIView?
    

    func loadKolodaView(sliderData: MovieSlider){
        
        
        if sliderData.content{
            
         
            
            if let movieCoverURL = sliderData.coverImage {
                movieCoverImage.sd_setImage(with: URL(string: movieCoverURL), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
            }
            
          
            
        }
            
        else {

            if let movieCoverURL = sliderData.image {
                movieCoverImage.sd_setImage(with: URL(string: movieCoverURL), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
                
            }
            
            
        }
        
     
    }
    
    
    override func awakeFromNib() {
        movieCoverImage.layer.cornerRadius = 30
        movieCoverImage.translatesAutoresizingMaskIntoConstraints = false
//        movieCoverImage.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], [.layerMinXMinYCorner, .layerMaxXMinYCorner],  radius: 30)
      
    }
    
    
    
    
    
    
    
    
    
}
