//
//  FeaturedMovieSliderCell.swift
//  CinemaGhar
//
//  Created by Sunil on 5/16/18.
//  Copyright © 2018 sunBi. All rights reserved.
//


import UIKit
import SDWebImage
import Cosmos


class FeaturedMovieSwipeCard: UIView {
    @IBOutlet weak var movieCoverImage: UIImageView!
    
    
    private weak var shadowView: UIView?

    
    @IBOutlet weak var gradientView: GradientView!
  
    
    func loadKolodaView(sliderData: MovieSlider){
        movieCoverImage.translatesAutoresizingMaskIntoConstraints = false
        
        if sliderData.content{
//            self.movieGenreLabel.text = sliderData.movie.genre
            
         
            
            if let movieCoverURL = sliderData.coverImage {
                movieCoverImage.sd_setImage(with: URL(string: movieCoverURL), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
            }
            
            
        }
            
        else {

            if let movieCoverURL = sliderData.image {
                movieCoverImage.sd_setImage(with: URL(string: movieCoverURL), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
                
            }
            
            
        }
        
     
    }
    
    
    override func awakeFromNib() {
        movieCoverImage.roundCorners([.topLeft, .topRight], [.layerMinXMinYCorner, .layerMaxXMinYCorner],  radius: 10)
      
    }
    
    
    
    
    
    
    
    
    
}
