//
//  DownloadsTVC.swift
//  CinemaGhar
//
//  Created by Midas on 02/06/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import VidLoader
//struct VideoCellActions {
//    let deleteVideo: () -> Void
//    let cancelDownload: () -> Void
//    let startDownload: () -> Void
//    let resumeFailedVideo: () -> Void
//    let showRunningActions: () -> Void
//    let showPausedActions: () -> Void
//}
//
//struct VideoCellModel {
//    let identifier: String
//    let title: String
//    let thumbnailName: String
//    let state: DownloadState 
//    let actions: VideoCellActions
//}

class DownloadsTVC: UITableViewCell {

    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var failedLbl: UILabel!
    @IBOutlet weak var retryBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var moviewImage: UIImageView!
    private var actions: VideoCellActions?
    private var state: DownloadState = .unknown
    private var observer: VidObserver?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uisetup()
        //actions?.startDownload()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()

        VidLoaderHandler.shared.loader.remove(observer: observer)
        observer = nil
    }
    
    func uisetup() {
        backView.layer.cornerRadius = 8
        backView.addDropShadow(offset: CGSize(width: 0, height: 0), color: .black, radius: 3, opacity: 0.5)
        backView.layer.masksToBounds = false
    }
    
    func setup(model: VideoCellModel) {
        self.actions = model.actions
        setup(state: model.state)
//        titleLabel.text = model.title
//        thumbnailImageView.image = UIImage(named: model.thumbnailName)
        observer = VidObserver(type: .single(model.identifier),
                               stateChanged: { [weak self] item in self?.setup(state: item.state) })
        VidLoaderHandler.shared.loader.observe(with: observer)
    }
    // MARK: - Private functions

    private func setup(state: DownloadState) {
        self.state = state
        print("#### VideoCell: \(state)")
        switch state {
        case .keyLoaded:
            setupButton(title: "Asset loaded")
            print("key has been loaded")
        case .canceled, .unknown:
            setupButton(title: "Download")
        case .completed:
            print("completed")
            setupButton(title: "Downloaded")
        case .failed:
            print("failed")
            setupButton(title: "Failed", color: .red)
        case .prefetching:
            print("Prefetching")
            setupButton(title: "Prefetchin")
        case .running(let progress), .noConnection(let progress):
            print("Mausam")
            setupButton(title: "\(String(format: "%.0f", progress * 100)) %")
        case .paused(let progress):
            setupButton(title: "\(String(format: "%.0f", progress * 100)) %", color: .blue)
        case .waiting:
            print("waiting")
            setupButton(title: "Waiting")
        }
    }

    private func setupButton(title: String, color: UIColor = .red) {
        DispatchQueue.main.async {
//            self.stateButton.setTitle(title, for: .normal)
//            self.stateButton.setTitleColor(color, for: .normal)
        }
    }
    
    @IBAction func downloadTap(_ sender: Any) {
        switch state {
        case .completed:
            actions?.deleteVideo()
        case .keyLoaded, .prefetching:
            return
        case .failed:
            actions?.resumeFailedVideo()
        case .canceled, .unknown:
            actions?.startDownload()
        case .waiting:
            actions?.cancelDownload()
        case .running, .noConnection:
            actions?.showRunningActions()
        case .paused:
            actions?.showPausedActions()
        }
    }
}
