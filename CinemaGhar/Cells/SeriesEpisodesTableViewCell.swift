//
//  SeriesEpisodesTableViewCell.swift
//  CinemaGhar
//
//  Created by Sunil on 6/26/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import SDWebImage   

class SeriesEpisodesTableViewCell: UITableViewCell {

    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var episodeTitleLabel: UILabel!
    @IBOutlet weak var episodeImageView: UIImageView!
    
    
    func setEpisodeDetail(index: Int ,episode: Episode){
        
        episodeTitleLabel.text = "\(index). " + episode.title
        
        
        
        durationLabel.attributedText = makeMovieDetailLabel(preffix: "Runtime: ", suffix: episode.duration)
        
        if let posterImage = episode.poster {
            episodeImageView.sd_setImage(with: URL(string: posterImage), placeholderImage: UIImage(), options: [ .continueInBackground])
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.episodeImageView.layer.cornerRadius = 4
        self.episodeImageView.layer.masksToBounds = true
        // Initialization code
    }

    
    func makeMovieDetailLabel(preffix: String, suffix:String)-> NSAttributedString {
        
        let formattedString = NSMutableAttributedString()
        if suffix == ""{
            formattedString
                .movieDetailLabelFormatter(preffix,  13, UIFont.Weight.medium, .white )
                .movieDetailLabelFormatter("N/A", 14,UIFont.Weight.semibold, .white)
            
        }
        else {
            formattedString
                .movieDetailLabelFormatter(preffix,  13, UIFont.Weight.medium, .white)
                .movieDetailLabelFormatter(suffix, 14, UIFont.Weight.semibold, .white)
            
        }
        
        return formattedString
        
    }
    
    


    

}
