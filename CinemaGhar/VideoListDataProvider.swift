////
////  VideoListDataProvider.swift
////  VidLoaderExample
////
////  Created by Petre on 14.10.19.
////  Copyright © 2019 Petre. All rights reserved.
////
//
//import Foundation
//import AVFoundation
//import UIKit
//import VidLoader
//extension Array where Element:Equatable {
//    func removeDuplicates() -> [Element] {
//        var result = [Element]()
//
//        for value in self {
//            if result.contains(value) == false {
//                result.append(value)
//            }
//        }
//
//        return result
//    }
//}
//protocol VideoListDataProviding {
//    var items: [VideoData] { get }
//    func setup(videoListActions: VideoListActions)
//    func urlAsset(row: Int) -> AVURLAsset?
//    func videoModel(row: Int) -> VideoCellModel
//    func deleteVideo(with data: VideoData)
//    func startDownload(with data: VideoData)
//    func stopDownload(with data: VideoData)
//    func pauseDownload(with data: VideoData)
//    func resumeDownload(with data: VideoData)
//    func downloadvideoData(withd downloadData: VideoData)
//    func editbtnPresed()
//
//}
//
//struct VideoListActions {
//    let reloadData: () -> Void
//    let showRemoveActionSheet: (VideoData) -> Void
//    let showStopActionSheet: (VideoData) -> Void
//    let showFailedActionSheet: (VideoData) -> Void
//    let showRunningActions: (VideoData) -> Void
//    let showPausedActions: (VideoData) -> Void
//}
//
//final class VideoListDataProvider: VideoListDataProviding {
//    func editbtnPresed() {
//        guard let data = userDefaults.value(forKey: Constant.downloadsKey) as? Data,
//              let items = try? JSONDecoder().decode([VideoData].self, from: data) else {
//              return
//
//        }
//        for item in items {
//            if let itemlocation = item.location {
//                try? fileManager.removeItem(at: itemlocation)
//            }
//        }
//
//        userDefaults.set(Data(), forKey: Constant.downloadsKey)
//        self.items = []
//        self.videoListActions?.reloadData()
//    }
//
//    var downloadDatas: VideoData?
//    func downloadvideoData(withd downloadData: VideoData) {
//        self.downloadDatas = downloadData
//        items = extractItems(itemsKey: itemsKey)
//
//
//
//
//        print(items)
//
//
//        self.items.append(downloadData)
//        if let index = items.index(where: {$0.identifier == "offlineDownloads"}) {
//            items.remove(at: index)
//        }
//        items = removeDuplicateElements(post: items)
//       // items[0].location = URL(string: "Library/com.apple.UserManagedAssets.qDkKca/Damaru%20Ko%20Dandibiyo_1EF8FE0A3338F688.movpkg")
//        //self.items = [downloadData]
//       // save(items: [])
//    }
//
//
//
//    private let userDefaults: UserDefaults
//    private let itemsKey: String
//    private let vidLoaderHandler: VidLoaderHandler
//    private let fileManager: FileManager
//    private(set) var items: [VideoData] = []
//    private var observer: VidObserver?
//    private var videoListActions: VideoListActions?
//
//    init(userDefaults: UserDefaults = .standard,
//         itemsKey: String = "vid_loader_example_items",
//         vidLoaderHandler: VidLoaderHandler = .shared,
//         fileManager: FileManager = .default) {
//        self.userDefaults = userDefaults
//        self.itemsKey = itemsKey
//        self.vidLoaderHandler = vidLoaderHandler
//        self.fileManager = fileManager
////        self.items = extractItems(itemsKey: itemsKey)
//        self.items = []
//        self.observer = VidObserver(type: .all, stateChanged: { [weak self] item in
//            self?.update(item: item)
//        })
//        setupObservers()
//    }
//
//    func setup(videoListActions: VideoListActions) {
//        self.videoListActions = videoListActions
//    }
//
//    func urlAsset(row: Int) -> AVURLAsset? {
//
//        guard let location = items[row].location else { return nil }
//        print(location)
//        return vidLoaderHandler.loader.asset(location: location)
//    }
//
//    func videoModel(row: Int) -> VideoCellModel {
//        let videoData = items[row]
//        let actions = VideoCellActions(
//            deleteVideo: { [weak self] in self?.videoListActions?.showRemoveActionSheet(videoData) },
//            cancelDownload: { [weak self] in self?.videoListActions?.showStopActionSheet(videoData) },
//            startDownload: { [weak self] in self?.startDownload(with: videoData) },
//            resumeFailedVideo: { [weak self] in self?.videoListActions?.showFailedActionSheet(videoData) },
//            showRunningActions: { [weak self] in self?.videoListActions?.showRunningActions(videoData) },
//            showPausedActions: { [weak self] in self?.videoListActions?.showPausedActions(videoData) }
//        )
//
//        return VideoCellModel(identifier: videoData.identifier, title: videoData.title,
//                              thumbnailName: videoData.imageName, state: videoData.state,
//                              actions: actions, coverImage: videoData.coverImage ?? "", delete: false,duration: videoData.duration ?? "",year: videoData.year ?? ""
//        )
//
//    }
//
//    func deleteVideo(with data: VideoData) {
//        var items = extractItems(itemsKey: itemsKey)
//        for (index,item) in items.enumerated() {
//            if item.identifier == data.identifier {
//                items.remove(at: index)
//                self.videoListActions?.reloadData()
//            }
//        }
//        removeVideo(identifier: data.identifier, location: data.location, state: .unknown)
//        save(items: items)
//        self.videoListActions?.reloadData()
//    }
//
//    func startDownload(with data: VideoData) {
//        guard let url = URL(string: data.stringURL) else { return }
//        print(data)
//        let downloadValues = DownloadValues(identifier: data.identifier,
//                                            url: url,
//                                            title: data.title,
//                                            artworkData: UIImageJPEGRepresentation(UIImage(named: data.imageName) ?? UIImage(), 0.75),
//                                            minRequiredBitrate: nil, coverimage: data.coverImage ?? "")
//        vidLoaderHandler.loader.download(downloadValues)
//    }
//
//    func stopDownload(with data: VideoData) {
//        guard vidLoaderHandler.loader.state(for: data.identifier) != .unknown else {
//            removeVideo(identifier: data.identifier, location: nil, state: .unknown)
//            videoListActions?.reloadData()
//            return
//        }
//        vidLoaderHandler.loader.cancel(identifier: data.identifier)
//    }
//
//    func pauseDownload(with data: VideoData) {
//        vidLoaderHandler.loader.pause(identifier: data.identifier)
//    }
//
//    func resumeDownload(with data: VideoData) {
//        vidLoaderHandler.loader.resume(identifier: data.identifier)
//    }
//
//    // MARK: - Private functions
//
//    private func update(item: ItemInformation) {
//        guard let index = items.firstIndex(where: { $0.identifier == item.identifier }) else { return }
//        let videoData = items[index]
//        items[index] = VideoData(identifier: videoData.identifier, title: videoData.title,
//                                 imageName: videoData.imageName, state: item.state,
//                                 stringURL: videoData.stringURL, location: item.location, coverImage: videoData.coverImage ?? "", duration: videoData.duration ?? "",year: videoData.year ?? "")
//        print(item)
//        save(items: items)
//    }
//
//    private func removeVideo(identifier: String, location: URL?, state: DownloadState) {
//        guard let index = items.firstIndex(where: { $0.identifier == identifier }) else { return }
//        let data = items[index]
//        if let location = location ?? data.location { try? fileManager.removeItem(at: location) }
//        let videoData = VideoData(identifier: data.identifier, title: data.title,
//                                  imageName: data.imageName, state: state,
//                                  stringURL: data.stringURL, coverImage: data.coverImage ?? "",duration: data.duration ?? "",year: data.year ?? "")
//        items[index] = videoData
//        save(items: items)
//    }
//
//    private func setupObservers() {
//        observer = VidObserver(type: .all, stateChanged: { [weak self] item in
//            self?.update(item: item)
//        })
//        vidLoaderHandler.loader.observe(with: observer)
//    }
//
//    private func extractItems(itemsKey: String) -> [VideoData] {
//
//        guard let data = userDefaults.value(forKey: itemsKey) as? Data,
//              let items = try? JSONDecoder().decode([VideoData].self, from: data) else {
////                let items = generateDefaultItems()
////                save(items: items)
////                return items
//                  return []
//        }
//
//
//        return items
//
//    }
//
//
//    private func save(items: [VideoData]) {
//        guard let data = try? JSONEncoder().encode(items) else { return }
//        userDefaults.set(data, forKey: itemsKey)
//
//    }
//
//
//    func removeDuplicateElements(post: [VideoData]) -> [VideoData] {
//        var uniquePosts = [VideoData]()
//        for post in post {
//            if !uniquePosts.contains(where: {$0.identifier == post.identifier }) {
//                uniquePosts.append(post)
//            }
//        }
//        return uniquePosts
//    }
//}
//
//extension UIImage {
//    func toData (options: NSDictionary, type: CFString) -> Data? {
//        guard let cgImage = cgImage else { return nil }
//        return autoreleasepool { () -> Data? in
//            let data = NSMutableData()
//            guard let imageDestination = CGImageDestinationCreateWithData(data as CFMutableData, type, 1, nil) else { return nil }
//            CGImageDestinationAddImage(imageDestination, cgImage, options)
//            CGImageDestinationFinalize(imageDestination)
//            return data as Data
//        }
//    }
//}
//class getUserDefults {
//    class func getDownloadedMoview() -> [VideoData] {
//        let userDefaults: UserDefaults = .standard
//        guard let data = userDefaults.value(forKey: "vid_loader_example_items") as? Data,
//              let items = try? JSONDecoder().decode([VideoData].self, from: data) else {
//
//            return []
//
//        }
//        return items
//
//
//
//    }
//}
//
//  VideoListDataProvider.swift
//  VidLoaderExample
//
//  Created by Petre on 14.10.19.
//  Copyright © 2019 Petre. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import VidLoader
extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}
protocol VideoListDataProviding {
    var items: [VideoData] { get }
    func setup(videoListActions: VideoListActions)
    func urlAsset(row: Int) -> AVURLAsset?
    func videoModel(row: Int) -> VideoCellModel
    func deleteVideo(with data: VideoData)
    func startDownload(with data: VideoData)
    func stopDownload(with data: VideoData)
    func pauseDownload(with data: VideoData)
    func resumeDownload(with data: VideoData)
    func downloadvideoData(withd downloadData: VideoData)
    func editbtnPresed()
    
}

struct VideoListActions {
    let reloadData: () -> Void
    let showRemoveActionSheet: (VideoData) -> Void
    let showStopActionSheet: (VideoData) -> Void
    let showFailedActionSheet: (VideoData) -> Void
    let showRunningActions: (VideoData) -> Void
    let showPausedActions: (VideoData) -> Void
}

final class VideoListDataProvider: VideoListDataProviding {
    func editbtnPresed() {
        guard let data = userDefaults.value(forKey: Constant.downloadsKey) as? Data,
              let items = try? JSONDecoder().decode([VideoData].self, from: data) else {
              return
                
        }
        for item in items {
            if let itemlocation = item.location {
                try? fileManager.removeItem(at: itemlocation)
            }
        }
       
        userDefaults.set(Data(), forKey: Constant.downloadsKey)
        self.items = []
        self.videoListActions?.reloadData()
    }
    
    var downloadDatas: VideoData?
    func downloadvideoData(withd downloadData: VideoData) {
        self.downloadDatas = downloadData
        items = extractItems(itemsKey: itemsKey)
       
       
        
       
        print(items)
       
    
        self.items.append(downloadData)
        if let index = items.index(where: {$0.identifier == "offlineDownloads"}) {
            items.remove(at: index)
        }
        items = removeDuplicateElements(post: items)
       // items[0].location = URL(string: "Library/com.apple.UserManagedAssets.qDkKca/Damaru%20Ko%20Dandibiyo_1EF8FE0A3338F688.movpkg")
        //self.items = [downloadData]
       // save(items: [])
    }
    
   
    
    private let userDefaults: UserDefaults
    private let itemsKey: String
    private let vidLoaderHandler: VidLoaderHandler
    private let fileManager: FileManager
    private(set) var items: [VideoData] = []
    private var observer: VidObserver?
    private var videoListActions: VideoListActions?

    init(userDefaults: UserDefaults = .standard,
         itemsKey: String = "vid_loader_example_items",
         vidLoaderHandler: VidLoaderHandler = .shared,
         fileManager: FileManager = .default) {
        self.userDefaults = userDefaults
        self.itemsKey = itemsKey
        self.vidLoaderHandler = vidLoaderHandler
        self.fileManager = fileManager
//        self.items = extractItems(itemsKey: itemsKey)
        self.items = []
        self.observer = VidObserver(type: .all, stateChanged: { [weak self] item in
            self?.update(item: item)
        })
        setupObservers()
    }

    func setup(videoListActions: VideoListActions) {
        self.videoListActions = videoListActions
    }

    func urlAsset(row: Int) -> AVURLAsset? {
    
        guard let location = items[row].location else { return nil }
        print(location)
        return vidLoaderHandler.loader.asset(location: location)
    }

    func videoModel(row: Int) -> VideoCellModel {
        let videoData = items[row]
        let actions = VideoCellActions(
            deleteVideo: { [weak self] in self?.videoListActions?.showRemoveActionSheet(videoData) },
            cancelDownload: { [weak self] in self?.videoListActions?.showStopActionSheet(videoData) },
            startDownload: { [weak self] in self?.startDownload(with: videoData) },
            resumeFailedVideo: { [weak self] in self?.videoListActions?.showFailedActionSheet(videoData) },
            showRunningActions: { [weak self] in self?.videoListActions?.showRunningActions(videoData) },
            showPausedActions: { [weak self] in self?.videoListActions?.showPausedActions(videoData) }
        )

        return VideoCellModel(identifier: videoData.identifier, title: videoData.title,
                              thumbnailName: videoData.imageName, state: videoData.state,
                              actions: actions, coverImage: videoData.coverImage ?? "", delete: false,duration: videoData.duration ?? "",year: videoData.year ?? ""
        )
      
    }

    func deleteVideo(with data: VideoData) {
        var items = extractItems(itemsKey: itemsKey)
        for (index,item) in items.enumerated() {
            if item.identifier == data.identifier {
                items.remove(at: index)
                self.videoListActions?.reloadData()
            }
        }
        removeVideo(identifier: data.identifier, location: data.location, state: .unknown)
        save(items: items)
        self.videoListActions?.reloadData()
    }
    
    func startDownload(with data: VideoData) {
        guard let url = URL(string: data.stringURL) else { return }
        print(data)
        let downloadValues = DownloadValues(identifier: data.identifier,
                                            url: url,
                                            title: data.title,
                                            artworkData: UIImageJPEGRepresentation(UIImage(named: data.imageName) ?? UIImage(), 0.75),
                                            minRequiredBitrate: nil, coverimage: data.coverImage ?? "")
        vidLoaderHandler.loader.download(downloadValues)
    }
    
    func stopDownload(with data: VideoData) {
        guard vidLoaderHandler.loader.state(for: data.identifier) != .unknown else {
            removeVideo(identifier: data.identifier, location: nil, state: .unknown)
            videoListActions?.reloadData()
            return
        }
        vidLoaderHandler.loader.cancel(identifier: data.identifier)
    }
    
    func pauseDownload(with data: VideoData) {
        vidLoaderHandler.loader.pause(identifier: data.identifier)
    }

    func resumeDownload(with data: VideoData) {
        vidLoaderHandler.loader.resume(identifier: data.identifier)
    }

    // MARK: - Private functions

    private func update(item: ItemInformation) {
        guard let index = items.firstIndex(where: { $0.identifier == item.identifier }) else { return }
        let videoData = items[index]
        items[index] = VideoData(identifier: videoData.identifier, title: videoData.title,
                                 imageName: videoData.imageName, state: item.state,
                                 stringURL: videoData.stringURL, location: item.location, coverImage: videoData.coverImage ?? "", duration: videoData.duration ?? "",year: videoData.year ?? "")
        print(item)
        save(items: items)
    }

    private func removeVideo(identifier: String, location: URL?, state: DownloadState) {
        guard let index = items.firstIndex(where: { $0.identifier == identifier }) else { return }
        let data = items[index]
        if let location = location ?? data.location { try? fileManager.removeItem(at: location) }
        let videoData = VideoData(identifier: data.identifier, title: data.title,
                                  imageName: data.imageName, state: state,
                                  stringURL: data.stringURL, coverImage: data.coverImage ?? "",duration: data.duration ?? "",year: data.year ?? "")
        items[index] = videoData
        save(items: items)
    }

    private func setupObservers() {
        observer = VidObserver(type: .all, stateChanged: { [weak self] item in
            self?.update(item: item)
        })
        vidLoaderHandler.loader.observe(with: observer)
    }

    private func extractItems(itemsKey: String) -> [VideoData] {
       
        guard let data = userDefaults.value(forKey: itemsKey) as? Data,
              let items = try? JSONDecoder().decode([VideoData].self, from: data) else {
//                let items = generateDefaultItems()
//                save(items: items)
//                return items
                  return []
        }
        
      
        return items
      
    }
   

    private func save(items: [VideoData]) {
        guard let data = try? JSONEncoder().encode(items) else { return }
        userDefaults.set(data, forKey: itemsKey)
        
    }

   
    func removeDuplicateElements(post: [VideoData]) -> [VideoData] {
        var uniquePosts = [VideoData]()
        for post in post {
            if !uniquePosts.contains(where: {$0.identifier == post.identifier }) {
                uniquePosts.append(post)
            }
        }
        return uniquePosts
    }
}

extension UIImage {
    func toData (options: NSDictionary, type: CFString) -> Data? {
        guard let cgImage = cgImage else { return nil }
        return autoreleasepool { () -> Data? in
            let data = NSMutableData()
            guard let imageDestination = CGImageDestinationCreateWithData(data as CFMutableData, type, 1, nil) else { return nil }
            CGImageDestinationAddImage(imageDestination, cgImage, options)
            CGImageDestinationFinalize(imageDestination)
            return data as Data
        }
    }
}
class getUserDefults {
    class func getDownloadedMoview() -> [VideoData] {
        let userDefaults: UserDefaults = .standard
        guard let data = userDefaults.value(forKey: "vid_loader_example_items") as? Data,
              let items = try? JSONDecoder().decode([VideoData].self, from: data) else {
              
            return []
               
        }
        return items
       
      
       
    }
}
