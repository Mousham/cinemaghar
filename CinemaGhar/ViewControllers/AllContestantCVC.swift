//
//  AllContestantCVC.swift
//  CinemaGhar
//
//  Created by Midas on 13/05/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit

class AllContestantCVC: UICollectionViewCell {
   
    @IBOutlet weak var voteBtn: AnimatedButton!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var codeLbl: UILabel!
    @IBOutlet weak var iconimage: UIImageView!
    @IBOutlet weak var backView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uisetup()
    }
    func uisetup() {
        iconimage.layer.cornerRadius = 12
        backView.layer.cornerRadius = 12
        backView.backgroundColor = UIColor.init(hex: "#303744")
        codeLbl.font = UIFont(name: FontsType.titilliuimLight.rawValue, size: 5)
        codeLbl.textColor = AppColors.orangeColor
        nameLbl.font = UIFont(name: FontsType.titilliuimLight.rawValue, size: 5)
        nameLbl.textColor = .white
        addressLbl.textColor = AppColors.lightDarkColor
        addressLbl.font = UIFont(name: FontsType.titilliuimLight.rawValue, size: 12)
        
    }

}
