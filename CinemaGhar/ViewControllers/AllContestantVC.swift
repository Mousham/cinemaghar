//
//  AllContestantVC.swift
//  CinemaGhar
//
//  Created by Midas on 13/05/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMobileAds
class AllContestantVC: UIViewController {
    
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var votingDetailsData = [VotingDetailsModel]() {
        didSet {
            collectionView.reloadData()
        }
    }
    lazy var adBannerView: GADBannerView = {
        let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        adBannerView.adUnitID = Constant.googleBannerKey
        adBannerView.delegate = self
        adBannerView.rootViewController = self

        return adBannerView
    }()
    let adRequest = GADRequest()
    var votingData = VotingModel(json: JSON())
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        uisetup()
        getVotingDetails()
    }
    func uisetup() {
        adBannerView.load(GADRequest())
        navView.backgroundColor = AppColors.darkColor
        self.view.backgroundColor = AppColors.darkColor
        collectionView.backgroundColor = .clear
        collectionView.register(UINib(nibName: "AllContestantCVC", bundle: nil), forCellWithReuseIdentifier: "AllContestantCVC")
        collectionView.register(UINib(nibName: "AdCVC", bundle: nil), forCellWithReuseIdentifier: "AdCVC")
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    @IBAction func backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func notificationTap(_ sender: Any) {
    }
    
}
extension AllContestantVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return votingDetailsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (votingDetailsData.count - 1) == indexPath.row {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdCVC", for: indexPath) as! AdCVC
            cell.googleAdView.addSubview(self.adBannerView)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllContestantCVC", for: indexPath) as! AllContestantCVC
            cell.iconimage.sd_setImage(with: URL(string: votingDetailsData[indexPath.row].coverImage ?? ""), placeholderImage: UIImage(named: ""), options: [.scaleDownLargeImages], completed:nil)
            cell.nameLbl.text = votingDetailsData[indexPath.row].title
            cell.addressLbl.text = "Kathmandu,Nepal"
            cell.codeLbl.font = UIFont(name: FontsType.titilliuimLight.rawValue, size: 5)
            return cell
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (votingDetailsData.count - 1) == indexPath.row {
            return CGSize(width: collectionView.bounds.size.width , height: 100)
        } else {
            return CGSize(width: collectionView.bounds.size.width / 2 - 4, height: 272)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard.votingStoryboard.instantiateVoteNowVC()
        vc.votingData = votingDetailsData[indexPath.row]
        vc.votingPlatformData = votingData
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4.0
    }
    
}
extension AllContestantVC {
    //MARK: - GET VOTING DETAILS
    func getVotingDetails() {
        globalVotingId = Int(votingData.id ?? 0)
        APIHandler.shared.getVotingDetails { [weak self] (votings) in
            guard let strongSelf = self else { return }
            print(votings)
            strongSelf.votingDetailsData = votings
            let adCell = VotingDetailsModel(json: JSON())
            strongSelf.votingDetailsData.append(adCell)
            //strongSelf.datasource.votingDetailsData = votings
            //            self.votingData = votings
            //            print(self.votingData)
            //            self.votingCollectionView.reloadData()
            //            self.tutorialsDataArr = tutorials
            //            self.tutorialsTableView.reloadData()
        } failure: { (msg) in
            print(msg)
        } error: { (err) in
            print(err.localizedDescription)
        }
    }
}
//MARK: - GOOGLE AD DELEGATE
extension AllContestantVC: GADBannerViewDelegate {
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("bannerViewDidReceiveAd")
    }

    private func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
      print("bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    func adViewDidRecordImpression(_ bannerView: GADBannerView) {
      print("bannerViewDidRecordImpression")
    }

    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillPresentScreen")
    }

    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillDIsmissScreen")
    }

    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewDidDismissScreen")
    }
}

