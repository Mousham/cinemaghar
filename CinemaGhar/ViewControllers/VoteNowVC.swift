//
//  VoteNowVC.swift
//  CinemaGhar
//
//  Created by Midas on 16/05/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import SwiftyJSON
import EzPopup

class VoteNowVC: UIViewController {

    @IBOutlet weak var optionTitle: UILabel!
    @IBOutlet weak var votenowTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var codeLbl: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var topBackView: UIView!
    @IBOutlet weak var navView: UIView!
    var votingData = VotingDetailsModel(json: JSON())
    var votingDetailsData = [VotingDetailsModel]()
    var votingPrice = [VotingPriceModel]()
    var votingPlatformData = VotingModel(json: JSON())
    var votingResponse = DefaultResponse(json: JSON())
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetup()
        loadData()
        getVotingDetails()
        getVotingPrice()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        topBackView.layer.cornerRadius = 12
        topBackView.clipsToBounds = true
        topBackView.applyGradient(isVertical: true, colorArray: [UIColor.init(hex: "303744"),AppColors.darkColor])
    }
    func uisetup() {
        self.view.backgroundColor = AppColors.darkColor
        navView.backgroundColor = AppColors.darkColor
        votenowTitle.textColor = AppColors.orangeColor
        optionTitle.textColor = AppColors.lightDarkColor
        codeLbl.textColor = AppColors.orangeColor
        nameLbl.textColor = .white
        addressLbl.textColor = AppColors.lightDarkColor
        iconImage.layer.cornerRadius = 8
        tableView.register(UINib(nibName: "VotingRateTVC", bundle: nil), forCellReuseIdentifier: "VotingRateTVC")
        tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = .clear
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    func loadData() {
        iconImage.sd_setImage(with: URL(string: votingData.coverImage ?? ""), placeholderImage: UIImage(named: ""), options: [.scaleDownLargeImages], completed:nil)
        codeLbl.text = "TA0020"
        nameLbl.text = votingData.title
        addressLbl.text = "Kathmandu,Nepal"
        
    }
    
    //MARK: - SHOW POP UP
    func showPopup(votingPriceData: VotingPriceModel,votingDetailsData: VotingDetailsModel) {
            guard let customAlertVC = VotingFinalPopUpVC.instantiate() else { return }
            customAlertVC.delegate = self
            customAlertVC.votingPriceData = votingPriceData
        customAlertVC.votingDetailsData = votingDetailsData
        customAlertVC.votingPlatformData = votingPlatformData
        
            
           
            
            let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: SCREEN.WIDTH - 80, popupHeight: 365)
            popupVC.cornerRadius = 8
           
           
           present(popupVC, animated: true, completion: nil)
        }
    @IBAction func backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func notificationTap(_ sender: Any) {
    }
    

}
extension VoteNowVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return votingPrice.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VotingRateTVC", for: indexPath) as! VotingRateTVC
        cell.contentView.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        cell.votingQtyLbl.text = "\(votingPrice[indexPath.row].votes ?? 0)" + " Votes"
        cell.votingAmtLbl.text = "Rs. " + "\(votingPrice[indexPath.row].coins ?? 0)"
        if votingPrice[indexPath.row].isSelected == true {
            cell.backView.layer.borderWidth = 0.75
            cell.backView.layer.borderColor = AppColors.redColor.cgColor
            cell.votingQtyView.backgroundColor = AppColors.redColor
            cell.votingQtyLbl.textColor = .white
        } else {
            cell.backView.layer.borderWidth = 0
            cell.votingQtyView.backgroundColor = AppColors.unSelectedColor
            cell.votingQtyLbl.textColor = AppColors.orangeColor
            //cell.backView.layer.borderColor = .clear
        }
       // cell.layer.backgroundColor = .clear
        return cell
    }
    
    
}
extension VoteNowVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 77
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for (index,item) in votingPrice.enumerated() {
            if index == indexPath.row {
                votingPrice[index].isSelected = true
            } else {
                votingPrice[index].isSelected = false
            }
        }
//        if votingPrice[indexPath.row].isSelected == true {
//            votingPrice[indexPath.row].isSelected = false
//        } else {
//            votingPrice[indexPath.row].isSelected = true
//        }
        tableView.reloadData()
        showPopup(votingPriceData: votingPrice[indexPath.row], votingDetailsData: votingDetailsData.first ?? VotingDetailsModel(json: JSON()))
        
    }
    
}
extension VoteNowVC {
    //MARK: - GET VOTING DETAILS
    func getVotingDetails() {
        globalVotingId = Int(votingData.id ?? 0)
        APIHandler.shared.getVotingDetails { [weak self] (votings) in
            guard let strongSelf = self else { return }
            print(votings)
            strongSelf.votingDetailsData = votings
           // strongSelf.datasource.votingDetailsData = votings
//            self.votingData = votings
//            print(self.votingData)
//            self.votingCollectionView.reloadData()
//            self.tutorialsDataArr = tutorials
//            self.tutorialsTableView.reloadData()
        } failure: { (msg) in
            print(msg)
        } error: { (err) in
            print(err.localizedDescription)
        }
    }
    //MARK: - GET VOTING PRICE
    func getVotingPrice() {
        globalVotingId = Int(votingData.id ?? 0)
        APIHandler.shared.getVotingPrice { [weak self] (votingPrice) in
            guard let strongSelf = self else { return }
            print(votingPrice)
            strongSelf.votingPrice = votingPrice
            strongSelf.tableView.reloadData()
//            self.votingData = votings
//            print(self.votingData)
//            self.votingCollectionView.reloadData()
//            self.tutorialsDataArr = tutorials
//            self.tutorialsTableView.reloadData()
        } failure: { (msg) in
            print(msg)
        } error: { (err) in
            print(err.localizedDescription)
        }
    }
}
//MARK: - VOTING POP UP DELEGATE
extension VoteNowVC: VotingFinalPopupDelegate {
    func proceedAction(priceData: [VotingPriceModel]) {
        postVote(votes: priceData.first?.votes ?? 0 , coins: priceData.first?.coins ?? 0)
    }
    
    
}
extension VoteNowVC {
    //MARK: - POST VOTE
    func postVote(votes: Int,coins: Int) {
        
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "", presentingView: self.view)
        APIHandler.shared.postVote(contestantID: votingData.id ?? 0, votes: votes, coins: coins) { [weak self] (votingPrice) in
            guard let strongSelf = self else { return }
            print(votingPrice)
            strongSelf.votingResponse = votingPrice
            if votingPrice.code == 200 {
                APESuperHUD.removeHUD(animated: true, presentingView: strongSelf.view, completion: {
                    let alertController = DOAlertController(title: "Success!!!", message: strongSelf.votingResponse.message, preferredStyle: .alert)
                        alertController.alertViewBgColor = UIColor.white
                        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                        alertController.messageView.textAlignment = .left
                        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                        alertController.titleTextColor = UIColor.red
                        let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                            
                            strongSelf.dismiss(animated: true, completion: nil)
                            strongSelf.navigationController?.popViewController(animated: true)
                        })
                        // Add the action.
                        alertController.addAction(okAction)
                        // Show alert
                        strongSelf.present(alertController, animated: true, completion: nil)
                        
                        
                    
                    
                } )
            }
          
        } failure: { (msg) in
            print(msg)
        } error: { (err) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: err.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                
                
            } )
        }
    }
}
