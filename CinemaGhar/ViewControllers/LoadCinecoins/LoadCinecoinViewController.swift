//
//  LoadCinecoinViewController.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 4/15/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import Foundation

class LoadCinecoinViewController: UIViewController, UINavigationBarDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var cinecoinPickerView: UIView!
    @IBOutlet weak var amountPickerTextField: UITextField!
    @IBOutlet weak var accountNumberView: UIView!
    @IBOutlet weak var accountNumberTextField: UITextField!
  
    
    
    
    //Picker Amounts
    let pickerAmountData = [
        "100",
        "200",
        "500",
        "1000"
    ]
    var selectedAmount: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //UI Setups
        setupNavigationBar()
        setupCinecoinPickerView()
        setupAccountNumberView()
    }
    
    func setupNavigationBar(){
            self.navigationBar.backgroundColor = UIColor.clear
            self.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationBar.isTranslucent = true
            
            let button = UIButton(type: .system)
            button.setImage(UIImage(named: "back_arrow"), for: .normal)
    //        button.setTitle("Back", for: .normal)
    //        button.setTitleColor(UIColor.white, for: .normal)
            button.tintColor = UIColor.white
            button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
            button.sizeToFit()
            button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
            button.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)
            self.navItem.leftBarButtonItem = UIBarButtonItem(customView: button)
            self.navigationBar.delegate = self
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }
    
    func setupCinecoinPickerView(){
        
        //Rounded corners
        cinecoinPickerView.layer.cornerRadius = 10;
        cinecoinPickerView.layer.masksToBounds = true;
        
        //Add amount picker view
        let cinecoinAmountPicker = UIPickerView()
        cinecoinAmountPicker.delegate = self
        amountPickerTextField.inputView = cinecoinAmountPicker
        
    }
    
    func setupAccountNumberView(){
        
        //Rounded corners
        accountNumberView.layer.cornerRadius = 10;
        accountNumberView.layer.masksToBounds = true;
        
    }
    
   
    
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
}

//Extension for the amount picker
extension LoadCinecoinViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerAmountData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerAmountData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedAmount = pickerAmountData[row]
        amountPickerTextField.text = selectedAmount
    }
    
}
