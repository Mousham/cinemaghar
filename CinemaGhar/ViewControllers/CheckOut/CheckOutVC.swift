//
//  CheckOutViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 5/31/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import SwiftyJSON




class CheckOutVC: DesignableViewController {
    @IBAction func paytmBtnPressed(_ sender: Any) {
        
    }
    var promoCodeTextField = UITextField()
    var alertControllerDOA = DOAlertController()

    @IBOutlet weak var promoCodeBtn: UIButton!
    @IBOutlet weak var visualFXView: UIVisualEffectView!
    
    @IBOutlet weak var priceAfterDiscountLabel: UILabel!
    var spinner = UIActivityIndicatorView()
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBAction func promoCodeBtnPressed(_ sender: Any) {
        self.alertControllerDOA = DOAlertController(title: "Redeem Code", message: "Enter your promocode below." , preferredStyle: .alert)
        alertControllerDOA.titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        alertControllerDOA.messageView.font = UIFont.systemFont(ofSize: 13, weight: .medium)
        alertControllerDOA.addTextFieldWithConfigurationHandler { textField in
            if let promoCodeTextField = textField{
                self.promoCodeTextField  = promoCodeTextField
                promoCodeTextField.placeholder = "Promocode"
                promoCodeTextField.textColor = UIColor.black
                promoCodeTextField.font = UIFont.systemFont(ofSize: 13)
                promoCodeTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: promoCodeTextField.frame.height))
                promoCodeTextField.leftViewMode = .always
                promoCodeTextField.keyboardType = .default
                promoCodeTextField.keyboardAppearance = .dark
                
            }
        }
        alertControllerDOA.alertViewBgColor = UIColor.white
        alertControllerDOA.titleFont = UIFont.systemFont(ofSize: 21, weight: .medium)
        alertControllerDOA.messageView.textAlignment = .left
        alertControllerDOA.messageFont = UIFont.systemFont(ofSize: 13, weight: .light)
        alertControllerDOA.titleTextColor = Util.hexStringToUIColor(hex: "9B3567")
        let okAction = DOAlertAction(title: "Redeem", style: .default, handler: { action in
            if self.promoCodeTextField.text != "" {
//                self.visualFXView.isHidden = true
                self.spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 30, height:30))
                self.spinner.color = UIColor.lightGray
                self.promoCodeTextField.leftView = self.spinner
                self.spinner.hidesWhenStopped = true
                self.spinner.startAnimating()
                self.alertControllerDOA.messageView.text = "Checking your Promocode..."
                self.checkPromoCode(promoCode: self.promoCodeTextField.text!)
 
            }
            else {
                self.alertControllerDOA.messageView.textColor = UIColor.red
                self.alertControllerDOA.messageView.text = "Please enter your promocode below."
                self.alertControllerDOA.messageView.shake()
                self.promoCodeTextField.shake()
                self.promoCodeTextField.becomeFirstResponder()
            }
            
        })
        
        let cancelAction = DOAlertAction(title: "Cancel", style: .cancel, handler: { action in
            self.dismiss(animated: true, completion: {
            self.visualFXView.isHidden = true
            })
        })
        // Add the action.
        alertControllerDOA.addAction(cancelAction)
        alertControllerDOA.addAction(okAction)
        visualFXView.isHidden = false
        self.present(alertControllerDOA, animated: true, completion: nil)
        
        
        }
    
    
    
    
//    var merchant:PGMerchantConfiguration!

    
    @IBAction func esewaBtnPressed(_ sender: Any) {
        

        
    }
    
    @IBAction func khaltiPayBtnPressed(_ sender: Any) {
      
    }
    
    var contentId:Int?
    var priceNRS: Int?
    
    var priceDollar : Int?
    var paidVideoURL = ""
    var videoTite = ""
    var alertController: UIAlertController!
    var counter:Int = 5
    var timer: Timer?
    var type: MediaType = .movie
//    var sdk: EsewaSDK!
    var delegate: CastPlayerDelegate?

    
    
    var episodeId: Int = 100000000
    
    
   
    
    
    
    
    @IBAction func paypalPayBtnPressed(_ sender: Any) {
     
    }
  

    @IBAction func playMovieBtnPressed(_ sender: Any) {

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var rect = CGRect(x:10, y:33, width:30, height:30)
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
            rect = CGRect(x:10, y:50, width:30, height:30)
        }
        promoCodeBtn.layer.cornerRadius = promoCodeBtn.frame.height/2
        promoCodeBtn.layer.borderWidth = 0.5
        promoCodeBtn.layer.borderColor = UIColor.lightGray.cgColor
        
        let backButton = UIButton() // back button
        backButton.backgroundColor = UIColor.white
        backButton.layer.cornerRadius = 15
        backButton.addShadow(offset: CGSize(width: 1, height: 1), color: .darkGray, radius: 2, opacity: 0.9)
        backButton.frame = rect
        backButton.setImage(UIImage(named: "icon_navBar_Back"), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(22,22,22,23)
        backButton.imageView?.tintColor = UIColor.darkGray
        
        self.priceLabel.text = "NPR. \(self.priceNRS!/100)/-"
        
        self.view.addSubview(backButton)
        
        backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        
        APESuperHUD.appearance.backgroundBlurEffect = .dark
        
        APESuperHUD.appearance.titleFontSize = 20
        APESuperHUD.appearance.iconWidth = 48
        APESuperHUD.appearance.loadingActivityIndicatorColor = UIColor.darkText
        APESuperHUD.appearance.iconHeight = 48
        APESuperHUD.appearance.messageFontSize = 16
        APESuperHUD.appearance.textColor = UIColor.darkText
        APESuperHUD.appearance.messageFontName  = "Avenir-Medium"
        APESuperHUD.appearance.foregroundColor = .white
//        sdk = EsewaSDK(inViewController: self, environment: .development, delegate: self)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func dismissVC (){
        if type == .movie {
//            self.navigationController?.backToViewController(viewController: MovieDetailViewController.self)
            self.navigationController?.backToViewController(viewController: NewMovieDetailsVC.self)

        }
        
        else if type == .series {
            self.navigationController?.backToViewController(viewController: SeriesDetailViewController.self)

        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
        
        
    
    
    }
    
    
    func checkPromoCode(promoCode:String) {
        
        
        APIHandler.shared.checkPromoCode(promoCode: promoCode, success: {(discountPercent) in
            self.alertControllerDOA.dismiss(animated: true, completion: nil)
            self.visualFXView.isHidden = true
            self.priceNRS = self.priceNRS! - (self.priceNRS!*discountPercent/100)
            self.priceLabel.text = "NPR. \(self.priceNRS!/100)/-"
            self.promoCodeBtn.isEnabled = false
            self.promoCodeBtn.alpha = 0.3
            self.priceAfterDiscountLabel.text = "(With \(discountPercent)%) Promocode discount)"
            
        }, failure: {(message) in
            
            self.alertControllerDOA.messageView.text = message
            
            self.spinner.stopAnimating()
            self.promoCodeTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.promoCodeTextField.frame.height))
            
            self.alertControllerDOA.messageView.textColor = UIColor.red
            self.spinner.stopAnimating()

            
            
        }, error: {(error) in
            
            
            
            
            
        })
        
    }
    
    
    
    
 
    

    
    
   
    func validatePayment(paymentMethod:paymentGateway, token : String){
        APESuperHUD.showOrUpdateHUD( loadingIndicator: .standard, message: "Verifying Payment...", presentingView: self.view)
        
        APIHandler.shared.validatePayment(paymentGateway: paymentMethod , mediaType:self.type , contentId: Int(self.contentId!), episodeID: episodeId, paymentSuccessToken: token, transactionID: "", status: {(status, videoURL, message) in
            if status {
                APESuperHUD.showOrUpdateHUD(icon: .checkMark, message: "Success!", presentingView: self.view, completion: {
                    if self.type == .movie {
                        self.navigationController?.backToViewController(viewController: MovieDetailViewController.self)
                    }
                        
                    else if self.type == .series {
                        self.navigationController?.backToViewController(viewController: SeriesDetailViewController.self)
                    }
                    
                    if let delegate = self.delegate {
                        delegate.castMedia(url: videoURL, hasUserRated: false)
                    }
                    
                })
                
                
            }
            else {
                APESuperHUD.removeHUD(animated: true, presentingView: self.view)
                let controller = UIAlertController(title: "Whoops!", message: "An Error Occured While Verifying Your Payment. Please try again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .default, handler: { (action) -> Void in
                   self.validatePayment(paymentMethod: paymentMethod, token: token)
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
            
            
            
        }, failure: {(error) in
            
            APESuperHUD.removeHUD(animated: true, presentingView: self.view)
            
            if(error._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                    self.validatePayment(paymentMethod: paymentMethod, token: token)
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
            }
                
            else if (error._code == NSURLErrorNetworkConnectionLost){
                self.validatePayment(paymentMethod: paymentMethod, token: token)
            }

        })
    }
    
    
   

}


