//
//  NotificationsViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 11/23/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NotificationsViewController: UIViewController, UINavigationBarDelegate, UIGestureRecognizerDelegate {
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    var notificationsArray = [Notifications]()

    @IBOutlet weak var notificationsTableVIew: UITableView!
    
    @IBOutlet weak var navItem: UINavigationItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back_arrow"), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        button.sizeToFit()
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)
        self.navItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        self.navBar.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self

        notificationsTableVIew.tableFooterView = UIView()
        
        notificationsTableVIew.rowHeight = UITableViewAutomaticDimension
        notificationsTableVIew.estimatedRowHeight = UITableViewAutomaticDimension
        
        
       
        
        
        getNotifications()
        
        
        
    }
    
    
    
    func getNotifications(){
        
        indicatorView.startAnimating()
        
        let headers = APIHandler.shared.createHeader()
        
        AF.request(CinemaGharAPI.notificationsURL, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    //                    print(json)
                    if let notificationsJSONArray = json["data"].array{
                            for i in 0..<notificationsJSONArray.count{
                                
                                let notification = Notifications(fromJson: notificationsJSONArray[i])
                                self.notificationsArray.append(notification)
                            }
                        self.notificationsTableVIew.delegate = self
                        self.notificationsTableVIew.dataSource = self
                            self.indicatorView.stopAnimating()
                            self.notificationsTableVIew.reloadData()
                        
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        
                    }
                }
                break
                
            case .failure(let failure):
                
                self.indicatorView.stopAnimating()
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                if failure._code == NSURLErrorNotConnectedToInternet {
                    
                    let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                    // add the actions (buttons)
                    
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                        
                        self.getNotifications()
                    }))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    
                }
                    
                else if(failure._code == NSURLErrorTimedOut){
                    let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                    let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                    let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                        self.getNotifications()
                    })
                    controller.addAction(cancel)
                    controller.addAction(retry)
                    self.present(controller, animated: true, completion: nil)
                    
                    
                }
                    
                else if (failure._code == NSURLErrorNetworkConnectionLost){
                    
                    self.getNotifications()
                    
                }
                
                
                break
            }
            
        }
        
        
        
        
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    
    
    
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }

    
    
    
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension NotificationsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsTableViewCell", for: indexPath) as! NotificationsTableViewCell
        
        cell.notificationTitleLabel.text = self.notificationsArray[indexPath.row].title
        cell.notificationMessageLabel.text = self.notificationsArray[indexPath.row].body
        
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: Int = 0
        
        if self.notificationsArray.count>0{
            
            tableView.backgroundView = nil
            numOfSection = 1
        }
            
        else {
            
            
          
                let noDataLabel: UILabel = UILabel(frame: CGRect(x:0, y:0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text = "No new notifications at the moment."
                noDataLabel.textColor = UIColor.darkGray
                noDataLabel.numberOfLines = 0
                noDataLabel.font = UIFont.systemFont(ofSize: 17)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            
            
          
            
        }
        
        
        return numOfSection
        
    }
    
    
    
    
    
}
