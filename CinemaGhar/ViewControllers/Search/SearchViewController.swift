    //
    //  SearchViewController.swift
    //  CinemaGhar
    //
    //  Created by Sunil on 7/6/18.
    //  Copyright © 2018 sunBi. All rights reserved.
    //

    import UIKit

    class SearchViewController: UIViewController, UIGestureRecognizerDelegate, UINavigationBarDelegate, UIScrollViewDelegate {
        @IBOutlet weak var searchHintLabel: UILabel!
        @IBOutlet weak var moviesFilterCollectionView: UICollectionView!
        
        @IBOutlet weak var indicator: UIActivityIndicatorView!
        @IBOutlet weak var navItem: UINavigationItem!
        let searchController = UISearchController(searchResultsController: nil)
        
        var movies = [Movie]()
        var series = [Series]()
        
        var shouldTriggerKeyboard = true

        var filteredMovies   = [Movie]()
        
        var mediaTypeToSearch: MediaType = .movie {
            
            didSet {
                
                if mediaTypeToSearch == .series {
                    moviesFilterCollectionView.register(UINib(nibName: "AllSeriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AllSeriesCollectionViewCell")

                }
                else {
                    let nibName = UINib(nibName: "MovieCell", bundle:nil)
                    moviesFilterCollectionView.register(nibName, forCellWithReuseIdentifier: "MovieGenreCell")
                    
                }
            }
        }
        
        private var previousRun = Date()
        private let minInterval = 0.05
        
        private let spacing:CGFloat = 10



        @IBOutlet weak var navBar: UINavigationBar!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            // searchcontroller customization //
          
            customizeSearchBar()
            mediaTypeToSearch = .movie
           
            
            let layout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
      
            self.moviesFilterCollectionView.collectionViewLayout = layout
            
            
            
            self.moviesFilterCollectionView.delegate = self
            self.moviesFilterCollectionView.dataSource = self
            
            
            
            
           
        }
        
       
        
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            self.view.endEditing(true)
        }
        
        
        func customizeSearchBar(){
            searchController.searchResultsUpdater = self
            definesPresentationContext = true
            searchController.searchBar.placeholder = "Search for Movies or Web Series"
            searchController.searchBar.barStyle  = .default
            searchController.searchBar.isTranslucent = true
            searchController.searchBar.delegate = self
            navBar.clipsToBounds = false
            navBar.delegate = self
            self.searchController.delegate = self
            self.searchController.hidesNavigationBarDuringPresentation = false
            self.searchController.dimsBackgroundDuringPresentation = true
            self.searchController.obscuresBackgroundDuringPresentation = false
            searchController.searchBar.searchBarStyle = .minimal
            searchController.searchBar.returnKeyType = .done
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white]
            
            UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes([NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.white], for: .normal)
            let textFieldInsideSearchBar = searchController.searchBar.value(forKey: "searchField") as? UITextField
            let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
            textFieldInsideSearchBarLabel?.font = UIFont.systemFont(ofSize: 14)
            textFieldInsideSearchBarLabel?.textColor = UIColor.white
            
            let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
            glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
            glassIconView?.tintColor = UIColor.white
            
            let clearButton = textFieldInsideSearchBar?.value(forKey: "clearButton") as! UIButton
            clearButton.addTarget(self, action: #selector(clearViews), for: .touchUpInside)
            clearButton.setImage(UIImage(named: "icon_clear"), for: .normal)
            clearButton.tintColor = .white
            indicator.stopAnimating()
            
            
            
            self.navItem.titleView = searchController.searchBar
            
            
        }
        
        
        @objc func clearViews(){
            
            searchController.searchBar.text = ""
            searchHintLabel.text = "Search for Movies and Web Series."
            searchHintLabel.isHidden = false
            self.movies.removeAll()
            self.moviesFilterCollectionView.reloadData()
            
        }

        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        @IBAction func segmentChanged(_ sender: UISegmentedControl) {
            self.moviesFilterCollectionView.reloadData()

            if sender.selectedSegmentIndex == 0{
                self.mediaTypeToSearch = .movie
                if !searchBarIsEmpty() {
                    fetchResults(for: searchController.searchBar.text!)
                }
                
            }
            else {
                self.mediaTypeToSearch = .series

                if !searchBarIsEmpty() {
                    fetchResults(for: searchController.searchBar.text!)
                }
            }
            
            
            
        }
        override func viewDidAppear(_ animated: Bool) {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            searchController.isActive = true
            DispatchQueue.main.async{
                if self.shouldTriggerKeyboard{
                self.searchController.searchBar.becomeFirstResponder()
                }
            }
        }
        
        
       
        
        func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
            return true
        }
        
        
        func position(for bar: UIBarPositioning) -> UIBarPosition {
            return .topAttached
        }
        
        
        
        
        func searchBarIsEmpty() -> Bool {
            return searchController.searchBar.text?.isEmpty ?? true
        }
        
        func isFiltering() -> Bool {
            let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
            return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
        }
        
        func presentSearchController(_ searchController: UISearchController) {
            if self.shouldTriggerKeyboard{

            searchController.searchBar.becomeFirstResponder()
                
            }

        }

        
        
        
        
        

    }
    extension SearchViewController: UISearchBarDelegate {
        // MARK: - UISearchBar Delegate
        func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    //        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
            print("changed")
           
    //        fetchResults(for: searchBar.text)
            
           
            
            
        }
        
        
        func fetchResults(for text: String){
            self.searchHintLabel.isHidden = true
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            APIHandler.shared.search(searchText: text, mediaType: self.mediaTypeToSearch, success: {(movies, series) in
                self.indicator.stopAnimating()
                
             
                if self.mediaTypeToSearch == .movie{

                if movies.count > 0  {
                    self.movies = movies
                    self.searchHintLabel.isHidden = true
                    DispatchQueue.main.async(execute: self.moviesFilterCollectionView.reloadData)

                    
                    
                }
                
                else {
                    
                    self.movies.removeAll()
                    DispatchQueue.main.async(execute: self.moviesFilterCollectionView.reloadData)
                    self.searchHintLabel.isHidden = false
                    self.searchHintLabel.text = "No Search Results for " + "\"" + text + "\""

                }
                    
                
                }
                else {
                    
                    if series.count > 0  {
                        self.series = series
                        self.searchHintLabel.isHidden = true
                        DispatchQueue.main.async(execute: self.moviesFilterCollectionView.reloadData)
                        
                        
                        
                    }
                        
                    else {
                        
                        self.series.removeAll()
                        DispatchQueue.main.async(execute: self.moviesFilterCollectionView.reloadData)
                        self.searchHintLabel.isHidden = false
                        self.searchHintLabel.text = "No Search Results for " + "\"" + text + "\""
                        
                    }
                    
                }
                
                
            
                    
                    
       
                
            }, failure: {
                (error) in
                
                
                
                
                
            })
            
            
        }
        
        
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            print(searchText)
            
            if searchText.length < 3 {
                searchHintLabel.text = "Search for Movies and Web Series."
                searchHintLabel.isHidden = false
                self.movies.removeAll()
                self.series.removeAll()
                self.moviesFilterCollectionView.reloadData()
                
                indicator.stopAnimating()
            } else {
                
                indicator.startAnimating()
                searchHintLabel.isHidden = true
                
                fetchResults(for: searchText)
                
            }
            
            
        }
    }

    extension SearchViewController: UISearchResultsUpdating {
        // MARK: - UISearchResultsUpdating Delegate
        func updateSearchResults(for searchController: UISearchController) {
            let searchBar = searchController.searchBar
    //        filterContentForSearchText(searchController.searchBar.text!, scope: scope)
            
            
            
        }
    }

    extension SearchViewController: UISearchControllerDelegate{
        
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            self.navigationController?.popViewController(animated: true)
        }
        
        
    }



    extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
        
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            if self.mediaTypeToSearch == .movie{
                return self.movies.count
            }
            else {
                
                return self.series.count
            }
            
        }
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            if mediaTypeToSearch == .series {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllSeriesCollectionViewCell", for: indexPath) as! AllSeriesCollectionViewCell
                cell.setWebSeriesInfo(webSeries: self.series[indexPath.row], labelColor: UIColor.white)
                return cell
                
            }
                
            else {
                
                let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieGenreCell", for: indexPath) as! MovieGenreCell
                cell.setMovieInfos(movie: self.movies[indexPath.row])
                
                return cell
                
            }
            
            
            
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return CGFloat(10)
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            
            let numberOfItemsPerRow:CGFloat = 3
            let spacingBetweenCells:CGFloat = 10
            
            let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
            
            
            let width = (collectionView.bounds.width - totalSpacing)/numberOfItemsPerRow
            
            
            
            
            
            
            return CGSize(width: width, height: width*1.6)
            

            
            
            
            
            
        }
        
        
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            self.shouldTriggerKeyboard = false
            self.view.endEditing(true)
            
            if self.mediaTypeToSearch == .movie {
                let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
                
                movieDetailScene.movieDetail = movies[indexPath.row]
                self.navigationController?.pushViewController(movieDetailScene, animated: true)

                
            }
            else {
                
                let seriesDetailVC = SeriesDetailViewController.instantiate(fromAppStoryboard: .SeriesDetail)
                seriesDetailVC.series = self.series[indexPath.row]
                self.navigationController?.pushViewController(seriesDetailVC, animated: true)
                
                
                
            }
            
        }
        
        
        
        
        
        
    }
