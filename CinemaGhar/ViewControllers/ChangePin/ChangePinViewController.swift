//
//  ChangePinViewController.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 4/11/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import Foundation

class ChangePinViewController: UIViewController {
    
    
    @IBOutlet weak var oldPin1TextField: UITextField!
    @IBOutlet weak var oldPin2TextField: UITextField!
    @IBOutlet weak var oldPin3TextField: UITextField!
    @IBOutlet weak var oldPin4TextField: UITextField!
    
    @IBOutlet weak var newPin1TextField: UITextField!
    @IBOutlet weak var newPin2TextField: UITextField!
    @IBOutlet weak var newPin3TextField: UITextField!
    @IBOutlet weak var newPin4TextField: UITextField!
    
    @IBOutlet weak var confirmPin1TextField: UITextField!
    @IBOutlet weak var confirmPin2TextField: UITextField!
    @IBOutlet weak var confirmPin3TextField: UITextField!
    @IBOutlet weak var confirmPin4TextField: UITextField!
    
    @IBOutlet weak var codeTextField: UITextField!
    
    var oldPin: String!
    var newPin: String!
    var confirmPin: String!
    
    @IBAction func changePinAction(_ sender: Any) {
        if(oldPin == nil){
            showAlert(title: "Please fill all fields.", msg: "Old PIN is not valid!")
        }else if(newPin == nil){
            showAlert(title: "Please fill all fields.", msg: "New PIN is not valid!")
        }else if(confirmPin == nil){
            showAlert(title: "Please fill all fields.", msg: "Confirm PIN is not valid!")
        }else if(confirmPin != newPin){
            showAlert(title: "Mismatch.", msg: "New PIN and Confirm PIN do not match!")
        }else if(oldPin.length == 4 && newPin.length == 4 &&  confirmPin.length == 4){
            changePassword()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var rect = CGRect(x:10, y:33, width:30, height:30)
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
            rect = CGRect(x:10, y:50, width:30, height:30)
        }
        
        let backButton = UIButton() // back button
        backButton.backgroundColor = UIColor.white
        backButton.layer.cornerRadius = 15
        backButton.addShadow(offset: CGSize(width: 1, height: 1), color: .darkGray, radius: 2, opacity: 0.9)
        backButton.frame = rect
        backButton.setImage(UIImage(named: "icon_navBar_Back"), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(22,22,22,23)
        backButton.imageView?.tintColor = UIColor.darkGray
        self.view.addSubview(backButton)
        backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        
        codeTextField.isHidden = true
        codeTextField.delegate = self
        codeTextField.becomeFirstResponder()
        
        self.oldPin1TextField.backgroundColor = UIColor.lightGray
    }
    
    @objc func dismissVC (){
        self.navigationController?.popViewController(animated: true)

    }
    
    func changePassword(){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait...", presentingView: self.view)
        
        
        APIHandler.shared.changePassword(oldPassword: oldPin ?? "", newPassword: newPin ?? "", newPasswordConfirmation: confirmPin ?? "", success: { (status, message) in
            if status {
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                    let alertController = DOAlertController(title: "Success", message: message , preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        
                        self.dismiss(animated: true, completion: nil)
                        self.dismissVC()
                        
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                })

                
            }
            else {
            
                
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                        let alertController = DOAlertController(title: "Whoops!", message: message , preferredStyle: .alert)
                        alertController.alertViewBgColor = UIColor.white
                        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                        alertController.messageView.textAlignment = .left
                        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                        alertController.titleTextColor = UIColor.red
                        let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                            self.dismiss(animated: true, completion: nil)
                        })
                        // Add the action.
                        alertController.addAction(okAction)
                        // Show alert
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                    
                    
                } )
                
            }
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                
                
            } )
        }
        
    }
}


extension ChangePinViewController: UITextFieldDelegate {
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let code = textField.text! + string
        switch code.count{
        
        case 1:
            oldPin1TextField.text = string
            oldPin1TextField.backgroundColor = UIColor.lightGray
            oldPin2TextField.backgroundColor = UIColor.init(white: 1, alpha: 0.7)
        case 2:
            oldPin2TextField.text = string
            oldPin2TextField.backgroundColor = UIColor.lightGray
            oldPin3TextField.backgroundColor = UIColor.init(white: 1, alpha: 0.7)
        case 3:
            oldPin3TextField.text = string
            oldPin3TextField.backgroundColor = UIColor.lightGray
            oldPin4TextField.backgroundColor = UIColor.init(white: 1, alpha: 0.7)
            oldPin = nil
        case 4:
            oldPin4TextField.text = string
            oldPin4TextField.backgroundColor = UIColor.lightGray
            newPin1TextField.backgroundColor = UIColor.init(white: 1, alpha: 0.7)
            oldPin = "\(oldPin1TextField.text! + oldPin2TextField.text! + oldPin3TextField.text! + oldPin4TextField.text!)"
        case 5:
            newPin1TextField.text = string
            newPin1TextField.backgroundColor = UIColor.lightGray
            newPin2TextField.backgroundColor = UIColor.init(white: 1, alpha: 0.7)
        case 6:
            newPin2TextField.text = string
            newPin2TextField.backgroundColor = UIColor.lightGray
            newPin3TextField.backgroundColor = UIColor.init(white: 1, alpha: 0.7)
        case 7:
            newPin3TextField.text = string
            newPin3TextField.backgroundColor = UIColor.lightGray
            newPin4TextField.backgroundColor = UIColor.init(white: 1, alpha: 0.7)
            newPin = nil
        case 8:
            newPin4TextField.text = string
            newPin4TextField.backgroundColor = UIColor.lightGray
            confirmPin1TextField.backgroundColor = UIColor.init(white: 1, alpha: 0.7)
            newPin = "\(newPin1TextField.text! + newPin2TextField.text! + newPin3TextField.text! + newPin4TextField.text!)"
        case 9:
            confirmPin1TextField.text = string
            confirmPin1TextField.backgroundColor = UIColor.lightGray
            confirmPin2TextField.backgroundColor = UIColor.init(white: 1, alpha: 0.7)
        case 10:
            confirmPin2TextField.text = string
            confirmPin2TextField.backgroundColor = UIColor.lightGray
            confirmPin3TextField.backgroundColor = UIColor.init(white: 1, alpha: 0.7)
        case 11:
            confirmPin3TextField.text = string
            confirmPin3TextField.backgroundColor = UIColor.lightGray
            confirmPin4TextField.backgroundColor = UIColor.init(white: 1, alpha: 0.7)
            confirmPin = nil
        case 12:
            confirmPin4TextField.text = string
            confirmPin4TextField.backgroundColor = UIColor.lightGray
            confirmPin = "\(confirmPin1TextField.text! + confirmPin2TextField.text! + confirmPin3TextField.text! + confirmPin4TextField.text!)"
        //submit your code here
        default: break
        }
        return textField.text!.count + (string.count - range.length) <= 12;

    }
    
    
    func showAlert(title: String!, msg: String!){
        let alertController = DOAlertController(title: title, message: msg , preferredStyle: .alert)
        alertController.alertViewBgColor = UIColor.white
        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
        alertController.messageView.textAlignment = .left
        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
        alertController.titleTextColor = UIColor.red
        
        let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
            
            self.dismiss(animated: true, completion: nil)
            
        })
        // Add the action.
        alertController.addAction(okAction)
        // Show alert
        self.present(alertController, animated: true, completion: nil)
    }
    
}
