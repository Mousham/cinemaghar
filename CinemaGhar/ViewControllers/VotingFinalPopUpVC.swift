//
//  VotingFinalPopUpVC.swift
//  CinemaGhar
//
//  Created by Midas on 09/06/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol VotingFinalPopupDelegate: class {
    func proceedAction(priceData: [VotingPriceModel])
}
class VotingFinalPopUpVC: UIViewController {
    weak var delegate: VotingFinalPopupDelegate?
    @IBOutlet weak var cancelbtn: AnimatedButton!
    @IBOutlet weak var proceedbtn: AnimatedButton!
    @IBOutlet weak var cineCoinLbl: UILabel!
    @IBOutlet weak var votesLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var eventLbl: UILabel!
    @IBOutlet weak var votingDetailsBackView: UIView!
    @IBOutlet weak var votingtitleBackView: UIView!
    var votingPriceData = VotingPriceModel(json: JSON())
    var votingDetailsData = VotingDetailsModel(json: JSON())
    var votingPlatformData = VotingModel(json: JSON())
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetup()
        loadData()
    }
    

    static func instantiate() -> VotingFinalPopUpVC?{
        return UIStoryboard(name: "Voting", bundle: nil).instantiateViewController(withIdentifier: "VotingFinalPopUpVC") as? VotingFinalPopUpVC
    }
    func uisetup() {
        //votingDetailsBackView.addDropShadow(offset: CGSize(width: 0, height: 0), color: .black, radius: 3, opacity: 0.5)
        votingDetailsBackView.addShadow(view: votingDetailsBackView, width: 0, height: 0, shadowRadius: 3, shadowOpacity: 0.5)
        votingDetailsBackView.layer.masksToBounds = false
       // votingtitleBackView.addDropShadow(offset: CGSize(width: 0, height: 0), color: .black, radius: 3, opacity: 0.5)
        votingtitleBackView.addShadow(view: votingtitleBackView, width: 0, height: 0, shadowRadius: 3, shadowOpacity: 0.5)
        votingtitleBackView.layer.masksToBounds = false
        proceedbtn.backgroundColor = UIColor.init(hex: "479E3E")
        cancelbtn.backgroundColor = UIColor.init(hex: "E83323")
        
    }
    func loadData() {
        eventLbl.text = "Event: " + (votingPlatformData.title ?? "")
        nameLbl.text = "Name: " + (votingDetailsData.title ?? "")
        votesLbl.text = "Votes: " + "\(votingPriceData.votes ?? 0)"
        cineCoinLbl.text = "Cine Coin: " + "\(votingPriceData.coins ?? 0)"
        
    }
    @IBAction func proceedTap(_ sender: Any) {
        delegate?.proceedAction(priceData: [votingPriceData])
    }
    
    @IBAction func cancelTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
