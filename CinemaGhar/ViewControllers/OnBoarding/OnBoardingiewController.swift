//
//  OnBoardingViewController.swift
//  KBC Nepal
//
//  Created by Sunil Gurung on 3/4/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import StoreKit


class OnBoardingViewController: DesignableViewController {
    
    @IBOutlet weak var swiftyOnboard: SwiftyOnboard!
    override func viewDidLoad() {
        
        swiftyOnboard.style = .dark
        swiftyOnboard.fadePages = false
    
        
        swiftyOnboard.delegate = self
        swiftyOnboard.dataSource = self
        swiftyOnboard.backgroundColor = UIColor.white
        
    }
   
    
    
    @objc func handleSkip() {
//        swiftyOnboard?.goToPage(index: 2, animated: true)
        SessionManager().setOnBoardingFinished(status: true)
        
//        let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
//        loginVC.shouldAllowSkip = true
//
//        self.navigationController?.pushViewController(loginVC, animated: true)
        
        //let homeVC = CineGoldViewController.instantiate(fromAppStoryboard: .CineGold)
        let homeVC = HomeVC.instantiate(fromAppStoryboard: .CineGold)
    
                self.navigationController?.pushViewController(homeVC, animated: true)

        
    }
    
    @objc func handleContinue(sender: UIButton) {        
        let index = sender.tag
        if index != 2 {
        swiftyOnboard?.goToPage(index: index + 1, animated: true)
        }
        else {
            SessionManager().setOnBoardingFinished(status: true)            
//            let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
//            loginVC.shouldAllowSkip = true
            
            
//            let homeVC = CineGoldViewController.instantiate(fromAppStoryboard: .CineGold)
            let homeVC = HomeVC.instantiate(fromAppStoryboard: .CineGold)
            
            
            self.navigationController?.popViewController(animated: false)
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
        
        
    }
    
    
    
}

extension OnBoardingViewController: SwiftyOnboardDelegate, SwiftyOnboardDataSource {
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
        return 3
    }

    
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        let view = CustomPage.instanceFromNib() as? CustomPage
        if index == 0 {
            //On the first page, change the text in the labels to say the following:
            view?.image.image = UIImage(named: "movies")

            view?.titleLabel.text = "MOVIES"
            view?.subTitleLabel.text = "Latest and exclusive Nepali movies on demand before anywhere else."
        } else if index == 1 {
            //On the second page, change the text in the labels to say the following:
            view?.image.image = UIImage(named: "masti")

            view?.titleLabel.text = "SERIES"
            view?.subTitleLabel.text = "High quality web series on demand to satisfy your binge."
        } else {
            //On the thrid page, change the text in the labels to say the following:
            view?.image.image = UIImage(named: "magic")

            view?.titleLabel.text = "INFOTAINMENT"
            view?.subTitleLabel.text = "News and videos to keep you updated about the latest and happening news of k-town."
        }
        return view
    }
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
        let overlay = CustomOverlay.instanceFromNib() as? CustomOverlay
        overlay?.skip.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
        overlay?.buttonContinue.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)
        return overlay
    }
    
    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
        let overlay = overlay as! CustomOverlay
        let currentPage = round(position)
        overlay.pageControl.currentPage = Int(currentPage)
        overlay.contentControl.currentPage = Int(currentPage)
        overlay.buttonContinue.tag = Int(position)
    
        
        if currentPage == 0.0 || currentPage == 1.0 {
            overlay.buttonContinue.setTitle("Next", for: .normal)
            overlay.skip.setTitle("Skip", for: .normal)
            overlay.skip.isHidden = false
            overlay.buttonContinue.setTitleColor(UIColor.clear, for: .normal)
            UIView.animate(withDuration: 0.4, animations: {
                overlay.layoutIfNeeded()
                overlay.buttonContinue.setTitleColor(UIColor.white, for: .normal)
                
            })
        } else {
            overlay.buttonContinue.setTitleColor(UIColor.clear, for: .normal)

            overlay.buttonContinue.setTitle("Let's Go!", for: .normal)
            UIView.animate(withDuration: 0.4, animations: {
                overlay.layoutIfNeeded()
                overlay.buttonContinue.setTitleColor(UIColor.white, for: .normal)

            })
            
            overlay.skip.isHidden = true
        }
    }
}


