//
//  AppstoreReviewVC.swift
//  CinemaGhar
//
//  Created by Midas on 23/08/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
protocol AppStoreDelegate: class {
    func okBtnTap(showappstore: Bool)
}

class AppstoreReviewVC: UIViewController {
    @IBOutlet weak var subTitleLbl: UILabel!
    weak var delegate: AppStoreDelegate?
    var showAppStore: Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if showAppStore == true {
            subTitleLbl.text = "You have been rewarded with cinecoins. Please consider supporting us by reviewing us at appstore."
        } else {
            subTitleLbl.text = "Thank you, You've rewarded with Cine Coins."
        }
    }
    static func instantiate() -> AppstoreReviewVC {
        return (UIStoryboard(name: "reward", bundle: nil).instantiateViewController(withIdentifier: "AppstoreReviewVC") as? AppstoreReviewVC)!
    }
    
    @IBAction func oktap(_ sender: Any) {
        if showAppStore == true {
            delegate?.okBtnTap(showappstore: true)
        } else {
            delegate?.okBtnTap(showappstore: false)
        }
        self.dismiss(animated: true, completion: nil)
    }
}
