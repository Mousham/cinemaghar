//
//  voting+storyboard.swift
//  CinemaGhar
//
//  Created by Midas on 12/03/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import Foundation

extension UIStoryboard{
private struct Constants {
    static let votingStoryboard = "Voting"
    static let VotingVCIdentifier = "VotingVC"
    static let AllContestantVCIdentifier = "AllContestantVC"
    static let VoteNowVCID = "VoteNowVC"
   
   
}
static var votingStoryboard: UIStoryboard {
    return UIStoryboard(name: Constants.votingStoryboard, bundle: nil)
}
func instantiateVotingVC() -> VotingVC {
    guard let viewController = UIStoryboard.votingStoryboard.instantiateViewController(withIdentifier: Constants.VotingVCIdentifier) as? VotingVC else {
        fatalError("Couldn't instantiate HomeVC")
    }
    return viewController
}
    func instantiateAllContestantVC() -> AllContestantVC {
        guard let viewController = UIStoryboard.votingStoryboard.instantiateViewController(withIdentifier: Constants.AllContestantVCIdentifier) as? AllContestantVC else {
            fatalError("Couldn't instantiate AllContestantVC")
        }
        return viewController
    }
    func instantiateVoteNowVC() -> VoteNowVC {
        guard let viewController = UIStoryboard.votingStoryboard.instantiateViewController(withIdentifier: Constants.VoteNowVCID) as? VoteNowVC else {
            fatalError("Couldn't instantiate VoteNowVC")
        }
        return viewController
    }
}
