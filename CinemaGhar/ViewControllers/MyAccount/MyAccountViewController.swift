//
//  MyAccountViewController.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 4/15/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import Foundation
import PassKit
import GoogleMobileAds
import EzPopup


class MyAccountViewController: UIViewController, UINavigationBarDelegate, UIGestureRecognizerDelegate {
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var totalCineCoinView: UIView!
    @IBOutlet weak var loadYourWalletView: UIView!
    @IBOutlet weak var loadOtherWalletView: UIView!
    @IBOutlet weak var viewFinancialStatementView: UIView!
    
    //Label outlets
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var cinemagharIDLabel: UILabel!
    @IBOutlet weak var walletAmountLabel: UILabel!
    @IBOutlet weak var editProfileNameButton: UIImageView!
    
    //ImageView Outlets
    @IBOutlet weak var profileImageView: UIImageView!
    
    var profile: Profile!
    private var rewardedAd: GADRewardedAd?
    
    //Load Wallet through Apple Pay
    var loadWalletAlertController: UIAlertController!
    //Picker View
    var amountPickerTextField: UITextField!
    //Phone Number
    var phoneNumberTextField: UITextField!
    
    //Picker Amounts
    let pickerAmountData = [
        "100",
        "200",
        "300",
        "500",
        "1000"
    ]
    var selectedAmount: String?
    var isRewardEarned = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        // UI customization
        loadAd()
        setupNavigationBar()
        setupTotalCineCoinView()
        setupLoadWalletView()
        setupLoadOtherWalletView()
        setupFinancialStatementView()
        setupProfileImageView()
        setupUserNameLabel()
        
        //Get profile
        getProfile()
        
        //Notifications
        NotificationCenter.default.addObserver(self, selector: #selector(self.showLoadingProgress(isLoading:)), name: Notification.Name(rawValue: "IAPPaymentStatusLoading"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideLoadingProgress(notification:)), name: Notification.Name(rawValue: "IAPPaymentStatusNotLoading"), object: nil)
    }
    @IBAction func earnPointsTap(_ sender: Any) {
//        if rewardedAd?.isReady == true {
//               rewardedAd?.present(fromRootViewController: self, delegate: self)
//            }
    }
    
    //Loaders for payment
    @objc func showLoadingProgress(isLoading: Bool){
        DispatchQueue.main.async {
            APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
            APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Processing Payment...", presentingView: self.view)
        }
    }
    
    @objc func hideLoadingProgress(notification: NSNotification){
        print("sawsank: Hiding Progress")
//        DispatchQueue.main.async {
            APESuperHUD.removeHUD(animated: true, presentingView: self.view) {
                self.tempGetProfile()

                guard let userInfo = notification.userInfo else {
                    return
                }
                let alertController = DOAlertController(title: "Info", message: userInfo["message"] as? String, preferredStyle: .alert)
                alertController.alertViewBgColor = UIColor.white
                alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                alertController.messageView.textAlignment = .left
                alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                alertController.titleTextColor = UIColor.red
                let okAction = DOAlertAction(title: "OK", style: .default, handler: { action in
                    print("sawsank: Start get profile")
                    self.dismiss(animated: true, completion: nil)
                })
                // Add the action.
                alertController.addAction(okAction)
                // Show alert
                self.present(alertController, animated: true, completion: nil)
            }
            
//        }
    }
    
    //Temp
    func tempGetProfile(){
        getProfile()
    }
    
    //Get Profile <Start>
    func getProfile() {
        APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait...", presentingView: self.view)
        
        
        APIHandler.shared.getProfile(success: { (profile) in
            print("sawsank: Getting profile")
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                print("sawsank: Load Profile Removed HUD")
            })
            print(profile)
            self.profile = profile
            self.userNameLabel.text = profile.name
            self.walletAmountLabel.text = profile.walletBalance
            self.cinemagharIDLabel.text = "CinemagharID: " + profile.phoneNumber
            if(self.profile.image != ""){
                self.setProfileImageToImageView(url: profile.image)
            }

        }, failure: { (message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                   let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
                   alertController.alertViewBgColor = UIColor.white
                   alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                   alertController.messageView.textAlignment = .left
                   alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                   alertController.titleTextColor = UIColor.red
                   let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                       self.dismiss(animated: true, completion: nil)
                    self.dismissVC()
                   })
                   // Add the action.
                   alertController.addAction(okAction)
                   // Show alert
                   self.present(alertController, animated: true, completion: nil)
                   
                   
               
               
           } )
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                
                
            } )
        }
    }
    //Get Profile <End>
    
    
    //Network profile image access <Start>
    func fetchImage(from urlString: String, completionHandler: @escaping (_ data: Data?) -> ()) {
        let session = URLSession.shared
        let url = URL(string: urlString)
        let dataTask = session.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print("Error fetching the image! 😢")
                completionHandler(nil)
            } else {
                completionHandler(data)
            }
        }
            
        dataTask.resume()
    }
    
    func setProfileImageToImageView(url: String) {
        fetchImage(from: url) { (imageData) in
            if let data = imageData {
                // referenced imageView from main thread
                // as iOS SDK warns not to use images from
                // a background thread
                DispatchQueue.main.async {
                    self.profileImageView.image = UIImage(data: data)
                }
            } else {
                    // show as an alert if you want to
                print("Error loading image");
            }
        }
    }
    //Network profile image access <End>
    
    //UI Customization <Start>
    func setupNavigationBar(){
        
        //Transparent NavigationBar
        self.navigationBar.backgroundColor = UIColor.clear
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.isTranslucent = true
        
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back_arrow"), for: .normal)
//        button.setTitle("Back", for: .normal)
//        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        button.sizeToFit()
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)
        self.navItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationBar.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    func setupTotalCineCoinView(){
        
        //Rounded corners
        totalCineCoinView.layer.cornerRadius = 10;
        totalCineCoinView.layer.masksToBounds = true;
    }
    
    func setupLoadWalletView(){
        
        //Rounded corners
        loadYourWalletView.layer.cornerRadius = 10;
        loadYourWalletView.layer.masksToBounds = true;
        
        //Tap recognizer
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleLoadWalletTap(_:)))
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.earnPointTap(_:)))
        loadYourWalletView.addGestureRecognizer(tap)
        
        //Hide load wallet
//        loadYourWalletView.isHidden = true
    }
    
    func setupLoadOtherWalletView(){
        
        //Rounded corners
        loadOtherWalletView.layer.cornerRadius = 10;
        loadOtherWalletView.layer.masksToBounds = true;
        
        //Tap recognizer
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleLoadOthersWalletTap(_:)))
//        loadOtherWalletView.addGestureRecognizer(tap)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.showPopup(_:)))
        loadOtherWalletView.addGestureRecognizer(tap)
        
        //Hide load other wallet
//        loadOtherWalletView.isHidden = true
    }
    
    func setupFinancialStatementView(){
        
        //Rounded Corners
        viewFinancialStatementView.layer.cornerRadius = 10;
        viewFinancialStatementView.layer.masksToBounds = true;
        
        //Tap recognizer
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleFinancialStatementTap(_:)))
        viewFinancialStatementView.addGestureRecognizer(tap)
    }
    
    func setupProfileImageView(){
        
        //Circular image view
        profileImageView.layer.borderWidth = 2
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
        profileImageView.clipsToBounds = true
        
        profileImageView.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleProfileImageTap(_:)))
        profileImageView.addGestureRecognizer(tap)
    }
    
    func setupUserNameLabel(){
        editProfileNameButton.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleUserNameLabelTap(_:)))
        editProfileNameButton.addGestureRecognizer(tap)
    }
    //UI Customization <End>
    
    //Gesture Recognizers <Start>
    @objc func handleLoadWalletTap(_ sender: UITapGestureRecognizer? = nil) {
        
        if(profile.walletBalanceRaw! < 500000){
            loadWalletAction(others: false)
        }else{
            showToast(message: "You cannot load more than 5000 Cinecoins", font: UIFont.systemFont(ofSize: 16))
        }
        
    }
    
    //Gesture Recognizers <Start>
    @objc func handleLoadOthersWalletTap(_ sender: UITapGestureRecognizer? = nil) {
        
        loadWalletAction(others: true)
        
        
    }
    
    
    @objc func handleFinancialStatementTap(_ sender: UITapGestureRecognizer? = nil) {
        // Push financialStatement ViewController
        let financialStatementVC = FinancialStatementViewController.instantiate(fromAppStoryboard: .FinancialStatement)
        self.navigationController?.pushViewController(financialStatementVC, animated: true)
    }
    
    @objc func handleProfileImageTap(_ sender: UITapGestureRecognizer? = nil) {
        //Open choose image
        importImage()
        
    }
    
    @objc func handleUserNameLabelTap(_ sender:UITapGestureRecognizer? = nil){
        //Open alert with text view
        let alertController = DOAlertController(title: "Change your name", message: nil, preferredStyle: .alert)
        alertController.alertViewBgColor = UIColor.init(hex: "#252B38")
        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 17)
        alertController.messageView.textAlignment = .left
        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
        alertController.titleTextColor = UIColor.white
        
        alertController.addTextFieldWithConfigurationHandler { (userFirstnameTextField) in
            
            //Left padding
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            
            userFirstnameTextField?.placeholder = "Enter your Firstname"
            userFirstnameTextField?.leftView = paddingView
            userFirstnameTextField?.leftViewMode = .always
        }
        
        alertController.addTextFieldWithConfigurationHandler { (userLastnameTextField) in
            
            //Left padding
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            
            userLastnameTextField?.placeholder = "Enter your Lastname"
            userLastnameTextField?.leftView = paddingView
            userLastnameTextField?.leftViewMode = .always
        }
        
        let okAction = DOAlertAction(title: "Ok", style: .default, handler: { action in
            self.dismiss(animated: true) {
                let userFirstnameTextField = alertController.textFields![0] as! UITextField
                let userLastnameTextField = alertController.textFields![1] as! UITextField
                let name = "\(userFirstnameTextField.text!) \(userLastnameTextField.text!)"
                
                self.editProfileName(name: name)
                
            }
        })
        
        let cancelAction = DOAlertAction(title: "Cancel", style: .cancel, handler: { action in
            self.dismiss(animated: true, completion: nil)
        })
        // Add the action.
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        // Show alert
        self.present(alertController, animated: true, completion: nil)
    }
    //Gesture Recognizers <End>
    
    //MARK: - SHOW POP UP
        @objc func showPopup(_ sender: UITapGestureRecognizer? = nil) {
             let customAlertVC = RechargeCardPopUpVC.instantiate()
            customAlertVC.delegate = self
            
           
            
            let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: SCREEN.WIDTH - 60, popupHeight: 210)
            popupVC.cornerRadius = 8
           
           
           present(popupVC, animated: true, completion: nil)
        }
    
    
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func gotoWeb(){
        let url = URL(string: "https://cinema-ghar.com/home")!
        if  UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
}


extension MyAccountViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func importImage() {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = .photoLibrary
        
        image.allowsEditing = true
        self.present(image, animated: true){
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage{
            APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait...", presentingView: self.view)
            
            let name: String = self.profile.name
            
            editProfileImage(name: name, image: image)
            
        }else{
            //error
            let alertController = DOAlertController(title: "Whoops!", message: "Something went wrong! Please try again later.", preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func editProfileImage(name: String, image: UIImage){
        APIHandler.shared.editProfileImage(name: name, image: image, success: { (status) in
            if (status){
                self.profileImageView.image = image
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                    let alertController = DOAlertController(title: "Success!", message: "Your profile image is successfully uploaded.", preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                })
            }
        }, failure: { (message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
                alertController.alertViewBgColor = UIColor.white
                alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                alertController.messageView.textAlignment = .left
                alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                alertController.titleTextColor = UIColor.red
                let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                    self.dismiss(animated: true, completion: nil)
                })
                // Add the action.
                alertController.addAction(okAction)
                // Show alert
                self.present(alertController, animated: true, completion: nil)
            })
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
            })
        }
    }
    
    func editProfileName(name: String){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Updating your name...", presentingView: self.view)
        APIHandler.shared.editProfileName(name: name, success: { (status) in
            if (status){
                self.userNameLabel.text = name
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                    let alertController = DOAlertController(title: "Success!", message: "Your name has been successfully changed.", preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.green
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                })
            }
        }, failure: { (message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
                alertController.alertViewBgColor = UIColor.white
                alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                alertController.messageView.textAlignment = .left
                alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                alertController.titleTextColor = UIColor.red
                let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                    self.dismiss(animated: true, completion: nil)
                })
                // Add the action.
                alertController.addAction(okAction)
                // Show alert
                self.present(alertController, animated: true, completion: nil)
            })
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
            })
        }
    }
    
}


// MARK: Load wallet through Apple payment
extension MyAccountViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func loadWalletAction(others: Bool){
        let message = """
            You can use the cinecoin to subscribe to our Gold Membership.
            \n \n \n
        """
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
          
          let attributedMessageText = NSMutableAttributedString(
              string: message,
              attributes: [
                  NSAttributedString.Key.paragraphStyle: paragraphStyle,
                  NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0),
                NSAttributedString.Key.foregroundColor: UIColor.black
              ]
          )
        
        let attributedTitleText = NSMutableAttributedString(
            string: "Load Wallet",
            attributes: [
              NSAttributedString.Key.foregroundColor: UIColor.black
              
            ]
        )
        
        
        
        loadWalletAlertController = UIAlertController(title: "Load Wallet", message: message, preferredStyle: .alert)
        
        //Customize view
        loadWalletAlertController.view.tintColor = UIColor.init(hex:"#252B38")
        
        //Background color
        let subview :UIView = loadWalletAlertController.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.subviews.first!.backgroundColor = UIColor.init(hex: "#E59827")
        
        // set attributed message here
        loadWalletAlertController.setValue(attributedMessageText, forKey: "attributedMessage")
        loadWalletAlertController.setValue(attributedTitleText, forKey: "attributedTitle")

        
        
        loadWalletAlertController.addTextField { (coinPicker) in
            if(self.selectedAmount == nil){
                coinPicker.placeholder = "Choose an amount"
            }else{
                coinPicker.text = self.selectedAmount
            }
            coinPicker.placeholder = "Choose an amount"
            let cinecoinAmountPicker = UIPickerView()
            cinecoinAmountPicker.delegate = self
            self.amountPickerTextField = coinPicker
            coinPicker.inputView = cinecoinAmountPicker
        }
        
        if(others){
            loadWalletAlertController.addTextField { (phoneNumberTextField) in
                phoneNumberTextField.placeholder = "Enter the Cinemaghar ID"
                self.phoneNumberTextField = phoneNumberTextField
            }
        }
        
        

        //CineCoin Outlets
        var paymentButton: PKPaymentButton!
        paymentButton = PKPaymentButton.init(paymentButtonType: .buy, paymentButtonStyle: .black)
        paymentButton.translatesAutoresizingMaskIntoConstraints = false
        subview.addSubview(paymentButton)
        paymentButton.leftAnchor.constraint(equalTo: subview.leftAnchor, constant: 16).isActive = true
        paymentButton.rightAnchor.constraint(equalTo: subview.rightAnchor, constant: -16).isActive = true
        if(others){
            paymentButton.bottomAnchor.constraint(equalTo: subview.bottomAnchor, constant: -125).isActive = true
        }else{
            paymentButton.bottomAnchor.constraint(equalTo: subview.bottomAnchor, constant: -100).isActive = true
        }
        paymentButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        if(others){
            paymentButton.addTarget(self, action: #selector(checkOthersBalance(_:)), for: .touchUpInside)
        }else{
            globalisPurchasedFromApple = false
            paymentButton.addTarget(self, action: #selector(purchase(_:)), for: .touchUpInside)
        }
        
        
        loadWalletAlertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
        
        
        if(SessionManager().isLoggedIn()){
            self.present(loadWalletAlertController, animated: true)
        }else{
            let signupVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(signupVC, animated: true)
        }
    }
    
    @objc func purchase(_ sender: UITapGestureRecognizer) {
        let afterLoadBalance = (profile.walletBalanceRaw!) + (Int(selectedAmount!)!  * 100)
        
        if(afterLoadBalance <= 500000){
            startPayment()
        }else{
            loadWalletAlertController.dismiss(animated: true, completion: nil)
            showToast(message: "You cannot load more than 5000 Cinecoins.", font: UIFont.systemFont(ofSize: 12))
        }
    }
    
    func startPayment(){
       
            
            switch selectedAmount {
            case "100":
                IAPService.shared.purchase(product: IAPProduct.coin100)
                loadWalletAlertController.dismiss(animated: true)
            case "200":
                IAPService.shared.purchase(product: IAPProduct.coin200)
                loadWalletAlertController.dismiss(animated: true)
            case "300":
                IAPService.shared.purchase(product: IAPProduct.coin300)
                loadWalletAlertController.dismiss(animated: true)
            case "500":
                IAPService.shared.purchase(product: IAPProduct.coin500)
                loadWalletAlertController.dismiss(animated: true)
            case "1000":
                IAPService.shared.purchase(product: IAPProduct.coin1000)
                loadWalletAlertController.dismiss(animated: true)
            default:
                amountPickerTextField.shake()
                break
            }
        
    }
    
    @objc func checkOthersBalance(_ sender: UITapGestureRecognizer){
        let phone = self.phoneNumberTextField.text!
        let coins = Int(selectedAmount!)!
        UserDefaults.standard.setValue(phone, forKey: "loadOthersWalletPhone")
        APIHandler.shared.walletMaxCoinReached(phone: phone, coins: coins) { (msg) in
            print("sawsank: Check max -> \(msg)")
            self.startPayment()
        } failure: { (msg) in
            print("sawsank: Check max failure -> \(msg)")
            self.loadWalletAlertController.dismiss(animated: true, completion: nil)
            self.showToast(message: msg, font: UIFont.systemFont(ofSize: 12))
        } error: { (err) in
            print("sawsank: Check max error -> \(err.localizedDescription)")
        }

    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerAmountData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerAmountData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedAmount = pickerAmountData[row]
        amountPickerTextField.text = selectedAmount
    }

}
extension MyAccountViewController {
    //load your wallet previous function
    @objc func choosePaymentMethod(_ sender: UITapGestureRecognizer? = nil){
       
        
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: "Apple Pay", style: .default, handler: { (handler) in
            self.handleLoadWalletTap(UITapGestureRecognizer())
           
        }))
        
        controller.addAction(UIAlertAction(title: "Recharge Card", style: .default, handler: { (handler) in
            self.loadWithReachargeCard(num: "")
           
        }))
        
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        present(controller, animated: true, completion: nil)
    }
    //new load your wallter function(earn cinecoin)
    @objc func earnPointTap(_ sender: UITapGestureRecognizer? = nil){
//        if rewardedAd?.isReady == true {
//               rewardedAd?.present(fromRootViewController: self, delegate: self)
//            }
        loadAd()
        show()
    }
    
    
    func loadWithReachargeCard(num: String?) {
        //MARK: - POST VOTE
        APIHandler.shared.loadWalletWithReachargeCard(cardNo: num ?? "") { [weak self] (votingPrice) in
                guard let strongSelf = self else { return }
                print(votingPrice)
            if votingPrice.first?.status == true {
                strongSelf.showPopup(title: "Success", message: "You have successfully loded your wallet.")
                    strongSelf.getProfile()
            } else if votingPrice.first?.code == 422{
                strongSelf.showPopup(title: "Failure!", message: votingPrice.first?.message ?? "")
            } else {
                strongSelf.showPopup(title: "Failure!", message: votingPrice.first?.message ?? "")
            }
            strongSelf.getProfile()
            } failure: { (msg) in
                self.showPopup(title: "Failure!", message: msg)
            } error: { (err) in
                print(err.localizedDescription)
            }
        
    }
    func loadAd() {
       let request = GADRequest()
        GADRewardedAd.load(withAdUnitID: Constant.googleRewardKey,
                        request: request,
                        completionHandler: { [self] ad, error in
         if let error = error {
           print("Failed to load rewarded ad with error: \(error.localizedDescription)")
           return
         }
         rewardedAd = ad
         print("Rewarded ad loaded.")
         rewardedAd?.fullScreenContentDelegate = self
       }
       )
     }
    func show() {
      if let ad = rewardedAd {
        ad.present(fromRootViewController: self) {
          let reward = ad.adReward
          print("Reward received with currency \(reward.amount), amount \(reward.amount.doubleValue)")
            self.isRewardEarned = true
            self.loadWithReward()
            
          // TODO: Reward the user.
        }
      } else {
        print("Ad wasn't ready")
      }
    }
    //MARK: - LOAD WITH REWARD
    func loadWithReward() {
        APIHandler.shared.loadWalletWithReward() { [weak self] (votingPrice) in
                guard let strongSelf = self else { return }
                print(votingPrice)
            
        strongSelf.showPopup(title: "Success", message: "You have successfully earned 10 points!")
            //strongSelf.showAlert()
            strongSelf.getProfile()
               
            } failure: { (msg) in
                print(msg)
            } error: { (err) in
                print(err.localizedDescription)
            }
        
    }
    func showPopup(title: String,message: String) {
        DispatchQueue.main.async {
        let alertDetailController = DOAlertController(title: title, message: message, preferredStyle: .alert)
        alertDetailController.alertViewBgColor = UIColor.white
        alertDetailController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
        alertDetailController.messageView.textAlignment = .left
        alertDetailController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
        alertDetailController.titleTextColor = UIColor.red
        let okAction = DOAlertAction(title: "OK", style: .default, handler: { action in
           // self.getProfile()
            print("sawsank: OK Pressed")
            if self.isRewardEarned == true {
                
                UIApplication.shared.windows.last?.rootViewController?.dismiss(animated: false, completion: nil)
                self.isRewardEarned = false
            }else {
                self.dismiss(animated: true, completion: nil)
            }
           // self.dismiss(animated: true, completion: nil)
          
          
        })
        // Add the action.
        alertDetailController.addAction(okAction)
        // Show alert
            if self.isRewardEarned == true {
//                UIApplication.shared.keyWindow?.rootViewController?.present(alertDetailController, animated: true, completion: {
//
//                })
                UIApplication.shared.windows.last?.rootViewController?.present(alertDetailController, animated: true, completion: nil)
              
               // UIApplication.shared.windows.first?.makeKeyAndVisible()
            } else {
                self.present(alertDetailController, animated: true, completion: nil)
            }
       
    }
    }
   

     
}
     

//extension MyAccountViewController: GADRewardedAdDelegate {
//    func rewardedAd(_ rewardedAd: GADRewardedAd, userDidEarn reward: GADAdReward) {
//
//    }
//
//
////    func loadRewardedAd() {
////       let request = GADRequest()
////
////        rewardedAd = GADRewardedAd(adUnitID: "ca-app-pub-3940256099942544/1712485313")
////        rewardedAd?.load(GADRequest()) { error in
////              if let error = error {
////                // Handle ad failed to load case.
////                  print("error loading ad")
////              } else {
////                // Ad successfully loaded.
////                  print("ad successful")
////              }
////            }
////}
//}
extension MyAccountViewController: RechargecardDelegate {
    func submitTap(cardNum: String?) {
        guard let num = cardNum else {
    return
        }
        self.loadWithReachargeCard(num: num)
    }
    
    
}
extension MyAccountViewController: GADFullScreenContentDelegate {
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
       print("Ad did fail to present full screen content.")
     }

     /// Tells the delegate that the ad will present full screen content.
     func adWillPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
       print("Ad will present full screen content.")
        
     }

     /// Tells the delegate that the ad dismissed full screen content.
     func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
       print("Ad did dismiss full screen content.")
     }

}

