//
//  MyAccountVC.swift
//  CinemaGhar
//
//  Created by Midas on 24/06/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import Alamofire
import EzPopup

class MyAccountVC: UIViewController {

    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var cinemagharIDLabel: UILabel!
    @IBOutlet weak var walletAmountLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileImagebackView: UIView!
    var profile: Profile!
    var isRewardEarned = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetup()
        getProfile()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showLoadingProgress(isLoading:)), name: Notification.Name(rawValue: "IAPPaymentStatusLoading"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideLoadingProgress(notification:)), name: Notification.Name(rawValue: "IAPPaymentStatusNotLoading"), object: nil)
    }
    
    @IBAction func backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func uisetup() {
        navView.backgroundColor = AppColors.darkbackgound
        profileImagebackView.layer.cornerRadius = 72.5
        profileImage.layer.cornerRadius = 72.5
        profileImagebackView.layer.borderWidth = 3
        profileImagebackView.layer.borderColor = AppColors.yellowColor.cgColor
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleProfileImageTap(_:)))
        profileImage.addGestureRecognizer(tap)
    }
    @objc func handleProfileImageTap(_ sender: UITapGestureRecognizer? = nil) {
        //Open choose image
        importImage()
        
    }
    func setProfileImageToImageView(url: String) {
        fetchImage(from: url) { (imageData) in
            if let data = imageData {
                // referenced imageView from main thread
                // as iOS SDK warns not to use images from
                // a background thread
                DispatchQueue.main.async {
                    self.profileImage.image = UIImage(data: data)
                }
            } else {
                    // show as an alert if you want to
                print("Error loading image");
            }
        }
    }
//MARK: - SHOW LOADING PROGRESS
    @objc func showLoadingProgress(isLoading: Bool){
        DispatchQueue.main.async {
            APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
            APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Processing Payment...", presentingView: self.view)
        }
    }
    //MARK: - HIDE LOADING PROGRESS
    @objc func hideLoadingProgress(notification: NSNotification){
        print("sawsank: Hiding Progress")
//        DispatchQueue.main.async {
            APESuperHUD.removeHUD(animated: true, presentingView: self.view) {
                self.tempGetProfile()

                guard let userInfo = notification.userInfo else {
                    return
                }
                let alertController = DOAlertController(title: "Info", message: userInfo["message"] as? String, preferredStyle: .alert)
                alertController.alertViewBgColor = UIColor.white
                alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                alertController.messageView.textAlignment = .left
                alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                alertController.titleTextColor = UIColor.red
                let okAction = DOAlertAction(title: "OK", style: .default, handler: { action in
                    print("sawsank: Start get profile")
                    self.dismiss(animated: true, completion: nil)
                })
                // Add the action.
                alertController.addAction(okAction)
                // Show alert
                self.present(alertController, animated: true, completion: nil)
            }
            
//        }
    }
    func tempGetProfile(){
        getProfile()
    }
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func editProfileTap(_ sender: Any) {
        editAction()
    }
    @IBAction func linkedInTap(_ sender: Any) {
    }
    @IBAction func instaTap(_ sender: Any) {
        let url =  URL(string: "https://www.instagram.com/cinemaghar/")!
        
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func fbtap(_ sender: Any) {
        let url =  URL(string: "https://www.facebook.com/cinemaghargold")!
        
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func twitterTap(_ sender: Any) {
    }
    @IBAction func loadYourWallet(_ sender: Any) {//for statement
        let financialStatementVC = FinancialStatementViewController.instantiate(fromAppStoryboard: .FinancialStatement)
        self.navigationController?.pushViewController(financialStatementVC, animated: true)
    }
    @IBAction func loadOtherWallet(_ sender: Any) {//recharge card
        showPopup(nil)
    }
    @IBAction func viewStatement(_ sender: Any) {//for reward
        let vc = MyReawardVC.instantiate(fromAppStoryboard: .reward)
        self.navigationController?.pushViewController(vc, animated: true)
//        let financialStatementVC = FinancialStatementViewController.instantiate(fromAppStoryboard: .FinancialStatement)
//        self.navigationController?.pushViewController(financialStatementVC, animated: true)
    }
    //MARK: - SHOW POP UP
        @objc func showPopup(_ sender: UITapGestureRecognizer? = nil) {
             let customAlertVC = RechargeCardPopUpVC.instantiate()
            customAlertVC.delegate = self
            let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: SCREEN.WIDTH - 60)
            popupVC.cornerRadius = 8
           
           
           present(popupVC, animated: true, completion: nil)
        }
    func showPopup(title: String,message: String) {
        DispatchQueue.main.async {
        let alertDetailController = DOAlertController(title: title, message: message, preferredStyle: .alert)
        alertDetailController.alertViewBgColor = UIColor.white
        alertDetailController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
        alertDetailController.messageView.textAlignment = .left
        alertDetailController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
        alertDetailController.titleTextColor = UIColor.red
        let okAction = DOAlertAction(title: "OK", style: .default, handler: { action in
           // self.getProfile()
            print("sawsank: OK Pressed")
            if self.isRewardEarned == true {
                
                UIApplication.shared.windows.last?.rootViewController?.dismiss(animated: false, completion: nil)
                self.isRewardEarned = false
            }else {
                self.dismiss(animated: true, completion: nil)
            }
           // self.dismiss(animated: true, completion: nil)
          
          
        })
        // Add the action.
        alertDetailController.addAction(okAction)
        // Show alert
            if self.isRewardEarned == true {
//                UIApplication.shared.keyWindow?.rootViewController?.present(alertDetailController, animated: true, completion: {
//
//                })
                UIApplication.shared.windows.last?.rootViewController?.present(alertDetailController, animated: true, completion: nil)
              
               // UIApplication.shared.windows.first?.makeKeyAndVisible()
            } else {
                self.present(alertDetailController, animated: true, completion: nil)
            }
       
    }
    }
     func editAction(){
        //Open alert with text view
        let alertController = DOAlertController(title: "Change your name", message: nil, preferredStyle: .alert)
        alertController.alertViewBgColor = UIColor.init(hex: "#252B38")
        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 17)
        alertController.messageView.textAlignment = .left
        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
        alertController.titleTextColor = UIColor.white
        
        alertController.addTextFieldWithConfigurationHandler { (userFirstnameTextField) in
            
            //Left padding
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            
            userFirstnameTextField?.placeholder = "Enter your Firstname"
            userFirstnameTextField?.leftView = paddingView
            userFirstnameTextField?.leftViewMode = .always
        }
        
        alertController.addTextFieldWithConfigurationHandler { (userLastnameTextField) in
            
            //Left padding
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            
            userLastnameTextField?.placeholder = "Enter your Lastname"
            userLastnameTextField?.leftView = paddingView
            userLastnameTextField?.leftViewMode = .always
        }
        
        let okAction = DOAlertAction(title: "Ok", style: .default, handler: { action in
            self.dismiss(animated: true) {
                let userFirstnameTextField = alertController.textFields![0] as! UITextField
                let userLastnameTextField = alertController.textFields![1] as! UITextField
                let name = "\(userFirstnameTextField.text!) \(userLastnameTextField.text!)"
                
                self.editProfileName(name: name)
                
            }
        })
        
        let cancelAction = DOAlertAction(title: "Cancel", style: .cancel, handler: { action in
            self.dismiss(animated: true, completion: nil)
        })
        // Add the action.
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        // Show alert
        self.present(alertController, animated: true, completion: nil)
    }
}

extension MyAccountVC {
//MARK: - GET PROFILE
    func getProfile() {
        APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait...", presentingView: self.view)
        
        
        APIHandler.shared.getProfile(success: { (profile) in
            print("sawsank: Getting profile")
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                print("sawsank: Load Profile Removed HUD")
            })
            
            self.profile = profile
            print(self.profile)
            self.userNameLabel.text = profile.name
            self.walletAmountLabel.text = profile.walletBalance
            self.cinemagharIDLabel.text = "CinemagharID: " + profile.phoneNumber
            if(self.profile.image != ""){
                self.setProfileImageToImageView(url: profile.image)
               // self.profileImage.sd_setImage(with: URL(string: profile.image), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
            }

        }, failure: { (message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                   let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
                   alertController.alertViewBgColor = UIColor.white
                   alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                   alertController.messageView.textAlignment = .left
                   alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                   alertController.titleTextColor = UIColor.red
                   let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                       self.dismiss(animated: true, completion: nil)
                    self.dismissVC()
                   })
                   // Add the action.
                   alertController.addAction(okAction)
                   // Show alert
                   self.present(alertController, animated: true, completion: nil)
                   
                   
               
               
           } )
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                
                
            } )
        }
    }
    //Network profile image access <Start>
    func fetchImage(from urlString: String, completionHandler: @escaping (_ data: Data?) -> ()) {
        let session = URLSession.shared
        let url = URL(string: urlString)
        let dataTask = session.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print("Error fetching the image! 😢")
                completionHandler(nil)
            } else {
                completionHandler(data)
            }
        }
            
        dataTask.resume()
    }
    //MARK: - LOAD WITH RECHARGE CARD
    func loadWithReachargeCard(num: String?) {
       
      
           
        APIHandler.shared.loadWalletWithReachargeCard(cardNo: num ?? "") { [weak self] (votingPrice) in
                guard let strongSelf = self else { return }
                print(votingPrice)
            if votingPrice.first?.status == true {
                strongSelf.showPopup(title: "Success", message: "You have successfully loded your wallet.")
                    strongSelf.getProfile()
            } else if votingPrice.first?.code == 422{
                strongSelf.showPopup(title: "Failure!", message: votingPrice.first?.message ?? "")
            } else {
                strongSelf.showPopup(title: "Failure!", message: votingPrice.first?.message ?? "")
            }
         
               
            } failure: { (msg) in
                print(msg)
                self.showPopup(title: "Failure!", message: msg)
            } error: { (err) in
                print(err.localizedDescription)
            }
        
    }
}
extension MyAccountVC: RechargecardDelegate {
    func submitTap(cardNum: String?) {
        guard let num = cardNum else {
    return
        }
        self.loadWithReachargeCard(num: num)
    }
    
    
}
extension MyAccountVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func importImage() {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = .photoLibrary
        
        image.allowsEditing = true
        self.present(image, animated: true){
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage{
            APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait...", presentingView: self.view)
            
            let name: String = self.profile.name
            
            editProfileImage(name: name, image: image)
            
        }else{
            //error
            let alertController = DOAlertController(title: "Whoops!", message: "Something went wrong! Please try again later.", preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func editProfileImage(name: String, image: UIImage){
        APIHandler.shared.editProfileImage(name: name, image: image, success: { (status) in
            if (status){
                self.profileImage.image = image
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                    let alertController = DOAlertController(title: "Success!", message: "Your profile image is successfully uploaded.", preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                })
            }
        }, failure: { (message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
                alertController.alertViewBgColor = UIColor.white
                alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                alertController.messageView.textAlignment = .left
                alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                alertController.titleTextColor = UIColor.red
                let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                    self.dismiss(animated: true, completion: nil)
                })
                // Add the action.
                alertController.addAction(okAction)
                // Show alert
                self.present(alertController, animated: true, completion: nil)
            })
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
            })
        }
    }
    
    func editProfileName(name: String){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Updating your name...", presentingView: self.view)
        APIHandler.shared.editProfileName(name: name, success: { (status) in
            if (status){
                self.userNameLabel.text = name
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                    let alertController = DOAlertController(title: "Success!", message: "Your name has been successfully changed.", preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.green
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                })
            }
        }, failure: { (message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
                alertController.alertViewBgColor = UIColor.white
                alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                alertController.messageView.textAlignment = .left
                alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                alertController.titleTextColor = UIColor.red
                let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                    self.dismiss(animated: true, completion: nil)
                })
                // Add the action.
                alertController.addAction(okAction)
                // Show alert
                self.present(alertController, animated: true, completion: nil)
            })
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
            })
        }
    }
    
}
