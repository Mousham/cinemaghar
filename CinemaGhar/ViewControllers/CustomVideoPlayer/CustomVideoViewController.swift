////
////  CustomVideoViewController.swift
////  CinemaGhar
////
////  Created by Shashank Gnawali on 4/22/20.
////  Copyright © 2020 Cinemaghar. All rights reserved.
////
//
//import UIKit
//import AVFoundation
//import GoogleCast
//import AVKit
//import MediaPlayer
//
//let videoPlayerAppDelegate = UIApplication.shared.delegate as! AppDelegate
//
///* The player state. */
////enum PlaybackMode: Int {
////  case none = 0
////  case local
////  case remote
////}
//
//class CustomVideoViewController: UIViewController, RatingModalDelegate, GCKSessionManagerListener,
//GCKRemoteMediaClientListener, GCKRequestDelegate  {
//
//
//    //Player
//    @IBOutlet weak var videoPlayerView: UIView!
//    var player: AVPlayer!
//    var playerLayer: AVPlayerLayer!
//    var playerItem: AVPlayerItem!
//    let airplayView = UIView()
//
//    //Advertisements
//    var advertisement: Advertisements!
//    @IBOutlet weak var advertisementViewer: UIView!
//    @IBOutlet weak var advertImageView: UIImageView!
//    var advertPlayer: AVPlayer!
//    var advertPlayerLayer: AVPlayerLayer!
//
//    //Cast Media Controller UI
//    private var castMediaController: GCKUIMediaController!
//
//    //Cast Volume Controller
//    private var volumeController: GCKUIDeviceVolumeController!
//
//    //Player State remote|local
//    private var playbackMode = PlaybackMode.none
//
//    //Timer for Watermark Position Change
//    var watermarkTimer: Timer!
//
//    //Timer for idle tracking
//    var idleTrackingTimer: Timer!
//
//    //Watermark UniqueID Label
//    let uniqueIDLabel = UILabel()
//
//    // Controls
//    let controlsView = UIView()
//    let activityIndicatorView = UIActivityIndicatorView()
//    let playButton = UIButton(type: .system)
//    let forwardJumpButton = UIButton(type: .system)
//    let backJumpButton = UIButton(type: .system)
//    var videoIsPlaying = LocalPlayerState.playing
//    let videoLengthLabel = UILabel()
//    let videoScrubber = UISlider()
//    let currentTimeLabel = UILabel()
//    let gradientLayer = CAGradientLayer()
//    let backButton = UIButton(type: .system)
//    let titleLabel = UILabel()
//    var castButton = GCKUICastButton()
//    let resizeVideo = UIButton()
//    var qualityControlButton = UIButton()
//    var isControlsViewVisible = true
//
//    //Media Info
//    var contentId: Int = 0
//    var urlString: String = ""
//    var videoTitle: String = ""
//    var mediaType: MediaType = .movie
//    var uniqueID: String = ""
//    var mediaInfo: GCKMediaInformation? {
//      didSet {
//        print("setMediaInfo: \(String(describing: mediaInfo))")
//      }
//    }
//    var isRated: Bool = false
//    var isComingSoon: Bool = false
//
//    //Seek Duration
//    var seekDuration = 10.0
//
//    //IsFullscreen
//    var isFullscreen = false
//
//    var playOffline = false
//    var offlineItem: AVPlayerItem?
//
//
//    //Google cast session manager
//    private var sessionManager: GCKSessionManager!
//
//
//    var isResetEdgesOnDisappear: Bool = false
//
//    required init?(coder: NSCoder) {
//        super.init(coder: coder)
//        self.castMediaController = GCKUIMediaController()
//        sessionManager = GCKCastContext.sharedInstance().sessionManager
//        self.volumeController = GCKUIDeviceVolumeController()
//    }
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//
//        videoPlayerAppDelegate.myOrientation = .landscapeRight
//        let value = UIInterfaceOrientation.landscapeRight.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
//
//        handleAdvertisement()
//        setupAVPlayer()
//
//
//        setupControlsView()
//
//        NotificationCenter.default.addObserver(self, selector: #selector(self.castDeviceDidChange),
//                    name: NSNotification.Name.gckCastStateDidChange,
//                    object: GCKCastContext.sharedInstance())
//
//   // airplaySetup()
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//
//        seekToPosition(position: SessionManager().getCurrentPlayingTime(id: String(contentId)))
//        //Observable to observe screenshots taken
//        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationUserDidTakeScreenshot, object: nil, queue: OperationQueue.main) { notification in
//
//            print("Screenshot taken!")
//        }
//
//        //Observable to observe screen recorder
//        UIScreen.main.addObserver(self, forKeyPath: "captured", options: .new, context: nil)
//    }
////    func airplaySetup() {
////           // airplay.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
////            let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
////
////            let routerPickerView = AVRoutePickerView(frame: buttonView.bounds)
////            routerPickerView.tintColor = UIColor.black
////            routerPickerView.activeTintColor = .green
////            buttonView.addSubview(routerPickerView)
////            self.airPlayView.addSubview(buttonView)
////       // videoPlayerView.bringSubview(toFront: airPlayView)
////
////    }
//
//    func handleAdvertisement(){
//        if(advertisement != nil){
//            videoPlayerView.isHidden = true
//            if(advertisement.image != ""){
//                print("hasAdvert: image")
//
//
//                if  let advertImage = advertisement.image{
//
//                    advertImageView.sd_setImage(with: URL(string: advertImage), placeholderImage: UIImage(named: "placeholder_cell"), options: [.continueInBackground, .progressiveLoad])
//
//
//                }
//                advertisementViewer.addSubview(advertImageView)
//                //Center the activity indicator view
//                advertImageView.leftAnchor.constraint(equalTo: advertisementViewer.leftAnchor).isActive = true
//                advertImageView.topAnchor.constraint(equalTo: advertisementViewer.topAnchor).isActive = true
//                advertImageView.rightAnchor.constraint(equalTo: advertisementViewer.rightAnchor).isActive = true
//                advertImageView.bottomAnchor.constraint(equalTo: advertisementViewer.bottomAnchor).isActive = true
//                //Show Countdown
//                let countdownView = UIView()
//                countdownView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
//
//                let countdownLabel = UILabel()
//                countdownLabel.textColor = UIColor.white
//                countdownLabel.font = UIFont.boldSystemFont(ofSize: 20)
//                var secondsRemaining = 9
//                Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
//                        if secondsRemaining > 0 {
//                            countdownLabel.text = "Playing in \(String(secondsRemaining))..."
//                            secondsRemaining -= 1
//                        } else {
//                            Timer.invalidate()
//                        }
//                    }
//                countdownLabel.translatesAutoresizingMaskIntoConstraints = false
//                countdownView.addSubview(countdownLabel)
//                countdownLabel.centerXAnchor.constraint(equalTo: countdownView.centerXAnchor).isActive = true
//                countdownLabel.centerYAnchor.constraint(equalTo: countdownView.centerYAnchor).isActive = true
//                countdownView.translatesAutoresizingMaskIntoConstraints = false
//                advertisementViewer.addSubview(countdownView)
//                //Place the countdown in advertisementViewer
//                countdownView.bottomAnchor.constraint(equalTo: advertisementViewer.bottomAnchor, constant: -24).isActive = true
//                countdownView.rightAnchor.constraint(equalTo: advertisementViewer.rightAnchor, constant: -24).isActive = true
//                countdownView.heightAnchor.constraint(equalToConstant: 40).isActive = true
//                countdownView.widthAnchor.constraint(equalToConstant: 200).isActive = true
//                DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
//                    self.videoPlayerView.isHidden = false
//                    self.advertisementViewer.isHidden = true
//                    self.player.play()
//                }
//
//            }else if(advertisement.video != ""){
//                print("hasAdvert: video")
//                advertImageView.isHidden = true
//                self.advertPlayer = AVPlayer(url: URL(string: advertisement.video)!)
//                self.advertPlayerLayer = AVPlayerLayer(player: advertPlayer)
//                advertPlayerLayer.videoGravity = .resizeAspectFill
//                advertisementViewer.layer.addSublayer(advertPlayerLayer)
//                advertPlayer.play()
//                NotificationCenter.default.addObserver(self, selector: #selector(advetisementDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
//            }
//        }else{
//            advertisementViewer.isHidden = true
//
//        }
//    }
//
//    @objc func advetisementDidFinishPlaying(){
//        print("hasAdvert: video finished playing")
//        advertisementViewer.isHidden = true
//        videoPlayerView.isHidden = false
//        self.player.play()
//    }
//
//    func seekToPosition(position: Float){
//        if (position > 0.0){
//            let timeToPlayInSeconds = Double(SessionManager().getCurrentPlayingTime(id: String(contentId)))
//            let timeToPlay = CMTime(seconds: timeToPlayInSeconds, preferredTimescale: 1)
//            self.player.seek(to: timeToPlay)
//            self.resetIdleTimer()
//        }else{
//            self.resetIdleTimer()
//        }
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        let hasConnectedSession: Bool = (sessionManager.hasConnectedSession())
//        if hasConnectedSession, (playbackMode != .remote) {
////          populateMediaInfo(false, playPosition: 0)
//          switchToRemotePlayback()
//        } else if sessionManager.currentSession == nil, (playbackMode != .local) {
//          switchToLocalPlayback()
//        }
//
//        sessionManager.add(self)
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        sessionManager.remove(self)
//        player.pause()
//        sendWatchedDuration()
//        super.viewWillDisappear(animated)
//    }
//
//
//
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        playerLayer.frame = videoPlayerView.bounds
//        if(advertisement != nil){
//            if(advertisement.video != ""){
//                advertPlayerLayer.frame = self.advertisementViewer.bounds
//            }
//        }
//
//        controlsView.frame = self.videoPlayerView.bounds
//        gradientLayer.frame = videoPlayerView.bounds
//
//
//    }
//
//    func sendWatchedDuration(){
//        APIHandler.shared.saveWatchedDuration(duration: 100, contentId: 100) { (status) in
//            print("Uploading duration")
//            print(status)
//        } failure: { (e) in
//            print("Failed to upload duration")
//            print(e)
//        }
//    }
//
//    func createHeaderForAVPlayer() -> [String: String]{
//        let deviceID = UIDevice.current.identifierForVendor!.uuidString
//        let deviceName = UIDevice.current.name
//        let headers = ["Accept": "application/json", "X-VERSION": "1.1", "X-DEVICE-TYPE": "ios", "X-DEVICE-ID": deviceID, "X-DEVICE-MODEL": deviceName, "Authorization": SessionManager().getAccessToken() ?? ""]
//
//        return headers
//    }
//
//    func setupAVPlayer(){
//
//
//        if playOffline == true {
//            print(offlineItem)
//            playerLayer = AVPlayerLayer()
//            playerLayer.frame = self.view.bounds
//            player = AVPlayer(playerItem: offlineItem)
////            playerLayer.player = player
////            self.view.layer.addSublayer(playerLayer)
////            playerLayer.player?.play()
//        } else {
//            let url = mediaInfo?.contentURL
//
//            let _header = createHeaderForAVPlayer()
//            let asset = AVURLAsset(url: url!, options: ["AVURLAssetHTTPHeaderFieldsKey": _header])
//            playerItem = AVPlayerItem(asset: asset)
//            player = AVPlayer(playerItem: playerItem)
//        }
//
//
//
//        //Interval for periodic time observer
//        let interval = CMTime(value: 1, timescale: 10)
//
//
//
//        //Add observer
//        //Buffer observer
//        player.currentItem!.addObserver(self, forKeyPath: "playbackBufferEmpty", options: .new, context: nil)
//        player.currentItem!.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: .new, context: nil)
//        player.currentItem!.addObserver(self, forKeyPath: "playbackBufferFull", options: .new, context: nil)
//        //Time Observer
//        player.addObserver(self, forKeyPath: "currentItem.loadedTimeRanges", options: .new, context: nil)
//        player.addPeriodicTimeObserver(forInterval: interval, queue: nil) { (progressTime) in
//            let seconds = CMTimeGetSeconds(progressTime)
//            SessionManager().setCurrentPlayingTime(id: String(self.contentId), duration: seconds)
//            var secondsText = ""
//            secondsText = String(format: "%02d", Int(Int(seconds) % 60))
//            var minutesText = ""
//            minutesText = String(format: "%02d", Int(Int(seconds) / 60))
//            self.currentTimeLabel.text = "\(minutesText):\(secondsText)"
//
//            //moving the scrubber
//            if let duration = self.player.currentItem?.duration {
//                let durationSeconds = CMTimeGetSeconds(duration)
//
//                self.videoScrubber.value = Float(seconds / durationSeconds)
//
//
//                //show the Controls view when the video ends
//                if (seconds == durationSeconds){
//                    self.controlsView.isHidden = false
//                    self.videoIsPlaying = LocalPlayerState.stopped
//                    self.playButton.setImage(UIImage(named: "play_circle"), for: .normal)
//                    self.showRatingModalVC()
//                }
//            }
//        }
//        //End Observer
//        player.pause()
//
//
//        //Adding video layer
//        playerLayer = AVPlayerLayer(player: player)
//        playerLayer.videoGravity = .resizeAspect
//
//        videoPlayerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
//        videoPlayerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
//        videoPlayerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
//        videoPlayerView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
//        videoPlayerView.layer.addSublayer(playerLayer)
//
//
//    }
//
//
//    func setupControlsView(){
//
//
//        //Adding watermark view
//        uniqueIDLabel.text = UserDefaults.standard.string(forKey: AppConstants.uniqueId)
//        uniqueIDLabel.textColor = UIColor(white: 1, alpha: 0.2)
////        uniqueIDLabel.font = UIFont.boldSystemFont(ofSize: 18)
//        uniqueIDLabel.font = UIFont.init(name: "PingFangTC-Regular", size: 14)
//        uniqueIDLabel.translatesAutoresizingMaskIntoConstraints = false
//        uniqueIDLabel.textAlignment = .center
//        //Anchor to the top left corner
//        videoPlayerView.addSubview(uniqueIDLabel)
//        uniqueIDLabel.leftAnchor.constraint(equalTo: videoPlayerView.leftAnchor, constant: 3).isActive = true
//        uniqueIDLabel.topAnchor.constraint(equalTo: videoPlayerView.topAnchor, constant: 90).isActive = true
//
//
//        //Change watermark position periodically
//        watermarkRevolver()
//
//        //Setting up gradient view
//        setupGradientLayer()
//
//        //loading indicator view
//        activityIndicatorView.activityIndicatorViewStyle = .whiteLarge
//        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
//        activityIndicatorView.startAnimating()
//        controlsView.addSubview(activityIndicatorView)
//        //Center the activity indicator view
//        activityIndicatorView.centerXAnchor.constraint(equalTo: controlsView.centerXAnchor).isActive = true
//        activityIndicatorView.centerYAnchor.constraint(equalTo: controlsView.centerYAnchor).isActive = true
//        activityIndicatorView.widthAnchor.constraint(equalToConstant: 75).isActive = true
//        activityIndicatorView.heightAnchor.constraint(equalToConstant: 75).isActive = true
//
//        // play/pause button
//        let playButtonImage = UIImage(named: "pause_circle")
//        playButton.setImage(playButtonImage, for: .normal)
//        playButton.translatesAutoresizingMaskIntoConstraints = false
//        playButton.tintColor = UIColor.init(hex: "#FFFFFF")
//        playButton.isHidden = true
//        playButton.addTarget(self, action: #selector(handlePlayPause), for: .touchUpInside)
//        controlsView.addSubview(playButton)
//        //Center the play button
//        playButton.centerXAnchor.constraint(equalTo: controlsView.centerXAnchor).isActive = true
//        playButton.centerYAnchor.constraint(equalTo: controlsView.centerYAnchor).isActive = true
//        playButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        playButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
//
//
//        //Video Length Label
//        videoLengthLabel.text = "--:--:--"
//        videoLengthLabel.textColor = .white
//        videoLengthLabel.font = UIFont.boldSystemFont(ofSize: 14)
//        videoLengthLabel.textAlignment = .right
//        videoLengthLabel.translatesAutoresizingMaskIntoConstraints = false
//        controlsView.addSubview(videoLengthLabel)
//        //Anchor the label to bottom right of controlsView
//        videoLengthLabel.rightAnchor.constraint(equalTo: controlsView.rightAnchor, constant: -8).isActive = true
//        videoLengthLabel.bottomAnchor.constraint(equalTo: controlsView.bottomAnchor, constant: -6).isActive = true
//
//        //Current time label
//        currentTimeLabel.text = "--:--:--"
//        currentTimeLabel.textColor = .white
//        currentTimeLabel.font = UIFont.boldSystemFont(ofSize: 14)
//        currentTimeLabel.textAlignment = .left
//        currentTimeLabel.translatesAutoresizingMaskIntoConstraints = false
//        controlsView.addSubview(currentTimeLabel)
//        //Anchor to the bottom left of the controls view
//        currentTimeLabel.leftAnchor.constraint(equalTo: controlsView.leftAnchor, constant: 8).isActive = true
//        currentTimeLabel.bottomAnchor.constraint(equalTo: controlsView.bottomAnchor, constant: -6).isActive = true
//
//        //Video Scrubber
//        videoScrubber.translatesAutoresizingMaskIntoConstraints = false
//        videoScrubber.minimumTrackTintColor = UIColor.init(hex: "#FFFFFF")
//        videoScrubber.thumbTintColor = UIColor.init(hex: "#FFFFFF")
//        videoScrubber.maximumTrackTintColor = .white
//        //handle scrubbing
//        videoScrubber.addTarget(self, action: #selector(handleVideoScrubbing), for: .valueChanged)
//        //Place scrubber into controls view
//        controlsView.addSubview(videoScrubber)
//        videoScrubber.rightAnchor.constraint(equalTo: videoLengthLabel.leftAnchor, constant: -3).isActive = true
//        videoScrubber.bottomAnchor.constraint(equalTo: controlsView.bottomAnchor).isActive = true
//        videoScrubber.leftAnchor.constraint(equalTo: currentTimeLabel.rightAnchor, constant: 3).isActive = true
//        videoScrubber.heightAnchor.constraint(equalToConstant: 30).isActive = true
//
//        //Adding back button
//        backButton.setImage(UIImage(named: "back_arrow"), for: .normal)
//        backButton.setTitle("Back", for: .normal)
//        backButton.tintColor = .white
//        backButton.translatesAutoresizingMaskIntoConstraints = false
//        backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
//        controlsView.addSubview(backButton)
//        //Anchoring to the top left corner
//        backButton.leftAnchor.constraint(equalTo: controlsView.leftAnchor, constant: 8).isActive = true
//        backButton.topAnchor.constraint(equalTo: controlsView.topAnchor, constant: 16).isActive = true
//        backButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
//        backButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
//        backButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: -10)
//
//        //Adding title label
//        titleLabel.text = videoTitle
//        titleLabel.textColor = .white
//        titleLabel.translatesAutoresizingMaskIntoConstraints = false
//        titleLabel.textAlignment = .center
//        controlsView.addSubview(titleLabel)
//        //Anchoring to the top middle
//        titleLabel.topAnchor.constraint(equalTo: controlsView.topAnchor, constant: 16).isActive = true
//        titleLabel.leftAnchor.constraint(equalTo: backButton.rightAnchor, constant: 8).isActive = true
//        titleLabel.centerXAnchor.constraint(equalTo: controlsView.centerXAnchor).isActive = true
//        titleLabel.heightAnchor.constraint(equalToConstant: 24).isActive = true
//
//        controlsView.backgroundColor = UIColor(white: 0, alpha: 1)
//
//        //Adding Google Cast Button
//        let viewWidth = self.view!.bounds.height
//        castButton = GCKUICastButton(frame: CGRect(x: viewWidth - 75, y: 16, width: 24, height: 24))
//        castButton.tintColor = UIColor.gray
//        controlsView.addSubview(castButton)
//        //Anchoring to the top right
//        castButton.topAnchor.constraint(equalTo: controlsView.topAnchor, constant: 16).isActive = true
//        castButton.rightAnchor.constraint(equalTo: controlsView.rightAnchor, constant: 8).isActive = true
//        castButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
//        castButton.widthAnchor.constraint(equalToConstant: 24).isActive = true
//
//
//        //Adding quality control setting button
//        let originalMenuIcon = UIImage(named: "icons8-menu-vertical")
//        originalMenuIcon?.withRenderingMode(.alwaysTemplate)
//        qualityControlButton = UIButton(frame: CGRect(x: viewWidth - 36, y: 16, width: 24, height: 24))
//        qualityControlButton.setImage(originalMenuIcon, for: .normal)
//        qualityControlButton.tintColor = UIColor.white
//
//        controlsView.addSubview(qualityControlButton)
//
//        //Anchoring to the top right
//        qualityControlButton.topAnchor.constraint(equalTo: controlsView.topAnchor, constant: 24).isActive = true
//        qualityControlButton.rightAnchor.constraint(equalTo: controlsView.rightAnchor, constant: 24).isActive = true
//        qualityControlButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
//        qualityControlButton.widthAnchor.constraint(equalToConstant: 24).isActive = true
//
//
//        //MARK:- Resize video
//        resizeVideo.setImage(UIImage(named: "fullscreen"), for: .normal)
//        resizeVideo.translatesAutoresizingMaskIntoConstraints = false
//        resizeVideo.addTarget(self, action: #selector(handleResizeVideo), for: .touchUpInside)
//        controlsView.addSubview(resizeVideo)
//
//        //Anchoring resize button to the right of back button
//        resizeVideo.topAnchor.constraint(equalTo: controlsView.topAnchor, constant: 16).isActive = true
//        resizeVideo.rightAnchor.constraint(equalTo: castButton.leftAnchor, constant: -16).isActive = true
//        resizeVideo.heightAnchor.constraint(equalToConstant: 24).isActive = true
//        resizeVideo.widthAnchor.constraint(equalToConstant: 24).isActive = true
//
//        //MARK: - AIRPLAY BUTTON
//
//        //airplayView.backgroundColor = .clear
//        let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
//
//        let routerPickerView = AVRoutePickerView(frame: buttonView.bounds)
//        //let routerPickerView = AVRoutePickerView()
//        routerPickerView.tintColor = .white
//        routerPickerView.activeTintColor = .green
//        airplayView.addSubview(routerPickerView)
//        controlsView.addSubview(airplayView)
//        airplayView.frame = CGRect(x: UIScreen.main.bounds.width - 165, y: 12, width: 30, height: 30)
////        airplayView.translatesAutoresizingMaskIntoConstraints = false
////        routerPickerView.translatesAutoresizingMaskIntoConstraints = false
////
////        routerPickerView.topAnchor.constraint(equalTo: airplayView.topAnchor, constant: 0).isActive = true
////        routerPickerView.rightAnchor.constraint(equalTo: airplayView.leftAnchor, constant: 0).isActive = true
////        routerPickerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
////        routerPickerView.widthAnchor.constraint(equalToConstant: 30).isActive = true
////
////        airplayView.topAnchor.constraint(equalTo: controlsView.topAnchor, constant: 16).isActive = true
////        airplayView.rightAnchor.constraint(equalTo: resizeVideo.leftAnchor, constant: -16).isActive = true
////        airplayView.heightAnchor.constraint(equalToConstant: 30).isActive = true
////        airplayView.widthAnchor.constraint(equalToConstant: 30).isActive = true
//
//
//
//        //Forward on  tap
//        let forwardJumpImage = UIImage(named: "forward-10")
//        forwardJumpButton.setImage(forwardJumpImage, for: .normal)
//        forwardJumpButton.translatesAutoresizingMaskIntoConstraints = false
//        forwardJumpButton.tintColor = UIColor.init(hex: "#FFFFFF")
//        controlsView.addSubview(forwardJumpButton)
//
//        forwardJumpButton.addTarget(self, action: #selector(seekForward), for: .touchUpInside)
//        //Anchoring forward tap to the right of play button
//        forwardJumpButton.centerYAnchor.constraint(equalTo: controlsView.centerYAnchor).isActive = true
//        forwardJumpButton.leftAnchor.constraint(equalTo: playButton.rightAnchor, constant: 65).isActive = true
//        forwardJumpButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        forwardJumpButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
//
//        //Backward on  tap
//        let backwardJumpImage = UIImage(named: "back-10")
//        backJumpButton.setImage(backwardJumpImage, for: .normal)
//        backJumpButton.translatesAutoresizingMaskIntoConstraints = false
//        backJumpButton.tintColor = UIColor.init(hex: "#FFFFFF")
//        controlsView.addSubview(backJumpButton)
//
//        backJumpButton.addTarget(self, action: #selector(seekBackward), for: .touchUpInside)
//        //Anchoring forward tap to the right of play button
//        backJumpButton.centerYAnchor.constraint(equalTo: controlsView.centerYAnchor).isActive = true
//        backJumpButton.rightAnchor.constraint(equalTo: playButton.leftAnchor, constant: -65).isActive = true
//        backJumpButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        backJumpButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
//
//        //Handle on screen tap
//        let tapOnVideoPlayer = UITapGestureRecognizer(target: self, action: #selector(controlsViewTapped))
//        videoPlayerView.addGestureRecognizer(tapOnVideoPlayer)
//
//        videoPlayerView.addSubview(controlsView)
//
//
//
//
//    }
//
//    @objc func seekForward(){
//
//        guard let duration  = player.currentItem?.duration else{
//                return
//            }
//            let playerCurrentTime = CMTimeGetSeconds(player.currentTime())
//            let newTime = playerCurrentTime + seekDuration
//
//            if newTime < CMTimeGetSeconds(duration) {
//
//                let time2: CMTime = CMTimeMake(Int64(newTime * 1000 as Float64), 1000)
//                player.seek(to: time2)
//            }
//    }
//
//    @objc func seekBackward(){
//
//        let playerCurrentTime = CMTimeGetSeconds(player.currentTime())
//        var newTime = playerCurrentTime - seekDuration
//
//        if newTime < 0 {
//            newTime = 0
//        }
//        let time2: CMTime = CMTimeMake(Int64(newTime * 1000 as Float64), 1000)
//        player.seek(to: time2)
//    }
//
//
//    @objc func handleResizeVideo(){
//        if(!isFullscreen){
//            print("sawsank: going full screen")
//            playerLayer!.frame = CGRect(x: 0,y: 0,width: self.view.frame.width, height: self.view.frame.height)
//            playerLayer.videoGravity = .resizeAspectFill
//
//            playerLayer.contentsScale = 1.2
//            resizeVideo.setImage(UIImage(named: "fullscreen_exit"), for: .normal)
//        }else{
//            print("sawsank: leaving full screen")
//            playerLayer.videoGravity = .resizeAspect
//            playerLayer.contentsScale = 1
//            resizeVideo.setImage(UIImage(named: "fullscreen"), for: .normal)
//        }
//        self.view.setNeedsLayout()
//        isFullscreen = !isFullscreen
//
//
//    }
//
//
//    func watermarkRevolver(){
//        //Timer for the revolver
//        watermarkTimer = Timer.scheduledTimer(withTimeInterval: 300, repeats: true) { timer in
//            let randomNumber = Int.random(in: 1...8)
//            print(randomNumber)
//            self.uniqueIDLabel.removeFromSuperview()
//                switch(randomNumber){
//                    case 1:
//                        //top left
//                        self.videoPlayerView.addSubview(self.uniqueIDLabel)
//                        self.uniqueIDLabel.leftAnchor.constraint(equalTo: self.videoPlayerView.leftAnchor, constant: 30).isActive = true
//                        self.uniqueIDLabel.centerYAnchor.constraint(equalTo: self.videoPlayerView.centerYAnchor).isActive = true
//                        break
//                    case 2:
//                        //top right
//                        self.videoPlayerView.addSubview(self.uniqueIDLabel)
//                        self.uniqueIDLabel.rightAnchor.constraint(equalTo: self.videoPlayerView.rightAnchor, constant: -30).isActive = true
//                        self.uniqueIDLabel.centerYAnchor.constraint(equalTo: self.videoPlayerView.centerYAnchor, constant: 90).isActive = true
//                        break
//                    case 3:
//                        //bottom left
//                        self.videoPlayerView.addSubview(self.uniqueIDLabel)
//                        self.uniqueIDLabel.leftAnchor.constraint(equalTo: self.videoPlayerView.leftAnchor, constant: 30).isActive = true
//                        self.uniqueIDLabel.centerYAnchor.constraint(equalTo: self.videoPlayerView.centerYAnchor, constant: -90).isActive = true
//                        break
//                    case 4:
//                        //bottom right
//                        self.videoPlayerView.addSubview(self.uniqueIDLabel)
//                        self.uniqueIDLabel.rightAnchor.constraint(equalTo: self.videoPlayerView.rightAnchor, constant: -30).isActive = true
//                        self.uniqueIDLabel.bottomAnchor.constraint(equalTo: self.videoPlayerView.bottomAnchor, constant: -90).isActive = true
//                        break
//                    case 5...8:
//                        break
//                    default:
//                        break
//                }
//            }
//    }
//
//    //Screen tap to toggle the controls view
//    @objc func controlsViewTapped(){
//        if (!isControlsViewVisible){
//            controlsView.isHidden = false
//            resetIdleTimer()
//        }else{
//            controlsView.isHidden = true
//            idleTrackingTimer.invalidate()
//        }
//        isControlsViewVisible = !isControlsViewVisible
//    }
//
//    //Reset timer to hide the controls view when idle
//    func resetIdleTimer() {
//       // print("sawsank: bitrate \(playerItem.preferredPeakBitRate)")
//        idleTrackingTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { timer in
//            print("reset")
//            if(self.isControlsViewVisible){
//                self.controlsView.isHidden = true
//                self.isControlsViewVisible = false
//            }
//        }
//    }
//
//    private func setupGradientLayer(){
//        gradientLayer.colors = [UIColor.black.cgColor, UIColor.clear.cgColor, UIColor.black.cgColor]
//        gradientLayer.locations = [-0.4, 0.7, 1.2]
//        controlsView.layer.addSublayer(gradientLayer)
//    }
//
//    @objc func handleVideoScrubbing(){
//        if let duration = player.currentItem?.duration {
//            let totalSeconds = CMTimeGetSeconds(duration)
//            let value = Float64(videoScrubber.value) * totalSeconds
//            let seekTime = CMTime(value: Int64(value), timescale: 1)
//
//            player.seek(to: seekTime)
//        }
//
//
//    }
//
//    func popViewController(updateRatingBar: Bool, rating: Double) {
//        dismissVC()
//    }
//
//    func ratingComplete(title: String, message: String, rating: Double) {
//        dismissVC()
//    }
//
//    func showRatingModalVC(){
//        player.pause()
//        let ratingPopUpVC = MovieRatingViewController.instantiate(fromAppStoryboard: .MovieRating)
//        ratingPopUpVC.modalPresentationStyle = .overCurrentContext
//        ratingPopUpVC.delegate = self
//        ratingPopUpVC.contentID = self.contentId
//        self.present(ratingPopUpVC, animated: true, completion: nil)
//
//    }
//
//    @objc func handlePlayPause(){
//        if videoIsPlaying == LocalPlayerState.playing {
//            idleTrackingTimer.invalidate()
//            player.pause()
//            videoIsPlaying = LocalPlayerState.paused
//            playButton.setImage(UIImage(named: "play_circle"), for: .normal)
//        }else{
//            controlsView.isHidden = true
//            player.play()
//            videoIsPlaying = LocalPlayerState.playing
//            playButton.setImage(UIImage(named: "pause_circle"), for: .normal)
//        }
//    }
//
//    func showSplashScreen(){
////        player.
//    }
//
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//
//        //Playback time value observer
//        if keyPath == "currentItem.loadedTimeRanges" {
//            activityIndicatorView.stopAnimating()
//            controlsView.backgroundColor = .clear
//            playButton.isHidden = false
//            if let duration = player.currentItem?.duration {
//                let seconds = CMTimeGetSeconds(duration)
//
//                var secondsText = ""
//                secondsText = String(format: "%02d", Int(Int(seconds) % 60))
//                var minutesText = ""
//                minutesText = String(format: "%02d", Int(Int(seconds) / 60))
//                videoLengthLabel.text = "\(minutesText):\(secondsText)"
//
//            }
//
//
//            if #available(iOS 11.0, *) {
//                let isCaptured = UIScreen.main.isCaptured
//                if(isCaptured){
//                    screenBlock()
//                }
//            } else {
//                // Fallback on earlier versions
//            }
//
//
//        }
//
//        //Screen Recorder value observer
//        if keyPath == "captured" {
//            if #available(iOS 11.0, *) {
//                let isCaptured = UIScreen.main.isCaptured
//                if(isCaptured){
//                    screenBlock()
//                }
//            } else {
//                // Fallback on earlier versions
//            }
//
//        }
//
//        //Buffer Observers
//        if object is AVPlayerItem {
//            switch keyPath {
//                case "playbackBufferEmpty":
//                    // Show loader
//                    activityIndicatorView.startAnimating()
//
//                case "playbackLikelyToKeepUp":
//                    // Hide loader
//                    activityIndicatorView.stopAnimating()
//
//                case "playbackBufferFull":
//                    // Hide loader
//                    activityIndicatorView.stopAnimating()
//                default:
//                    activityIndicatorView.startAnimating()
//            }
//        }
//    }
//
//    func screenBlock(){
//        player.pause()
//        let controller = UIAlertController(title: "Notice", message: "We have detected a recorder or screen mirroring. This action is not allowed by our policy. Please close the recorder or screen mirroring and try again.", preferredStyle: .alert)
//        let ok = UIAlertAction(title: "Okay", style: .cancel, handler: { (action) -> Void in
//            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
//            self.navigationController?.popViewController(animated: true);
//
//        })
//        controller.addAction(ok)
//        self.present(controller, animated: true, completion: nil)
//    }
//
//
//    override func viewDidDisappear(_ animated: Bool) {
//        videoPlayerAppDelegate.myOrientation = .portrait
//        let value = UIInterfaceOrientation.portrait.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
//        player.pause()
//        watermarkTimer.invalidate()
//    }
//
//    @objc func dismissVC(){
//
//        if(!isComingSoon){
//            if(!isRated){
//                self.showRatingModalVC()
//                isRated = true
//            }
//        }
//
//        videoPlayerAppDelegate.myOrientation = .portrait
//        let value = UIInterfaceOrientation.portrait.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
//        UIViewController.attemptRotationToDeviceOrientation()
//
//        if (mediaType == .series){
//            self.navigationController?.backToViewController(viewController: SeriesDetailViewController.self)
//        }else{
////            self.navigationController?.backToViewController(viewController: MovieDetailViewController.self)
//            self.navigationController?.backToViewController(viewController: NewMovieDetailsVC.self)
//
//        }
//        watermarkTimer.invalidate()
//    }
//
//
//    // MARK: - GCKSessionManagerListener
//
//    func sessionManager(_: GCKSessionManager, didStart session: GCKSession) {
//      print("MediaViewController: sessionManager didStartSession \(session)")
//      switchToRemotePlayback()
//    }
//
//    func sessionManager(_: GCKSessionManager, didResumeSession session: GCKSession) {
//      print("MediaViewController: sessionManager didResumeSession \(session)")
//      switchToRemotePlayback()
//    }
//
//    func sessionManager(_: GCKSessionManager, didEnd _: GCKSession, withError error: Error?) {
//      print("session ended with error: \(String(describing: error))")
//      let message = "The Casting session has ended.\n\(String(describing: error))"
//        if let window = appDelegate.window {
//        Toast.displayMessage(message, for: 3, in: window)
//      }
//      switchToLocalPlayback()
//    }
//
//    func sessionManager(_: GCKSessionManager, didFailToStartSessionWithError error: Error?) {
//      if let error = error {
//        if let window = appDelegate.window {
//          Toast.displayMessage("Failed to start a session\n " + error.localizedDescription, for: 3, in: window)
//        }
//      }
//    }
//
//    func sessionManager(_: GCKSessionManager,
//                        didFailToResumeSession _: GCKSession, withError _: Error?) {
//      if let window = UIApplication.shared.delegate?.window {
//        Toast.displayMessage("The Casting session could not be resumed.",
//                             for: 3, in: window)
//      }
//      switchToLocalPlayback()
//    }
//
//}
//
////Google Cast Fuctionalities
//extension CustomVideoViewController {
//
//
//
//    func populateMediaInfo(_ autoPlay: Bool, playPosition: TimeInterval) {
//      print("populateMediaInfo")
////      _titleLabel.text = mediaInfo?.metadata?.string(forKey: kGCKMetadataKeyTitle)
//      var subtitle = mediaInfo?.metadata?.string(forKey: kGCKMetadataKeyArtist)
//      if subtitle == nil {
//        subtitle = mediaInfo?.metadata?.string(forKey: kGCKMetadataKeyStudio)
//      }
////      _subtitleLabel.text = subtitle
////      let description = mediaInfo?.metadata?.string(forKey: kMediaKeyDescription)
////      _descriptionTextView.text = description?.replacingOccurrences(of: "\\n", with: "\n")
////      _localPlayerView.loadMedia(mediaInfo, autoPlay: autoPlay, playPosition: playPosition)
//    }
//
//    func switchToRemotePlayback() {
//      print("switchToRemotePlayback; mediaInfo is \(String(describing: mediaInfo))")
//      if playbackMode == .remote {
//        return
//      }
//    let timeToPlayInSeconds = Double(SessionManager().getCurrentPlayingTime(id: String(contentId)))
//      // If we were playing locally, load the local media on the remote player
//      if playbackMode == .local, (videoIsPlaying != .stopped), (mediaInfo != nil) {
//        print("loading media: \(String(describing: mediaInfo?.contentURL))")
//        let paused: Bool = (videoIsPlaying == .paused)
//        let mediaQueueItemBuilder = GCKMediaQueueItemBuilder()
//        mediaQueueItemBuilder.mediaInformation = mediaInfo
//        mediaQueueItemBuilder.autoplay = !paused
//        mediaQueueItemBuilder.preloadTime = timeToPlayInSeconds
//        mediaQueueItemBuilder.startTime = timeToPlayInSeconds
//        let mediaQueueItem = mediaQueueItemBuilder.build()
//
//        let queueDataBuilder = GCKMediaQueueDataBuilder(queueType: .generic)
//        queueDataBuilder.items = [mediaQueueItem]
//        queueDataBuilder.repeatMode = .off
//
//        let mediaLoadRequestDataBuilder = GCKMediaLoadRequestDataBuilder()
//        mediaLoadRequestDataBuilder.queueData = queueDataBuilder.build()
//
//        let request = sessionManager.currentCastSession?.remoteMediaClient?.loadMedia(with: mediaLoadRequestDataBuilder.build())
//        request?.delegate = self
//      }
//        player.replaceCurrentItem(with: nil)
//        GCKCastContext.sharedInstance().presentDefaultExpandedMediaControls()
////      _localPlayerView.showSplashScreen()
//      sessionManager.currentCastSession?.remoteMediaClient?.add(self)
//      playbackMode = .remote
//    }
//
//    func switchToLocalPlayback() {
//      print("switchToLocalPlayback")
//      if playbackMode == .local {
//        return
//      }
//      var playPosition: TimeInterval = 0
//      var paused: Bool = false
//      var ended: Bool = false
//      if playbackMode == .remote {
//        playPosition = castMediaController.lastKnownStreamPosition
//        paused = (castMediaController.lastKnownPlayerState == .paused)
//        ended = (castMediaController.lastKnownPlayerState == .idle)
//        print("last player state: \(castMediaController.lastKnownPlayerState), ended: \(ended)")
//      }
//        seekToPosition(position: Float(playPosition))
//      populateMediaInfo((!paused && !ended), playPosition: playPosition)
//      sessionManager.currentCastSession?.remoteMediaClient?.remove(self)
//      playbackMode = .local
//    }
//
//    @objc func playSelectedItemRemotely() {
//        self.loadSelectedItem(byAppending: false)
//        GCKCastContext.sharedInstance().presentDefaultExpandedMediaControls()
//    }
//
//        @objc func enqueueSelectedItemRemotely() {
//        self.loadSelectedItem(byAppending: true)
//    //    let message = "Added \"\(self.mediaInfo?.metadata?.string(forKey: kGCKMetadataKeyTitle) ?? "")\" to queue."
//        if let window = UIApplication.shared.delegate?.window {
////          Toast.displayMessage(message, for: 3, in: window)
//        }
//      }
//
//    /**
//     * Loads the currently selected item in the current cast media session.
//     * @param appending If YES, the item is appended to the current queue if there
//     * is one. If NO, or if
//     * there is no queue, a new queue containing only the selected item is created.
//     */
//
//    func loadSelectedItem(byAppending appending: Bool) {
//      print("enqueue item \(String(describing: self.mediaInfo))")
//      if let remoteMediaClient = GCKCastContext.sharedInstance().sessionManager.currentCastSession?.remoteMediaClient {
//        let builder = GCKMediaQueueItemBuilder()
//        builder.mediaInformation = self.mediaInfo
//        builder.autoplay = true
//        builder.preloadTime = 0
//        let item = builder.build()
//        if ((remoteMediaClient.mediaStatus) != nil) && appending {
//          let request = remoteMediaClient.queueInsert(item, beforeItemWithID: kGCKMediaQueueInvalidItemID)
//          request.delegate = self
//        } else {
//          let repeatMode = remoteMediaClient.mediaStatus?.queueRepeatMode ?? .off
//          let request = remoteMediaClient.queueLoad([item], start: 0, playPosition: 0,
//                                                                              repeatMode: repeatMode, customData: nil)
//
//          request.delegate = self
//        }
//      }
//    }
//
//    @objc func castDeviceDidChange(_ notification: Notification) {
//      if GCKCastContext.sharedInstance().castState != .noDevicesAvailable {
//        // You can present the instructions on how to use Google Cast on
//        // the first time the user uses you app
//        GCKCastContext.sharedInstance().presentCastInstructionsViewControllerOnce(with: GCKUICastButton())
//      }
//    }
//
//}

//
//  CustomVideoViewController.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 4/22/20.
//  Copyright © 2020 Cinemaghar. All rights reserved.
//

import UIKit
import AVFoundation
import GoogleCast
import AVKit
import MediaPlayer
import MediaWatermark
import CoreImage.CIFilterBuiltins
import Alamofire

let videoPlayerAppDelegate = UIApplication.shared.delegate as! AppDelegate

/* The player state. */
//enum PlaybackMode: Int {
//  case none = 0
//  case local
//  case remote
//}

class CustomVideoViewController: UIViewController, RatingModalDelegate, GCKSessionManagerListener,
GCKRemoteMediaClientListener, GCKRequestDelegate  {
    
   
    //Player
    @IBOutlet weak var videoPlayerView: UIView!
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    var playerItem: AVPlayerItem!
    let airplayView = UIView()
    
    //Advertisements
    var advertisement: Advertisements!
    @IBOutlet weak var advertisementViewer: UIView!
    @IBOutlet weak var advertImageView: UIImageView!
    var advertPlayer: AVPlayer!
    var advertPlayerLayer: AVPlayerLayer!
    
    //Cast Media Controller UI
    private var castMediaController: GCKUIMediaController!
    
    //Cast Volume Controller
    private var volumeController: GCKUIDeviceVolumeController!
    
    //Player State remote|local
    private var playbackMode = PlaybackMode.none
    
    //Timer for Watermark Position Change
    var watermarkTimer: Timer!
    
    //Timer for idle tracking
    var idleTrackingTimer: Timer!
    
    //Watermark UniqueID Label
    let uniqueIDLabel = UILabel()
   
    //let uniqueIDLabel = UIView()
    
    
    // Controls
    let controlsView = UIView()
    let activityIndicatorView = UIActivityIndicatorView()
    let playButton = UIButton(type: .system)
    let forwardJumpButton = UIButton(type: .system)
    let backJumpButton = UIButton(type: .system)
    var videoIsPlaying = LocalPlayerState.playing
    let videoLengthLabel = UILabel()
    let videoScrubber = UISlider()
    let currentTimeLabel = UILabel()
    let gradientLayer = CAGradientLayer()
    let backButton = UIButton(type: .system)
    let titleLabel = UILabel()
    var castButton = GCKUICastButton()
    let resizeVideo = UIButton()
    var qualityControlButton = UIButton()
    var isControlsViewVisible = true
    
    //Media Info
    var contentId: Int = 0
    var urlString: String = ""
    var videoTitle: String = ""
    var mediaType: MediaType = .movie
    var uniqueID: String = ""
    var mediaInfo: GCKMediaInformation? {
      didSet {
        print("setMediaInfo: \(String(describing: mediaInfo))")
      }
    }
    var isRated: Bool = false
    var isComingSoon: Bool = false
    
    //Seek Duration
    var seekDuration = 10.0

    //IsFullscreen
    var isFullscreen = false
    
    var playOffline = false
    var offlineItem: AVPlayerItem?
    
    
    //Google cast session manager
    private var sessionManager: GCKSessionManager!
    
    
    var isResetEdgesOnDisappear: Bool = false
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.castMediaController = GCKUIMediaController()
        sessionManager = GCKCastContext.sharedInstance().sessionManager
        self.volumeController = GCKUIDeviceVolumeController()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(preventScreenRecording), name: NSNotification.Name.UIScreenCapturedDidChange, object: nil)

        videoPlayerAppDelegate.myOrientation = .landscapeRight
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        handleAdvertisement()
        setupAVPlayer()
        
        setupControlsView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.castDeviceDidChange),
                    name: NSNotification.Name.gckCastStateDidChange,
                    object: GCKCastContext.sharedInstance())
        
   // airplaySetup()
    }
    @objc func preventScreenRecording() {
//        if UIScreen.main.isCaptured == true {
//            print("is captured")
//            player.allowsExternalPlayback = false
//        } else {
//            print("not captured")
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        seekToPosition(position: SessionManager().getCurrentPlayingTime(id: String(contentId)))
        //Observable to observe screenshots taken
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationUserDidTakeScreenshot, object: nil, queue: OperationQueue.main) { notification in
            
            print("Screenshot taken!")
        }
        
        //Observable to observe screen recorder
        UIScreen.main.addObserver(self, forKeyPath: "captured", options: .new, context: nil)
    }
//    func airplaySetup() {
//           // airplay.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//            let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
//
//            let routerPickerView = AVRoutePickerView(frame: buttonView.bounds)
//            routerPickerView.tintColor = UIColor.black
//            routerPickerView.activeTintColor = .green
//            buttonView.addSubview(routerPickerView)
//            self.airPlayView.addSubview(buttonView)
//       // videoPlayerView.bringSubview(toFront: airPlayView)
//
//    }
    
    func handleAdvertisement(){
        if(advertisement != nil){
            videoPlayerView.isHidden = true
            if(advertisement.image != ""){
                print("hasAdvert: image")
                
                
                if  let advertImage = advertisement.image{
                    
                    advertImageView.sd_setImage(with: URL(string: advertImage), placeholderImage: UIImage(named: "placeholder_cell"), options: [.continueInBackground, .progressiveLoad])
                   

                }
                advertisementViewer.addSubview(advertImageView)
                //Center the activity indicator view
                advertImageView.leftAnchor.constraint(equalTo: advertisementViewer.leftAnchor).isActive = true
                advertImageView.topAnchor.constraint(equalTo: advertisementViewer.topAnchor).isActive = true
                advertImageView.rightAnchor.constraint(equalTo: advertisementViewer.rightAnchor).isActive = true
                advertImageView.bottomAnchor.constraint(equalTo: advertisementViewer.bottomAnchor).isActive = true
                //Show Countdown
                let countdownView = UIView()
                countdownView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
                
                let countdownLabel = UILabel()
                countdownLabel.textColor = UIColor.white
                countdownLabel.font = UIFont.boldSystemFont(ofSize: 20)
                var secondsRemaining = 9
                Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
                        if secondsRemaining > 0 {
                            countdownLabel.text = "Playing in \(String(secondsRemaining))..."
                            secondsRemaining -= 1
                        } else {
                            Timer.invalidate()
                        }
                    }
                countdownLabel.translatesAutoresizingMaskIntoConstraints = false
                countdownView.addSubview(countdownLabel)
                countdownLabel.centerXAnchor.constraint(equalTo: countdownView.centerXAnchor).isActive = true
                countdownLabel.centerYAnchor.constraint(equalTo: countdownView.centerYAnchor).isActive = true
                countdownView.translatesAutoresizingMaskIntoConstraints = false
                advertisementViewer.addSubview(countdownView)
                //Place the countdown in advertisementViewer
                countdownView.bottomAnchor.constraint(equalTo: advertisementViewer.bottomAnchor, constant: -24).isActive = true
                countdownView.rightAnchor.constraint(equalTo: advertisementViewer.rightAnchor, constant: -24).isActive = true
                countdownView.heightAnchor.constraint(equalToConstant: 40).isActive = true
                countdownView.widthAnchor.constraint(equalToConstant: 200).isActive = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                    self.videoPlayerView.isHidden = false
                    self.advertisementViewer.isHidden = true
                    self.player.play()
                }
                
            }else if(advertisement.video != ""){
                print("hasAdvert: video")
                advertImageView.isHidden = true
                self.advertPlayer = AVPlayer(url: URL(string: advertisement.video)!)
                self.advertPlayerLayer = AVPlayerLayer(player: advertPlayer)
                advertPlayerLayer.videoGravity = .resizeAspectFill
                advertisementViewer.layer.addSublayer(advertPlayerLayer)
                advertPlayer.play()
                NotificationCenter.default.addObserver(self, selector: #selector(advetisementDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
            }
        }else{
            advertisementViewer.isHidden = true
            
        }
    }
    
    @objc func advetisementDidFinishPlaying(){
        print("hasAdvert: video finished playing")
        advertisementViewer.isHidden = true
        videoPlayerView.isHidden = false
        self.player.play()
    }
    func getAttributedStrings(text: String) -> [NSAttributedString]
    {
        let words:[String] = text.components(separatedBy: " , ")

        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.backgroundColor: UIColor.blue]

        let attribWords = words.map({
            return NSAttributedString(string: " \($0) ", attributes: attributes)
        })
        return attribWords
    }
    
    
    func seekToPosition(position: Float){
        if (position > 0.0){
            let timeToPlayInSeconds = Double(SessionManager().getCurrentPlayingTime(id: String(contentId)))
            let timeToPlay = CMTime(seconds: timeToPlayInSeconds, preferredTimescale: 1)
            self.player.seek(to: timeToPlay)
            self.resetIdleTimer()
        }else{
            self.resetIdleTimer()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let hasConnectedSession: Bool = (sessionManager.hasConnectedSession())
        if hasConnectedSession, (playbackMode != .remote) {
//          populateMediaInfo(false, playPosition: 0)
          switchToRemotePlayback()
        } else if sessionManager.currentSession == nil, (playbackMode != .local) {
          switchToLocalPlayback()
        }

        sessionManager.add(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        sessionManager.remove(self)
        player.pause()
        sendWatchedDuration()
        super.viewWillDisappear(animated)
    }
        
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        playerLayer.frame = videoPlayerView.bounds
        if(advertisement != nil){
            if(advertisement.video != ""){
                advertPlayerLayer.frame = self.advertisementViewer.bounds
            }
        }
       
        controlsView.frame = self.videoPlayerView.bounds
        gradientLayer.frame = videoPlayerView.bounds
        
        
    }
    
    func sendWatchedDuration(){
        APIHandler.shared.saveWatchedDuration(duration: 100, contentId: 100) { (status) in
            print("Uploading duration")
            print(status)
        } failure: { (e) in
            print("Failed to upload duration")
            print(e)
        }
    }
    
    func createHeaderForAVPlayer() -> [String: String]{
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let deviceName = UIDevice.current.name
        let headers = ["Accept": "application/json", "X-VERSION": "1.1", "X-DEVICE-TYPE": "ios", "X-DEVICE-ID": deviceID, "X-DEVICE-MODEL": deviceName, "Authorization": SessionManager().getAccessToken() ?? ""]
        
        return headers
    }
    func playVideo(url: URL, view: UIView) {
        playerLayer?.removeFromSuperlayer()
        player = AVPlayer(url: url)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = view.bounds
        view.layer.addSublayer(playerLayer)
        player.play()
    }
    
    func setupAVPlayer(){
        if playOffline == true {
            print(offlineItem)
            playerLayer = AVPlayerLayer()
            playerLayer.frame = self.view.bounds
            player = AVPlayer(playerItem: offlineItem)
//            playerLayer.player = player
//            self.view.layer.addSublayer(playerLayer)
//            playerLayer.player?.play()
        } else {
            let url = mediaInfo?.contentURL

            let _header = createHeaderForAVPlayer()
            let asset = AVURLAsset(url: url!, options: ["AVURLAssetHTTPHeaderFieldsKey": _header])
            playerItem = AVPlayerItem(asset: asset)
            player = AVPlayer(playerItem: playerItem)
            
//            let url = mediaInfo?.contentURL
//            let _header = createHeaderForAVPlayer()
//            print(url)
////            let asset = AVURLAsset(url: url!, options: ["AVURLAssetHTTPHeaderFieldsKey": _header])
//            let asset = AVURLAsset(url: url!)
//            guard let path = Bundle.main.path(forResource: "SampleVideo", ofType:"mp4") else {
//                       debugPrint("video.m4v not found")
//                       return
//                   }
//            let waterfallAsset = AVAsset(url: URL(fileURLWithPath: path))
//
//            let titleComposition = AVMutableVideoComposition(asset: waterfallAsset) { request in
//            //Create a white shadow for the text
//            let whiteShadow = NSShadow()
//            whiteShadow.shadowBlurRadius = 5
//            whiteShadow.shadowColor = UIColor.white
//            let attributes = [
//              NSAttributedString.Key.foregroundColor : UIColor.blue,
//              NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10),
//              NSAttributedString.Key.shadow : whiteShadow
//            ]
//            //Create an Attributed String
//            let waterfallText = NSAttributedString(string: "Waterfall!", attributes: attributes)
//            //Convert attributed string to a CIImage
//
//                if #available(iOS 13.0, *) {
//                    let textfilter = CIFilter.attributedTextImageGenerator()
//                    textfilter.text = waterfallText
//                    textfilter.scaleFactor = 4.0
//                //Center text and move 200 px from the origin
//                //source image is 720 x 1280
//                let positionedText = textfilter.outputImage!.transformed(by: CGAffineTransform(translationX: (request.renderSize.width - textfilter.outputImage!.extent.width)/2, y: 200))
//                //Compose text over video image
//                request.finish(with: positionedText.composited(over: request.sourceImage), context: nil)
//                } else {
//                    // Fallback on earlier versions
//                }
//
//
//            }
            //addingWaterMarknew()
            
//            playerItem = AVPlayerItem(asset: asset)
//            playerItem.videoComposition = titleComposition
//            player = AVPlayer(playerItem: playerItem)
//
            
           
        }
      
        
        
        //Interval for periodic time observer
        let interval = CMTime(value: 1, timescale: 10)
        
        
        
        //Add observer
        //Buffer observer
        player.currentItem!.addObserver(self, forKeyPath: "playbackBufferEmpty", options: .new, context: nil)
        player.currentItem!.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: .new, context: nil)
        player.currentItem!.addObserver(self, forKeyPath: "playbackBufferFull", options: .new, context: nil)
        //Time Observer
        player.addObserver(self, forKeyPath: "currentItem.loadedTimeRanges", options: .new, context: nil)
        player.addPeriodicTimeObserver(forInterval: interval, queue: nil) { (progressTime) in
            let seconds = CMTimeGetSeconds(progressTime)
            SessionManager().setCurrentPlayingTime(id: String(self.contentId), duration: seconds)
            var secondsText = ""
            secondsText = String(format: "%02d", Int(Int(seconds) % 60))
            var minutesText = ""
            minutesText = String(format: "%02d", Int(Int(seconds) / 60))
            self.currentTimeLabel.text = "\(minutesText):\(secondsText)"
            
            //moving the scrubber
            if let duration = self.player.currentItem?.duration {
                let durationSeconds = CMTimeGetSeconds(duration)
                
                self.videoScrubber.value = Float(seconds / durationSeconds)
                
                
                //show the Controls view when the video ends
                if (seconds == durationSeconds){
                    self.controlsView.isHidden = false
                    self.videoIsPlaying = LocalPlayerState.stopped
                    self.playButton.setImage(UIImage(named: "play_circle"), for: .normal)
                    self.showRatingModalVC()
                }
            }
        }
        //End Observer
        player.pause()
        
        
        //Adding video layer
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspect
        
        videoPlayerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        videoPlayerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        videoPlayerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        videoPlayerView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        videoPlayerView.layer.addSublayer(playerLayer)
        
        //new watermark method
       
        
        
    }
    

    func setupControlsView(){
//        //Adding watermark view
        uniqueIDLabel.text = UserDefaults.standard.string(forKey: AppConstants.uniqueId)
        uniqueIDLabel.textColor = UIColor(white: 1, alpha: 0.2)
//        uniqueIDLabel.font = UIFont.boldSystemFont(ofSize: 18)
        uniqueIDLabel.font = UIFont.init(name: "PingFangTC-Regular", size: 14)
        uniqueIDLabel.translatesAutoresizingMaskIntoConstraints = false
        uniqueIDLabel.textAlignment = .center
        //Anchor to the top left corner
        videoPlayerView.addSubview(uniqueIDLabel)
        uniqueIDLabel.leftAnchor.constraint(equalTo: videoPlayerView.leftAnchor, constant: 3).isActive = true
        uniqueIDLabel.topAnchor.constraint(equalTo: videoPlayerView.topAnchor, constant: 90).isActive = true
        
        //Adding watermark view
        let id = getAttributedStrings(text: UserDefaults.standard.string(forKey: AppConstants.uniqueId)!)
        //Anchor to the top left corner
        //let item = MediaItem(asset: <#AVURLAsset#>)
        let idlbl = MediaElement(text: id.first!)
       
       
        
        
        //Change watermark position periodically
        watermarkRevolver()
        
        //Setting up gradient view
        setupGradientLayer()
        
        //loading indicator view
        activityIndicatorView.activityIndicatorViewStyle = .whiteLarge
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorView.startAnimating()
        controlsView.addSubview(activityIndicatorView)
        //Center the activity indicator view
        activityIndicatorView.centerXAnchor.constraint(equalTo: controlsView.centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: controlsView.centerYAnchor).isActive = true
        activityIndicatorView.widthAnchor.constraint(equalToConstant: 75).isActive = true
        activityIndicatorView.heightAnchor.constraint(equalToConstant: 75).isActive = true
        
        // play/pause button
        let playButtonImage = UIImage(named: "pause_circle")
        playButton.setImage(playButtonImage, for: .normal)
        playButton.translatesAutoresizingMaskIntoConstraints = false
        playButton.tintColor = UIColor.init(hex: "#FFFFFF")
        playButton.isHidden = true
        playButton.addTarget(self, action: #selector(handlePlayPause), for: .touchUpInside)
        controlsView.addSubview(playButton)
        //Center the play button
        playButton.centerXAnchor.constraint(equalTo: controlsView.centerXAnchor).isActive = true
        playButton.centerYAnchor.constraint(equalTo: controlsView.centerYAnchor).isActive = true
        playButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        playButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        //Video Length Label
        videoLengthLabel.text = "--:--:--"
        videoLengthLabel.textColor = .white
        videoLengthLabel.font = UIFont.boldSystemFont(ofSize: 14)
        videoLengthLabel.textAlignment = .right
        videoLengthLabel.translatesAutoresizingMaskIntoConstraints = false
        controlsView.addSubview(videoLengthLabel)
        //Anchor the label to bottom right of controlsView
        videoLengthLabel.rightAnchor.constraint(equalTo: controlsView.rightAnchor, constant: -8).isActive = true
        videoLengthLabel.bottomAnchor.constraint(equalTo: controlsView.bottomAnchor, constant: -6).isActive = true
        
        //Current time label
        currentTimeLabel.text = "--:--:--"
        currentTimeLabel.textColor = .white
        currentTimeLabel.font = UIFont.boldSystemFont(ofSize: 14)
        currentTimeLabel.textAlignment = .left
        currentTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        controlsView.addSubview(currentTimeLabel)
        //Anchor to the bottom left of the controls view
        currentTimeLabel.leftAnchor.constraint(equalTo: controlsView.leftAnchor, constant: 8).isActive = true
        currentTimeLabel.bottomAnchor.constraint(equalTo: controlsView.bottomAnchor, constant: -6).isActive = true
        
        //Video Scrubber
        videoScrubber.translatesAutoresizingMaskIntoConstraints = false
        videoScrubber.minimumTrackTintColor = UIColor.init(hex: "#FFFFFF")
        videoScrubber.thumbTintColor = UIColor.init(hex: "#FFFFFF")
        videoScrubber.maximumTrackTintColor = .white
        //handle scrubbing
        videoScrubber.addTarget(self, action: #selector(handleVideoScrubbing), for: .valueChanged)
        //Place scrubber into controls view
        controlsView.addSubview(videoScrubber)
        videoScrubber.rightAnchor.constraint(equalTo: videoLengthLabel.leftAnchor, constant: -3).isActive = true
        videoScrubber.bottomAnchor.constraint(equalTo: controlsView.bottomAnchor).isActive = true
        videoScrubber.leftAnchor.constraint(equalTo: currentTimeLabel.rightAnchor, constant: 3).isActive = true
        videoScrubber.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //Adding back button
        backButton.setImage(UIImage(named: "back_arrow"), for: .normal)
        backButton.setTitle("Back", for: .normal)
        backButton.tintColor = .white
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        controlsView.addSubview(backButton)
        //Anchoring to the top left corner
        backButton.leftAnchor.constraint(equalTo: controlsView.leftAnchor, constant: 8).isActive = true
        backButton.topAnchor.constraint(equalTo: controlsView.topAnchor, constant: 16).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
        backButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: -10)
        
        //Adding title label
        titleLabel.text = videoTitle
        titleLabel.textColor = .white
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textAlignment = .center
        controlsView.addSubview(titleLabel)
        //Anchoring to the top middle
        titleLabel.topAnchor.constraint(equalTo: controlsView.topAnchor, constant: 16).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: backButton.rightAnchor, constant: 8).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: controlsView.centerXAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        controlsView.backgroundColor = UIColor(white: 0, alpha: 1)
        
        //Adding Google Cast Button
        let viewWidth = self.view!.bounds.height
        castButton = GCKUICastButton(frame: CGRect(x: viewWidth - 75, y: 16, width: 24, height: 24))
        castButton.tintColor = UIColor.gray
        controlsView.addSubview(castButton)
        //Anchoring to the top right
        castButton.topAnchor.constraint(equalTo: controlsView.topAnchor, constant: 32).isActive = true
        castButton.rightAnchor.constraint(equalTo: controlsView.rightAnchor, constant: 8).isActive = true
        castButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
        castButton.widthAnchor.constraint(equalToConstant: 24).isActive = true
        
        
        //Adding quality control setting button
        let originalMenuIcon = UIImage(named: "icons8-menu-vertical")
        originalMenuIcon?.withRenderingMode(.alwaysTemplate)
        qualityControlButton = UIButton(frame: CGRect(x: viewWidth - 36, y: 16, width: 24, height: 24))
        qualityControlButton.setImage(originalMenuIcon, for: .normal)
        qualityControlButton.tintColor = UIColor.white
        
        controlsView.addSubview(qualityControlButton)
        
        //Anchoring to the top right
        qualityControlButton.topAnchor.constraint(equalTo: controlsView.topAnchor, constant: 24).isActive = true
        qualityControlButton.rightAnchor.constraint(equalTo: controlsView.rightAnchor, constant: 24).isActive = true
        qualityControlButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
        qualityControlButton.widthAnchor.constraint(equalToConstant: 24).isActive = true
        
        
        //MARK:- Resize video
        resizeVideo.setImage(UIImage(named: "fullscreen"), for: .normal)
        resizeVideo.translatesAutoresizingMaskIntoConstraints = false
        resizeVideo.addTarget(self, action: #selector(handleResizeVideo), for: .touchUpInside)
        controlsView.addSubview(resizeVideo)

        //Anchoring resize button to the right of back button
        resizeVideo.topAnchor.constraint(equalTo: controlsView.topAnchor, constant: 16).isActive = true
        resizeVideo.rightAnchor.constraint(equalTo: castButton.leftAnchor, constant: -16).isActive = true
        resizeVideo.heightAnchor.constraint(equalToConstant: 24).isActive = true
        resizeVideo.widthAnchor.constraint(equalToConstant: 24).isActive = true
        
        //MARK: - AIRPLAY BUTTON
       
        //airplayView.backgroundColor = .clear
        let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        
        let routerPickerView = AVRoutePickerView(frame: buttonView.bounds)
        //let routerPickerView = AVRoutePickerView()
        routerPickerView.tintColor = .white
        routerPickerView.activeTintColor = .green
        airplayView.addSubview(routerPickerView)
        //MARK: - REMOVING AIRPLAY BUTTON
       // controlsView.addSubview(airplayView)
        airplayView.frame = CGRect(x: UIScreen.main.bounds.width - 165, y: 12, width: 30, height: 30)
//        airplayView.translatesAutoresizingMaskIntoConstraints = false
//        routerPickerView.translatesAutoresizingMaskIntoConstraints = false
//
//        routerPickerView.topAnchor.constraint(equalTo: airplayView.topAnchor, constant: 0).isActive = true
//        routerPickerView.rightAnchor.constraint(equalTo: airplayView.leftAnchor, constant: 0).isActive = true
//        routerPickerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
//        routerPickerView.widthAnchor.constraint(equalToConstant: 30).isActive = true
//
//        airplayView.topAnchor.constraint(equalTo: controlsView.topAnchor, constant: 16).isActive = true
//        airplayView.rightAnchor.constraint(equalTo: resizeVideo.leftAnchor, constant: -16).isActive = true
//        airplayView.heightAnchor.constraint(equalToConstant: 30).isActive = true
//        airplayView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        
        
        //Forward on  tap
        let forwardJumpImage = UIImage(named: "forward-10")
        forwardJumpButton.setImage(forwardJumpImage, for: .normal)
        forwardJumpButton.translatesAutoresizingMaskIntoConstraints = false
        forwardJumpButton.tintColor = UIColor.init(hex: "#FFFFFF")
        controlsView.addSubview(forwardJumpButton)
        
        forwardJumpButton.addTarget(self, action: #selector(seekForward), for: .touchUpInside)
        //Anchoring forward tap to the right of play button
        forwardJumpButton.centerYAnchor.constraint(equalTo: controlsView.centerYAnchor).isActive = true
        forwardJumpButton.leftAnchor.constraint(equalTo: playButton.rightAnchor, constant: 65).isActive = true
        forwardJumpButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        forwardJumpButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //Backward on  tap
        let backwardJumpImage = UIImage(named: "back-10")
        backJumpButton.setImage(backwardJumpImage, for: .normal)
        backJumpButton.translatesAutoresizingMaskIntoConstraints = false
        backJumpButton.tintColor = UIColor.init(hex: "#FFFFFF")
        controlsView.addSubview(backJumpButton)
        
        backJumpButton.addTarget(self, action: #selector(seekBackward), for: .touchUpInside)
        //Anchoring forward tap to the right of play button
        backJumpButton.centerYAnchor.constraint(equalTo: controlsView.centerYAnchor).isActive = true
        backJumpButton.rightAnchor.constraint(equalTo: playButton.leftAnchor, constant: -65).isActive = true
        backJumpButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        backJumpButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //Handle on screen tap
        let tapOnVideoPlayer = UITapGestureRecognizer(target: self, action: #selector(controlsViewTapped))
        videoPlayerView.addGestureRecognizer(tapOnVideoPlayer)
        
        videoPlayerView.addSubview(controlsView)
        
        
        
        
    }
    
    
    @objc func seekForward(){
        
        guard let duration  = player.currentItem?.duration else{
                return
            }
            let playerCurrentTime = CMTimeGetSeconds(player.currentTime())
            let newTime = playerCurrentTime + seekDuration

            if newTime < CMTimeGetSeconds(duration) {

                let time2: CMTime = CMTimeMake(Int64(newTime * 1000 as Float64), 1000)
                player.seek(to: time2)
            }
    }
    
    @objc func seekBackward(){
        
        let playerCurrentTime = CMTimeGetSeconds(player.currentTime())
        var newTime = playerCurrentTime - seekDuration

        if newTime < 0 {
            newTime = 0
        }
        let time2: CMTime = CMTimeMake(Int64(newTime * 1000 as Float64), 1000)
        player.seek(to: time2)
    }
    
   
    @objc func handleResizeVideo(){
        if(!isFullscreen){
            print("sawsank: going full screen")
            playerLayer!.frame = CGRect(x: 0,y: 0,width: self.view.frame.width, height: self.view.frame.height)
            playerLayer.videoGravity = .resizeAspectFill
            
            playerLayer.contentsScale = 1.2
            resizeVideo.setImage(UIImage(named: "fullscreen_exit"), for: .normal)
        }else{
            print("sawsank: leaving full screen")
            playerLayer.videoGravity = .resizeAspect
            playerLayer.contentsScale = 1
            resizeVideo.setImage(UIImage(named: "fullscreen"), for: .normal)
        }
        self.view.setNeedsLayout()
        isFullscreen = !isFullscreen

        
    }
    
    
    func watermarkRevolver(){
        //Timer for the revolver
        watermarkTimer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { timer in
            let randomNumber = Int.random(in: 1...8)
            print(randomNumber)
            self.uniqueIDLabel.removeFromSuperview()
                switch(randomNumber){
                    case 1:
                        //top left
                        self.videoPlayerView.addSubview(self.uniqueIDLabel)
                        self.uniqueIDLabel.leftAnchor.constraint(equalTo: self.videoPlayerView.leftAnchor, constant: 30).isActive = true
                        self.uniqueIDLabel.centerYAnchor.constraint(equalTo: self.videoPlayerView.centerYAnchor).isActive = true
                        break
                    case 2:
                        //top right
                        self.videoPlayerView.addSubview(self.uniqueIDLabel)
                        self.uniqueIDLabel.rightAnchor.constraint(equalTo: self.videoPlayerView.rightAnchor, constant: -30).isActive = true
                        self.uniqueIDLabel.centerYAnchor.constraint(equalTo: self.videoPlayerView.centerYAnchor, constant: 90).isActive = true
                        break
                    case 3:
                        //bottom left
                        self.videoPlayerView.addSubview(self.uniqueIDLabel)
                        self.uniqueIDLabel.leftAnchor.constraint(equalTo: self.videoPlayerView.leftAnchor, constant: 30).isActive = true
                        self.uniqueIDLabel.centerYAnchor.constraint(equalTo: self.videoPlayerView.centerYAnchor, constant: -90).isActive = true
                        break
                    case 4:
                        //bottom right
                        self.videoPlayerView.addSubview(self.uniqueIDLabel)
                        self.uniqueIDLabel.rightAnchor.constraint(equalTo: self.videoPlayerView.rightAnchor, constant: -30).isActive = true
                        self.uniqueIDLabel.bottomAnchor.constraint(equalTo: self.videoPlayerView.bottomAnchor, constant: -90).isActive = true
                        break
                    case 5...8:
                        break
                    default:
                        break
                }
            }
    }
    
    //Screen tap to toggle the controls view
    @objc func controlsViewTapped(){
        if (!isControlsViewVisible){
            controlsView.isHidden = false
            resetIdleTimer()
        }else{
            controlsView.isHidden = true
            idleTrackingTimer.invalidate()
        }
        isControlsViewVisible = !isControlsViewVisible
    }
    
    //Reset timer to hide the controls view when idle
    func resetIdleTimer() {
       // print("sawsank: bitrate \(playerItem.preferredPeakBitRate)")
        idleTrackingTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { timer in
            print("reset")
            if(self.isControlsViewVisible){
                self.controlsView.isHidden = true
                self.isControlsViewVisible = false
            }
        }
    }
    
    private func setupGradientLayer(){
        gradientLayer.colors = [UIColor.black.cgColor, UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.locations = [-0.4, 0.7, 1.2]
        controlsView.layer.addSublayer(gradientLayer)
    }
    
    @objc func handleVideoScrubbing(){
        if let duration = player.currentItem?.duration {
            let totalSeconds = CMTimeGetSeconds(duration)
            let value = Float64(videoScrubber.value) * totalSeconds
            let seekTime = CMTime(value: Int64(value), timescale: 1)
            
            player.seek(to: seekTime)
        }
        
        
    }
    
    func popViewController(updateRatingBar: Bool, rating: Double) {
        dismissVC()
    }
    
    func ratingComplete(title: String, message: String, rating: Double) {
        dismissVC()
    }
    
    func showRatingModalVC(){
        player.pause()
        let ratingPopUpVC = MovieRatingViewController.instantiate(fromAppStoryboard: .MovieRating)
        ratingPopUpVC.modalPresentationStyle = .overCurrentContext
        ratingPopUpVC.delegate = self
        ratingPopUpVC.contentID = self.contentId
        self.present(ratingPopUpVC, animated: true, completion: nil)
        
    }
    
    @objc func handlePlayPause(){
        if videoIsPlaying == LocalPlayerState.playing {
            idleTrackingTimer.invalidate()
            player.pause()
            videoIsPlaying = LocalPlayerState.paused
            playButton.setImage(UIImage(named: "play_circle"), for: .normal)
        }else{
            controlsView.isHidden = true
            player.play()
            videoIsPlaying = LocalPlayerState.playing
            playButton.setImage(UIImage(named: "pause_circle"), for: .normal)
        }
    }
    
    func showSplashScreen(){
//        player.
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        //Playback time value observer
        if keyPath == "currentItem.loadedTimeRanges" {
            activityIndicatorView.stopAnimating()
            controlsView.backgroundColor = .clear
            playButton.isHidden = false
            if let duration = player.currentItem?.duration {
                let seconds = CMTimeGetSeconds(duration)
                
                var secondsText = ""
                secondsText = String(format: "%02d", Int(Int(seconds) % 60))
                var minutesText = ""
                minutesText = String(format: "%02d", Int(Int(seconds) / 60))
                videoLengthLabel.text = "\(minutesText):\(secondsText)"
                
            }
            
            
            if #available(iOS 11.0, *) {
                let isCaptured = UIScreen.main.isCaptured
                if(isCaptured){
                    screenBlock()
                }
            } else {
                // Fallback on earlier versions
            }
            
            
        }
        
        //Screen Recorder value observer
        if keyPath == "captured" {
            if #available(iOS 11.0, *) {
                let isCaptured = UIScreen.main.isCaptured
                if(isCaptured){
                    screenBlock()
                }
            } else {
                // Fallback on earlier versions
            }
            
        }
            
        //Buffer Observers
        if object is AVPlayerItem {
            switch keyPath {
                case "playbackBufferEmpty":
                    // Show loader
                    activityIndicatorView.startAnimating()

                case "playbackLikelyToKeepUp":
                    // Hide loader
                    activityIndicatorView.stopAnimating()

                case "playbackBufferFull":
                    // Hide loader
                    activityIndicatorView.stopAnimating()
                default:
                    activityIndicatorView.startAnimating()
            }
        }
    }
    
    func screenBlock(){
        player.pause()
        let controller = UIAlertController(title: "Notice", message: "We have detected a recorder or screen mirroring. This action is not allowed by our policy. Please close the recorder or screen mirroring and try again.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Okay", style: .cancel, handler: { (action) -> Void in
            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
            self.navigationController?.popViewController(animated: true);

        })
        controller.addAction(ok)
        self.present(controller, animated: true, completion: nil)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        videoPlayerAppDelegate.myOrientation = .portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        player.pause()
        watermarkTimer.invalidate()
    }

    @objc func dismissVC(){
  
        if(!isComingSoon){
            if(!isRated){
                self.showRatingModalVC()
                isRated = true
            }
        }
                
        videoPlayerAppDelegate.myOrientation = .portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()

        if (mediaType == .series){
            self.navigationController?.backToViewController(viewController: SeriesDetailViewController.self)
        }else{
            //self.navigationController?.backToViewController(viewController: NewMovieDetailsVC.self)
            self.navigationController?.popViewController(animated: true)
        }
        watermarkTimer.invalidate()
    }
    
    
    // MARK: - GCKSessionManagerListener

    func sessionManager(_: GCKSessionManager, didStart session: GCKSession) {
      print("MediaViewController: sessionManager didStartSession \(session)")
      switchToRemotePlayback()
    }

    func sessionManager(_: GCKSessionManager, didResumeSession session: GCKSession) {
      print("MediaViewController: sessionManager didResumeSession \(session)")
      switchToRemotePlayback()
    }

    func sessionManager(_: GCKSessionManager, didEnd _: GCKSession, withError error: Error?) {
      print("session ended with error: \(String(describing: error))")
      let message = "The Casting session has ended.\n\(String(describing: error))"
        if let window = appDelegate.window {
        Toast.displayMessage(message, for: 3, in: window)
      }
      switchToLocalPlayback()
    }

    func sessionManager(_: GCKSessionManager, didFailToStartSessionWithError error: Error?) {
      if let error = error {
        if let window = appDelegate.window {
          Toast.displayMessage("Failed to start a session\n " + error.localizedDescription, for: 3, in: window)
        }
      }
    }

    func sessionManager(_: GCKSessionManager,
                        didFailToResumeSession _: GCKSession, withError _: Error?) {
      if let window = UIApplication.shared.delegate?.window {
        Toast.displayMessage("The Casting session could not be resumed.",
                             for: 3, in: window)
      }
      switchToLocalPlayback()
    }

}

//Google Cast Fuctionalities
extension CustomVideoViewController {
    
    

    func populateMediaInfo(_ autoPlay: Bool, playPosition: TimeInterval) {
      print("populateMediaInfo")
//      _titleLabel.text = mediaInfo?.metadata?.string(forKey: kGCKMetadataKeyTitle)
      var subtitle = mediaInfo?.metadata?.string(forKey: kGCKMetadataKeyArtist)
      if subtitle == nil {
        subtitle = mediaInfo?.metadata?.string(forKey: kGCKMetadataKeyStudio)
      }
//      _subtitleLabel.text = subtitle
//      let description = mediaInfo?.metadata?.string(forKey: kMediaKeyDescription)
//      _descriptionTextView.text = description?.replacingOccurrences(of: "\\n", with: "\n")
//      _localPlayerView.loadMedia(mediaInfo, autoPlay: autoPlay, playPosition: playPosition)
    }

    func switchToRemotePlayback() {
      print("switchToRemotePlayback; mediaInfo is \(String(describing: mediaInfo))")
      if playbackMode == .remote {
        return
      }
    let timeToPlayInSeconds = Double(SessionManager().getCurrentPlayingTime(id: String(contentId)))
      // If we were playing locally, load the local media on the remote player
      if playbackMode == .local, (videoIsPlaying != .stopped), (mediaInfo != nil) {
        print("loading media: \(String(describing: mediaInfo?.contentURL))")
        let paused: Bool = (videoIsPlaying == .paused)
        let mediaQueueItemBuilder = GCKMediaQueueItemBuilder()
        mediaQueueItemBuilder.mediaInformation = mediaInfo
        mediaQueueItemBuilder.autoplay = !paused
        mediaQueueItemBuilder.preloadTime = timeToPlayInSeconds
        mediaQueueItemBuilder.startTime = timeToPlayInSeconds
        let mediaQueueItem = mediaQueueItemBuilder.build()

        let queueDataBuilder = GCKMediaQueueDataBuilder(queueType: .generic)
        queueDataBuilder.items = [mediaQueueItem]
        queueDataBuilder.repeatMode = .off

        let mediaLoadRequestDataBuilder = GCKMediaLoadRequestDataBuilder()
        mediaLoadRequestDataBuilder.queueData = queueDataBuilder.build()

        let request = sessionManager.currentCastSession?.remoteMediaClient?.loadMedia(with: mediaLoadRequestDataBuilder.build())
        request?.delegate = self
      }
        player.replaceCurrentItem(with: nil)
        GCKCastContext.sharedInstance().presentDefaultExpandedMediaControls()
//      _localPlayerView.showSplashScreen()
      sessionManager.currentCastSession?.remoteMediaClient?.add(self)
      playbackMode = .remote
    }
    
    func switchToLocalPlayback() {
      print("switchToLocalPlayback")
      if playbackMode == .local {
        return
      }
      var playPosition: TimeInterval = 0
      var paused: Bool = false
      var ended: Bool = false
      if playbackMode == .remote {
        playPosition = castMediaController.lastKnownStreamPosition
        paused = (castMediaController.lastKnownPlayerState == .paused)
        ended = (castMediaController.lastKnownPlayerState == .idle)
        print("last player state: \(castMediaController.lastKnownPlayerState), ended: \(ended)")
      }
        seekToPosition(position: Float(playPosition))
      populateMediaInfo((!paused && !ended), playPosition: playPosition)
      sessionManager.currentCastSession?.remoteMediaClient?.remove(self)
      playbackMode = .local
    }
    
    @objc func playSelectedItemRemotely() {
        self.loadSelectedItem(byAppending: false)
        GCKCastContext.sharedInstance().presentDefaultExpandedMediaControls()
    }

        @objc func enqueueSelectedItemRemotely() {
        self.loadSelectedItem(byAppending: true)
    //    let message = "Added \"\(self.mediaInfo?.metadata?.string(forKey: kGCKMetadataKeyTitle) ?? "")\" to queue."
        if let window = UIApplication.shared.delegate?.window {
//          Toast.displayMessage(message, for: 3, in: window)
        }
      }
    
    /**
     * Loads the currently selected item in the current cast media session.
     * @param appending If YES, the item is appended to the current queue if there
     * is one. If NO, or if
     * there is no queue, a new queue containing only the selected item is created.
     */

    func loadSelectedItem(byAppending appending: Bool) {
      print("enqueue item \(String(describing: self.mediaInfo))")
      if let remoteMediaClient = GCKCastContext.sharedInstance().sessionManager.currentCastSession?.remoteMediaClient {
        let builder = GCKMediaQueueItemBuilder()
        builder.mediaInformation = self.mediaInfo
        builder.autoplay = true
        builder.preloadTime = 0
        let item = builder.build()
        if ((remoteMediaClient.mediaStatus) != nil) && appending {
          let request = remoteMediaClient.queueInsert(item, beforeItemWithID: kGCKMediaQueueInvalidItemID)
          request.delegate = self
        } else {
          let repeatMode = remoteMediaClient.mediaStatus?.queueRepeatMode ?? .off
          let request = remoteMediaClient.queueLoad([item], start: 0, playPosition: 0,
                                                                              repeatMode: repeatMode, customData: nil)
            
          request.delegate = self
        }
      }
    }
    
    @objc func castDeviceDidChange(_ notification: Notification) {
      if GCKCastContext.sharedInstance().castState != .noDevicesAvailable {
        // You can present the instructions on how to use Google Cast on
        // the first time the user uses you app
        GCKCastContext.sharedInstance().presentCastInstructionsViewControllerOnce(with: GCKUICastButton())
      }
    }

}
extension CustomVideoViewController {
    func addingWaterMarknew() {
        let url = mediaInfo?.contentURL
                   let _header = createHeaderForAVPlayer()
                   let asset = AVURLAsset(url: url!, options: ["AVURLAssetHTTPHeaderFieldsKey": _header])
                   playerItem = AVPlayerItem(asset: asset)
                   player = AVPlayer(playerItem: playerItem)
//            player.currentItem?.videoComposition = ""
        if let item = MediaItem(url: (mediaInfo?.contentURL)!) {
            let logoImage = UIImage(named: "airplay_connected")
//            let firstElement = MediaElement(image: logoImage!)
            let te = getAttributedStrings(text: "Mausam")
            let firstElement = MediaElement(text: te.first!)
            firstElement.frame = CGRect(x: 20, y: 20, width: 100, height: 30)
            
            let secondElement = MediaElement(image: logoImage!)
            secondElement.frame = CGRect(x: 150, y: 150, width: 100, height: 30)
            
            item.add(elements: [firstElement, secondElement])
            
            let mediaProcessor = MediaProcessor()
            mediaProcessor.processElements(item: item) { [weak self] (result, error) in
                print(result,error)
                DispatchQueue.main.async {
                    self?.playVideo(url: result.processedUrl!, view: (self?.videoPlayerView)!)
                }
            }
        }
    }
}

