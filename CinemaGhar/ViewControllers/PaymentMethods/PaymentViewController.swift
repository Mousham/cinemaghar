//
//  Payment.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 2/16/21.
//  Copyright © 2021 Cinemaghar. All rights reserved.
//

import Foundation
import StoreKit
import PassKit

class PaymentViewController: UIViewController{

    //CineCoin Outlets
    var paymentButton: PKPaymentButton!
    
//    static let shared = PaymentViewController()
    
    override func viewDidLoad(){
        setupView()
        IAPService.shared.getProducts()
    }
    
    func setupView(){      
        paymentButton = PKPaymentButton.init(paymentButtonType: .buy, paymentButtonStyle: .black)
        paymentButton.translatesAutoresizingMaskIntoConstraints = false
        paymentButton.cornerRadius = 25
        view.addSubview(paymentButton)
        
        paymentButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        paymentButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
        paymentButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        paymentButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        paymentButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
       
        paymentButton.addTarget(self, action: #selector(purchase(_:)), for: .touchUpInside)
      
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showProgress(isLoading:)), name: Notification.Name(rawValue: "IAPPaymentStatusLoading"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideProgress(notification:)), name: Notification.Name(rawValue: "IAPPaymentStatusNotLoading"), object: nil)
        
    }
    
    @objc func purchase(_ sender: UITapGestureRecognizer) {
        print("sawsank: Purchasing From apple")
        IAPService.shared.purchase(product: IAPProduct.cinemagharGoldSubscription)
    }
    
    @IBAction func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
   
    @objc func showProgress(isLoading: Bool){
        DispatchQueue.main.async {
            APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
            APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait...", presentingView: self.view)
        }

            

    }
    
    @objc func hideProgress(notification: NSNotification){
        DispatchQueue.main.async {
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: nil)
            guard let userInfo = notification.userInfo else {
                return
            }
            let alertController = DOAlertController(title: "Info", message: userInfo["message"] as? String, preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
             self.dismissVC()
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

