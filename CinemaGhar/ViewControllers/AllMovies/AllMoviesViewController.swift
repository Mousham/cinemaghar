//
//  AllMoviesViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 9/24/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll

class AllMoviesViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var allMoviesCollectionView: UICollectionView!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var navBar: UINavigationBar!
    
    var moviesArr = [Movie]()
    var seriesArr = [Series]()
    var movieType: MovieType = .exclusive

    var contentType: MediaType = .movie
    private let spacing:CGFloat = 8
    
    var titleLabel: String = ""
    var nextPageId = 2

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navBar.delegate = self
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true        
       self.navItem.title = self.titleLabel
        
        if self.contentType == .series{
            
             allMoviesCollectionView.register(UINib(nibName: "AllSeriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AllSeriesCollectionViewCell")
        }
        
        else{
            let nibName = UINib(nibName: "MovieCell", bundle:nil)
            allMoviesCollectionView.register(nibName, forCellWithReuseIdentifier: "MovieGenreCell")
        }
        
        
        allMoviesCollectionView.dataSource = self
        allMoviesCollectionView.delegate = self
        
        
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back_arrow"), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        button.sizeToFit()
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)
        self.navItem.leftBarButtonItem = UIBarButtonItem(customView: button)

        
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.minimumLineSpacing = 3
        layout.minimumInteritemSpacing = spacing
        self.allMoviesCollectionView.collectionViewLayout = layout

        
        allMoviesCollectionView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        allMoviesCollectionView.infiniteScrollIndicatorMargin = 50
        
      
        allMoviesCollectionView?.addInfiniteScroll {  (allMoviesCollectionView) -> Void in
            
            self.loadMoreData(pageId: String(self.nextPageId))
            
            
        }

        // Do any additional setup after loading the view.
    }
    
    
    func loadMoreData(pageId: String){
        
        if self.contentType == .movie {
            
            
            APIHandler.shared.getMovies(pageId: nextPageId, movieType: self.movieType, success: { (status, movies, nextPageId) in
                
                if status{
                    if nextPageId != "null"{
                        print(nextPageId)
                        self.nextPageId+=1
                        let photoCount = self.moviesArr.count

                        let (start, end) = (photoCount, movies.count + photoCount)
                        let indexPaths = (start..<end).map { return IndexPath(row: $0, section: 0) }
                        
                        self.moviesArr.append(contentsOf: movies)
                        self.allMoviesCollectionView.performBatchUpdates({ () -> Void in
                                self.allMoviesCollectionView.insertItems(at: indexPaths)
                            
                        }, completion: { (finished) -> Void in
                            // finish infinite scroll animations
                            self.allMoviesCollectionView.finishInfiniteScroll()
                        });
                        
                        
                        
                        
                        
                    }
                    
                    
                    
                }
                
                
            }, failure: {(failure) in
                
                
                
                
                
                
            })
            
            
        }
        
      
//
        
    }
    
    
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    

}



extension AllMoviesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.contentType == .series{
            
            return self.seriesArr.count
        }
        
        else {
            return self.moviesArr.count
        }
    }
    
    
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if contentType == .series {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllSeriesCollectionViewCell", for: indexPath) as! AllSeriesCollectionViewCell
            cell.setWebSeriesInfo(webSeries: self.seriesArr[indexPath.row], labelColor: UIColor.white)
            return cell
            
        }
        
        else {
            
            let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieGenreCell", for: indexPath) as! MovieGenreCell
            cell.setMovieInfos(movie: self.moviesArr[indexPath.row])
            return cell
            
        }
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let numberOfItemsPerRow:CGFloat = 3
        let spacingBetweenCells:CGFloat = 10
        
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
    
            
            let width = (collectionView.bounds.width - totalSpacing)/numberOfItemsPerRow
            if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
                
                return CGSize(width: width, height: UIScreen.main.bounds.height*0.38-80)

            }
            
            else {
                
                return CGSize(width: width, height: UIScreen.main.bounds.height*0.38-60)

            }
            
       
        
        
    
        
      
        
}
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
        if self.contentType == .movie {
//            let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
            let movieDetailScene = NewMovieDetailsVC.instantiate(fromAppStoryboard: .MovieDetail)
            
            movieDetailScene.movieDetail = moviesArr[indexPath.row]
            self.navigationController?.pushViewController(movieDetailScene, animated: true)
            
            
        }
        else {
            
            let seriesDetailVC = SeriesDetailViewController.instantiate(fromAppStoryboard: .SeriesDetail)
            seriesDetailVC.series = self.seriesArr[indexPath.row]
            self.navigationController?.pushViewController(seriesDetailVC, animated: true)
            
            
            
        }
        
    }
    
    
    
    
    
}


extension AllMoviesViewController: UINavigationBarDelegate{
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}
