//
//  LogoAnimationView.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 12/29/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import Foundation
import SwiftyGif

class LogoAnimationView: UIView {

    var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        
        do {
            let gif = try UIImage(gifName: "cinemaghar_logo_gif.gif")
            imageView = UIImageView(gifImage: gif, loopCount: 1) // Will loop 3 times
            backgroundColor = UIColor(white: 246.0 / 255.0, alpha: 1)
            addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
            imageView.widthAnchor.constraint(equalToConstant: 280).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: 108).isActive = true
            
        } catch {
            print(error)
        }
        
    }
}
