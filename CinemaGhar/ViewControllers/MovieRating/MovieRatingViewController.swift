//
//  MovieRatingViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 5/20/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import Cosmos

class MovieRatingViewController: UIViewController {
    var delegate: RatingModalDelegate?
    
    var contentID: Int?
    

    @IBOutlet weak var ratingContentVIew: CardView!
    
    @IBOutlet weak var ratingLabel: UILabel!
    @IBAction func cancelBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if let delegate = self.delegate {
                delegate.popViewController(updateRatingBar: false, rating: 0)
            }
        })
    }
    
    @IBOutlet weak var ratingContentView: CardView!
    @IBOutlet weak var ratingBar: CosmosView!
    var cardViewInitialOrigin:CGRect?
    
    @IBAction func submitRatingBtnPressed(_ sender: Any) {
        
        
    rateMovie()
        
        
        
    }
    
    
    
    
    func rateMovie(){
        APESuperHUD.showOrUpdateHUD( loadingIndicator: .standard, message: "One Moment Please...", presentingView: self.view)
        APIHandler.shared.rateMovie(rating: String(ratingBar.rating), contentId: self.contentID!, success:{(status) in
            
            APESuperHUD.removeHUD(animated: true, presentingView: self.view)
            
            if status {
                
                self.dismiss(animated: true, completion: {
                    
                    if let delegate = self.delegate {
                        delegate.ratingComplete(title: "Success!", message: "Your rating has been successfully received, Thank You.", rating: self.ratingBar.rating)
                    }
                    
                })
                
            }
                
            else {
                
                self.dismiss(animated: true, completion: {
                    
                    if let delegate = self.delegate {
                        delegate.ratingComplete(title: "Failure!", message: "An error occurred while submitting your rating. Please try again later.", rating: self.ratingBar.rating)
                    }
                    
                })
            }
            
            
        }, failure: {(failure) in
            
            
            if failure._code == NSURLErrorNotConnectedToInternet {
                APESuperHUD.removeHUD(animated: true, presentingView: self.view)

                let alert = UIAlertController(title: "No Internet Connection!", message: "Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.rateMovie()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                APESuperHUD.removeHUD(animated: true, presentingView: self.view)

                let controller = UIAlertController(title: "Request Timed Out!", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                    self.rateMovie()
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                self.rateMovie()
                
            }
            
            
            
        })
        
        
        
    }
    
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
       
        ratingBar.settings.fillMode = .half
        cardViewInitialOrigin = CGRect(x: (ratingContentView.bounds.origin.x), y: (ratingContentView.bounds.origin.y), width: ratingContentView.frame.width, height: ratingContentView.frame.height)
        ratingBar.didFinishTouchingCosmos = {rating in
            
        self.ratingLabel.text = String (rating)
            
            APESuperHUD.appearance.backgroundBlurEffect = .light
            
            APESuperHUD.appearance.iconWidth = 48
            APESuperHUD.appearance.loadingActivityIndicatorColor = .black
            APESuperHUD.appearance.iconHeight = 48
            APESuperHUD.appearance.messageFontSize = 16
            APESuperHUD.appearance.textColor = UIColor.black
            APESuperHUD.appearance.messageFontName  = "Avenir-Medium"
            
            
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        self.submitBtn.layer.cornerRadius = 10
        self.cancelBtn.layer.masksToBounds = true
        self.submitBtn.layer.masksToBounds = true
        self.submitBtn.clipsToBounds = true
        self.cancelBtn.clipsToBounds = true
        
        self.cancelBtn.layer.cornerRadius = 10	
    }
    

    
}
