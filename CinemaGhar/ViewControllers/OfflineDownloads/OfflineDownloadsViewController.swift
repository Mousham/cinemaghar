//
//  OfflineDownloadsViewController.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 1/24/21.
//  Copyright © 2021 sunBi. All rights reserved.
//

import Foundation
import AVFoundation
import AVKit

class OfflineDownloadsViewController: UIViewController, AVAssetDownloadDelegate {
    
    let configIdentifier = "testConfigIndentifier1"
    
//    let url = URL(string: "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8")
        let url = URL(string: "https://nepal.cinema-ghar.com/videos/thekingsman/thekingsman.m3u8?query=eyJ1c2VyX2lkIjoyMDczMSwiY29udGVudF9pZCI6MzA1LCJkZXZpY2VfdHlwZSI6ImlvcyIsImRldmljZV9pZCI6IjYyRDAzMTJCLTM1RjMtNEQ1MC04MUZBLUE2NDFFNkNDNEU2MSIsImlwIjoiMTEwLjQ0LjEyMS40NCIsInZpZGVvIjoidGhla2luZ3NtYW4iLCJtZWRpYV90eXBlIjoiY29udGVudHMiLCJtZXRhIjp7ImhlYWRlcnMiOnsiYWNjZXB0LWVuY29kaW5nIjpbImJyO3E9MS4wLCBnemlwO3E9MC45LCBkZWZsYXRlO3E9MC44Il0sIngtZGV2aWNlLWlkIjpbIjYyRDAzMTJCLTM1RjMtNEQ1MC04MUZBLUE2NDFFNkNDNEU2MSJdLCJ4LWRldmljZS1tb2RlbCI6WyJTaGFzaGFuayJdLCJ4LXZlcnNpb24iOlsiMS4xIl0sIngtZGV2aWNlLXR5cGUiOlsiaW9zIl0sImFjY2VwdC1sYW5ndWFnZSI6WyJlbi1DQTtxPTEuMCwgaGktQ0E7cT0wLjkiXSwidXNlci1hZ2VudCI6WyJDaW5lbWFHaGFyXC8zLjYuOSAoY29tLnNhZ2Fya2hhcmVsLmNpbmVtYWdoYXJoZDsgYnVpbGQ6Njc7IGlPUyAxNC4yLjApIEFsYW1vZmlyZVwvNS40LjEiXSwiYXV0aG9yaXphdGlvbiI6WyJCZWFyZXIgZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5LmV5SnBjM01pT2lKb2RIUndjenBjTDF3dmMzUmhaMmx1Wnk1amFXNWxiV0V0WjJoaGNpNWpiMjFjTDJGd2FWd3ZZWFYwYUZ3dmJHOW5hVzRpTENKcFlYUWlPakUyTVRFMk5UUTBOVEVzSW1WNGNDSTZNVGd3TURnM01EUTFNU3dpYm1KbUlqb3hOakV4TmpVME5EVXhMQ0pxZEdraU9pSTRjMnRzWlRWSFptcFVSVkpuYzFBeElpd2ljM1ZpSWpveU1EY3pNU3dpY0hKMklqb2lPRGRsTUdGbU1XVm1PV1prTVRVNE1USm1aR1ZqT1RjeE5UTmhNVFJsTUdJd05EYzFORFpoWVNJc0ltUmxkbWxqWlY5cFpDSTZJall5UkRBek1USkNMVE0xUmpNdE5FUTFNQzA0TVVaQkxVRTJOREZGTmtORE5FVTJNU0o5Lmw1OU1qTlBmcU5QUnQwVm11RUkwR3RoNnhzLXVqWnR1Q2N3UTZxQl9uenciXSwiYWNjZXB0IjpbImFwcGxpY2F0aW9uXC9qc29uIl0sImhvc3QiOlsic3RhZ2luZy5jaW5lbWEtZ2hhci5jb20iXSwiY29udGVudC1sZW5ndGgiOlsiIl0sImNvbnRlbnQtdHlwZSI6WyIiXX19fQ==")
    
    var playerLayer: AVPlayerLayer!
    let defaults = UserDefaults.standard
    var assetSessionTask: AVAssetDownloadTask!

    
    @IBOutlet weak var playButtonOutlet: UIButton!
    @IBOutlet weak var progessBar: UIProgressView!
    
    @IBAction func goBack(){
        self.navigationController?.backToViewController(viewController: CineGoldViewController.self)
    }
    
    @IBAction func downloadTest(){
        //Asset URL
      
        let hlsAsset = AVURLAsset(url: url!, options: ["AVURLAssetHTTPHeaderFieldsKey": createHeaderForDownloadURLTask()])
        
        //Background Configuration
        let backgroundConfiguration = URLSessionConfiguration.background(withIdentifier: configIdentifier)
        let assetURLSession = AVAssetDownloadURLSession(configuration: backgroundConfiguration, assetDownloadDelegate: self, delegateQueue: OperationQueue.main)
        assetURLSession.getAllTasks { (tasks) in
            //Check for pending tasks
            print("sawsank: \(tasks.count)")
            if(tasks.count == 0){
                //Download Task
                let assetDownloadTask = assetURLSession.makeAssetDownloadTask(asset: hlsAsset, assetTitle: "Kingsman Trailer", assetArtworkData: nil, options: [AVAssetDownloadTaskMinimumRequiredMediaBitrateKey : 2000000])!
                assetDownloadTask.resume()
            }else{
                print("sawsank: Config Download already exits")
            }
        }
    }
    
    @IBAction func playVideo(){
        //Asset URL
        self.createAVPlayer()


    }
    
    //Monitor the download progress
    func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didLoad timeRange: CMTimeRange, totalTimeRangesLoaded loadedTimeRanges: [NSValue], timeRangeExpectedToLoad: CMTimeRange) {
        print("sawsank: session.config \(session.configuration.identifier)")
        print("sawsank: loadedTimeRanges \(assetDownloadTask.loadedTimeRanges)")
        
        // Convert loadedTimeRanges to CMTimeRanges
        var percentageCompleted = 0.0
        for value in loadedTimeRanges{
            let loadedTimeRange: CMTimeRange = value.timeRangeValue
            percentageCompleted += CMTimeGetSeconds(loadedTimeRange.duration) / CMTimeGetSeconds(timeRangeExpectedToLoad.duration)
            progessBar.isHidden = false
            progessBar.setProgress(Float(percentageCompleted), animated: true)
            //Stop Session when percentage reaches 100
            if(percentageCompleted == 100.0){
                print("sawsank: 100% completed")
                session.invalidateAndCancel()
            }
        }
        
        percentageCompleted *= 100
        print("sawsank: percentage completed \(percentageCompleted)")
    }
    
    //Listen for completion
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let error = error as NSError? {
                switch (error.domain, error.code) {
                case (NSURLErrorDomain, NSURLErrorCancelled):
                    print("sawsank: First case error")
                case (NSURLErrorDomain, NSURLErrorUnknown):
                    print("sawsank: Downloading HLS streams is not supported in the simulator.")

                default:
                    print("sawsank: An unexpected error occured \n\(error.code) - \(error.domain) - \(error.localizedDescription)")
                }
            } else {
                // downloaded
                print("sawsank: Downloaded")
            }
        
    }
    
    //Store Download Location
    func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didFinishDownloadingTo location: URL) {
        //Store URL Asset to database
        assetSessionTask = assetDownloadTask
        AssetDownloadTaskManager.shared.setAssetDownloadTask(assetTask: assetDownloadTask)
        defaults.setValue(location.relativeString, forKey: "savedHLS")
        
        print("sawsank: \(defaults.string(forKey: "savedHLS"))")
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("sawsank: view did layout subviews")
    }
    
    override func viewDidLoad() {
        print("Offline View")
        progessBar.isHidden = true
    }
    
    //Create Headers for Download URL Task
    func createHeaderForDownloadURLTask() -> [String: String]{
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let deviceName = UIDevice.current.name
        let headers = ["Accept": "application/json", "X-VERSION": "1.1", "X-DEVICE-TYPE": "ios", "X-DEVICE-ID": deviceID, "X-DEVICE-MODEL": deviceName, "Authorization": SessionManager().getAccessToken() ?? ""]
        
        return headers
    }
    
    //MARK:- Temporary AVPlayer
    func createAVPlayer(){
        print("sawsank: creating AV player")
        let assetURL = defaults.string(forKey: "savedHLS")!
        let asset = AssetDownloadTaskManager.shared.assetDownloadTask
//        let asset = AVURLAsset(url: URL(fileURLWithPath: assetURL))
        let playerItem = AVPlayerItem(asset: asset!.urlAsset)
        
                
        let player = AVPlayer(playerItem: playerItem)
        
        
        //Play Video
        player.play()
        
        
        //AVController test
        let aVController = AVPlayerViewController()
        aVController.modalPresentationStyle = .overCurrentContext
        aVController.modalPresentationStyle = .popover
        aVController.allowsPictureInPicturePlayback = true
        aVController.entersFullScreenWhenPlaybackBegins = true
        aVController.player = player
        aVController.showsPlaybackControls = true
        
        
        let uniqueIDLabel = UILabel()
        uniqueIDLabel.text = "uniqueID"
        uniqueIDLabel.adjustsFontSizeToFitWidth = true
        uniqueIDLabel.translatesAutoresizingMaskIntoConstraints = false
        aVController.contentOverlayView!.addSubview(uniqueIDLabel)
        uniqueIDLabel.centerXAnchor.constraint(equalTo: aVController.contentOverlayView!.centerXAnchor).isActive=true
        uniqueIDLabel.centerYAnchor.constraint(equalTo: aVController.contentOverlayView!.centerYAnchor).isActive=true
        uniqueIDLabel.heightAnchor.constraint(equalToConstant: 100).isActive = true
        uniqueIDLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        aVController.contentOverlayView?.addSubview(uniqueIDLabel)
        
        self.present(aVController, animated: true) {
            print("sawsank: completed")
        }
        
        
        
        
    }
    
    
    //MARK:- END Temporary AVPlayer
}

