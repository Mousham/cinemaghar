//
//  AssetDownloadTaskManager.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 1/27/21.
//  Copyright © 2021 sunBi. All rights reserved.
//

import Foundation
import AVKit

class AssetDownloadTaskManager {
    
    static let shared = AssetDownloadTaskManager()
    
    var assetDownloadTask: AVAssetDownloadTask!
    
    
    func setAssetDownloadTask(assetTask: AVAssetDownloadTask){
        assetDownloadTask = assetTask
    }

    
}
