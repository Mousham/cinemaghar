//
//  OfflineDownloadsTableViewCell.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 1/24/21.
//  Copyright © 2021 Cinemaghar. All rights reserved.
//

import UIKit

class OfflineDownloadsTableViewCell: UITableViewCell {

    @IBOutlet weak var posterImageView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    func setDownloadItem(){
        
    }
    

}
