//
//  IntroViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 5/9/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import PhoneNumberKit
import Spring
import  CountryPickerView
import  SnapKit

class SignUpViewController: DesignableViewController {
    @IBOutlet weak var passCodeErrorLabelPlaceHolder: UILabel!
    var emailTextField = UITextField()
    var shouldAllowSkip = true
    
    var  isFromTheatre = false
    @IBAction func forgotPasswordBtnPressed(_ sender: Any) {
        
        
         self.alertController = DOAlertController(title: "Forgot Password?", message: "Your password reset link will be sent to the email attached to the phone number below." , preferredStyle: .alert)
            alertController.addTextFieldWithConfigurationHandler { textField in
            if let phoneTextField = textField{
                self.emailTextField = phoneTextField
                phoneTextField.text = self.cp.selectedCountry.phoneCode + self.mobileNumberTextField.text!
                phoneTextField.placeholder = " Mobile Number"
                phoneTextField.textColor = UIColor.black
                phoneTextField.font = UIFont.systemFont(ofSize: 13)
                phoneTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: phoneTextField.frame.height))
                phoneTextField.leftViewMode = .always
                phoneTextField.delegate = self
                phoneTextField.keyboardType = .emailAddress
                phoneTextField.keyboardAppearance = .dark
                phoneTextField.isEnabled = false
                
            }
        }
        alertController.alertViewBgColor = UIColor.white
        alertController.titleFont = UIFont.systemFont(ofSize: 18, weight: .medium)
        alertController.messageView.textAlignment = .left
        alertController.messageFont = UIFont.systemFont(ofSize: 13, weight: .light)
        alertController.titleTextColor = Util.hexStringToUIColor(hex: "9B3567")
        alertController.overlayColor = UIColor.black.withAlphaComponent(0.7)
        let okAction = DOAlertAction(title: "Reset", style: .destructive, handler: { action in
            if self.emailTextField.text != "" {
                    self.alertController.view.endEditing(true)
                    self.resetPassword(phoneNumber: self.emailTextField.text!)
            }
            else {
                self.alertController.messageView.textColor = UIColor.red
                self.alertController.messageView.text = "Please enter a valid email address."
                self.alertController.messageView.shake()
                self.emailTextField.shake()
                self.emailTextField.becomeFirstResponder()
            }
   
        })
        
        let cancelAction = DOAlertAction(title: "Cancel", style: .cancel, handler: { action in
            self.dismiss(animated: true, completion: nil)
            
        })
        // Add the action.
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        // Show alert
        if(isValidMobileNum){
            print("true")
            self.present(alertController, animated: true, completion: nil)
        }else{
            phoneNumberErrorPlaceHolder.text = "Enter Valid Mobile Number".uppercased()
            mobileNumberTextField.shake()
            self.phoneNumberErrorPlaceHolder.shake()
        }
        

    }
    @IBOutlet weak var forgotPasswordBtnHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginCardView: CardView!
    @IBOutlet weak var phoneNumberErrorPlaceHolder: UILabel!
    @IBOutlet weak var signUpPromptLabel: UILabel!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var passCodeTextField: UITextField!
    @IBOutlet weak var loginBtnView: GradientView!
    @IBOutlet weak var signUpButtonTopConstraint: NSLayoutConstraint!
    var alert = UIView()
    var alertController = DOAlertController()
    
    var delegate: RefreshTableViewDataDelegate?
    var castPlayDelegate: CastPlayerDelegate?

    
    let phoneNumberKit = PhoneNumberKit()
    var countryCode = ""
    var isValidMobileNum: Bool = false
    var isValidPassCode: Bool = false
    
    var mediaType: MediaType = .movie
    
    var episodeId = 1000000
    var contentId = 0
    
    
    var shouldDismissAfterLogin = false


    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    var isValidEmailAddr: Bool = false
    var isValidFirstName: Bool = false
    var isValidLastName: Bool = false
    
    var hasOptedSignUp = false
    var signUpStep = 0
    var mobileNumber:String = ""
    let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 100, height: 20))

    var firstNameTextField = SkyFloatingLabelTextField()
    var lastNameTextField = SkyFloatingLabelTextField()
    var emailAddressTextField = SkyFloatingLabelTextField()
    
  
    weak var cpvTextField: CountryPickerView!
    let cpvInternal = CountryPickerView()

    
    @IBOutlet weak var sceneTitle: UILabel!
    
    var locale = Locale.current
    
    @IBAction func signUpBtnPressed(_ sender: Any) {
        
        if(hasOptedSignUp == false){
            hasOptedSignUp = true
            self.forgotPasswordBtnHeightConstraint.constant = 0
            self.forgotPasswordButton.setTitle("", for: .normal)
            self.passCodeErrorLabelPlaceHolder.removeFromSuperview()

            self.signUpButtonTopConstraint.isActive = false
            signUpBtn.setTitle("NEXT", for: .normal)
            
            view.addConstraint(NSLayoutConstraint(item: signUpBtn, attribute: .top, relatedBy: .equal, toItem: self.mobileNumberTextField, attribute: .bottom, multiplier: 1, constant: 24))
            
            UIView.animate(withDuration: 0.5) {
                
                self.view.layoutIfNeeded()
                self.loginCardView.layoutIfNeeded()
                
                let formattedString = NSMutableAttributedString()
                formattedString
                    .bold("Enter your phone number. \n")
                    .normal("Make sure you can receive SMS to this number.", 12)
                
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 5
                paragraphStyle.alignment = .center
                paragraphStyle.minimumLineHeight = 5
                
                formattedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, formattedString.length))
                self.sceneTitle.fadeTransition(1)
                self.sceneTitle.attributedText = formattedString
            }
            
            self.signUpPromptLabel.removeFromSuperview()
            self.loginBtnView.removeFromSuperview()
            self.passCodeTextField.removeFromSuperview()
            
            
        }
            
            
        else {
            if(signUpStep == 0){
                
                if(isValidMobileNum){
                    signUpStep += 1
                    self.mobileNumber = cp.selectedCountry.phoneCode + mobileNumberTextField.text!

                    phoneNumberErrorPlaceHolder.text = ""
                                loginCardView.shadowColor = UIColor.clear
                                loginCardView.shadowOpacity = 0
                    self.sceneTitle.text = ""
                    self.sceneTitle.font = UIFont.systemFont(ofSize: 14, weight: .light)
                    signUpBtn.setTitle("SIGN UP", for: .normal)
                    self.mobileNumberTextField.removeFromSuperview()
                    
                    let orLabel = UILabel()
                    orLabel.text = ""
                    orLabel.font = UIFont.systemFont(ofSize: 14, weight: .light)
                    orLabel.textColor = self.sceneTitle.textColor
                    orLabel.textAlignment = .center
                    orLabel.translatesAutoresizingMaskIntoConstraints = false //very important
                    loginCardView.addSubview(orLabel)


                    view.addConstraint(NSLayoutConstraint(item: orLabel, attribute: .trailing, relatedBy: .equal, toItem: loginCardView, attribute: .trailing, multiplier: 1, constant: -20))
                    view.addConstraint(NSLayoutConstraint(item: orLabel, attribute: .leading, relatedBy: .equal, toItem: loginCardView, attribute: .leading, multiplier: 1, constant: 20))
                    view.addConstraint(NSLayoutConstraint(item: orLabel, attribute: .top, relatedBy: .equal, toItem: sceneTitle, attribute: .bottom, multiplier: 1, constant: 20))
                    
                    
                    customizeTextField(textField: firstNameTextField, placeHolder: "Firstname")
                      firstNameTextField.translatesAutoresizingMaskIntoConstraints = false
                    firstNameTextField.autocapitalizationType = .words
                    firstNameTextField.returnKeyType = .next

                    
                    self.loginCardView.addSubview(firstNameTextField)
                    
                    firstNameTextField.snp.makeConstraints({(make) in
                        make.leading.equalTo(loginCardView).offset(20)
                        make.trailing.equalTo(loginCardView).offset(-20)
                        make.top.equalTo(orLabel.snp.bottom).offset(15)
                        make.height.equalTo(40)
                        
                    })
                    
                    customizeTextField(textField: lastNameTextField, placeHolder: "Lastname")
                    lastNameTextField.translatesAutoresizingMaskIntoConstraints = false
                    lastNameTextField.autocapitalizationType = .words
                    lastNameTextField.returnKeyType = .next

                    
                    self.loginCardView.addSubview(lastNameTextField)
                    
                    lastNameTextField.snp.makeConstraints({(make) in
                        make.leading.equalTo(loginCardView).offset(20)
                        make.trailing.equalTo(loginCardView).offset(-20)
                        make.top.equalTo(firstNameTextField.snp.bottom).offset(15)
                        make.height.equalTo(40)
                        
                    })
                    
                    customizeTextField(textField: emailAddressTextField, placeHolder: "Email")
                    emailAddressTextField.keyboardType = .emailAddress
                    emailAddressTextField.autocapitalizationType = .none

                    emailAddressTextField.translatesAutoresizingMaskIntoConstraints = false
                    self.loginCardView.addSubview(emailAddressTextField)
                    emailAddressTextField.returnKeyType = .done
                    
                    emailAddressTextField.snp.makeConstraints({(make) in
                        make.leading.equalTo(loginCardView).offset(20)
                        make.trailing.equalTo(loginCardView).offset(-20)
                        make.top.equalTo(lastNameTextField.snp.bottom).offset(15)
                        make.height.equalTo(40)
                    })
                    
                    signUpBtn.snp.makeConstraints{(make) in
                        make.top.equalTo(emailAddressTextField.snp.bottom).offset(30)
                    }
             
                    self.emailAddressTextField.delegate = self
                    self.firstNameTextField.delegate = self
                    self.lastNameTextField.delegate = self
                    
                    let plainAttributedString = NSMutableAttributedString(string: "By continuing, you agree to our ", attributes: nil)
                    let string = "Terms and Condition"
                    let attributedLinkString = NSMutableAttributedString(string: string, attributes:[  NSAttributedString.Key.foregroundColor: UIColor(hex: "E43130")])
                    let fullAttributedString = NSMutableAttributedString()
                    fullAttributedString.append(plainAttributedString)
                    fullAttributedString.append(attributedLinkString)
                    

                    
                    let agreeTnc = UILabel()
                    agreeTnc.isUserInteractionEnabled = true
                    let tncTap = UITapGestureRecognizer(target: self, action: #selector(gotoTnc))
                    agreeTnc.addGestureRecognizer(tncTap)
                    agreeTnc.attributedText = fullAttributedString
                    agreeTnc.adjustsFontSizeToFitWidth = true
                    loginCardView.addSubview(agreeTnc)

                    agreeTnc.snp.makeConstraints { (make) in
                        make.top.equalTo(signUpBtn.snp.bottom).offset(8)
                        make.right.equalTo(loginCardView.snp.right).offset(-16)
                        make.left.equalTo(loginCardView.snp.left).offset(16)
                    }
                    
                    
                    UIView.animate(withDuration: 0.3) {
                        self.view.layoutIfNeeded()
                        self.loginCardView.shadowColor = .black
                        self.loginCardView.shadowOpacity = 0.5
                    }
                    
                    let when = DispatchTime.now() + 0.3// change 2 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                     
                        orLabel.text = "Sign Up with Email Address."
                     
                        self.firstNameTextField.layer.opacity = 1
                        self.lastNameTextField.layer.opacity = 1
                        self.emailAddressTextField.layer.opacity = 1
          
                    }
                    
                }
                else{
                    self.mobileNumberTextField.becomeFirstResponder()
                    phoneNumberErrorPlaceHolder.text = "Enter Valid Mobile Number".uppercased()
                    mobileNumberTextField.shake()
                    
                }
                
                
                
                
            }
                
                
            else { // real signup falidate email and fullname field
                
                if isValidFirstName == false{
                    
                    firstNameTextField.errorMessage = "Your firstname must not be empty".uppercased()
                    firstNameTextField.shake()
                   
                    
                    
                } else if isValidLastName == false {
                    
                    lastNameTextField.errorMessage = "Your lastname must not be empty".uppercased()
                    lastNameTextField.shake()
                    
                    
                }
                
                else if isValidEmailAddr == false {
                    
                    emailAddressTextField.errorMessage = "Must be a valid email address".uppercased()
                    emailAddressTextField.shake()
                    
                }
                else {
                    
                 
                    signUpUser()
                }
                
            }
        }
    }
    
    @objc func gotoTnc(){
        let url =  URL(string: "https://cinema-ghar.com/terms-and-conditions")!
        
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func signUpUser(){
 

        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Signing Up...", presentingView: self.view)
        let fullName = "\(firstNameTextField.text!) \(lastNameTextField.text!)"
        APIHandler.shared.signUp(signUpType: .normal, fullName: fullName, emailAddress: emailAddressTextField.text!, phoneNumber: mobileNumber, success: {(status, signUpModel) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let pincodeVerificationVC =     PassCodeVerificationViewController.instantiate(fromAppStoryboard: .SignUp)
                pincodeVerificationVC.phoneNumber = self.mobileNumber
                pincodeVerificationVC.userId = signUpModel.id
                pincodeVerificationVC.castPlayDelegate = self.castPlayDelegate
                pincodeVerificationVC.mediaType = self.mediaType
                self.navigationController?.pushViewController(pincodeVerificationVC, animated: true)




            })





        }, failure:{(status, errorMessage) in

            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Something went wrong!!!", message: errorMessage , preferredStyle: .alert)
                alertController.alertViewBgColor = UIColor.white
                alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                alertController.messageView.textAlignment = .left
                alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                alertController.titleTextColor = UIColor.red

                let okAction = DOAlertAction(title: "Close", style: .default, handler: { action in

                    self.dismiss(animated: true, completion: nil)


                })
                // Add the action.
                alertController.addAction(okAction)
                // Show alert
                self.present(alertController, animated: true, completion: nil)

            })




        }, error: {(error) in



        })
        
        
    }
    
    
    
    
    @IBAction func loginBtnPressed(_ sender: Any) {
        
        if(isValidMobileNum == false){
            phoneNumberErrorPlaceHolder.text = "Enter Valid Mobile Number".uppercased() 
            mobileNumberTextField.shake()
            self.phoneNumberErrorPlaceHolder.shake()
            if (isValidPassCode == false){
                self.passCodeErrorLabelPlaceHolder.text = "Enter your 4 digit passcode".uppercased()
                self.passCodeTextField.shake()
                self.passCodeErrorLabelPlaceHolder.shake()

            }
            
        }
        else if (isValidPassCode == false){
            self.passCodeErrorLabelPlaceHolder.text = "Enter your 4 digit passcode".uppercased()
            self.passCodeTextField.shake()
            self.passCodeErrorLabelPlaceHolder.shake()
    
        }
        
        else {
            self.view.endEditing(true)
            APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Logging In...", presentingView: self.view)
            
            APIHandler.shared.loginUser(mobileNumber: cp.selectedCountry.phoneCode + mobileNumberTextField.text!, passCode: passCodeTextField.text!, success: {(loginModel) in
                
                UserDefaults.standard.set(loginModel.id, forKey: AppConstants.userId )
                UserDefaults.standard.set(loginModel.accessToken, forKey: AppConstants.accessToken)
                UserDefaults.standard.set(loginModel.name, forKey: AppConstants.userName)
                UserDefaults.standard.set(loginModel.email, forKey: AppConstants.userEmail)
                UserDefaults.standard.set(loginModel.uniqueId, forKey: AppConstants.uniqueId)
                SessionManager().setAccessToken(token: loginModel.accessToken)
                SessionManager().setLogin(loginStatus: true)

                
                if self.shouldDismissAfterLogin{
                  self.navigationController?.popToRootViewController(animated: true)
                    if let delegate = self.delegate {
                        delegate.refreshTheatreTableView()
                    }
           
                }
                else {
                    if self.shouldAllowSkip {
                        self.skipLogin()
                        
                    }
                    
                    else {
                        self.navigationController?.popToRootViewController(animated: true)
                        if let castDelegate = self.castPlayDelegate {
                            castDelegate.sendPlayBtnClickAction()
                        }
                    }
                    
                    
                   
                }
                
         
                
                
            }, failure: {(failureMessage, customCode) in
                
                if(customCode == "USER_NOT_VERIFIED"){
                    print("sawsank: Failure customcode \(customCode)")
                    APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Sending Code...", presentingView: self.view)
                    APIHandler.shared.resendPassword(phoneNumber: self.cp.selectedCountry.phoneCode + self.mobileNumberTextField.text!) { (status, msg) in
                        print("sawsank: Resend code status \(status)")
                        if(status){
                            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                                
                                let pincodeVerificationVC =     PassCodeVerificationViewController.instantiate(fromAppStoryboard: .SignUp)
                                pincodeVerificationVC.phoneNumber = self.cp.selectedCountry.phoneCode + self.mobileNumberTextField.text!
                                pincodeVerificationVC.castPlayDelegate = self.castPlayDelegate
                                pincodeVerificationVC.mediaType = self.mediaType
                                self.navigationController?.pushViewController(pincodeVerificationVC, animated: true)
                                
                                
                      
                                
                            })
                        }else{
                            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                                let alertController = DOAlertController(title: "Something went wrong!!!", message: msg , preferredStyle: .alert)
                                alertController.alertViewBgColor = UIColor.white
                                alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                                alertController.messageView.textAlignment = .left
                                alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                                alertController.titleTextColor = UIColor.red
                                
                                let okAction = DOAlertAction(title: "Close", style: .default, handler: { action in
                                    self.dismiss(animated: true, completion: nil)
                                    
                                })
                                // Add the action.
                                alertController.addAction(okAction)
                                // Show alert
                                self.present(alertController, animated: true, completion: nil)                })
                        }
                    } failure: { (err) in
                        APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                            let alertController = DOAlertController(title: "Something went wrong!!!", message: err.localizedDescription , preferredStyle: .alert)
                            alertController.alertViewBgColor = UIColor.white
                            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                            alertController.messageView.textAlignment = .left
                            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                            alertController.titleTextColor = UIColor.red
                            
                            let okAction = DOAlertAction(title: "Close", style: .default, handler: { action in
                                self.dismiss(animated: true, completion: nil)
                                
                            })
                            // Add the action.
                            alertController.addAction(okAction)
                            // Show alert
                            self.present(alertController, animated: true, completion: nil)                })
                    }

                }else{
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                    let alertController = DOAlertController(title: "Something went wrong!!!", message: failureMessage , preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    
                    let okAction = DOAlertAction(title: "Close", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                        
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)                })
                
            }
            }, error: {(error) in })
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {




            })
            
        }
        
        
       
        
    }
    
    func resetPassword(phoneNumber:String) {
        self.alertController.dismiss(animated: true, completion: nil)

        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait...", presentingView: self.view)


        
        APIHandler.shared.forgotPassword(phoneNumber: phoneNumber, success: {(status, message) in
            
            if status {
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                    let alertController = DOAlertController(title: "Success", message: message , preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                        
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                })

                
            }
            else {
                
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                        let alertController = DOAlertController(title: "Whoops!", message: message , preferredStyle: .alert)
                        alertController.alertViewBgColor = UIColor.white
                        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                        alertController.messageView.textAlignment = .left
                        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                        alertController.titleTextColor = UIColor.red
                        let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                            self.dismiss(animated: true, completion: nil)
                        })
                        // Add the action.
                        alertController.addAction(okAction)
                        // Show alert
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                    
                    
                } )
                
            }
            
            
            
        }, failure: {(error) in
            
            
            
            
            
            
            
        })
        
    }
    
    
    
    @IBOutlet weak var signUpBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
      
        customizeViews()
        
        
       
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func socialButton(title:String, image:UIImage) -> SpringButton{
        let socialButton = SpringButton()
        socialButton.animation = "fadeIn"
        socialButton.autohide = false
        socialButton.duration = 3
        socialButton.leftImage(image: image)
        socialButton.autostart = false
        socialButton.setTitle(title, for: .normal)
        socialButton.backgroundColor = UIColor.blue
        socialButton.translatesAutoresizingMaskIntoConstraints = false
        return socialButton
    }
    
    func customizeTextField(textField: SkyFloatingLabelTextField, placeHolder:String){
        
        textField.layer.opacity = 0
        textField.placeholder = placeHolder
        textField.selectedLineColor = Util.hexStringToUIColor(hex: "9B3567")
        textField.selectedTitleColor = Util.hexStringToUIColor(hex: "9B3567")
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.lineHeight = 1
        textField.selectedLineHeight = 1
    }
    
    @objc func skipLogin (){
        UserDefaults.standard.set(true, forKey: "hasSkippedLogin")
//        let vc = CineGoldViewController.instantiate(fromAppStoryboard: .CineGold)
        let vc = HomeVC.instantiate(fromAppStoryboard: .CineGold)
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    
    
    @objc func dismissVC (){
       
            
            self.navigationController?.popViewController(animated: true)

            
        
    }
    
    
    func customizeViews(){
        
        self.alert = UIView(frame: CGRect(x: 0, y: 0, width: 350, height: 250))
        //alert customization
        self.alert.backgroundColor = UIColor.white
        self.alert.layer.cornerRadius = 10
        self.alert.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.alert)
        self.alert.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.alert.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        self.alert.heightAnchor.constraint(equalToConstant: 250).isActive = true
        self.alert.widthAnchor.constraint(equalToConstant: 350).isActive = true
        
        //Custom Title and Image Logo
        let logoImageView = UIImageView(frame: CGRect(x: 101.5, y: 10, width: 147, height: 20))
        logoImageView.image = UIImage(named: "watermark")
        
        let title = UILabel(frame: CGRect(x: 25, y: 70, width: 300, height: 20))
        title.text = "Let's Get Started"
        title.textAlignment = .center
        title.textColor = UIColor.init(hex: "#D72535")
        title.font = UIFont.systemFont(ofSize: 12)

        //Custom Buttons
        let signUpButton = UIButton(frame: CGRect(x: 25, y: 110, width: 300, height: 40))
        signUpButton.setTitle("NEW USER", for: .normal)
        signUpButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        signUpButton.backgroundColor = UIColor.init(hex: "#D72535")
        signUpButton.layer.cornerRadius = 20
        signUpButton.layer.borderColor = UIColor.init(hex: "#D72535").cgColor
        signUpButton.layer.borderWidth = 1
        signUpButton.addTarget(self, action: #selector(onNewUserTap), for: .touchUpInside)
        

        let loginButton = UIButton(frame: CGRect(x: 25, y: 172, width: 300, height: 40))
        loginButton.setTitle("EXISTING USER", for: .normal)
        loginButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        loginButton.backgroundColor = UIColor.init(hex: "#D72535")
        loginButton.layer.cornerRadius = 20
        loginButton.layer.borderColor = UIColor.init(hex: "#D72535").cgColor
        loginButton.layer.borderWidth = 1
        loginButton.addTarget(self, action: #selector(onExistingUserTap), for: .touchUpInside)
        
        
        alert.addSubview(signUpButton)
        alert.addSubview(loginButton)
        alert.addSubview(title)
        alert.addSubview(logoImageView)
        
        
        // Show alert
        self.loginCardView.isHidden = true

            
        let skipLoginBtn = UIButton() // back button
        skipLoginBtn.backgroundColor = UIColor.clear
        skipLoginBtn.titleLabel?.textColor = UIColor.white
        skipLoginBtn.setTitle("BACK", for: .normal)
        skipLoginBtn.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        self.view.addSubview(skipLoginBtn)
        
        skipLoginBtn.snp.makeConstraints{(make) in
            make.left.equalTo(self.view.snp.left).offset(20)
            
            if  #available(iOS 11.0, *){
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin).offset(20)
                
            }
            else {
                make.top.equalTo(self.view.snp.top).offset(40)

            }
            
        }

        skipLoginBtn.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)

        self.navigationController?.isNavigationBarHidden = true
      
        mobileNumberTextField.leftView = cp
        mobileNumberTextField.leftViewMode = .always
        passCodeTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: passCodeTextField  .frame.height))
        passCodeTextField.leftViewMode = .always
        self.cpvTextField = cp
        cp.countryDetailsLabel.font = UIFont.systemFont(ofSize: 13)
        cp.dataSource = self
        cp.delegate = self
        
        cp.flagSpacingInView = 2
        cp.flagImageView.snp.makeConstraints{(make) in
            make.width.equalTo(20)
            make.height.equalTo(15)
            make.left.equalTo(10)
        }
        
        cp.layoutIfNeeded()

        self.signUpBtn.layer.cornerRadius = 20
        self.signUpBtn.layer.borderWidth = 0.8
        self.signUpBtn.layer.borderColor = Util.hexStringToUIColor(hex: "C63939").cgColor
      
        mobileNumberTextField.layer.borderColor = Util.hexStringToUIColor(hex: "000000").cgColor
       
        
        mobileNumberTextField.delegate = self
        passCodeTextField.delegate = self
        sceneTitle.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
        signUpBtn.setBackgroundColor(color: Util.hexStringToUIColor(hex: "C84750"), forState: .highlighted)
        signUpBtn.clipsToBounds = true
        
        
        
        APESuperHUD.appearance.backgroundBlurEffect = .dark
        APESuperHUD.appearance.titleFontSize = 20
        APESuperHUD.appearance.iconWidth = 48
        APESuperHUD.appearance.loadingActivityIndicatorColor = UIColor.white
        APESuperHUD.appearance.iconHeight = 48
        APESuperHUD.appearance.messageFontSize = 16
        APESuperHUD.appearance.textColor = UIColor.white
        APESuperHUD.appearance.backgroundColor = .clear
        APESuperHUD.appearance.foregroundColor = .clear
        APESuperHUD.appearance.loadingActivityIndicatorColor = .white
        
        
    }
    
    //On New User Tap
    @objc func onNewUserTap(sender: Any?){
        self.alert.isHidden = true
        self.loginCardView.isHidden = false
        self.signUpBtnPressed(self)
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //On Existing User Tap
    @objc func onExistingUserTap(sender: Any?){
        self.alert.isHidden = true
        self.loginCardView.isHidden = false
        self.signUpBtn.isHidden = true
        self.forgotPasswordButton.isHidden = false
        
        self.view.addConstraint(NSLayoutConstraint(item: self.forgotPasswordButton, attribute: .top, relatedBy: .equal, toItem: self.signUpPromptLabel, attribute: .bottom, multiplier: 1, constant: 16))

        self.view.addConstraint(NSLayoutConstraint(item: self.loginCardView, attribute: .bottom, relatedBy: .equal, toItem: self.forgotPasswordButton, attribute: .bottom, multiplier: 1, constant: 24))
        

        self.dismiss(animated: true, completion: nil)
    }
    

    
    
    
    
    //IAP Purchase Success
    
    @objc func handlePurchaseNotification(_ notification: Notification) {
        guard let productID = notification.object as? String
            else { return }
        
        
        
        
        guard let appStoreReceiptURL = Bundle.main.appStoreReceiptURL, FileManager.default.fileExists(atPath: appStoreReceiptURL.path) else {
            
            return
            
        }
        
        let receiptData = try! Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
        let receiptString = receiptData.base64EncodedString()
        
        
        self.navigationController?.popViewController(animated: true)
        
        if let delegate = self.castPlayDelegate {
            delegate.sendPlayBtnClickAction()
            
        }
        
    }
    
}

extension SignUpViewController: CountryPickerViewDataSource, CountryPickerViewDelegate{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country.name)
        
        do {
            let result = mobileNumberTextField.text ?? "";
            try phoneNumberKit.parse(cp.selectedCountry.phoneCode + result)
            phoneNumberErrorPlaceHolder.text = ""
            self.isValidMobileNum = true
        }
        catch {
            self.isValidMobileNum = false
           phoneNumberErrorPlaceHolder.text = "Enter Valid Mobile Number.".uppercased()

        }
    }
    
}


extension SignUpViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == mobileNumberTextField){
            
            guard let text = textField.text else { return true }
            let result = (text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
           
            if(textField.text == ""){
                phoneNumberErrorPlaceHolder.text = "Enter Valid Mobile Number".uppercased()
                isValidMobileNum = false
     
            }
                
            else{
                
                do {
                    try phoneNumberKit.parse(cp.selectedCountry.phoneCode + result)
                    phoneNumberErrorPlaceHolder.text = ""
                    self.isValidMobileNum = true
                }
                catch {
                    self.isValidMobileNum = false
                   phoneNumberErrorPlaceHolder.text = "Enter Valid Mobile Number.".uppercased()
                    
                }
                
            }
            
            
            
        }
            
        else if textField == passCodeTextField {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            if(text == "" || newLength<4){
                self.passCodeErrorLabelPlaceHolder.text = "Enter your 4 digit passcode".uppercased()
                self.isValidPassCode = false
            }
            else if newLength == 4 {
                self.passCodeErrorLabelPlaceHolder.text = ""
                self.isValidPassCode = true
            }
            
            return newLength <= 4
            
        }
        
        
        else if textField == firstNameTextField {
            guard let text = textField.text else { return true }

            let result = (text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
            
            
            
            if isValidUserName(fullName: result) == false{
                self.firstNameTextField.errorMessage = "Must be a valid firstname.".uppercased()
                isValidFirstName = false
                
            }
            
            else {
                
            self.firstNameTextField.errorMessage = ""
            isValidFirstName = true
            }
            
            
        }
        
        else if textField == lastNameTextField {
            guard let text = textField.text else { return true }

            let result = (text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
            
            
            
            if isValidUserName(fullName: result) == false{
                self.lastNameTextField.errorMessage = "Must be a valid lastname.".uppercased()
                isValidLastName = false
                
            }
            
            else {
                
            self.lastNameTextField.errorMessage = ""
                isValidLastName = true
            }
            
            
        }
        
        else if textField == emailAddressTextField{
            guard let text = textField.text else { return true }
            
            let result = (text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
            if isValidEmail(testStr: result) == false{
                self.emailAddressTextField.errorMessage = "must be a valid email addres".uppercased()
                isValidEmailAddr = false
                
            }
            else {
                self.emailAddressTextField.errorMessage = ""
                isValidEmailAddr = true
                
                
            }
            
            
        }
        
        else if textField == emailTextField {
            guard let text = textField.text else { return true }
            let result = (text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
            if isValidEmail(testStr: result) == false{
                self.alertController.messageView.text = "Please enter a valid email address."
                self.alertController.messageView.textColor = UIColor.red
                self.alertController.buttons[1].backgroundColor = UIColor.green
                
              
                
            }
            else {
                self.alertController.messageView.textColor = UIColor.blue
                self.alertController.messageView.text = "Tap reset button to receive a new password in this email."
                self.alertController.messageView.textColor = Util.hexStringToUIColor(hex: "#33B8FF")

                
                
            }
            
            
        }
        
        return true
    }
    
    
    func isValidUserName(fullName:String)-> Bool{
        
        
        let RegEx = "([A-Za-z ]{2,})"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: fullName)
    }
    
    
   
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        
        if textField == firstNameTextField{
            firstNameTextField.resignFirstResponder()
            lastNameTextField.becomeFirstResponder()
            
        }
        if textField == lastNameTextField{
            lastNameTextField.resignFirstResponder()
            emailAddressTextField.becomeFirstResponder()
            
        }
        else if textField == emailAddressTextField {
            
            
            emailAddressTextField.resignFirstResponder()
        }
        
        return false
    }
   
    
    
}






extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.light)]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func movieDetailLabelFormatter(_ text: String, _ size: CGFloat, _ weight: UIFont.Weight, _ labelColor: UIColor) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: size, weight: weight), .foregroundColor:labelColor]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    
    @discardableResult func normal(_ text: String, _ size: CGFloat) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: size), .foregroundColor: Util.hexStringToUIColor(hex: "808080")]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func boldBig(_ text: String) -> NSMutableAttributedString {
        
        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.alignment = .left
        
        
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Helvetica", size: 16)!,  NSAttributedStringKey.paragraphStyle: titleParagraphStyle ]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func boldBigCenter(_ text: String) -> NSMutableAttributedString {
        
        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.alignment = .center
        titleParagraphStyle.lineSpacing = 2
        
        
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Helvetica", size: 16)!,  NSAttributedStringKey.paragraphStyle: titleParagraphStyle ]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func boldOut(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), .foregroundColor: Util.hexStringToUIColor(hex: "808080")  ]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func boldOutWithLargeSpace(_ text: String) -> NSMutableAttributedString {
        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.alignment = .center
        titleParagraphStyle.lineSpacing = 2
        
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), .foregroundColor: Util.hexStringToUIColor(hex: "808080"), NSAttributedStringKey.paragraphStyle: titleParagraphStyle  ]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func boldWithColor(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 12.5, weight: UIFont.Weight.medium) , .foregroundColor: Util.hexStringToUIColor(hex: "808080")]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    
    @discardableResult func boldWithColor(_ text: String, color: UIColor) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold) , .foregroundColor: color]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func boldBigWithColor(_ text: String, color: UIColor) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold) , .foregroundColor: color]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    
}
extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionFade
        animation.duration = duration
        layer.add(animation, forKey: kCATransitionFade)
    }
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -15.0, 15.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
        func roundCorners(_ corners:UIRectCorner,_ cormerMask:CACornerMask, radius: CGFloat) {
            if #available(iOS 11.0, *){
                self.clipsToBounds = true
                self.layer.cornerRadius = radius
                self.layer.maskedCorners = cormerMask
            }else{
                let rectShape = CAShapeLayer()
                rectShape.bounds = self.frame
                rectShape.position = self.center
                rectShape.path = UIBezierPath(roundedRect: self.bounds,    byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
                self.layer.mask = rectShape
            }
        
        
    }
    
    func addDropShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    func startShimmering() {
        //        let light = UIColor.init(white: 0, alpha: 0.1).cgColor
        let light = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        let dark = UIColor.black.cgColor
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [dark, light, dark]
        gradient.frame = CGRect(x: -self.bounds.size.width, y: 0, width: 3*self.bounds.size.width, height: self.bounds.size.height)
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.525)
        gradient.locations = [0.4, 0.5, 0.6]
        self.layer.mask = gradient
        
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [0.0, 0.1, 0.2]
        animation.toValue = [0.8, 0.9, 1.0]
        
        animation.duration = 1.5
        animation.repeatCount = HUGE
        gradient.add(animation, forKey: "shimmer")
        
        
    }
    
    func stopShimmering() {
        self.layer.mask = nil
    }

    
}

extension UIImageView {
    
//    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
//        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//        self.layer.mask = mask
//    }
    
}

extension UIButton {
    
    /// Add image on left view
    func leftImage(image: UIImage) {
        self.setImage(image, for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: image.size.width+5)
    }
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
}

