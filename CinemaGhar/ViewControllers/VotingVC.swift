//
//  VotingVC.swift
//  CinemaGhar
//
//  Created by Midas on 12/03/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import FSPagerView
import SwiftyJSON
import SDWebImage
struct VotingAllModel {
    var votingDetailsData = [VotingDetailsModel]()
    var votingPrice = [VotingPriceModel]()
    var profileData = Profile(fromJson: JSON())
}
class VotingVC: UIViewController {

   
    
    @IBOutlet weak var coinsAmtLbl: UILabel!
    @IBOutlet weak var descBackView: UIView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var contestantImage: UIImageView!
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var promoBtn: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var sliderView: FSPagerView! {
        didSet {
            self.sliderView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.sliderView.itemSize = FSPagerView.automaticSize
        }
    }
    //    @IBOutlet weak var topView: GradientView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var voteView: UIView!
    @IBOutlet weak var contestantView: UIView!
    @IBOutlet weak var amountTitle: UILabel!
    @IBOutlet weak var voteTitle: UILabel!
    @IBOutlet weak var contestantTitle: UILabel!
    @IBOutlet weak var amountBackView: UIView!
    @IBOutlet weak var numofVoteBackView: UIView!
    @IBOutlet weak var contestantBackView: UIView!
    var contestantArray = ["Michael","Steve", "Kane", "Son","Sancho","Pogba","Cavani"]
    var votesArray = ["1","5", "50", "100","200","300","500"]
    var votingDataSource = ["iconTheVoice","iconNepalIdol"]
    var imagesArray = [String]()
    let yourAttributes: [NSAttributedString.Key: Any] = [
         .font: UIFont.systemFont(ofSize: 14),
         .foregroundColor: UIColor.init(hex: "8F8D97"),
         .underlineStyle: NSUnderlineStyle.styleSingle.rawValue
     ] // .double.rawValue, .thick.rawValue
    var votingdata = VotingModel(json: JSON())
    var votingDetailsData = [VotingDetailsModel]()
    var votingPrice = [VotingPriceModel]()
    var votingResponse = DefaultResponse(json: JSON())
    var contestantID: Int?
    var contestantName: String? {
        didSet {
            
        }
    }
    var votes: String? {
        didSet {
            amountBackView.isHidden = false
        }
    }
    var coins: Int?
    var votingNo: Int?
    var datasource = VotingAllModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetup()
        setUpAdView()
        getVotingDetails()
        getVotingPrice()
        getProfile()
    
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let gradientLayer:CAGradientLayer = CAGradientLayer()
         gradientLayer.frame.size = self.topView.frame.size
         gradientLayer.colors =
        [UIColor.init(hex: "AAD5FB").cgColor,UIColor.init(hex: "939DF6").cgColor]
        //Use diffrent colors
         topView.layer.addSublayer(gradientLayer)
    
    }
    func uisetup() {
        contestantTitle.text = "-Select Contestant-"
        descBackView.isHidden = true
        contestantView.layer.cornerRadius = 9
        amountView.layer.cornerRadius = 9
        voteView.layer.cornerRadius = 9
        imageBackView.isHidden = true
        amountBackView.isHidden = true
        topView.addDropShadow(offset: CGSize(width: 1, height: 0.3), color: .black, radius: 3, opacity: 0.5)
        topView.layer.cornerRadius = 12
        topView.clipsToBounds = true
        let attributeString = NSMutableAttributedString(
               string: "Have a promo code?",
               attributes: yourAttributes
            )
            promoBtn.setAttributedTitle(attributeString, for: .normal)
      
//        setBorderColor(thisView: contestantView)
//        setBorderColor(thisView: amountView)
//        setBorderColor(thisView: voteView)
        
    }
    private func setUpAdView() {
        imagesArray = ["theVoice", "nepalIdol","cokeStudio"]
       // sliderView.height
       // sliderView.setHeight(height: UIScreen.main.bounds.height * 0.25)
        //pagerView.height = UIScreen.main.bounds.height * 0.25
//        self.pageControl.numberOfPages = imagesArray.count
//        self.pageControl.contentHorizontalAlignment = .center
//        self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        sliderView.dataSource = self
        //self.sliderView.delegate = self
        self.sliderView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.sliderView.itemSize = FSPagerView.automaticSize
        self.sliderView.automaticSlidingInterval = 3
        sliderView.transformer = FSPagerViewTransformer(type: .linear)
        let transform = CGAffineTransform(scaleX: 0.6, y: 0.75)
        self.sliderView.itemSize = self.sliderView.frame.size.applying(transform)
        self.sliderView.decelerationDistance = FSPagerView.automaticDistance
        sliderView.backgroundColor = .clear
        //    self.pagerView.itemSize = pagerView.frame.size.applying(CGAffineTransform(scaleX: 1, y: 1))
    }
    func setBorderColor(thisView: UIView) {
        thisView.layer.borderWidth = 1
        thisView.layer.borderColor = AppColors.yellowColor.cgColor
        thisView.layer.cornerRadius = 8
    }
    func showContestant(){
        let controller = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)

        for item in votingDetailsData {
            controller.addAction(UIAlertAction(title: item.title, style: .default, handler: { (action) in
                print("action :: \(String(describing: action.title))")

//                let title = action.title
                self.contestantTitle.text = item.title
                self.contestantImage.sd_setImage(with: URL(string: item.coverImage ?? ""), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed: nil)
                self.descBackView.isHidden = false
                self.descLabel.text = item.description
                self.imageBackView.isHidden = false
                self.contestantID = item.id

            }))
        }
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in

        }))
        present(controller, animated: true, completion: nil)

    }
    func showVotes(){
        let controller = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)

        for item in votingPrice {
            controller.addAction(UIAlertAction(title: String(item.votes ?? 0), style: .default, handler: { (action) in
                print("action :: \(String(describing: action.title))")

//                let title = action.title
                self.voteTitle.text = "\(item.votes ?? 0)"
                self.votingNo = item.votes
                self.amountTitle.text = "\(item.coins ?? 0)"
                self.coins = item.coins
                self.amountBackView.isHidden = false
            }))
        }
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in

        }))
        present(controller, animated: true, completion: nil)

    }
//
    @IBAction func backbtnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func contestantTap(_ sender: Any) {
        showContestant()
    }
    @IBAction func voteTap(_ sender: Any) {
        showVotes()
    }
    @IBAction func amountTap(_ sender: Any) {
    }
    @IBAction func promoBtnTap(_ sender: Any) {
        let pincodeVerificationVC =     PassCodeVerificationViewController.instantiate(fromAppStoryboard: .SignUp)
        self.navigationController?.pushViewController(pincodeVerificationVC, animated: true)
    }
    @IBAction func proceedTap(_ sender: Any) {
        contestantView.shake()
        if(SessionManager().isLoggedIn()){
           validate()
        }else{
            let signupVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(signupVC, animated: true)
        }
        
    }
//MARK: - VOTE VALIDATION
    func validate() {
        guard let _ = contestantID else {
            contestantView.shake()
            return
        }
        guard let _ = votingNo else {
            voteView.shake()
            return
        }
        postVote()
    }
}
extension VotingVC: FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        print(imagesArray.count)
        return imagesArray.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
       
        
       // cell.imageView?.sd_setImage(with: URL(string: imagesArray[index]), placeholderImage: UIImage(named: ""))
        cell.imageView?.image = UIImage(named: imagesArray[index])
        //           cell.textLabel?.text = ...
        return cell
    }
}
extension VotingVC {
    //MARK: - GET VOTING DETAILS
    func getVotingDetails() {
        globalVotingId = Int(votingdata.id ?? 0)
        APIHandler.shared.getVotingDetails { [weak self] (votings) in
            guard let strongSelf = self else { return }
            print(votings)
            strongSelf.votingDetailsData = votings
            strongSelf.datasource.votingDetailsData = votings
//            self.votingData = votings
//            print(self.votingData)
//            self.votingCollectionView.reloadData()
//            self.tutorialsDataArr = tutorials
//            self.tutorialsTableView.reloadData()
        } failure: { (msg) in
            print(msg)
        } error: { (err) in
            print(err.localizedDescription)
        }
    }
    //MARK: - GET VOTING PRICE
    func getVotingPrice() {
        globalVotingId = Int(votingdata.id ?? 0)
        APIHandler.shared.getVotingPrice { [weak self] (votingPrice) in
            guard let strongSelf = self else { return }
            print(votingPrice)
            strongSelf.votingPrice = votingPrice
//            self.votingData = votings
//            print(self.votingData)
//            self.votingCollectionView.reloadData()
//            self.tutorialsDataArr = tutorials
//            self.tutorialsTableView.reloadData()
        } failure: { (msg) in
            print(msg)
        } error: { (err) in
            print(err.localizedDescription)
        }
    }
    //MARK: - POST VOTE
    func postVote() {
        globalVotingId = Int(votingdata.id ?? 0)
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "", presentingView: self.view)
        APIHandler.shared.postVote(contestantID: self.contestantID ?? 0, votes: self.votingNo ?? 0, coins: self.coins ?? 0) { [weak self] (votingPrice) in
            guard let strongSelf = self else { return }
            print(votingPrice)
            strongSelf.votingResponse = votingPrice
            if votingPrice.code == 200 {
                APESuperHUD.removeHUD(animated: true, presentingView: strongSelf.view, completion: {
                    let alertController = DOAlertController(title: "Success!!!", message: strongSelf.votingResponse.message, preferredStyle: .alert)
                        alertController.alertViewBgColor = UIColor.white
                        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                        alertController.messageView.textAlignment = .left
                        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                        alertController.titleTextColor = UIColor.red
                        let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                            
                            strongSelf.dismiss(animated: true, completion: nil)
                            strongSelf.navigationController?.popViewController(animated: true)
                        })
                        // Add the action.
                        alertController.addAction(okAction)
                        // Show alert
                        strongSelf.present(alertController, animated: true, completion: nil)
                        
                        
                    
                    
                } )
            }
          
        } failure: { (msg) in
            print(msg)
        } error: { (err) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: err.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                
                
            } )
        }
    }
    //MARK: - GET PROFILE AND WALLET AMT
    func getProfile() {
        APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait...", presentingView: self.view)
        
        
        APIHandler.shared.getProfile(success: { [weak self](profile) in
            guard let strongSef = self else { return }
            strongSef.datasource.profileData = profile
            print(profile)
            
            print("sawsank: Getting profile")
            APESuperHUD.removeHUD(animated: true, presentingView: strongSef.view, completion: {
                print("sawsank: Load Profile Removed HUD")
            })
            strongSef.coinsAmtLbl.text = strongSef.datasource.profileData.walletBalance
//            self.profile = profile
//            self.userNameLabel.text = profile.name
//            self.walletAmountLabel.text = profile.walletBalance
//            self.cinemagharIDLabel.text = "CinemagharID: " + profile.phoneNumber
//            if(self.profile.image != ""){
//                self.setProfileImageToImageView(url: profile.image)
           // }

        }, failure: { (message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                   let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
                   alertController.alertViewBgColor = UIColor.white
                   alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                   alertController.messageView.textAlignment = .left
                   alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                   alertController.titleTextColor = UIColor.red
                   let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                       self.dismiss(animated: true, completion: nil)
                    self.dismissVC()
                   })
                   // Add the action.
                   alertController.addAction(okAction)
                   // Show alert
                   self.present(alertController, animated: true, completion: nil)
                   
                   
               
               
           } )
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                
                
            } )
        }
    }
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
}




