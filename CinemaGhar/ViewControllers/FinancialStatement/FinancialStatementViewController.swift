//
//  FinancialStatementViewController.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 4/16/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import Foundation

class FinancialStatementViewController: UIViewController, UINavigationBarDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var totalCinecoinView: UIView!
    @IBOutlet weak var totalCinecoinLabel: UILabel!
    @IBOutlet weak var statementTableView: UITableView!
    
    var testStatements: [Statement] = []
    var statementsArr: [Statement] = []
    var nextPageId = 1
    var isLoading = false
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupTotalCinecoinView()
        
        //setup table view delegate and datasource
        statementTableView.delegate = self
        statementTableView.dataSource = self
        
        //Get Profile
        getProfile()
        
       
    }
    

    
    func setupNavigationBar(){
            self.navigationBar.backgroundColor = UIColor.clear
            self.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationBar.isTranslucent = true
            
            let button = UIButton(type: .system)
            button.setImage(UIImage(named: "back_arrow"), for: .normal)
    //        button.setTitle("Back", for: .normal)
    //        button.setTitleColor(UIColor.white, for: .normal)
            button.tintColor = UIColor.white
            button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
            button.sizeToFit()
            button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
            button.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)
            self.navItem.leftBarButtonItem = UIBarButtonItem(customView: button)
            self.navigationBar.delegate = self
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }
    
    func setupTotalCinecoinView() {
        totalCinecoinView.layer.cornerRadius = 10
        totalCinecoinView.layer.masksToBounds = true
    }
    
    //Get Profile <Start>
    func getProfile() {
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait...", presentingView: self.view)
        
        
        APIHandler.shared.getProfile(success: { (profile) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                self.totalCinecoinLabel.text = profile.walletBalance
                //Get Statements
                self.getStatements()
            })

        }, failure: { (message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                   let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
                   alertController.alertViewBgColor = UIColor.white
                   alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                   alertController.messageView.textAlignment = .left
                   alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                   alertController.titleTextColor = UIColor.red
                   let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                       self.dismiss(animated: true, completion: nil)
                    self.dismissVC()
                   })
                   // Add the action.
                   alertController.addAction(okAction)
                   // Show alert
                   self.present(alertController, animated: true, completion: nil)
                   
                   
               
               
           } )
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                
                
            } )
        }
    }
    //Get Profile <End>
    
    //Get Statements <Start>
    func getStatements(){
        if(!isLoading && nextPageId != 0){
        isLoading = true

        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Getting Statements...", presentingView: self.view)
        
        
        APIHandler.shared.getStatements(pageId: nextPageId,success: { (statement, nextPageLink) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
           
                    self.statementsArr.append(contentsOf: statement)
                    DispatchQueue.main.async {
                        self.statementTableView.reloadData()
                    }
                    if nextPageLink == "" {
                        self.nextPageId = 0
                    }else{
                        self.nextPageId += 1
                    }
                    self.isLoading = false

            })

        }, failure: { (message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                   let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
                   alertController.alertViewBgColor = UIColor.white
                   alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                   alertController.messageView.textAlignment = .left
                   alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                   alertController.titleTextColor = UIColor.red
                   let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                       self.dismiss(animated: true, completion: nil)
                    self.dismissVC()
                   })
                   // Add the action.
                   alertController.addAction(okAction)
                   // Show alert
                   self.present(alertController, animated: true, completion: nil)
                   
                   
               
               
           } )
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                
                
            } )
        }
    }
    }
    
    //Get Statements <End>
    
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
}


//Extension for table view
extension FinancialStatementViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statementsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let statement = statementsArr[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "FinancialStatementTableViewCell") as! FinancialStatementTableViewCell
        cell.selectionStyle = .none
        cell.setStatement(statement: statement)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let statement = statementsArr[indexPath.row]
        if(statement.mediaType == "voting"){
            UIPasteboard.general.string = statement.voucherCode
            self.showToast(message: "Copied Coupon to Clipboard", font: .systemFont(ofSize: 12.0))

        }
    }
}

extension FinancialStatementViewController:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
//        print("sawsank: position \(position)")
//        print("sawsank: position compared to \(scrollView.contentSize.height-scrollView.frame.size.height)")
        if(position > (scrollView.contentSize.height-scrollView.frame.size.height)){
           getStatements()
        }
    }
}
