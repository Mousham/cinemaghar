//
//  FinancialStatementTableViewCell.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 4/16/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import UIKit

class FinancialStatementTableViewCell: UITableViewCell {

    @IBOutlet weak var statementView: UIView!
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var voucherCode: UILabel!
    @IBOutlet weak var gateway: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    func setStatement(statement: Statement){
        
        //Add Badge View
        let badgeView = UIView()
        badgeView.translatesAutoresizingMaskIntoConstraints = false

        statementView.addSubview(badgeView)
        badgeView.heightAnchor.constraint(equalTo: statementView.heightAnchor).isActive = true
        badgeView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        badgeView.leftAnchor.constraint(equalTo: statementView.leftAnchor).isActive = true
        
        //Add Copy Code View
        let copyView = UIView()
        let copyLabel = UILabel()
        copyLabel.text = "Copy Coupon"
        copyLabel.font = UIFont.systemFont(ofSize: 14)
        copyLabel.textColor = UIColor(hex: "#E59827")
        copyLabel.translatesAutoresizingMaskIntoConstraints = false
        copyView.addSubview(copyLabel)
        
        copyLabel.centerXAnchor.constraint(equalTo: copyView.centerXAnchor).isActive = true
        copyLabel.centerYAnchor.constraint(equalTo: copyView.centerYAnchor).isActive = true
        
        copyView.layer.borderWidth = 1
        copyView.layer.borderColor = UIColor(hex: "#E59827").cgColor
        copyView.layer.cornerRadius = 15
        copyView.layer.masksToBounds = true
        
        
        
        //Border and corner radius
        statementView.layer.borderWidth = 2
        statementView.layer.borderColor = UIColor.init(white: 1.0, alpha: 0.8).cgColor
        statementView.layer.cornerRadius = 15
        statementView.layer.masksToBounds = true
        
        //Set data
        timeLabel.text = statement.createdAt
        priceLabel.text = statement.price
        
        //Customize view according to type
        if(statement.type == "deposit"){
            priceLabel.textColor = UIColor.green
            itemLabel.text = "Wallet Deposit"
            gateway.text = "Gateway: \(statement.paymentGateway ?? "Wallet")"
            badgeView.backgroundColor = UIColor.green
            heightConstraint.constant = 90
        }else if(statement.type == "withdraw"){
            priceLabel.textColor = UIColor.red
            itemLabel.text = statement.mediaTitle
            gateway.text = "Gateway: \(statement.paymentGateway ?? "Wallet")"
            badgeView.backgroundColor = UIColor.red
            if(statement.mediaType == "voting"){
               itemLabel.text = "Bought \(statement.mediaTitle ?? "voting")"
                voucherCode.text = "Voucher Code: \(statement.voucherCode ?? "000")"
                
                //Adding Copy Coupon View
                copyView.translatesAutoresizingMaskIntoConstraints = false
                statementView.addSubview(copyView)
                
                copyView.rightAnchor.constraint(equalTo: statementView.rightAnchor, constant: -16).isActive = true
                copyView.topAnchor.constraint(equalTo: statementView.topAnchor, constant: 16).isActive = true
                copyView.heightAnchor.constraint(equalToConstant: 30).isActive = true
                copyView.widthAnchor.constraint(equalToConstant: 116).isActive = true
           }
        }
    }
    

}
