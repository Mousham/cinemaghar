//
//  NewsFeedViewController.swift
//  KBC Nepal
//
//  Created by Sunil on 2/6/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMobileAds

import SwiftyJSON
import UIScrollView_InfiniteScroll


class NewsFeedViewController: DesignableViewController, UITableViewDelegate, UITableViewDataSource,  UIGestureRecognizerDelegate  {
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var navItem: UINavigationItem!
   
    
    var numberOfLines = 4
    
    
    var galleryCellHeight = CGFloat(300)
    
    var newsCellHeight = CGFloat(140)
    
    var videoCellHeight = CGFloat(270)
    
    var newsFeedDataArr = [NewsFeedData]()
    var  isLoadingNews:Bool = true
    var pictures = [CollieGalleryPicture]()
    
    var shouldReloadTableView = true
    
    var navBar:UINavigationBar = UINavigationBar()
    
    var isCollapsed:Bool = false
    
    var nexPageURL = CinemaGharAPI.newsFeedURL
    
    @IBOutlet var toolbarTitleMarginTop: NSLayoutConstraint!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
      
            return newsFeedDataArr.count
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
            if(self.newsFeedDataArr[indexPath.row].type == "video"){
                
                let youtubePlayerVC = FreeMoviePlayerViewController.instantiate(fromAppStoryboard: .FreeMoviePlayer)
                if let youtubeURL = self.newsFeedDataArr[indexPath.row].video{
                    youtubePlayerVC.youtubeURL = youtubeURL
                    self.navigationController?.pushViewController(youtubePlayerVC, animated: true)
                }
                
//                playInYoutube(youtubeId: getYoutubeId(youtubeURL: self.newsFeedDataArr[indexPath.row].video)!)
//
            }
                
            else if(self.newsFeedDataArr[indexPath.row].type == "news"){
               
                let viewController = NewsDetailViewController.instantiate(fromAppStoryboard: .NewsFeed)
                
                viewController.newsTitle = self.newsFeedDataArr[indexPath.row].title
                viewController.newsDetail = self.newsFeedDataArr[indexPath.row].descriptionField
                viewController.imageURL = self.newsFeedDataArr[indexPath.row].image
             self.navigationController?.pushViewController(viewController, animated: true)
                
            }
                
            else if (self.newsFeedDataArr[indexPath.row].type == "gallery"){
                self.pictures.removeAll()
                CollieGalleryOptions.sharedOptions.enableSave = false
                CollieGalleryOptions.sharedOptions.enableInteractiveDismiss = true
                
                for i in 0..<self.newsFeedDataArr[indexPath.row].images.count{
                    self.pictures.append(CollieGalleryPicture(url: self.newsFeedDataArr[indexPath.row].images[i]))
                }
                
                let gallery = CollieGallery(pictures: pictures)
                gallery.presentInViewController(self)
            }
            
        
    }
    
    func playInYoutube(youtubeId: String) {
        if let youtubeURL = URL(string: "youtube://\(youtubeId)"),
            UIApplication.shared.canOpenURL(youtubeURL) {
            // redirect to app
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        } else if let youtubeURL = URL(string: "https://www.youtube.com/watch?v=\(youtubeId)") {
            // redirect through safari
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        
     
            
            if(self.newsFeedDataArr[indexPath.row].type == "video"){
                
                let videoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedTableViewVideoCell", for: indexPath) as! NewsFeedTableViewVideoCell
                
                videoTableViewCell.videoTitleLabel.text = self.newsFeedDataArr[indexPath.row].title
                
               
            
                
                if let imgUrl = self.newsFeedDataArr[indexPath.row].video {
                    videoTableViewCell.videoThumbnailImageView.sd_setImage(with: URL(string: getYoutubeThumbNail(youtubeUrl: imgUrl)! ), placeholderImage: UIImage(named: "placeholder_cell"), options: [.continueInBackground, .progressiveLoad])
                }
                
                cell = videoTableViewCell as NewsFeedTableViewVideoCell
            }
                
            else if(self.newsFeedDataArr[indexPath.row].type == "news"){
                let newsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedTableViewNormalCell", for: indexPath) as! NewsFeedTableViewNormalCell
                
             
              
                
                newsTableViewCell.newsTitleLabel.text = self.newsFeedDataArr[indexPath.row].title!
                
//                if newsTableViewCell.newsTitleLabel.numberOfVisibleLines == 1 {
//                    print("number of line : 1")
//                    newsTableViewCell.newsDetailLabel.numberOfLines = 5
//                }
//                else {
//                    newsTableViewCell.newsDetailLabel.numberOfLines = 4
//
//                }
                print(self.newsFeedDataArr[indexPath.row].descriptionField)
                newsTableViewCell.newsDetailLabel.setHTMLFromString(htmlText: self.newsFeedDataArr[indexPath.row].descriptionField)
               // newsTableViewCell.newsDetailLabel.text = self.newsFeedDataArr[indexPath.row].descriptionField.htmlToStrings()
                newsTableViewCell.newsDetailLabel.textColor = UIColor.lightGray
               
                newsTableViewCell.newsDetailLabel.numberOfLines = self.numberOfLines
                newsTableViewCell.newsDetailLabel.lineBreakMode = .byTruncatingTail
               
                
                if let imgUrl = self.newsFeedDataArr[indexPath.row].image {
                    newsTableViewCell.newsImageView.sd_setImage(with: URL(string: imgUrl ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad, .scaleDownLargeImages])
                }
                
                cell = newsTableViewCell as NewsFeedTableViewNormalCell
            }
                
            else if(self.newsFeedDataArr[indexPath.row].type == "gallery"){
                
                let newsGalleryCell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedTableViewGalleryCell", for: indexPath) as! NewsFeedTableViewGalleryCell
                
                newsGalleryCell.galleryTitleLabel.text = self.newsFeedDataArr[indexPath.row].title
                
                
                if(self.newsFeedDataArr[indexPath.row].images.count == 1){
                    newsGalleryCell.thirdView.isHidden = true
                    newsGalleryCell.galleryImageView1.sd_setImage(with: URL(string:  self.newsFeedDataArr[indexPath.row].images[0] ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad])
                    newsGalleryCell.galleryImageView2.isHidden = true
                    
                }
                    
                else if (self.newsFeedDataArr[indexPath.row].images.count == 2) {
                    newsGalleryCell.overLayViewLabel.isHidden = true
                    newsGalleryCell.overLayView.isHidden = true
                    newsGalleryCell.thirdView.isHidden = true
                    newsGalleryCell.galleryImageView2.isHidden = false
                    newsGalleryCell.galleryImageView1.isHidden = false
                    
                    newsGalleryCell.galleryImageView1.sd_setImage(with: URL(string:  self.newsFeedDataArr[indexPath.row].images[0] ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad])
                    
                    newsGalleryCell.galleryImageView2.sd_setImage(with: URL(string:  self.newsFeedDataArr[indexPath.row].images[1] ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad])
                    
                    newsGalleryCell.thirdView.isHidden = true
                    
                    
                }
                    
                else if (self.newsFeedDataArr[indexPath.row].images.count == 3){
                    newsGalleryCell.thirdView.isHidden = false
                    newsGalleryCell.overLayView.isHidden = true
                    newsGalleryCell.galleryImageView2.isHidden = false
                    newsGalleryCell.galleryImageView1.isHidden = false
                    newsGalleryCell.galleryImageView3.isHidden = false
                    
                    
                    newsGalleryCell.galleryImageView1.sd_setImage(with: URL(string:  self.newsFeedDataArr[indexPath.row].images[0] ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad])
                    
                    newsGalleryCell.galleryImageView2.sd_setImage(with: URL(string:  self.newsFeedDataArr[indexPath.row].images[1] ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad])
                    
                    newsGalleryCell.galleryImageView3.sd_setImage(with: URL(string:  self.newsFeedDataArr[indexPath.row].images[2] ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad])
                    
                    
                }
                    
                else if (self.newsFeedDataArr[indexPath.row].images.count>3){
                    newsGalleryCell.overLayViewLabel.isHidden = false
                    newsGalleryCell.overLayView.isHidden = false
                    newsGalleryCell.thirdView.isHidden = false
                    newsGalleryCell.galleryImageView2.isHidden = false
                    newsGalleryCell.galleryImageView1.isHidden = false
                    newsGalleryCell.galleryImageView3.isHidden = false
                    

                    
                    newsGalleryCell.galleryImageView1.sd_setImage(with: URL(string:  self.newsFeedDataArr[indexPath.row].images[0] ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad])
                    
                    newsGalleryCell.galleryImageView2.sd_setImage(with: URL(string:  self.newsFeedDataArr[indexPath.row].images[1] ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad])
                    
                    newsGalleryCell.galleryImageView3.sd_setImage(with: URL(string:  self.newsFeedDataArr[indexPath.row].images[2] ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad])
                    
                    newsGalleryCell.moreCountLabel.text = "+\(self.newsFeedDataArr[indexPath.row].images.count-3) \nMore"
                    
                }
                
                cell = newsGalleryCell as NewsFeedTableViewGalleryCell
                
            
            
            
            } else if (self.newsFeedDataArr[indexPath.row].type == nil) {
                let cell = newsFeedTableView.dequeueReusableCell(withIdentifier: "AdTVC", for: indexPath) as! AdTVC
                let adRequest = GADRequest()
                cell.adBannerView.load(GADRequest())
                cell.adBannerView.delegate = self
                cell.adBannerView.rootViewController = self
                cell.googleAdView.addSubview(cell.adBannerView)
                return cell
            }
        
        
        return cell
    }
    
    @IBOutlet var newsFeedTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newsFeedTableView.delegate = self
        newsFeedTableView.dataSource = self
        newsFeedTableView.register(UINib(nibName: "AdTVC", bundle: nil), forCellReuseIdentifier: "AdTVC")
        
        
        let insets = UIEdgeInsets(top: 8 , left: 0, bottom: 0, right: 0)
        self.newsFeedTableView.contentInset = insets
        
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back_arrow"), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        button.sizeToFit()
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)

        
        self.navItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
        
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        
        newsFeedTableView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        newsFeedTableView.infiniteScrollIndicatorMargin = 50
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            
            videoCellHeight = 400
            galleryCellHeight = 400
            
            newsCellHeight  = 200
            numberOfLines = 8
            
        }
        
        

        
        
        getNewsFeedData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func dismissVC(){
        
        self.navigationController?.popViewController(animated: true)
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    func getNewsFeedData(){
 
        
     
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    
        let headers = APIHandler.shared.createHeader()
        AF.request(nexPageURL, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print("Getting news data")
                    print(json)
                    if(json["status"].boolValue){
                        if let newsFeedArray = json["data"].array{
                            if(newsFeedArray.count>0){
                                                        if self.shouldReloadTableView {
                                    var count = 1
                                    var diff = 5
                                    for i in 0..<newsFeedArray.count{
                                        if diff == i {
                                            count += 1
                                            diff = diff * count
                                            let newsfeed = NewsFeedData(json: JSON())
                                            self.newsFeedDataArr.append(newsfeed)
                                        } else {
                                            let newsFeed = NewsFeedData(json: newsFeedArray[i])
                                            self.newsFeedDataArr.append(newsFeed)
                                        }
                                    
                                       
                                    }
                                    print(self.newsFeedDataArr)
                                    self.activityIndicatorView.stopAnimating()
                                self.newsFeedTableView.reloadData()
                                }
                                else {
                                
                                    let photoCount = self.newsFeedDataArr.count
                                    let (start, end) = (photoCount, newsFeedArray.count + photoCount)
                                    let indexPaths = (start..<end).map { return IndexPath(row: $0, section: 0) }
                                    var count = 1
                                    var diff = 5
                                    for i in 0..<newsFeedArray.count{
                                        if diff == i {
                                            count += 1
                                            diff = diff * count
                                            let newsfeed = NewsFeedData(json: JSON())
                                            self.newsFeedDataArr.append(newsfeed)
                                        } else {
                                            let newsFeed = NewsFeedData(json: newsFeedArray[i])
                                            self.newsFeedDataArr.append(newsFeed)
                                        }
                                       
                                    }
                                    
                                    if #available(iOS 11.0, *) {
                                        self.newsFeedTableView.performBatchUpdates({ () -> Void in
                                            self.newsFeedTableView.insertRows(at: indexPaths, with:  .bottom)
                                            
                                        }, completion: { (finished) -> Void in
                                            self.newsFeedTableView.finishInfiniteScroll()
                                        })
                                    } else {
                                            self.newsFeedTableView.beginUpdates()
                                            self.newsFeedTableView.insertRows(at: indexPaths, with: .bottom)
                                        
                                            self.newsFeedTableView.endUpdates()

                                    };
                                    
                                }
                                
                                self.newsFeedTableView.finishInfiniteScroll()
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                
                            }
                            
                            
                                    
                            self.nexPageURL = json["links"]["next"].stringValue
                            self.shouldReloadTableView = false
                            
                            
                            self.newsFeedTableView?.addInfiniteScroll {  (movieCollectionView) -> Void in
                                
                                if self.nexPageURL != ""{
                                    self.getNewsFeedData()
                                    
                                }
                                    
                                else {
                                    self.newsFeedTableView.finishInfiniteScroll()
                                    
                                }
                                
                                
                            }
                            
                            

                            
                            
                            
                        }
                        
                    }
                }
                break
                
            case .failure(let error):
                print(error.localizedDescription)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                if(error._code == NSURLErrorNotConnectedToInternet){
                    let controller = UIAlertController(title: "No Internet Connection.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                    let cancel = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                    let OKAction = UIAlertAction(title: "Retry", style: .default) { (action:UIAlertAction!) in
                        self.getNewsFeedData()
                    }
                    controller.addAction(OKAction)
                    controller.addAction(cancel)
                    self.present(controller, animated: true, completion: nil)
                    
                }
                    
                else if(error._code == NSURLErrorTimedOut){
                    let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                    let cancel = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                    let OKAction = UIAlertAction(title: "Retry", style: .default) { (action:UIAlertAction!) in
                        self.getNewsFeedData()
                    }
                    controller.addAction(OKAction)
                    controller.addAction(cancel)
                    self.present(controller, animated: true, completion: nil)
                }
                break
            }
            
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       
            if(self.newsFeedDataArr[indexPath.row].type == "video"){
                
                return self.videoCellHeight
            }
            else if (self.newsFeedDataArr[indexPath.row].type == "gallery") {
                if( UIScreen.main.nativeBounds.height == 1136 ){
                    
                    return 170
                }
                    
                else {
                    
                    if self.newsFeedDataArr[indexPath.row].images.count == 1 {
                        
                        return 600
                    }
                    else {
                        return galleryCellHeight

                    }
                    
                }
            } else if (self.newsFeedDataArr[indexPath.row].type == nil) {
                return 170
            }
                
            else {
                return self.newsCellHeight
            }
        
        
    }
    
    
    
    
    func getYoutubeThumbNail(youtubeUrl: String) -> String? {
        
        if let  videoId = URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value{
            return "https://img.youtube.com/vi/"+videoId+"/hqdefault.jpg"
            
        }
        else {
            return ""
        }
        
    }
    
}





func getYoutubeId(youtubeURL:String) -> String?{
    
    return URLComponents(string: youtubeURL)?.queryItems?.first(where: { $0.name == "v" })?.value
    
}



extension UILabel {
   // "#424242"
    func setHTMLFromString(htmlText: String, hexCode: String = "#FFFFFF") {
        let modifiedFont = NSString(format:"<span style=\"color:\(hexCode);font-family: '-apple-system', 'HelveticaNeue-Medium'; font-size: \(13)\">%@</span>" as NSString, htmlText) as String
        //process collection values
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        
        self.attributedText = attrStr
    }
    
    
    
    
        var numberOfVisibleLines: Int {
            let textSize = CGSize(width: CGFloat(self.frame.size.width), height: CGFloat(MAXFLOAT))
            let rHeight: Int = lroundf(Float(self.sizeThatFits(textSize).height))
            let charSize: Int = lroundf(Float(self.font.pointSize))
            return rHeight / charSize
        
    }
}
extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}
extension NewsFeedViewController: GADBannerViewDelegate {
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("bannerViewDidReceiveAd")
    }

    private func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
      print("bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    func adViewDidRecordImpression(_ bannerView: GADBannerView) {
      print("bannerViewDidRecordImpression")
    }

    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillPresentScreen")
    }

    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillDIsmissScreen")
    }

    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewDidDismissScreen")
    }
}
extension String {
    func htmlToStrings() -> String {
        return  try! NSAttributedString(data: self.data(using: .utf8)!,
                                        options: [.documentType: NSAttributedString.DocumentType.html],
                                        documentAttributes: nil).string
    }
}


