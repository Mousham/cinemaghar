//
//  NewsDetailViewController.swift
//  KBC Nepal
//
//  Created by Sunil on 2/7/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit

class NewsDetailViewController: DesignableViewController, UIScrollViewDelegate {
    var navBar:UINavigationBar = UINavigationBar()
    
    var newsTitle:String?
    var  newsDetail: String?
    var imageURL = ""
    let imageView = UIImageView()
    var button = UIButton()
    
    var headerHeight = CGFloat(250)

    
    var isCollapsed:Bool = false
    @IBOutlet var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            headerHeight = 500
            
 
            
        }
        scrollView.contentInset = UIEdgeInsets(top:headerHeight, left: 0, bottom: 0, right: 0)

        
        view.addSubview(imageView)
        let rect = CGRect(x:0, y:0, width:view.bounds.width, height:headerHeight)
        
        
        
        imageView.frame = rect
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        
        
        
        self.imageView.sd_setImage(with: URL(string: imageURL ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad])
        setNavigationBar()
        
        
        self.newsDetailLabel.setHTMLFromString(htmlText: newsDetail!)
        self.newsDetailLabel.textColor = .lightGray
        self.newsTitleLabel.text = newsTitle
        
        
        

        // Do any additional setup after loading the view.
    }
    @IBOutlet var newsTitleLabel: UILabel!
    
    @IBOutlet var newsDetailLabel: UILabel!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let y = headerHeight - (scrollView.contentOffset.y + headerHeight)
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {

            let h = max(76, y)
            let rect = CGRect(x:0, y:0, width:view.bounds.width, height:h)
            imageView.frame = rect
            
            
            if(h == 76){
                button.setTitleColor(UIColor.white, for: .normal)
                button.tintColor = UIColor.white
                isCollapsed = true
                navBar.setBackgroundImage(nil, for: UIBarMetrics.default)
                navBar.shadowImage = UIImage()
                navBar.isTranslucent = false
//                self.navBar.barTintColor = Util.hexStringToUIColor(hex: "FFA900")
                self.navBar.barTintColor = Util.hexStringToUIColor(hex: "0E1525")
                
                imageView.isHidden = true
                //UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
                self.setNeedsStatusBarAppearanceUpdate()
                
                
            }
            else {
                isCollapsed = false
                button.setTitleColor(UIColor.black, for: .normal)
                button.tintColor = UIColor.black
                imageView.isHidden = false
                self.navBar.barTintColor = UIColor.clear
                navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                navBar.shadowImage = UIImage()
                navBar.isTranslucent = true
                //            UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
                self.setNeedsStatusBarAppearanceUpdate()
                
                
            }
            
        }
        else {
            let h = max(64, y)
            let rect = CGRect(x:0, y:0, width:view.bounds.width, height:h)
            
            imageView.frame = rect
            
            
            
            
            if(h == 64){
                isCollapsed = true
                button.setTitleColor(UIColor.white, for: .normal)
                
                button.tintColor = UIColor.white

                navBar.setBackgroundImage(nil, for: UIBarMetrics.default)
                navBar.shadowImage = UIImage()
                navBar.isTranslucent = false
//                self.navBar.barTintColor = Util.hexStringToUIColor(hex: "F4BE45")
                self.navBar.barTintColor = Util.hexStringToUIColor(hex: "0E1525")
                
                imageView.isHidden = true
                //            UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
                self.setNeedsStatusBarAppearanceUpdate()
                
                
                
            }
            else {
                isCollapsed = false
                button.setTitleColor(UIColor.white, for: .normal)
                button.tintColor = UIColor.white

                imageView.isHidden = false
                self.navBar.barTintColor = UIColor.clear
                navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                navBar.shadowImage = UIImage()
                navBar.isTranslucent = true
                //            UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
                self.setNeedsStatusBarAppearanceUpdate()
                
                
            }
        }
        
    }
    
   
    
    func setNavigationBar() {
        let screenSize: CGRect = UIScreen.main.bounds
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
            navBar = UINavigationBar(frame: CGRect(x: 0, y: 36, width: screenSize.width, height: 44))
            
        }
        else {
            navBar = UINavigationBar(frame: CGRect(x: 0, y: 20, width: screenSize.width, height: 44))
        }
        
        
        navBar.titleTextAttributes = [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.white]
        let navItem = UINavigationItem(title: "")
        
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
        
        button = UIButton(type: .system)
        button.setImage(UIImage(named: "back_arrow"), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.medium)
        button.addTarget(self, action: #selector(done), for: .touchUpInside)
        button.sizeToFit()
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)
        navItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        navBar.setItems([navItem], animated: false)
        self.view.addSubview(navBar)
    }
    
    
    
    @objc func done(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    

}
