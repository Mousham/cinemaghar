//
//  ReviewVC.swift
//  CinemaGhar
//
//  Created by Midas on 08/07/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire
protocol ReviewDelegate: class {
    func continueTap(rating: String,reviewTxt: String)
}
class ReviewVC: UIViewController {
    @IBOutlet weak var reviewFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var reviewField: UITextView!
    @IBOutlet weak var starView: CosmosView!
    weak var delegate: ReviewDelegate?
    var rating: Double? = 4.5
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetp()
    }
    
    static func instantiate() -> ReviewVC {
        return (UIStoryboard(name: "reward", bundle: nil).instantiateViewController(withIdentifier: "ReviewVC") as? ReviewVC)!
    }
    func uisetp() {
        starView.rating = rating ?? 0.0
        reviewFieldHeight.constant = 0
        starView.didTouchCosmos = didTouchCosmos
        reviewField.delegate = self
        reviewField.text = "Enter your review here"
        reviewField.textColor = UIColor.lightGray
       
    }
    private func didTouchCosmos(_ rating: Double) {
        print(rating)
        self.rating = rating
       
//      ratingSlider.value = Float(rating)
//      updateRating(requiredRating: rating)
//      ratingLabel.text = ViewController.formatValue(rating)
//      ratingLabel.textColor = UIColor(red: 133/255, green: 116/255, blue: 154/255, alpha: 1)
    }
    @IBAction func continueTap(_ sender: Any) {
        guard let _ = rating else { return }
        guard let _ = reviewField.text else { return }
      
        self.dismiss(animated: true) {
            self.delegate?.continueTap(rating: String(self.rating ?? 0), reviewTxt: self.reviewField.text ?? "")
        }
    }
    

}
extension ReviewVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if reviewField.textColor == UIColor.lightGray {
            reviewField.text = nil
            reviewField.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if reviewField.text.isEmpty {
            reviewField.text = "Enter your review here"
            reviewField.textColor = UIColor.lightGray
        }
    }
}
