//
//  SeriesDetailViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 6/26/18.
//  Copyright © 2018 sunBi. All rights rescheckIfContentIsPaiderved.
//

import UIKit
import SDWebImage
import GoogleCast

class SeriesDetailViewController: DesignableViewController, UIGestureRecognizerDelegate, UIScrollViewDelegate, UINavigationBarDelegate {
    @IBOutlet weak var scrolViewMain: UIScrollView!
    @IBOutlet weak var seriesBackGroundImageView: UIImageView!
    @IBOutlet weak var seriesTitleLabel: UILabel!
    @IBOutlet weak var episodesTableView: UITableView!
    @IBOutlet weak var seriesPriceLabel: UILabel!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var seriesPosterImageView: UIImageView!
    var shouldMoveUP: Bool = true
    
    @IBOutlet weak var seasonChooserBtn: UIButton!
    @IBAction func seasonsBtnPressed(_ sender: Any) {
        
        if seriesSeason.count>1{
            
            let actionsheet = UIAlertController(title: self.series?.name, message: "Select a Season", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            for i in 0..<seriesSeason.count{
                
                actionsheet.addAction(UIAlertAction(title: "Season \(i+1)", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                    
                    self.seasonChooserBtn.setTitle("Season \(i+1)", for: .normal)
                    self.filterSeriesBySeason(index: i)
                    
                }))
            }
            actionsheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            actionsheet.popoverPresentationController?.sourceView = self.view
            actionsheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            actionsheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                        
            self.present(actionsheet, animated: true, completion: nil)
            
        }
    }
    @IBOutlet weak var castsLabel: UILabel!
    @IBOutlet weak var seriesPriceLabelHolderView: UIView!
    @IBOutlet weak var SeriesPosterImageHolderView: UIView!
    
    @IBOutlet weak var descrLabel: UILabel!
    @IBOutlet weak var titleTopMargin: NSLayoutConstraint!
    var seriesSeason = [SeriesSeason]()
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var series: Series?
    var seasonIndex = 0
    var seriesID = "0"
    var sessionManager = SessionManager()
    
    func filterSeriesBySeason(index: Int){
        seasonIndex = index
        episodesTableView.reloadData()
        self.tableViewHeightConstraint.constant = CGFloat(Int (self.seriesSeason[self.seasonIndex].episodes.count * 100 + 50))

    }
    
    @IBOutlet weak var titleLabelTopMarginCOnstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    
    
    override func viewDidLoad() {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
//        series?.isBought = true
        
        SeriesPosterImageHolderView.backgroundColor = .clear
        SeriesPosterImageHolderView.layer.borderColor = UIColor.gray.cgColor
        SeriesPosterImageHolderView .layer.borderWidth = 0.5
        //        var rect = CGRect(x:10, y:33, width:30, height:30)
        //        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
        //            rect = CGRect(x:10, y:50, width:30, height:30)
        //            titleTopMargin.constant = 20
        //        }
        
        let backButton = UIButton() // back button
        backButton.backgroundColor = UIColor.white
        backButton.layer.cornerRadius = 15
        backButton.addShadow(offset: CGSize(width: 1, height: 1), color: .darkGray, radius: 2, opacity: 0.9)
        
        backButton.setImage(UIImage(named: "icon_navBar_Back"), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(22,22,22,23)
        backButton.imageView?.tintColor = UIColor.darkGray
        self.view.addSubview(backButton)
        backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        backButton.snp.makeConstraints{(make) in
            
            make.centerY.equalTo(self.navBar).offset(0)
            make.height.equalTo(30)
            make.width.equalTo(30)
            make.left.equalTo(self.view).offset(12)
        }
        
        
        
        let backgroundColorView = UIView()
        backgroundColorView.backgroundColor = UIColor.clear
        UITableViewCell.appearance().selectedBackgroundView = backgroundColorView
        
        APESuperHUD.appearance.backgroundBlurEffect = .dark
        
        APESuperHUD.appearance.titleFontSize = 20
        APESuperHUD.appearance.iconWidth = 48
        APESuperHUD.appearance.loadingActivityIndicatorColor = UIColor.darkText
        APESuperHUD.appearance.iconHeight = 48
        APESuperHUD.appearance.messageFontSize = 16
        APESuperHUD.appearance.textColor = UIColor.darkText
        
        APESuperHUD.appearance.messageFontName  = "Avenir-Medium"
        APESuperHUD.appearance.foregroundColor = .white
        navBar.backIndicatorImage = UIImage()
        navBar.alpha = 0
        navBar.isTranslucent = true
        navBar.shadowImage = UIImage()
        navBar.delegate = self
        scrolViewMain.delegate = self
        
        howToWatchBtn.layer.cornerRadius = howToWatchBtn.frame.height/2
        
        self.seriesPriceLabelHolderView.layer.cornerRadius = self.seriesPriceLabelHolderView.frame.height/2
        
        episodesTableView.register(UINib(nibName: "SeriesEpisodesTableViewCell", bundle: nil), forCellReuseIdentifier: "SeriesEpisodesTableViewCell")
        
        if(series == nil){
            APIHandler.shared.getSeriesDetail(seriesID: seriesID) { (status, seriesDetail) in
                if(status){
                    self.series = seriesDetail
                    self.renderView()
                }
            } failure: { (error) in
                print(error.localizedDescription)
            }

        }else{
            self.renderView()
        }
        
       
        
        
    }
    
    func renderView(){
        
        if let seriesTitle = series?.name, let posterImageURL = series?.image, let isPaidSeries = series?.paid, let casts = series?.casts, let desc = series?.desc, let releaseYear = series?.releaseYear, let isBought = series?.isBought, let isUnderSubscription = series?.isUnderSubscription{
            seriesTitleLabel.text = seriesTitle
            descrLabel.setHTMLFromString(htmlText: desc, hexCode: "#ffffff")
            castsLabel.text = casts
            titleLabel.text = seriesTitle
            releaseYearLabel.text = "Release Year: \(releaseYear)"
            seriesPosterImageView.sd_setImage(with: URL(string: posterImageURL), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad], completed:nil)
            
            seriesBackGroundImageView.sd_setImage(with: URL(string: posterImageURL), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad], completed:nil)
            
            if isPaidSeries {
                if isUnderSubscription {
                    self.seriesPriceLabelHolderView.backgroundColor = Util.hexStringToUIColor(hex: "#E59827")
                    self.seriesPriceLabel.text = " Gold Content "
                    self.howToWatchBtn.isHidden = false
                }else if isBought{
                    self.seriesPriceLabelHolderView.backgroundColor = Util.hexStringToUIColor(hex: "#08ABE8")
                    self.seriesPriceLabel.text = " PURCHASED "
                    self.howToWatchBtn.isHidden = true

                }
                else {
                    self.seriesPriceLabelHolderView.backgroundColor = .red
                    self.seriesPriceLabel.text = " PREMIUM "
                    self.howToWatchBtn.isHidden = false

                }
              
               
                
                
            }
            else {
                self.seriesPriceLabelHolderView.backgroundColor = Util.hexStringToUIColor(hex: "008000")
                self.seriesPriceLabel.text = " FREE "
                self.howToWatchBtn.isHidden = true

            }
            
            
            
            
        }
       
        getEpisodes(id: (series?.id!)!)
        
    }
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var releaseYearLabel: UILabel!
    
    @IBOutlet weak var seasonsCountLabel: UILabel!
    func getEpisodes(id:Int){
        
        indicator.startAnimating()
        
        APIHandler.shared.getSeriesDetail(seriesID: id, success: {(status, seriesSeasonArray) in
            
            self.seriesSeason = seriesSeasonArray
            self.episodesTableView.delegate = self
            self.episodesTableView.dataSource = self
            self.episodesTableView.reloadData()
            self.indicator.stopAnimating()
            self.tableViewHeightConstraint.constant = CGFloat(Int (self.seriesSeason[self.seasonIndex].episodes.count * 100 + 50))
            self.seasonsCountLabel.text = "Seasons: " + String(seriesSeasonArray.count)
            
        }, failure: {(failure) in
            
            
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.getEpisodes(id: id)
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                    self.getEpisodes(id: id)
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                self.getEpisodes(id: id)
                
            }
            
            
            
            
            
        })
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
//        print(offset)
        

        
        let h = max(60, offset)
//        print(h)
        
        if h == 60 {
            titleLabel.isHidden = true
            titleLabelTopMarginCOnstraint.constant = -h/2

//            print("not collapsed")
        }
        else {
            
            titleLabel.isHidden = false
//            print("collapsed")
        }


        
        
    }
  
    @IBOutlet weak var howToWatchBtn: UIButton!
    @IBAction func howToWatchBtnPressed(_ sender: Any) {
        let alert = UIAlertController(title: "How do i watch this?", message: "Use this app to watch movies and/or web series you buy from Cinemaghar on the web or other devices. Watch right here on your iPhone, or send the movie to any TV connected to a chromecast or Apple TV. \n \n In-app purchase is not supported on this device.", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
        
        
    }
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
}



extension SeriesDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.seriesSeason[self.seasonIndex].episodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SeriesEpisodesTableViewCell", for: indexPath) as! SeriesEpisodesTableViewCell
        
        
        cell.setEpisodeDetail(index: indexPath.row+1 ,episode: self.seriesSeason[self.seasonIndex].episodes[indexPath.row])
        cell.selectionColor = UIColor.clear
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if ((self.series?.paid)! && self.seriesSeason[seasonIndex].episodes[indexPath.row].paid) {
            
            if sessionManager.isLoggedIn() {
 
                if let id = self.series?.id, let episodeId = self.seriesSeason[seasonIndex].episodes[indexPath.row].id{
                    checkIfContentIsPaid(seriesID: id, episodeId: episodeId, episodeIndex: indexPath.row)

                }
            }
                
            else {
                
                let signUpVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                signUpVC.mediaType = .series
                signUpVC.contentId = (series?.id)!
                signUpVC.episodeId = self.seriesSeason[self.seasonIndex].episodes[indexPath.row].id
                signUpVC.shouldAllowSkip = false
                signUpVC.shouldDismissAfterLogin = true
                self.navigationController?.pushViewController(signUpVC, animated: true)
            }
            
        }
            
        else {
           
                
                if let isVideoFromYoutube = self.seriesSeason[self.seasonIndex].episodes[indexPath.row].videoIsFromYoutube {
                    
                    if isVideoFromYoutube {
                        let youtubePlayerVC             = FreeMoviePlayerViewController.instantiate(fromAppStoryboard: .FreeMoviePlayer)
                        if let youtubeURL               = self.seriesSeason[self.seasonIndex].episodes[indexPath.row].video {
                            youtubePlayerVC.youtubeURL  = youtubeURL
                            youtubePlayerVC.mediaType = .series
                            self.navigationController?.pushViewController(youtubePlayerVC, animated: true)
                        }
                    }
                    else {
                        
                            if let videoURL = self.seriesSeason[self.seasonIndex].episodes[indexPath.row].video {
//                                let viewController = UIStoryboard(name: "PaidVideoPlayer", bundle: nil).instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
////                                viewController.mediaInfo = self.buildMediaInformation(url: videoURL)
//                                viewController.mediaType = .series
//                                viewController.hasUserRatedMovie = true
//                                viewController.contentId = (self.series?.id)!
//                                viewController.videoTitle = (self.series?.name)!
//                                self.navigationController?.pushViewController(viewController, animated: true)
                                print(self.seriesSeason[self.seasonIndex].episodes[indexPath.row])
                                let videoPlayerVC = CustomVideoViewController.instantiate(fromAppStoryboard: .CustomVideoPlayer)
                                videoPlayerVC.mediaInfo = self.buildMediaInformation(urlString: videoURL)
                                videoPlayerVC.urlString = videoURL
                                videoPlayerVC.videoTitle = self.seriesSeason[self.seasonIndex].episodes[indexPath.row].title
                                videoPlayerVC.contentId = self.seriesSeason[self.seasonIndex].episodes[indexPath.row].id
                                videoPlayerVC.mediaType = .series
                                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
//
                            }
                        
                       
                        
                     


                }
                
                
              }
                
                

                
                
            }
            
           
            
        
        
    }
    
    private func buildMediaInformation(urlString:String) -> GCKMediaInformation {
        let metadata = GCKMediaMetadata(metadataType: GCKMediaMetadataType.movie)
        metadata.setString((series?.name)!,  forKey: kGCKMetadataKeyTitle)
        metadata.setString((series?.casts)!, forKey: kGCKMetadataKeyStudio)
        metadata.addImage(GCKImage(url: URL(string: (series?.image)!)!, width: 500, height: 500))
        var customData = [String:String]()

        if SessionManager().isLoggedIn() {
            if let title = self.series?.name{
                customData = ["userID": UserDefaults.standard.string(forKey: AppConstants.uniqueId)! , "userName": UserDefaults.standard.string(forKey: AppConstants.userName)! , "movieTitle": title]
            }
        }
        else {
            if let title = self.series?.name{

                customData = ["userID": " " , "userName": " " , "movieTitle": title]
            }

        }
        
        let url = URL.init(string: urlString)!
        

        let mediaInfoBuilder = GCKMediaInformationBuilder.init(contentURL: url)
        mediaInfoBuilder.streamType = GCKMediaStreamType.none;
//        mediaInfoBuilder.contentType =
        mediaInfoBuilder.metadata = metadata
        mediaInfoBuilder.contentID = urlString
        mediaInfoBuilder.customData = customData
        let mediaInformation = mediaInfoBuilder.build()
        
        
        return mediaInformation
    }
    
    
    
    
    
    
    
    func checkIfContentIsPaid(seriesID: Int, episodeId:Int, episodeIndex: Int) {
        
        APESuperHUD.showOrUpdateHUD( loadingIndicator: .standard, message: "Loading Content...", presentingView: self.view)
        APIHandler.shared.checkIfVideoIsPurchased(contentType: .series, contentId: seriesID, episodeId: episodeId, success: {(status, videoURL, hasUserBought, hasUserRated, isComingSoon) in
            
            if hasUserBought {
                APESuperHUD.removeHUD(animated: false, presentingView: self.view)
//                let viewController = UIStoryboard(name: "PaidVideoPlayer", bundle: nil).instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
//                viewController.mediaInfo = self.buildMediaInformation(url: videoURL)
//                viewController.mediaType = .series
//                viewController.hasUserRatedMovie = true
//                viewController.contentId = (self.series?.id)!
//                viewController.videoTitle = (self.series?.name)!
//                self.navigationController?.pushViewController(viewController, animated: true)
//
                let videoPlayerVC = CustomVideoViewController.instantiate(fromAppStoryboard: .CustomVideoPlayer)
                videoPlayerVC.mediaInfo = self.buildMediaInformation(urlString: videoURL)
                videoPlayerVC.urlString = videoURL
                videoPlayerVC.videoTitle = (self.series?.name)!
                videoPlayerVC.contentId = (self.series?.id)!
                videoPlayerVC.mediaType = .series
                videoPlayerVC.isComingSoon = isComingSoon
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
           
            }
            else {
                APESuperHUD.removeHUD(animated: false, presentingView: self.view)
                let alert = UIAlertController(title: "How do i watch this?", message: "Use this app to watch movies and/or web series you buy from Cinemaghar on the web or other devices. Watch right here on your iPhone, or send the movie to any TV connected to a chromecast or Apple TV. \n \n In-app purchase is not supported on this device.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                
                
                
            }
        }, failure: {(failure) in
            
            
            
            
        })
    }
}


extension UITableViewCell {
    var selectionColor: UIColor {
        set {
            let view = UIView()
            view.backgroundColor = UIColor.clear
            self.selectedBackgroundView = view
        }
        get {
            return self.selectedBackgroundView?.backgroundColor ?? UIColor.clear
        }
    }
}


extension UIView {
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}
