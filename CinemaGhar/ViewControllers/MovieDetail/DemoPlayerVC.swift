//
//  DemoPlayerVC.swift
//  CinemaGhar
//
//  Created by Midas on 31/05/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import AVFoundation

class DemoPlayerVC: UIViewController {

    @IBOutlet weak var playerView: UIView!
    var playerLayer: AVPlayerLayer!
    var offlineItem: AVPlayerItem?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetup()
    }
    func uisetup() {
        playerLayer = AVPlayerLayer()
        playerLayer.frame = playerView.bounds
      
        
        let player = AVPlayer(playerItem: offlineItem)
        playerLayer.player = player
        playerLayer.frame = playerView.bounds
        playerLayer.player?.play()
        playerView.layer.addSublayer(playerLayer)
    }
  
        
    

 

}
