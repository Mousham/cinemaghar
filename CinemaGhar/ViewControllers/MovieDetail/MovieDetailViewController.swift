//
//  MovieDetailViewController2.swift
//  CinemaGhar
//
//  Created by Sunil on 5/30/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import Cosmos
import GoogleCast
import StoreKit
import Alamofire
import SafariServices
import PassKit
import Photos
import HLSDownloader
import AVKit
import VidLoader
import GoogleMobileAds
var globalisPurchasedFromApple: Bool = false
var globalMovieDetail: Movie?
var globalVideoData = [VideoData]()
protocol CastPlayerDelegate {
    //    func popViewController()
    //    func  ratingComplete(title:String, message:String)    
    func castMedia(url:String, hasUserRated: Bool)
    
    func sendPlayBtnClickAction()
    
    func validatePayment(paymentMethod: paymentGateway, token: String , contentID: String, contentType: MediaType, episodeId: Int)
    
    
    func checkIfContentIsPaid(contentID: Int, shouldCast: Bool, contentType: MediaType, episodeId: Int )
    
    
}




class MovieDetailViewController: DesignableViewController,  UIGestureRecognizerDelegate, UINavigationBarDelegate   {
    @IBOutlet weak var downloadView: UIView!
    @IBOutlet weak var googleAdView: UIView!
  
    
    var subscribeAlertController: UIAlertController!
    var buyViaTokenAlertController: UIAlertController!
    var redirectToHome: Bool = false
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask? = nil
    var isDownloadBtnTap = false
    var avLayer: AVPlayerLayer!
    var sessionManager = SessionManager()
    lazy var adBannerView: GADBannerView = {
        let viewWidth = self.view.frame.size.width
        let adBannerView = GADBannerView(adSize: GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(viewWidth))
        adBannerView.adUnitID = Constant.googleBannerKey
        adBannerView.delegate = self
        adBannerView.rootViewController = self

        return adBannerView
    }()
    
    
    var manager: HLSManager {
        get {
            return ((UIApplication.shared.delegate!) as! AppDelegate).manager
        }
    }

    
    @IBAction func addToWishListBtnPressed(_ sender: Any) {
        
        if SessionManager().isLoggedIn(){
            
            if let isInWatchList = self.movieDetail?.isAddedToWishList{
                
                if isInWatchList {
                    
                    if let id = self.movieDetail?.id{
                        APIHandler.shared.editWishList(url:  CinemaGharAPI.removeFromWishListURL ,contentID: String(id), type: .movie, success: {(status) in
                            
                            if status {
                               
                                self.movieDetail?.isAddedToWishList = false
                            }
                            
                        }, error: {(err) in
                            
                            
                        })
                        
                    }
                }
                
                else {
                    if let id = self.movieDetail?.id{
                        APIHandler.shared.editWishList(url:  CinemaGharAPI.addToWishListURL ,contentID: String(id), type: .movie, success: {(status) in
                            
                            if status {
                                
                              
                                self.movieDetail?.isAddedToWishList = true
                        
                            }
                            
                        }, error: {(err) in
                            
                
                        })
                        
                    }
                    
                }
                
            }
            
         
            
            
        }
        
        else {
            
            let alert = UIAlertController(title: "Info!", message: "Please login with your Cinemaghar account to add this movie to your watchlist. Your watchlist is synced across all of your devices.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            
            
            alert.addAction(UIAlertAction(title: "Login", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in

                let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                loginVC.shouldAllowSkip = false
                loginVC.shouldDismissAfterLogin = true
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                
            }))

            
            self.present(alert, animated: true, completion: nil)
            
            
        }
        
        
    }
//    @IBOutlet weak var tagListView: TagListView!

    @IBAction func downloadtap(_ sender: Any) {
       
        if (self.sessionManager.isLoggedIn()){


        isDownloadBtnTap =  true
        checkIfContentIsPaid(contentID: (movieDetail?.id)!, shouldCast: false, contentType: .movie)

        } else {
            let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
//
    }
    
    
    
    @objc func gotoWeb(action: UIAlertAction){
        let url = URL(string: "https://cinema-ghar.com/" + (movieDetail?.slug ?? "") + "?" + "showPaymentModal=1")!
        if  UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    @IBOutlet weak var genreLabel: UILabel!
    
    @IBOutlet weak var navBar: UINavigationBar!
    func sendPlayBtnClickAction() {
       
        self.playButton.sendActions(for: .touchUpInside)
    }
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playButtonCardView: CardView!


    var isVCPushed:Bool = false
    
    
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
        
    }
    @IBOutlet weak var scrollView: UIScrollView!
    
    var refreshCollectionViewDelegate: RefreshCollectionViews?
    
    
    let application = UIApplication.shared.delegate as! AppDelegate

    
    @IBAction func purchaseBtnPressed(_ sender: Any) {
       
        
        
    }
    
    func setupSubscribeView(){
        let imageView = UIImageView(image: UIImage(named: "gold_crown"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        subscribeOutlet.addSubview(imageView)
        imageView.leftAnchor.constraint(equalTo: subscribeOutlet.leftAnchor, constant: 16).isActive = true
        imageView.topAnchor.constraint(equalTo: subscribeOutlet.topAnchor, constant: 16).isActive = true
        imageView.bottomAnchor.constraint(equalTo: subscribeOutlet.bottomAnchor, constant: -16).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        let mainLabel = UILabel()
        mainLabel.text = "Pay Per View"
        mainLabel.textColor = UIColor(hex: "#f3bc31")
        mainLabel.font = UIFont.systemFont(ofSize: 11, weight: .bold)
        mainLabel.translatesAutoresizingMaskIntoConstraints = false
        subscribeOutlet.addSubview(mainLabel)
        mainLabel.leftAnchor.constraint(equalTo: imageView.rightAnchor, constant: 8).isActive = true
        mainLabel.topAnchor.constraint(equalTo: subscribeOutlet.topAnchor, constant: 10).isActive = true
        mainLabel.rightAnchor.constraint(equalTo: subscribeOutlet.rightAnchor, constant: -16).isActive = true
        
        let secondaryLabel = UILabel()
        secondaryLabel.text = "to Cinemaghar Gold"
        secondaryLabel.textColor = UIColor(hex: "#f3bc31")
        secondaryLabel.font = UIFont.systemFont(ofSize: 8)
        secondaryLabel.translatesAutoresizingMaskIntoConstraints = false
        subscribeOutlet.addSubview(secondaryLabel)
        secondaryLabel.leftAnchor.constraint(equalTo: imageView.rightAnchor, constant: 8).isActive = true
        secondaryLabel.topAnchor.constraint(equalTo: mainLabel.bottomAnchor, constant: 8).isActive = true
        secondaryLabel.rightAnchor.constraint(equalTo: subscribeOutlet.rightAnchor, constant: -8).isActive = true
        secondaryLabel.bottomAnchor.constraint(equalTo: subscribeOutlet.bottomAnchor, constant: -10).isActive = true
        
        subscribeOutlet.backgroundColor = UIColor(white: 1, alpha: 0)
        subscribeOutlet.layer.cornerRadius = 10
        subscribeOutlet.layer.borderWidth = 3
        subscribeOutlet.layer.borderColor = UIColor(hex: "#f3bc31").cgColor
        
        
        let redeemImageView = UIImageView(image: UIImage(named: "redeem_ticket"))
        redeemImageView.translatesAutoresizingMaskIntoConstraints = false
        redeemOutlet.addSubview(redeemImageView)
        redeemImageView.leftAnchor.constraint(equalTo: redeemOutlet.leftAnchor, constant: 16).isActive = true
        redeemImageView.topAnchor.constraint(equalTo: redeemOutlet.topAnchor, constant: 16).isActive = true
        redeemImageView.bottomAnchor.constraint(equalTo: redeemOutlet.bottomAnchor, constant: -16).isActive = true
        redeemImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        redeemImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        let redeemMainLabel = UILabel()
        redeemMainLabel.text = "Redeem"
        redeemMainLabel.textColor = UIColor(hex: "#EC1122")
        redeemMainLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        redeemMainLabel.translatesAutoresizingMaskIntoConstraints = false
        redeemOutlet.addSubview(redeemMainLabel)
        redeemMainLabel.leftAnchor.constraint(equalTo: redeemImageView.rightAnchor, constant: 8).isActive = true
        redeemMainLabel.topAnchor.constraint(equalTo: redeemOutlet.topAnchor, constant: 10).isActive = true
        redeemMainLabel.rightAnchor.constraint(equalTo: redeemOutlet.rightAnchor, constant: -16).isActive = true
        
        let redeemSecondaryLabel = UILabel()
        redeemSecondaryLabel.text = "using Coupon Code"
        redeemSecondaryLabel.textColor = UIColor(hex: "#EC1122")
        redeemSecondaryLabel.font = UIFont.systemFont(ofSize: 8)
        redeemSecondaryLabel.translatesAutoresizingMaskIntoConstraints = false
        redeemOutlet.addSubview(redeemSecondaryLabel)
        redeemSecondaryLabel.leftAnchor.constraint(equalTo: redeemImageView.rightAnchor, constant: 8).isActive = true
        redeemSecondaryLabel.topAnchor.constraint(equalTo: redeemMainLabel.bottomAnchor, constant: 8).isActive = true
        redeemSecondaryLabel.rightAnchor.constraint(equalTo: redeemOutlet.rightAnchor, constant: -8).isActive = true
        redeemSecondaryLabel.bottomAnchor.constraint(equalTo: redeemOutlet.bottomAnchor, constant: -10).isActive = true
        
        redeemOutlet.backgroundColor = UIColor(white: 1, alpha: 0)
        redeemOutlet.layer.cornerRadius = 10
        redeemOutlet.layer.borderWidth = 3
        redeemOutlet.layer.borderColor = UIColor(hex: "#EC1122").cgColor
        
        let downloadImage = UIImageView(image: UIImage(named: "iconDownload1"))
        downloadImage.tintColor = UIColor.init(hex: "f3bc31")
        downloadImage.translatesAutoresizingMaskIntoConstraints = false
        downloadView.addSubview(downloadImage)
        downloadImage.leftAnchor.constraint(equalTo: downloadView.leftAnchor, constant: 16).isActive = true
        downloadImage.topAnchor.constraint(equalTo: downloadView.topAnchor, constant: 16).isActive = true
        downloadImage.bottomAnchor.constraint(equalTo: downloadView.bottomAnchor, constant: -16).isActive = true
        downloadImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        downloadImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
       
        
        let DownloadLabel = UILabel()
        if checkIfDownloadedOrNot() == true {
            DownloadLabel.text = "Downloaded"
        } else {
            DownloadLabel.text = "Download"
        }
       
        DownloadLabel.textColor = UIColor(hex: "#f3bc31")
        DownloadLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        DownloadLabel.translatesAutoresizingMaskIntoConstraints = false
        downloadView.addSubview(DownloadLabel)
        DownloadLabel.leftAnchor.constraint(equalTo: downloadImage.rightAnchor, constant: 8).isActive = true
        DownloadLabel.topAnchor.constraint(equalTo: downloadView.topAnchor, constant: 10).isActive = true
        DownloadLabel.rightAnchor.constraint(equalTo: downloadView.rightAnchor, constant: -16).isActive = true
        
        let downloadSubtitle = UILabel()
        if checkIfDownloadedOrNot() == true {
            
        } else {
            downloadSubtitle.text = "to play offline"
        }
       
        downloadSubtitle.textColor = UIColor(hex: "#f3bc31")
        downloadSubtitle.font = UIFont.systemFont(ofSize: 8)
        downloadSubtitle.translatesAutoresizingMaskIntoConstraints = false
        downloadView.addSubview(downloadSubtitle)
        downloadSubtitle.leftAnchor.constraint(equalTo: downloadImage.rightAnchor, constant: 8).isActive = true
        downloadSubtitle.topAnchor.constraint(equalTo: DownloadLabel.bottomAnchor, constant: 8).isActive = true
        downloadSubtitle.rightAnchor.constraint(equalTo: downloadView.rightAnchor, constant: -8).isActive = true
        downloadSubtitle.bottomAnchor.constraint(equalTo: downloadView.bottomAnchor, constant: -10).isActive = true
        
        downloadView.backgroundColor = UIColor(white: 1, alpha: 0)
        downloadView.layer.cornerRadius = 10
        downloadView.layer.borderWidth = 3
        downloadView.layer.borderColor = UIColor(hex: "#f3bc31").cgColor
        
        
        
    }
    
    @IBOutlet weak var subscribeOutlet: UIView!
    @IBOutlet weak var redeemOutlet: UIView!

    @IBAction func susbscribeAction(_ sender: Any){
        print("sawsank: subscribe")
        viewSubscriptionAlert()
    }
    

    func restorePurchase(){
        APESuperHUD.appearance.backgroundColor = .clear
        APESuperHUD.appearance.foregroundColor = .clear
        APESuperHUD.appearance.loadingActivityIndicatorColor = .white
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard , message: "Please Wait...", presentingView: self.view)
    }
    
    
    func getSettings() {
        APIHandler.shared.getSettings(success: { (json) in
            print(json)
            self.redirectToHome = json["redirectToHome"].boolValue
        }, failure: { (message) in
            let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
             self.dismissVC()
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
        }) { (error) in
            let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    
//    func castMedia(url: String, hasUserRated: Bool) {
//        playButton.isUserInteractionEnabled = true
//
//        if sessionManager.hasConnectedCastSession() == false  {
//
//
//            let viewController = UIStoryboard(name: "PaidVideoPlayer", bundle: nil).instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
//
//            viewController.mediaInfo = self.buildMediaInformation(url: url)
//            viewController.mediaType = .movie
//           viewController.hasUserRatedMovie = hasUserRated
//           viewController.contentId = (self.movieDetail?.id)!
//            viewController.videoTitle = (self.movieDetail?.title)!
//
//
////            self.navigationController?.isNavigationBarHidden = false
//            self.navigationController?.pushViewController(viewController, animated: true)
//
//
//        }
//
//        else {
//
//            if self.sessionManager.currentSession is GCKCastSession {
//                self.castSession = (self.sessionManager.currentSession as? GCKCastSession)
//            }
//            let options = GCKMediaLoadOptions()
//            options.autoplay = true
//            castSession?.remoteMediaClient?.loadMedia(self.buildMediaInformation(url: url), with: options)
//
//        }
//
//
//
//    }
    
    var  movieDetail:Movie? {
        didSet {
            print(movieDetail)
        }
    }
//    private var sessionManager: GCKSessionManager!
//    private var castSession: GCKCastSession?

    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
//        self.sessionManager = GCKCastContext.sharedInstance().sessionManager
    }
    
    
    @IBOutlet weak var ratingBar: CosmosView!
    @IBAction func reviewsBtnPressed(_ sender: Any) {
        let reviewVC = MovieReviewsViewController.instantiate(fromAppStoryboard: .MovieDetail)
        reviewVC.contentId = self.movieDetail?.id!
        self.navigationController?.pushViewController(reviewVC, animated: true)
        
    }
    
    
    @IBOutlet weak var reviewButton: CardView!
    @IBAction func playMovieBtnPressed(_ sender: UIButton) {
//         print("sawsank: Play button pressed.")
        
        sender.isUserInteractionEnabled = true
        
        if let isComingSoon = movieDetail?.isComingSoon{
            if isComingSoon{
                let alert = UIAlertController(title: "Coming Soon", message: "\"\(movieDetail?.title ?? "This content")\"  is coming soon. Please refer to the poster for the release date.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
        
       
        
        
        
       
        
        if let isPaid = movieDetail?.paid{
            if isPaid {
                if SessionManager().isLoggedIn(){
                    let result = checkIfDownloadedOrNot()
                    if result == true {
                        let location = getSelectedMovieLocation()
                        let vc = CustomVideoViewController.instantiate(fromAppStoryboard: .CustomVideoPlayer)
                        let vidloaderhanlder: VidLoaderHandler = .shared
                        guard let locationurl = location.first?.location else { return  }
                        let urlasset = vidloaderhanlder.loader.asset(location: locationurl)
                        vc.offlineItem = AVPlayerItem(asset: urlasset!)
                        vc.playOffline = true
                    self.navigationController?.pushViewController(vc, animated: true)
                        
                    } else {
                        checkIfContentIsPaid(contentID: (movieDetail?.id)!, shouldCast: false, contentType: .movie)
                    }
//                    if showOffline == true {
//                       let result = checkIfDownloadedOrNot()
//                        print(result)
//                    } else {
//
//                    }
                   
                }else{
                    //User is not logged in.
                    let alert = UIAlertController(title: "You must login first!", message: "Please login with your Cinemaghar account to watch this movie.", preferredStyle: .alert)

                   alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))


                   alert.addAction(UIAlertAction(title: "Login", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in

                       let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                       loginVC.shouldAllowSkip = false
                       loginVC.shouldDismissAfterLogin = true
                       self.navigationController?.pushViewController(loginVC, animated: true)


                   }))


                   self.present(alert, animated: true, completion: nil)
                }
            } else {
                //playOffline("")
                sender.isUserInteractionEnabled = true
                if let isVideoFromYoutube = self.movieDetail?.videoIsFromYoutube, let youtubeURL = self.movieDetail?.video {
                    if isVideoFromYoutube {
                        let youtubePlayerVC = FreeMoviePlayerViewController.instantiate(fromAppStoryboard: .FreeMoviePlayer)
                        youtubePlayerVC.youtubeURL  = youtubeURL
                        self.navigationController?.pushViewController(youtubePlayerVC, animated: true)


                    }else{
                        print("sawsank: Checking when free")
                        if SessionManager().isLoggedIn(){
                            checkIfContentIsPaid(contentID: (movieDetail?.id)!, shouldCast: false, contentType: .movie)

                        }else{
                                let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                                loginVC.shouldAllowSkip = false
                                loginVC.shouldDismissAfterLogin = true
                                self.navigationController?.pushViewController(loginVC, animated: true)
                            }
                    }

                }


            }
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.playButton.isUserInteractionEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
    }
    func checkIfDownloadedOrNot() -> Bool {
        let downloadedmovies = getUserDefults.getDownloadedMoview()
        let exists = downloadedmovies.filter{$0.identifier == "\(movieDetail?.id ?? 0)" }
        if exists.first?.state == .completed {
            return true
        } else {
            return false
        }
        
    }
    func getSelectedMovieLocation() -> [VideoData] {
        let downloadedmovies = getUserDefults.getDownloadedMoview()
        let exists = downloadedmovies.filter{$0.identifier == "\(movieDetail?.id ?? 0)" }
        return exists
    }
    
    
    @IBAction func watchTrailerBtnPressed(_ sender: Any) {
        
        let youtubePlayerVC = FreeMoviePlayerViewController.instantiate(fromAppStoryboard: .FreeMoviePlayer)
        if let youtubeURL = self.movieDetail?.trailerUrl{
            if youtubeURL.isEmpty{
                
            }
            
            else {
                youtubePlayerVC.youtubeURL = youtubeURL
                self.navigationController?.pushViewController(youtubePlayerVC, animated: true)
            }
          
        }
        
        
    }
    @IBOutlet weak var priceIndicatorBackgroundView: CardView!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var releaseYearLabel: UILabel!
    @IBOutlet weak var runTimeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var watchTrailerBtn: CardView!
    @IBOutlet weak var watchTrailerButton: UIButton!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var mainBackgroundImage: UIImageView!
    @IBOutlet weak var castLabel: UILabel!
    @IBOutlet weak var moviePosterImageHolderView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var titleTopMargin: NSLayoutConstraint!
    @IBOutlet weak var moviePosterImageView: UIImageView!
    
    var contentID = "0"
    
    func getMovieDetailData(){
        print("sawsank: getting movie detail data")
        
        if (movieDetail != nil){
            contentID = String((movieDetail?.id)!)
        }
        print("sawsank: contentID: \(contentID)")
        
        APIHandler.shared.getMoviesDetail(contentID: contentID) { (status, movieDetail) in
            if(status){
                print(movieDetail)
                self.movieDetail = movieDetail
                self.renderView()
            }
        } failure: { (error) in
            print(error)
        }
    }
    var showOffline = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //movieDetail?.isBought = true
        googleAdViewSetup()
        print(movieDetail)
        globalMovieDetail = movieDetail
        getSettings()
        if(movieDetail == nil){
            getMovieDetailData()
        }
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
            titleTopMargin.constant = 12
        }
        scrollView.delegate = self
        
        playButton.setBackgroundColor(color: Util.hexStringToUIColor(hex: "DF0018"), forState: .normal)
        
        playButton.setBackgroundColor(color: Util.hexStringToUIColor(hex: "BB291D"), forState: .selected)

        playButton.layer.cornerRadius = 30
        playButton.layer.masksToBounds = false
        playButton.clipsToBounds = true
    
        playButton.imageView?.tintColor = .white
        
        self.priceIndicatorBackgroundView.cornerRadius = self.priceIndicatorBackgroundView.frame.height/2
        
        let backButton = UIButton() // back button
        backButton.backgroundColor = UIColor.white
        backButton.layer.cornerRadius = 15
//                backButton.addShadow(offset: CGSize(width: 1, height: 1), color: .darkGray, radius: 2, opacity: 0.9)
        backButton.setImage(UIImage(named: "icon_navBar_Back"), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(22,22,22,23)
        backButton.imageView?.tintColor = UIColor.darkGray
        self.view.addSubview(backButton)
        backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        
        backButton.snp.makeConstraints{(make) in
            make.centerY.equalTo(self.navBar).offset(0)
            make.height.equalTo(30)
            make.width.equalTo(30)
            make.left.equalTo(self.view).offset(12)
        }
        
        moviePosterImageHolderView.backgroundColor = .clear
        moviePosterImageHolderView.layer.borderColor = UIColor.gray.cgColor
        moviePosterImageHolderView.layer.borderWidth = 0.5
        
        APESuperHUD.appearance.backgroundBlurEffect = .dark
        APESuperHUD.appearance.titleFontSize = 20
        APESuperHUD.appearance.iconWidth = 48
        APESuperHUD.appearance.loadingActivityIndicatorColor = UIColor.darkText
        APESuperHUD.appearance.iconHeight = 48
        APESuperHUD.appearance.messageFontSize = 16
        APESuperHUD.appearance.textColor = UIColor.white
        APESuperHUD.appearance.foregroundColor = .white
        
        ratingBar.settings.fillMode = .half
        navBar.backIndicatorImage = UIImage()
        navBar.alpha = 0
        navBar.isTranslucent = true
        navBar.shadowImage = UIImage()
        navBar.delegate = self
        
       
        
        renderView()
        setupSubscribeView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showProgress(isLoading:)), name: Notification.Name(rawValue: "IAPPaymentStatusLoading"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideProgress(notification:)), name: Notification.Name(rawValue: "IAPPaymentStatusNotLoading"), object: nil)
        
       

    }
    func googleAdViewSetup() {
        let adRequest = GADRequest()
     
        adBannerView.load(GADRequest())
        googleAdView.addSubview(adBannerView)
        googleAdView.backgroundColor = .clear
    }
    
    @objc func showProgress(isLoading: Bool){
        DispatchQueue.main.async {
            APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
            APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Preparing Payment...", presentingView: self.view)
        }
    }
    func playOffline(_ url: String?) {
        if check(url: url) {
            do {
                let (isExist, hls) = try manager.isExist(url!, firstUrl: "\(movieDetail?.id ?? 0)")
                if isExist {
                    print("[HLS Exist]\(url!)")
                    
                    if HLS.State(rawValue: hls!.state) == .downloaded {
                        avLayer = AVPlayerLayer()
                       
                        print("HLS Exists")
                        let item = manager.getLocalAsset(hls!)
                       
//                        let player = AVPlayer(playerItem: item)
//                        avLayer.player = player
//                        avLayer.frame = self.view.bounds
//                        self.view.layer.addSublayer(avLayer)
//                        avLayer.player?.play()
                        
                        let player = AVPlayer(url: URL(string: "https://nepal.cinema-ghar.com/videos/1c82cc8ccb5b40639d2789a384dbcf39noises/1c82cc8ccb5b40639d2789a384dbcf39noises.m3u8?query=eyJ1c2VyX2lkIjoyMDc5OCwiY29udGVudF9pZCI6MzI4LCJkZXZpY2VfdHlwZSI6ImlvcyIsImRldmljZV9pZCI6IjAyRDYxRkM2LTFFRjQtNDI3Qi05OEYwLTJGMjUyRDE3NTg1MiIsImlwIjoiMjcuMzQuMjAuMjAwIiwidmlkZW8iOiIxYzgyY2M4Y2NiNWI0MDYzOWQyNzg5YTM4NGRiY2YzOW5vaXNlcyIsIm1lZGlhX3R5cGUiOiJjb250ZW50cyIsInR5cGUiOiJwbGF5IiwibWV0YSI6eyJoZWFkZXJzIjp7ImFjY2VwdC1lbmNvZGluZyI6WyJicjtxPTEuMCwgZ3ppcDtxPTAuOSwgZGVmbGF0ZTtxPTAuOCJdLCJ4LWRldmljZS1pZCI6WyIwMkQ2MUZDNi0xRUY0LTQyN0ItOThGMC0yRjI1MkQxNzU4NTIiXSwieC1kZXZpY2UtbW9kZWwiOlsiaVBob25lIl0sIngtdmVyc2lvbiI6WyIxLjEiXSwieC1kZXZpY2UtdHlwZSI6WyJpb3MiXSwidXNlci1hZ2VudCI6WyJDaW5lbWFHaGFyXC8zLjcuMiAoY29tLnNhZ2Fya2hhcmVsLmNpbmVtYWdoYXJoZDsgYnVpbGQ6NzA7IGlPUyAxNC42LjApIEFsYW1vZmlyZVwvNS40LjEiXSwiYWNjZXB0LWxhbmd1YWdlIjpbImVuLU5QO3E9MS4wIl0sImF1dGhvcml6YXRpb24iOlsiQmVhcmVyIGV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5leUpwYzNNaU9pSm9kSFJ3Y3pwY0wxd3ZjM1JoWjJsdVp5NWphVzVsYldFdFoyaGhjaTVqYjIxY0wyRndhVnd2WVhWMGFGd3ZiRzluYVc0aUxDSnBZWFFpT2pFMk5USTNPRGsxTkRBc0ltVjRjQ0k2TVRnME1qQXdOVFUwTUN3aWJtSm1Jam94TmpVeU56ZzVOVFF3TENKcWRHa2lPaUpWZWs1S1duWnNVblZVT0UxUE1GVm9JaXdpYzNWaUlqb3lNRGM1T0N3aWNISjJJam9pT0RkbE1HRm1NV1ZtT1daa01UVTRNVEptWkdWak9UY3hOVE5oTVRSbE1HSXdORGMxTkRaaFlTSXNJbVJsZG1salpWOXBaQ0k2SWpBeVJEWXhSa00yTFRGRlJqUXROREkzUWkwNU9FWXdMVEpHTWpVeVJERTNOVGcxTWlKOS5qa2ZPX0l3WUdxaUx2VVhSa1d4WVVfUTZac3BJNFRWLW9xZGVha2xiazUwIl0sImFjY2VwdCI6WyJhcHBsaWNhdGlvblwvanNvbiJdLCJob3N0IjpbInN0YWdpbmcuY2luZW1hLWdoYXIuY29tIl0sImNvbnRlbnQtbGVuZ3RoIjpbIiJdLCJjb250ZW50LXR5cGUiOlsiIl19fX0=")!)
                        let playerViewController = AVPlayerViewController()
                        playerViewController.player = player
                        self.present(playerViewController, animated: true) {
                            playerViewController.player!.play()
                        }
                        
//                        let vc = DemoPlayerVC.instantiate(fromAppStoryboard: .MovieDetail)
//                        vc.offlineItem = item
//                        self.navigationController?.pushViewController(vc, animated: true)
                        
//                        let vc = CustomVideoViewController.instantiate(fromAppStoryboard: .CustomVideoPlayer)
//                        vc.offlineItem = item
//                        vc.playOffline = true
//                        self.navigationController?.pushViewController(vc, animated: true)
                        
//                        if cell.playerLayer.player == nil {
//                            let item = manager.getLocalAsset(hls!)
//                            let player = AVPlayer(playerItem: item)
//                            cell.playerLayer.player = player
//                            cell.playerLayer.frame = cell.playerView.bounds
//                        }
//                        cell.playerLayer.player?.play()
                    }
                    else {
                        print("[HLS Not Downloaded]\(url!)")
                    }
                }
                else {
                    print("[HLS Not Exist]\(url!)")
                }
            } catch {
                print("[ERROR]Repository problem")
            }
        }
        
    }
//MARK: - DOWLLOAD MOVIE
   // func downloadAndSaveVideoToGallery(videoURL: String, id: String = "default") {
    
    @objc func hideProgress(notification: NSNotification){
        print("sawsank: hiding progress")
        self.getMovieDetailData()

        DispatchQueue.main.async {
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: nil)
            guard let userInfo = notification.userInfo else {
                return
            }
            let alertDetailController = DOAlertController(title: "Info Detail", message: userInfo["message"] as? String, preferredStyle: .alert)
            alertDetailController.alertViewBgColor = UIColor.white
            alertDetailController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertDetailController.messageView.textAlignment = .left
            alertDetailController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertDetailController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "OK", style: .default, handler: { action in
                print("sawsank: OK Pressed")
               
                self.dismiss(animated: true, completion: nil)
            })
            // Add the action.
            alertDetailController.addAction(okAction)
            // Show alert
            self.present(alertDetailController, animated: true, completion: nil)
            

        }
        let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
        movieDetailScene.movieDetail = self.movieDetail
        self.navigationController?.pushViewController(movieDetailScene, animated: true)
        
    }
    
    @IBOutlet weak var redeemCouponView: CardView!
    
    func renderView(){
        if let posterURL = movieDetail?.poster, let coverURL = movieDetail?.BackGroundImage , let rating = movieDetail?.rating, let movieTitle = movieDetail?.title, let movieCasts = movieDetail?.cast, let movieDescription = movieDetail?.descriptionField  {
            moviePosterImageView.sd_setImage(with: URL(string: posterURL), placeholderImage: UIImage(named: "placeholder_cell"), options: [.scaleDownLargeImages], completed:nil)
            mainBackgroundImage.sd_setImage(with: URL(string: coverURL), placeholderImage: UIImage(), options: [.scaleDownLargeImages], completed:nil)
            movieTitleLabel.text = movieTitle
            descriptionLabel.setHTMLFromString(htmlText: movieDescription, hexCode: "#ffffff")
            castLabel.text = movieCasts
            ratingBar.rating = Double(rating)
           
 
            
        }
        
        self.playButton.isEnabled = true
        self.playButton.isUserInteractionEnabled = true
        
        if let releaseYear = movieDetail?.releaseYear{
            releaseYearLabel.attributedText = makeMovieDetailLabel(preffix: "Release Year: ", suffix: releaseYear)
        }
        
        if let runTime = movieDetail?.duration {
            runTimeLabel.attributedText = makeMovieDetailLabel(preffix: "Runtime: ", suffix: runTime)
        }
  
        
        
        if let isPaid = movieDetail?.paid, let isAddedToWishList = movieDetail?.isAddedToWishList{

            if isPaid { // paid movie
                
                
                if let isBought = self.movieDetail?.isBought {
                    if isBought {
                        
                   
                        self.animatePlayButton()
                        priceLabel.text = " PURCHASED "
                        priceIndicatorBackgroundView.backgroundColor = UIColor(hex: "#08ABE8")
                        
                        redeemOutlet.isHidden = true
                        subscribeOutlet.isHidden = true
                        
                    }
                    else {
                        if (self.movieDetail?.isUnderSubscription) != nil {
                            priceLabel.text = " Gold Content "
                            priceIndicatorBackgroundView.isHidden = true
                            priceIndicatorBackgroundView.backgroundColor = UIColor(hex: "#E59827")
                        }else{
                            priceLabel.text = " PREMIUM "
                            priceIndicatorBackgroundView.isHidden = true
                            priceIndicatorBackgroundView.backgroundColor = UIColor(hex: "DF0018")
                        }
                        
                    }
                    
                    
                }
               

                
            }
            else {
                priceLabel.text = " FREE "
                priceIndicatorBackgroundView.backgroundColor = UIColor(hex: "008000")
                expiryDateLabel.isHidden = true
                
                redeemOutlet.isHidden = true
                
                animatePlayButton()
           
            }
        }
        
        
        if let youtubeURL = self.movieDetail?.trailerUrl{
            if youtubeURL.isEmpty{
                watchTrailerBtn.alpha  = 0.8
            }
            else {
                watchTrailerBtn.alpha  = 1

            }
            
        }
    }
    
    
    func animatePlayButton(){
        UIView.animate(withDuration: 1,
                       delay: 0,
                       usingSpringWithDamping: 8,
                       initialSpringVelocity: 2,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.playButtonCardView.transform = .identity
            },
                       completion: nil)
    }
    
    
    
    func getsProductDetails(){
        self.playButtonCardView.isHidden = true
    }
    
    
    
    func validatePayment(paymentMethod: paymentGateway, token: String , contentID: Int                                                                                               , contentType: MediaType, episodeId: Int = 100000){
        
        APIHandler.shared.validatePayment(paymentGateway: paymentMethod , mediaType: contentType , contentId: contentID, episodeID: episodeId, paymentSuccessToken: token, transactionID: "", status: {(status, videoURL, message) in
            if status {
               
                APESuperHUD.showOrUpdateHUD(icon: .checkMark, message: "Success!", presentingView: self.view, completion: {
                    
                    self.application.isMoviePurchased = true
                    self.animatePlayButton()
//                    self.castMedia(url: videoURL, hasUserRated: false)
                   
                    
                })
                
                
            }
            else {
                APESuperHUD.removeHUD(animated: true, presentingView: self.view)
                let controller = UIAlertController(title: "Info!", message: "An Error Occured While Verifying Your Purchase. \(message)", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .default, handler: { (action) -> Void in
                    self.validatePayment(paymentMethod: paymentMethod, token: token, contentID: contentID, contentType: contentType)
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
            
            
            
        }, failure: {(error) in
            
            APESuperHUD.removeHUD(animated: true, presentingView: self.view)
            
            if(error._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                    self.validatePayment(paymentMethod: paymentMethod, token: token, contentID: contentID, contentType: contentType)
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
            }
                
            else if (error._code == NSURLErrorNetworkConnectionLost){
                self.validatePayment(paymentMethod: paymentMethod, token: token, contentID: contentID, contentType: contentType)
            }
            
        })
        
    }
    
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    func checkIfContentIsPaid(contentID: Int, shouldCast: Bool, contentType: MediaType, episodeId: Int = 0 ) {
        
        APESuperHUD.appearance.backgroundColor = .clear
        APESuperHUD.appearance.foregroundColor = .clear
        APESuperHUD.appearance.loadingActivityIndicatorColor = .white
        APESuperHUD.showOrUpdateHUD( loadingIndicator: .standard, message: "", presentingView: self.view)
        
        APIHandler.shared.checkIfVideoIsPurchased(contentType: contentType, contentId: contentID, episodeId: episodeId, success: { [self](status, videoURL, hasBoughtMovie, hasRatedMovie, isComingSoon) in
            if hasBoughtMovie  {
                print(videoURL)
                if self.isDownloadBtnTap == true {
                    self.isDownloadBtnTap = false
                    APESuperHUD.removeHUD(animated: true, presentingView: self.view)
                    print(videoURL)
                   // self.download(videoURL)
                    DispatchQueue.main.async {
                        let vc = VideoListController.instantiate(fromAppStoryboard: .MovieDetail)
                        vc.dataProvider = VideoListDataProvider()
                        vc.movideDetail = movieDetail
                        vc.videoUrl = videoURL
                        
//                        let videodata = VideoData(identifier: "\(movieDetail?.id ?? 0)", title: movieDetail?.title ?? "", imageName: movieDetail?.coverImage ?? "", state: .unknown, stringURL: videoURL, location: nil)
//                        vc.downloadsData = [videodata]
//                        vc.showDonwloading = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                } else if self.showOffline == true {
                    self.playOffline(videoURL)
                } else {
                
                APESuperHUD.removeHUD(animated: true, presentingView: self.view)
                
                let videoPlayerVC = CustomVideoViewController.instantiate(fromAppStoryboard: .CustomVideoPlayer)
                print("sawsank: \(videoURL)")
                videoPlayerVC.mediaInfo = self.buildMediaInformation(urlString: videoURL)
                videoPlayerVC.urlString = videoURL
                videoPlayerVC.videoTitle = (self.movieDetail?.title)!
                videoPlayerVC.contentId = (self.movieDetail?.id)!
                videoPlayerVC.mediaType = .movie
                videoPlayerVC.isRated = hasRatedMovie
                videoPlayerVC.isComingSoon = isComingSoon
                if(self.movieDetail?.advertisements.count == 0){
                    videoPlayerVC.advertisement = nil
                }else {
                    videoPlayerVC.advertisement = (self.movieDetail?.advertisements.first)!
                }
                
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }


            }
            else {
                
                APIHandler.shared.getProfile { (profile) in
                    if(!profile.isSubscribed){
                        APESuperHUD.removeHUD(animated: true, presentingView: self.view)
                        self.viewSubscriptionAlert()
                        
                        if let reloadDelegate = self.refreshCollectionViewDelegate{
                            reloadDelegate.refreshCollectionViews()
                        }
                    }
                } failure: { (msg) in
                    print("Failure: \(msg)")
                } error: { (err) in
                    print("Error: \(err.localizedDescription)")
                }
               
            }


        }, failure: {(failure) in

            APESuperHUD.removeHUD(animated: true, presentingView: self.view)
            
            
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                    
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in

                        self.checkIfContentIsPaid(contentID: contentID, shouldCast: shouldCast, contentType: contentType)
                    }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                    self.checkIfContentIsPaid(contentID: contentID, shouldCast: shouldCast, contentType: contentType)
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                self.checkIfContentIsPaid(contentID: contentID, shouldCast: shouldCast, contentType: contentType)

            }
            
            

        })

        
    }
    
    @objc func viewSubscriptionAlert(){
        let message = """
            Things to know: \n \n
            Subscription will be valid till 31st Dec of 2021 \n
            This subscription will only be valid on 2 devices.
            \n \n \n \n \n \n
        """
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
          
          let attributedMessageText = NSMutableAttributedString(
              string: message,
              attributes: [
                  NSAttributedString.Key.paragraphStyle: paragraphStyle,
                  NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0),
                NSAttributedString.Key.foregroundColor: UIColor.black
              ]
          )
        
        let attributedTitleText = NSMutableAttributedString(
            string: "Gold Subscription",
            attributes: [
              NSAttributedString.Key.foregroundColor: UIColor.black
              
            ]
        )
        
        
        
        subscribeAlertController = UIAlertController(title: "Gold Subscription", message: message, preferredStyle: .alert)
        
        //Customize view
        subscribeAlertController.view.tintColor = UIColor.init(hex:"#252B38")
        
        //Background color
        let subview :UIView = subscribeAlertController.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.subviews.first!.backgroundColor = UIColor.init(hex: "#E59827")
        
        // set attributed message here
        subscribeAlertController.setValue(attributedMessageText, forKey: "attributedMessage")
        subscribeAlertController.setValue(attributedTitleText, forKey: "attributedTitle")

        
        //CineCoin Outlets
        var paymentButton: PKPaymentButton!
        paymentButton = PKPaymentButton.init(paymentButtonType: .setUp, paymentButtonStyle: .white)
        paymentButton.translatesAutoresizingMaskIntoConstraints = false
        subview.addSubview(paymentButton)
        paymentButton.leftAnchor.constraint(equalTo: subview.leftAnchor, constant: 16).isActive = true
        paymentButton.rightAnchor.constraint(equalTo: subview.rightAnchor, constant: -16).isActive = true
        paymentButton.bottomAnchor.constraint(equalTo: subview.bottomAnchor, constant: -100).isActive = true
        paymentButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    
        let paymentButtonTop = UIButton()
        paymentButtonTop.layer.cornerRadius = 4
        paymentButtonTop.isUserInteractionEnabled = false
        paymentButtonTop.setTitle("Pay from Apple Pay", for: .normal)
        paymentButtonTop.backgroundColor = .black
        paymentButtonTop.translatesAutoresizingMaskIntoConstraints = false
        subview.addSubview(paymentButtonTop)
        paymentButtonTop.leftAnchor.constraint(equalTo: subview.leftAnchor, constant: 16).isActive = true
        paymentButtonTop.rightAnchor.constraint(equalTo: subview.rightAnchor, constant: -16).isActive = true
        paymentButtonTop.bottomAnchor.constraint(equalTo: subview.bottomAnchor, constant: -100).isActive = true
        paymentButtonTop.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        //Please visit our site for more info.
        var websiteButton: UIButton!
        websiteButton = UIButton()
        if(redirectToHome){
            websiteButton.setTitle("Visit our website for more info", for: .normal)
        }else{
            websiteButton.setTitle("Pay from website", for: .normal)
        }
       
        websiteButton.backgroundColor = UIColor(hex: "#252B38")
        websiteButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        websiteButton.layer.cornerRadius = 16
        websiteButton.translatesAutoresizingMaskIntoConstraints = false
        subview.addSubview(websiteButton)
        websiteButton.leftAnchor.constraint(equalTo: subview.leftAnchor, constant: 16).isActive = true
        websiteButton.rightAnchor.constraint(equalTo: subview.rightAnchor, constant: -16).isActive = true
        websiteButton.bottomAnchor.constraint(equalTo: subview.bottomAnchor, constant: -50 ).isActive = true
        websiteButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        websiteButton.addTarget(self, action: #selector(gotoWeb(action:)), for: .touchUpInside)
        
        subscribeAlertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        paymentButton.addTarget(self, action: #selector(purchase(_:)), for: .touchUpInside)
                
        
        
        if(SessionManager().isLoggedIn()){
            self.present(subscribeAlertController, animated: true)
        }else{
            let signupVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(signupVC, animated: true)
        }
    }
    
    @objc func purchase(_ sender: UITapGestureRecognizer) {
        globalisPurchasedFromApple = true
        print("sawsank: Purchasing From apple")
        IAPService.shared.purchase(product: IAPProduct.coin100)
        subscribeAlertController.dismiss(animated: true)
    }

    
    @IBAction func buyViaTokenButton(_ sender: UIButton){
        let message = """
            Things to know: \n \n
            This purchase will be valid till 31st Dec of 2021 \n
            Your account will only be accessible by 2 devicdes \n
        """
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
          
          let attributedMessageText = NSMutableAttributedString(
              string: message,
              attributes: [
                  NSAttributedString.Key.paragraphStyle: paragraphStyle,
                  NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0),
                NSAttributedString.Key.foregroundColor: UIColor.black
              ]
          )
        
        let attributedTitleText = NSMutableAttributedString(
            string: "Buy \(movieDetail?.title ?? "this movie")",
            attributes: [
              NSAttributedString.Key.foregroundColor: UIColor.black
              
            ]
        )
        
        
        
        buyViaTokenAlertController = UIAlertController(title: "Buy \(movieDetail?.title ?? "this movie")", message: message, preferredStyle: .alert)
        
        //Customize view
        buyViaTokenAlertController.view.tintColor = UIColor.init(hex:"#252B38")
        
        //Background color
        let subview :UIView = buyViaTokenAlertController.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.subviews.first!.backgroundColor = UIColor.init(hex: "#E59827")
        
        // set attributed message here
        buyViaTokenAlertController.setValue(attributedMessageText, forKey: "attributedMessage")
        buyViaTokenAlertController.setValue(attributedTitleText, forKey: "attributedTitle")

        var couponCodeTextField: UITextField!
        buyViaTokenAlertController.addTextField { (textField) in
            couponCodeTextField = textField
        }
        
        buyViaTokenAlertController.addAction(UIAlertAction(title: "Redeem", style: .default, handler: { (action) in
            print("ID -> \(String(describing: self.movieDetail?.id)), Coupon -> \(String(describing: couponCodeTextField.text))")
            let token = self.buyViaTokenAlertController.textFields?.first?.text
            self.buyViaToken(token: token!)
        }))
        
        buyViaTokenAlertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
        
        
        if(SessionManager().isLoggedIn()){
            self.present(buyViaTokenAlertController, animated: true)
        }else{
            let signupVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(signupVC, animated: true)
        }
    }
    
    func buyViaToken(token: String){
        APESuperHUD.showOrUpdateHUD(title: "Processing...", message: "", presentingView: self.view)
        APIHandler.shared.buyViaToken(token: token, id: movieDetail!.id) { (status, message) in
            print("sawsank: success message -> \(message)")
            self.getMovieDetailData()
            APESuperHUD.removeHUD(animated: true, presentingView: self.view)
            self.showToast(message: message, font: UIFont.systemFont(ofSize: 12))
        } failure: { (status, message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view)
            self.showToast(message: message, font: UIFont.systemFont(ofSize: 12))
        } error: { (err) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view)
            self.showToast(message: err.localizedDescription, font: UIFont.systemFont(ofSize: 12))
        }
        

    }
    
    
    private func buildMediaInformation(urlString:String) -> GCKMediaInformation {
        let metadata = GCKMediaMetadata(metadataType: GCKMediaMetadataType.movie)
        metadata.setString((movieDetail?.title)!,  forKey: kGCKMetadataKeyTitle)
        metadata.setString((movieDetail?.cast)!, forKey: kGCKMetadataKeyStudio)
        metadata.addImage(GCKImage(url: URL(string: (movieDetail?.BackGroundImage)!)!, width: 500, height: 500))
        var customData = [String:String]()

        if SessionManager().isLoggedIn() {
            if let title = self.movieDetail?.title{
                customData = ["userID": UserDefaults.standard.string(forKey: AppConstants.uniqueId)! , "userName": UserDefaults.standard.string(forKey: AppConstants.userName)! , "movieTitle": title]
            }
        }
        else {
            if let title = self.movieDetail?.title{

                customData = ["userID": " " , "userName": " " , "movieTitle": title]
            }

        }
    
        let url = URL.init(string: urlString)!
        

        let mediaInfoBuilder = GCKMediaInformationBuilder.init(contentURL: url)
        mediaInfoBuilder.streamType = GCKMediaStreamType.buffered;
        mediaInfoBuilder.contentType = "application/x-mpegurl"
        mediaInfoBuilder.metadata = metadata
        mediaInfoBuilder.contentID = urlString
        mediaInfoBuilder.customData = customData
        let mediaInformation = mediaInfoBuilder.build()
        
        
        return mediaInformation
    }
    
    
    
    
    
    
    func displayAlert(title: String, message: String){
        
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "ok", style: .default, handler: { (action) -> Void in
                     })
        controller.addAction(ok)

        self.present(controller, animated: true, completion: nil)
    }
    
    
   
    
    
    
    
    
    func makeMovieDetailLabel(preffix: String, suffix:String)-> NSAttributedString {
        
        let formattedString = NSMutableAttributedString()
        if suffix == ""{
            formattedString
                .movieDetailLabelFormatter(preffix,  16, UIFont.Weight.medium, .white )
                .movieDetailLabelFormatter("N/A", 16,UIFont.Weight.semibold, .white)
            
        }
        else {
            formattedString
                .movieDetailLabelFormatter(preffix,  14, UIFont.Weight.regular, .white)
                .movieDetailLabelFormatter(suffix, 16, UIFont.Weight.medium, .white)
            
        }
        
        return formattedString
        
    }
    
    
    
    
}


extension UIDevice {
    static var isIphoneX: Bool {
        var modelIdentifier = ""
        if isSimulator {
            modelIdentifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] ?? ""
        } else {
            var size = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: size)
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            modelIdentifier = String(cString: machine)
        }
        
        return modelIdentifier == "iPhone10,3" || modelIdentifier == "iPhone10,6"
    }
    
    
    static var isIphone6 : Bool {
        var modelIdentifier = ""
        if isSimulator {
            modelIdentifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] ?? ""
        } else {
            var size = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: size)
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            modelIdentifier = String(cString: machine)
        }
        
        return modelIdentifier == "iPhone7,2" || modelIdentifier == "iPhone8,1"
        
    }
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
    
    
    
    enum ScreenType: String {
        case iPhones_4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhones_X_XS = "iPhone X or iPhone XS"
        case iPhone_XR = "iPhone XR"
        case iPhone_XSMax = "iPhone XS Max"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhones_4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1792:
            return .iPhone_XR
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhones_X_XS
        case 2688:
            return .iPhone_XSMax
        default:
            return .unknown
        }
    }
    
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
extension UINavigationController {
    
    func backToViewController(viewController: Swift.AnyClass) {
        
        for element in viewControllers as Array {
            if element.isKind(of: viewController) {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
}
extension UIButton {
    
    func setBackgroundColor(color: UIColor, forState: UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
}

}

extension SKProduct {
    
    func localizedPrice() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = self.priceLocale
        
        return formatter.string(from: self.price)!
    }
    
}

extension MovieDetailViewController: UIScrollViewDelegate {
}
extension MovieDetailViewController {
    func download(_ url: String?) {
        if check(url: url) {
            do {
                let (isExist, _) = try manager.isExist(url!, firstUrl: "\(movieDetail?.id ?? 0)")
                if isExist {
                    print("[URL is exist] \(url!)")
                } else {
                    print("[URL is not exist] \(url!)")
                    let hls = try manager.createHLS(url!, movieid: "\(movieDetail?.id ?? 0)")
                    try manager.download(hls)
                   
                }
            }
            catch let err {
                print(err)
            }
        }
    }
    private func check(url: String?) -> Bool {
        return true
        //return urls.contains(url ?? "")
    }
}
extension MovieDetailViewController {
    //MARK: - DOWNLOAD MOVIE FROM VIDLOADER
//    func startDownload(with data: VideoData) {
//        guard let url = URL(string: data.stringURL) else { return }
//        let downloadValues = DownloadValues(identifier: data.identifier,
//                                            url: url,
//                                            title: data.title,
//                                            artworkData: UIImage(named: data.imageName)?.jpegData(compressionQuality: 1),
//                                            minRequiredBitrate: 1)
//        vidLoaderHandler.loader.download(downloadValues)
//    }
}
//MARK: - GOOGLE AD DELEGATE
extension MovieDetailViewController: GADBannerViewDelegate {
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("bannerViewDidReceiveAd")
    }

    private func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
      print("bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    func adViewDidRecordImpression(_ bannerView: GADBannerView) {
      print("bannerViewDidRecordImpression")
    }

    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillPresentScreen")
    }

    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillDIsmissScreen")
    }

    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewDidDismissScreen")
    }
}

