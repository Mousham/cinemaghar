//
//  MoviePromoDetailViewController.swift
//  CinemaGhar
//
//  Created by Sunil Gurung on 5/26/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import ParallaxHeader



class MoviePromoDetailViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var detailLabel: UILabel!
    
    @IBOutlet weak var promoTitleLabel: UILabel!
    var coverImageURL:String = ""
    var promoDetail:String = ""
    var promoTitle:String = ""
    
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        let imageView = UIImageView() // parallax header
        imageView.contentMode = .scaleAspectFill
        
        scrollView.parallaxHeader.view = imageView;
        
        
        scrollView.parallaxHeader.height = UIScreen.main.bounds.height/2.4;
        scrollView.parallaxHeader.mode = .center;
        scrollView.parallaxHeader.minimumHeight = 80;
        scrollView.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
        }
    
        var rect = CGRect(x:10, y:33, width:30, height:30)
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
            rect = CGRect(x:10, y:36, width:30, height:30)
        }
        
        let backButton = UIButton() // back button
        backButton.backgroundColor = UIColor.white
        backButton.layer.cornerRadius = 15
        backButton.addShadow(offset: CGSize(width: 1, height: 1), color: .darkGray, radius: 2, opacity: 0.9)
        backButton.frame = rect
        backButton.setImage(UIImage(named: "icon_navBar_Back"), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(22,22,22,23)
        backButton.imageView?.tintColor = UIColor.darkGray
        view.addSubview(backButton)
        backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        imageView.sd_setImage(with: URL(string: coverImageURL), placeholderImage: UIImage(), options: [.continueInBackground, .scaleDownLargeImages], completed:nil)
        detailLabel.text = promoDetail.htmlToString
        promoTitleLabel.text = promoTitle
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }

   

}
