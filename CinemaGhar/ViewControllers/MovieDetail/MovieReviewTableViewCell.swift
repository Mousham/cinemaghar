//
//  MovieReviewTableViewCell.swift
//  CinemaGhar
//
//  Created by Sunil on 7/3/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit

class MovieReviewTableViewCell: UITableViewCell {
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var reviewLabel: UILabel!
    
    @IBOutlet weak var reviewTitle: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var reviewerName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = .clear
        
    }
    
    
    func loadReviewCell(review: Review) {
        
        if let descr = review.descriptionField, let title = review.title, let reviewerName = review.reviewedBy, let rating = review.rating{
        
        self.reviewLabel.text = descr
        self.reviewTitle.text = title
        self.reviewerName.text = review.reviewedBy
        self.ratingLabel.text = "\(rating)/10 ★"
        
        }
        
        
    }

    

}
