//
//  NewMovieDetailsVC.swift
//  CinemaGhar
//
//  Created by Midas on 21/06/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import Cosmos
import GoogleCast
import StoreKit
import Alamofire
import SafariServices
import PassKit
import Photos
import HLSDownloader
import AVKit
import VidLoader
import GoogleMobileAds
import ParallaxHeader
class TGradientView: UIView {
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = [UIColor.init(white: 0.7, alpha: 0).cgColor, UIColor.init(hex: "0E1525").withAlphaComponent(0.2).cgColor,UIColor.init(hex: "0E1525").withAlphaComponent(0.3).cgColor,
                                UIColor.init(hex: "0E1525").withAlphaComponent(0.7).cgColor, UIColor.init(hex: "0E1525").withAlphaComponent(0.9).cgColor, UIColor.init(hex: "0E1525").withAlphaComponent(1).cgColor
                                
        ]
        backgroundColor = UIColor.clear
    }
}
struct CastCrewModel {
    var image: String?
    var name: String?
}
protocol MovieDetailDelegate: class {
    func reloadData()
}

class NewMovieDetailsVC: UIViewController {
    @IBOutlet weak var mainStackHeight: NSLayoutConstraint!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var genreBackView: UIView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var imageTopView: UIView!
    @IBOutlet var imageBackView: UIView!
    @IBOutlet weak var fireeCineCoinLbl: UILabel!
    @IBOutlet weak var noadsPriceLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var genreLbl: UILabel!
    @IBOutlet weak var downloadBackView: UIView!
    @IBOutlet weak var toplayofflineLbl: UILabel!
    @IBOutlet weak var downloadLbl: UILabel!
    @IBOutlet weak var payStackView: UIStackView!
    @IBOutlet weak var playButton: AnimatedButton!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var cinecoinBackView: UIView!
    @IBOutlet weak var buyWithNoadsView: UIView!
    @IBOutlet weak var releaseDateLbl: UILabel!
    @IBOutlet weak var runtimeLbl: UILabel!
    @IBOutlet weak var reviewLbl: UILabel!
    @IBOutlet weak var watchTrailerBtn: AnimatedButton!
    @IBOutlet weak var reviewView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var googleAdView: UIView!
    @IBOutlet weak var movieimage: UIImageView!
    let countryCode = NSLocale.current.regionCode
    var manager: HLSManager {
        get {
            return ((UIApplication.shared.delegate!) as! AppDelegate).manager
        }
    }
    var subscribeAlertController: UIAlertController!
    var isDownloadBtnTap = false
    var castData = [CastCrewModel]()
    var  movieDetail:Movie?
    var contentID = "0"
    var sessionManager = SessionManager()
    lazy var adBannerView: GADBannerView = {
        let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        adBannerView.adUnitID = Constant.googleBannerKey
        adBannerView.delegate = self
        adBannerView.rootViewController = self
        return adBannerView
    }()
    var refreshCollectionViewDelegate: RefreshCollectionViews?
    var redirectToHome: Bool = false
    var showOffline = false
    var avLayer: AVPlayerLayer!
    var price: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupParallaxHeader()
        uisetup()
        addData()
        collectionsetup()
        onviewdidload()
    }
    
    
    func uisetup() {
        parentView.backgroundColor = UIColor.init(hex: "0E1525")
        genreBackView.backgroundColor = UIColor.init(hex: "0E1525")
        imageTopView.layer.borderColor = UIColor.clear.cgColor
        buyWithNoadsView.layer.borderWidth = 1
        buyWithNoadsView.layer.cornerRadius = 4
        buyWithNoadsView.layer.borderColor = AppColors.darkRedColor.cgColor
        cinecoinBackView.layer.borderWidth = 1
        cinecoinBackView.layer.cornerRadius = 4
        cinecoinBackView.layer.borderColor = AppColors.yellowColor.cgColor
        reviewView.clipsToBounds = true
        reviewView.layer.cornerRadius = 10
        //reviewView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
        reviewView.layer.cornerRadius = 29
        downloadBackView.layer.cornerRadius = 26
        downloadBackView.layer.borderColor = UIColor.init(hex: "2D880E").cgColor
        downloadBackView.layer.borderWidth = 3
        if countryCode?.uppercased() == "NP".uppercased() {
            price = String((movieDetail?.price ?? 0) / 100)
           
            
        } else {
            price = String((movieDetail?.price ?? 0) / 10000)
//            noadsPriceLbl.text = "$" + (price ?? "")
            fireeCineCoinLbl.text = "(with " + String((movieDetail?.price ?? 0) / 100) + " CineCoins)"
            
        }
    }
    
    private func setupParallaxHeader() {
        //setup blur vibrant view
      
        movieimage.blurView.setup(style: UIBlurEffect.Style.dark, alpha: 1).enable()
        movieimage.blurView.alpha = 0
        
        // headerImageView = imageView
        tableView.parallaxHeader.view = headerView
        tableView.parallaxHeader.height = 336
        tableView.parallaxHeader.minimumHeight = 0
        tableView.parallaxHeader.mode = .centerFill
        tableView.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
            //update alpha of blur view on top of image view
            parallaxHeader.view.blurView.alpha = 1 - parallaxHeader.progress
        }
        tableView.tableFooterView = footerView
    }
    
    @IBAction func buyWithCineCoinTap(_ sender: Any) {
        let alert = UIAlertController(title: "Paying with CineCoin", message: "", preferredStyle: .alert)
        //Customize view
        alert.view.tintColor = UIColor.init(hex:"#F3BC31")
        let attributedTitleText = NSMutableAttributedString(
            string: "Paying with CineCoin",
            attributes: [
                NSAttributedString.Key.foregroundColor: UIColor.white
                
            ]
        )
        
        // set attributed message here
        alert.setValue(attributedTitleText, forKey: "attributedTitle")
        
        //Background color
        let subview :UIView = alert.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.subviews.first!.backgroundColor = UIColor.init(hex: "#252B38")
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter your pin"
            textField.isSecureTextEntry = true
        }
        let pinTextField = alert.textFields?.first
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { action in
            self.handlePayment(pinTextField: pinTextField!)
        }))
        if(self.sessionManager.isLoggedIn()){
            self.present(alert, animated: true)
        }else{
            let signupVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(signupVC, animated: true)
        }
        
    }
    @IBAction func buyWithNoadsTap(_ sender: Any) {
        playTap(sender: UIButton())
    }
    @IBAction func playBtnTap(_ sender: UIButton) {
        playTap(sender: sender)
    }
    @IBAction func downloadTap(_ sender: Any) {
        if (self.sessionManager.isLoggedIn()){
            isDownloadBtnTap =  true
            checkIfContentIsPaid(contentID: (movieDetail?.id)!, shouldCast: false, contentType: .movie)
        } else {
            let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    @IBAction func watchTrailerTap(_ sender: Any) {
        let youtubePlayerVC = FreeMoviePlayerViewController.instantiate(fromAppStoryboard: .FreeMoviePlayer)
        if let youtubeURL = self.movieDetail?.trailerUrl{
            if youtubeURL.isEmpty{
            }
            else {
                youtubePlayerVC.youtubeURL = youtubeURL
                self.navigationController?.pushViewController(youtubePlayerVC, animated: true)
            }
        }
    }
    @IBAction func backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func onviewdidload() {
        googleAdViewSetup()
        print(movieDetail)
        globalMovieDetail = movieDetail
        getSettings()
        if(movieDetail == nil){
            getMovieDetailData()
        }
        //        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
        //            titleTopMargin.constant = 12
        //        }
        //        let backButton = UIButton() // back button
        //        backButton.backgroundColor = UIColor.white
        //        backButton.layer.cornerRadius = 15
        //        backButton.setImage(UIImage(named: "icon_navBar_Back"), for: .normal)
        //        backButton.imageEdgeInsets = UIEdgeInsetsMake(22,22,22,23)
        //        backButton.imageView?.tintColor = UIColor.darkGray
        //        self.view.addSubview(backButton)
        //        backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        //
        //        backButton.snp.makeConstraints{(make) in
        //            make.centerY.equalTo(self.navBar).offset(0)
        //            make.height.equalTo(30)
        //            make.width.equalTo(30)
        //            make.left.equalTo(self.view).offset(12)
        //        }
        
        APESuperHUD.appearance.backgroundBlurEffect = .dark
        APESuperHUD.appearance.titleFontSize = 20
        APESuperHUD.appearance.iconWidth = 48
        APESuperHUD.appearance.loadingActivityIndicatorColor = UIColor.darkText
        APESuperHUD.appearance.iconHeight = 48
        APESuperHUD.appearance.messageFontSize = 16
        APESuperHUD.appearance.textColor = UIColor.white
        APESuperHUD.appearance.foregroundColor = .white
        renderView()
        setupSubscribeView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showProgress(isLoading:)), name: Notification.Name(rawValue: "IAPPaymentStatusLoading"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideProgress(notification:)), name: Notification.Name(rawValue: "IAPPaymentStatusNotLoading"), object: nil)
    }
    func handlePayment( pinTextField: UITextField){
        let pin = pinTextField.text!
        APIHandler.shared.walletVerifyPin(pin: pin, type: "content", id: String(movieDetail?.id ?? 0) , season: "") { (status, token) in
            if(status){
                APIHandler.shared.validatePaymentFromCineCoin(paymentGateway: .wallet, mediaType: .movie, contentId: Int(self.movieDetail?.id ?? 0), episodeID: 0, paymentSuccessToken: token, transactionID: "") { (status, url, msg) in
                    if(status){
                        let alert = UIAlertController(title: "Success!", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                        //                        self.getSliderData()
                        //                        self.getTrendingData()
                        //                        self.getSubscriptionData()
                        self.movieDetail?.isBought = true
                        self.renderView()
                        NotificationCenter.default.post(name: Notification.Name("ReloadData"), object: nil,userInfo: ["movieid": globalMovieDetail?.id ?? ""])
                        self.present(alert, animated: true)
                    }else{
                        let alert = UIAlertController(title: "Error!", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: {_ in
                            let vc = MyReawardVC.instantiate(fromAppStoryboard: .reward)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }))
                        self.present(alert, animated: true)
                    }
                } failure: { (err) in
                    let alert = UIAlertController(title: "Error!", message: err.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
                
            }else{
                let alert = UIAlertController(title: "Error!", message: token, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }
        } failure: { (error) in
            let alert = UIAlertController(title: "Error!", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
        
    }
    func playTap(sender: UIButton) {
        print("sawsank: Play button pressed.")
        sender.isUserInteractionEnabled = true
        if let isComingSoon = movieDetail?.isComingSoon{
            if isComingSoon{
                let alert = UIAlertController(title: "Coming Soon", message: "\"\(movieDetail?.title ?? "This content")\"  is coming soon. Please refer to the poster for the release date.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
        if let isPaid = movieDetail?.paid{
            if isPaid {
                if SessionManager().isLoggedIn(){
                    let result = checkIfDownloadedOrNot()
                    if result == true {
                        let location = getSelectedMovieLocation()
                        let vc = CustomVideoViewController.instantiate(fromAppStoryboard: .CustomVideoPlayer)
                        let vidloaderhanlder: VidLoaderHandler = .shared
                        guard let locationurl = location.first?.location else { return  }
                        let urlasset = vidloaderhanlder.loader.asset(location: locationurl)
                        vc.offlineItem = AVPlayerItem(asset: urlasset!)
                        vc.playOffline = true
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    } else {
                        checkIfContentIsPaid(contentID: (movieDetail?.id)!, shouldCast: false, contentType: .movie)
                    }
                    //                    if showOffline == true {
                    //                       let result = checkIfDownloadedOrNot()
                    //                        print(result)
                    //                    } else {
                    //
                    //                    }
                    
                }else{
                    //User is not logged in.
                    let alert = UIAlertController(title: "You must login first!", message: "Please login with your Cinemaghar account to watch this movie.", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    
                    
                    alert.addAction(UIAlertAction(title: "Login", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                        
                        let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                        loginVC.shouldAllowSkip = false
                        loginVC.shouldDismissAfterLogin = true
                        self.navigationController?.pushViewController(loginVC, animated: true)
                        
                        
                    }))
                    
                    
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                //playOffline("")
                sender.isUserInteractionEnabled = true
                if let isVideoFromYoutube = self.movieDetail?.videoIsFromYoutube, let youtubeURL = self.movieDetail?.video {
                    if isVideoFromYoutube {
                        let youtubePlayerVC = FreeMoviePlayerViewController.instantiate(fromAppStoryboard: .FreeMoviePlayer)
                        youtubePlayerVC.youtubeURL  = youtubeURL
                        self.navigationController?.pushViewController(youtubePlayerVC, animated: true)
                        
                        
                    }else{
                        print("sawsank: Checking when free")
                        if SessionManager().isLoggedIn(){
                            checkIfContentIsPaid(contentID: (movieDetail?.id)!, shouldCast: false, contentType: .movie)
                            
                        }else{
                            let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                            loginVC.shouldAllowSkip = false
                            loginVC.shouldDismissAfterLogin = true
                            self.navigationController?.pushViewController(loginVC, animated: true)
                        }
                    }
                }
            }
        }
    }
//    func checkIfContentIsPaid(contentID: Int, shouldCast: Bool, contentType: MediaType, episodeId: Int = 0 ) {
//
//        APESuperHUD.appearance.backgroundColor = .clear
//        APESuperHUD.appearance.foregroundColor = .clear
//        APESuperHUD.appearance.loadingActivityIndicatorColor = .white
//        APESuperHUD.showOrUpdateHUD( loadingIndicator: .standard, message: "", presentingView: self.view)
//
//        APIHandler.shared.checkIfVideoIsPurchased(contentType: contentType, contentId: contentID, episodeId: episodeId, success: { [self](status, videoURL, hasBoughtMovie, hasRatedMovie, isComingSoon) in
//            if hasBoughtMovie  {
//                print(videoURL)
//                if self.isDownloadBtnTap == true {
//                    self.isDownloadBtnTap = false
//                    APESuperHUD.removeHUD(animated: true, presentingView: self.view)
//                    print(videoURL)
//                    // self.download(videoURL)
//                    DispatchQueue.main.async {
//                        let vc = VideoListController.instantiate(fromAppStoryboard: .MovieDetail)
//                        vc.dataProvider = VideoListDataProvider()
//                        vc.movideDetail = movieDetail
//                        vc.videoUrl = videoURL
//
//                        //                        let videodata = VideoData(identifier: "\(movieDetail?.id ?? 0)", title: movieDetail?.title ?? "", imageName: movieDetail?.coverImage ?? "", state: .unknown, stringURL: videoURL, location: nil)
//                        //                        vc.downloadsData = [videodata]
//                        //                        vc.showDonwloading = true
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//
//                } else if self.showOffline == true {
//                    self.playOffline(videoURL)
//                } else {
//
//                    APESuperHUD.removeHUD(animated: true, presentingView: self.view)
//
//                    let videoPlayerVC = CustomVideoViewController.instantiate(fromAppStoryboard: .CustomVideoPlayer)
//                    print("sawsank: \(videoURL)")
//                    videoPlayerVC.mediaInfo = self.buildMediaInformation(urlString: videoURL)
//                    videoPlayerVC.urlString = videoURL
//                    videoPlayerVC.videoTitle = (self.movieDetail?.title)!
//                    videoPlayerVC.contentId = (self.movieDetail?.id)!
//                    videoPlayerVC.mediaType = .movie
//                    videoPlayerVC.isRated = hasRatedMovie
//                    videoPlayerVC.isComingSoon = isComingSoon
//                    if(self.movieDetail?.advertisements.count == 0){
//                        videoPlayerVC.advertisement = nil
//                    }else {
//                        videoPlayerVC.advertisement = (self.movieDetail?.advertisements.first)!
//                    }
//
//                    self.navigationController?.pushViewController(videoPlayerVC, animated: true)
//                }
//
//
//            }
//            else {
//
//                APIHandler.shared.getProfile { (profile) in
//                    if(!profile.isSubscribed){
//                        APESuperHUD.removeHUD(animated: true, presentingView: self.view)
//                        self.viewSubscriptionAlert()
//
//                        if let reloadDelegate = self.refreshCollectionViewDelegate{
//                            reloadDelegate.refreshCollectionViews()
//                        }
//                    }
//                } failure: { (msg) in
//                    print("Failure: \(msg)")
//                } error: { (err) in
//                    print("Error: \(err.localizedDescription)")
//                }
//
//            }
//
//
//        }, failure: {(failure) in
//
//            APESuperHUD.removeHUD(animated: true, presentingView: self.view)
//
//
//            if failure._code == NSURLErrorNotConnectedToInternet {
//
//                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
//                // add the actions (buttons)
//
//                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
//
//                }))
//                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in
//
//                    self.checkIfContentIsPaid(contentID: contentID, shouldCast: shouldCast, contentType: contentType)
//                }))
//
//                // show the alert
//                self.present(alert, animated: true, completion: nil)
//
//            }
//
//            else if(failure._code == NSURLErrorTimedOut){
//                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
//                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
//                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
//                    self.checkIfContentIsPaid(contentID: contentID, shouldCast: shouldCast, contentType: contentType)
//                })
//                controller.addAction(cancel)
//                controller.addAction(retry)
//                self.present(controller, animated: true, completion: nil)
//
//
//            }
//
//            else if (failure._code == NSURLErrorNetworkConnectionLost){
//
//                self.checkIfContentIsPaid(contentID: contentID, shouldCast: shouldCast, contentType: contentType)
//
//            }
//        })
//    }
    func checkIfContentIsPaid(contentID: Int, shouldCast: Bool, contentType: MediaType, episodeId: Int = 0 ) {
        APESuperHUD.appearance.backgroundColor = .clear
        APESuperHUD.appearance.foregroundColor = .clear
        APESuperHUD.appearance.loadingActivityIndicatorColor = .white
        APESuperHUD.showOrUpdateHUD( loadingIndicator: .standard, message: "", presentingView: self.view)
        APIHandler.shared.checkIfVideoIsPurchased(contentType: contentType, contentId: contentID, episodeId: episodeId, success: { [self](status, videoURL, hasBoughtMovie, hasRatedMovie, isComingSoon) in
            if hasBoughtMovie  {
                print(videoURL)
                if self.isDownloadBtnTap == true {
                    self.isDownloadBtnTap = false
                    APESuperHUD.removeHUD(animated: true, presentingView: self.view)
                    print(videoURL)
                   // self.download(videoURL)
                    DispatchQueue.main.async {
                        let vc = VideoListController.instantiate(fromAppStoryboard: .MovieDetail)
                        vc.dataProvider = VideoListDataProvider()
                        vc.movideDetail = movieDetail
                        vc.videoUrl = videoURL
//                        let videodata = VideoData(identifier: "\(movieDetail?.id ?? 0)", title: movieDetail?.title ?? "", imageName: movieDetail?.coverImage ?? "", state: .unknown, stringURL: videoURL, location: nil)
//                        vc.downloadsData = [videodata]
//                        vc.showDonwloading = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                } else if self.showOffline == true {
                    self.playOffline(videoURL)
                } else {
                APESuperHUD.removeHUD(animated: true, presentingView: self.view)
                let videoPlayerVC = CustomVideoViewController.instantiate(fromAppStoryboard: .CustomVideoPlayer)
                print("sawsank: \(videoURL)")
                videoPlayerVC.mediaInfo = self.buildMediaInformation(urlString: videoURL)
                videoPlayerVC.urlString = videoURL
                videoPlayerVC.videoTitle = (self.movieDetail?.title)!
                videoPlayerVC.contentId = (self.movieDetail?.id)!
                videoPlayerVC.mediaType = .movie
                videoPlayerVC.isRated = hasRatedMovie
                videoPlayerVC.isComingSoon = isComingSoon
                if(self.movieDetail?.advertisements.count == 0){
                    videoPlayerVC.advertisement = nil
                }else {
                    videoPlayerVC.advertisement = (self.movieDetail?.advertisements.first)!
                }
                
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }


            }
            else {
                APIHandler.shared.getProfile { (profile) in
                    if(!profile.isSubscribed){
                        APESuperHUD.removeHUD(animated: true, presentingView: self.view)
                        self.viewSubscriptionAlert()
                        
                        if let reloadDelegate = self.refreshCollectionViewDelegate{
                            reloadDelegate.refreshCollectionViews()
                        }
                    }
                } failure: { (msg) in
                    print("Failure: \(msg)")
                } error: { (err) in
                    print("Error: \(err.localizedDescription)")
                }
            }
        }, failure: {(failure) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view)
            if failure._code == NSURLErrorNotConnectedToInternet {
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in

                        self.checkIfContentIsPaid(contentID: contentID, shouldCast: shouldCast, contentType: contentType)
                    }))
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                    self.checkIfContentIsPaid(contentID: contentID, shouldCast: shouldCast, contentType: contentType)
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
            }
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                self.checkIfContentIsPaid(contentID: contentID, shouldCast: shouldCast, contentType: contentType)

            }
        })
    }
    
    private func buildMediaInformation(urlString:String) -> GCKMediaInformation {
        let metadata = GCKMediaMetadata(metadataType: GCKMediaMetadataType.movie)
        metadata.setString((movieDetail?.title)!,  forKey: kGCKMetadataKeyTitle)
        metadata.setString((movieDetail?.cast)!, forKey: kGCKMetadataKeyStudio)
        metadata.addImage(GCKImage(url: URL(string: (movieDetail?.BackGroundImage)!)!, width: 500, height: 500))
        var customData = [String:String]()
        
        if SessionManager().isLoggedIn() {
            if let title = self.movieDetail?.title{
                customData = ["userID": UserDefaults.standard.string(forKey: AppConstants.uniqueId)! , "userName": UserDefaults.standard.string(forKey: AppConstants.userName)! , "movieTitle": title]
            }
        }
        else {
            if let title = self.movieDetail?.title{
                
                customData = ["userID": " " , "userName": " " , "movieTitle": title]
            }
            
        }
        
        let url = URL.init(string: urlString)!
        
        
        let mediaInfoBuilder = GCKMediaInformationBuilder.init(contentURL: url)
        mediaInfoBuilder.streamType = GCKMediaStreamType.buffered;
        mediaInfoBuilder.contentType = "application/x-mpegurl"
        mediaInfoBuilder.metadata = metadata
        mediaInfoBuilder.contentID = urlString
        mediaInfoBuilder.customData = customData
        let mediaInformation = mediaInfoBuilder.build()
        
        
        return mediaInformation
    }
    @objc func viewSubscriptionAlert(){
        //        let message = "\n\n\n\n\n\n"
        //        let message = """
        //            Things to know: \n
        //            Price 0.99 USD \n
        //            The purhcase will be valid for a week. \n
        //            \n \n \n \n \n
        //        """
        let message = """
            The purchase will be valid for a week.\n  Price 0.99 USD \n
            \n
           
            \n \n
        """
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        
        let attributedMessageText = NSMutableAttributedString(
            string: message,
            attributes: [
                NSAttributedString.Key.paragraphStyle: paragraphStyle,
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0),
                NSAttributedString.Key.foregroundColor: UIColor.black
            ]
        )
        let attributedTitleText = NSMutableAttributedString(
            
            string: "Confirm your \nIn-App Purchase",
            attributes: [
                NSAttributedString.Key.foregroundColor: UIColor.black
                
            ]
        )
        subscribeAlertController = UIAlertController(title: "Gold Subscription", message: message, preferredStyle: .alert)
        
        //Customize view
        subscribeAlertController.view.tintColor = UIColor.init(hex:"#252B38")
        
        //Background color
        let subview :UIView = subscribeAlertController.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.subviews.first!.backgroundColor = UIColor.init(hex: "#E59827")
        
        // set attributed message here
        subscribeAlertController.setValue(attributedMessageText, forKey: "attributedMessage")
        subscribeAlertController.setValue(attributedTitleText, forKey: "attributedTitle")
        
        
        //CineCoin Outlets
        var paymentButton: PKPaymentButton!
        //        if #available(iOS 15.0, *) {
        //            paymentButton = PKPaymentButton.init(paymentButtonType: ., paymentButtonStyle: .black)
        //        } else {
        //            // Fallback on earlier versions
        //            paymentButton = PKPaymentButton.init(paymentButtonType: .plain, paymentButtonStyle: .black)
        //        }
        paymentButton = PKPaymentButton.init(paymentButtonType: .buy, paymentButtonStyle: .black)
        paymentButton.translatesAutoresizingMaskIntoConstraints = false
        subview.addSubview(paymentButton)
        paymentButton.leftAnchor.constraint(equalTo: subview.leftAnchor, constant: 16).isActive = true
        paymentButton.rightAnchor.constraint(equalTo: subview.rightAnchor, constant: -16).isActive = true
        paymentButton.bottomAnchor.constraint(equalTo: subview.bottomAnchor, constant: -100).isActive = true
        paymentButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        let paymentButtonTop = UIButton()
        paymentButtonTop.layer.cornerRadius = 4
        paymentButtonTop.isUserInteractionEnabled = false
        paymentButtonTop.setTitle("Buy", for: .normal)
        paymentButtonTop.backgroundColor = .black
        paymentButtonTop.translatesAutoresizingMaskIntoConstraints = false
        subview.addSubview(paymentButtonTop)
        paymentButtonTop.leftAnchor.constraint(equalTo: subview.leftAnchor, constant: 16).isActive = true
        paymentButtonTop.rightAnchor.constraint(equalTo: subview.rightAnchor, constant: -16).isActive = true
        paymentButtonTop.bottomAnchor.constraint(equalTo: subview.bottomAnchor, constant: -100).isActive = true
        paymentButtonTop.heightAnchor.constraint(equalToConstant: 50).isActive = true

        //Please visit our site for more info.
        var websiteButton: UIButton!
        websiteButton = UIButton()
        if(redirectToHome){
            websiteButton.setTitle("Visit our website for more info", for: .normal)
            websiteButton.isHidden = true
        }else{
            websiteButton.setTitle("Pay from website", for: .normal)
            websiteButton.isHidden = false
        }
        websiteButton.backgroundColor = UIColor(hex: "#252B38")
        websiteButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        websiteButton.layer.cornerRadius = 16
        websiteButton.translatesAutoresizingMaskIntoConstraints = false
        subview.addSubview(websiteButton)
        websiteButton.leftAnchor.constraint(equalTo: subview.leftAnchor, constant: 16).isActive = true
        websiteButton.rightAnchor.constraint(equalTo: subview.rightAnchor, constant: -16).isActive = true
        websiteButton.bottomAnchor.constraint(equalTo: subview.bottomAnchor, constant: -50 ).isActive = true
        websiteButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        websiteButton.addTarget(self, action: #selector(gotoWeb(action:)), for: .touchUpInside)
        subscribeAlertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        paymentButton.addTarget(self, action: #selector(purchase(_:)), for: .touchUpInside)
        if(SessionManager().isLoggedIn()){
            self.present(subscribeAlertController, animated: true)
        }else{
            let signupVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(signupVC, animated: true)
        }
    }
    @objc func purchase(_ sender: UITapGestureRecognizer) {
        globalisPurchasedFromApple = true
        print("sawsank: Purchasing From apple")
        IAPService.shared.purchase(product: IAPProduct.coin100)
        subscribeAlertController.dismiss(animated: true)
    }
    func playOffline(_ url: String?) {
        if check(url: url) {
            do {
                let (isExist, hls) = try manager.isExist(url!, firstUrl: "\(movieDetail?.id ?? 0)")
                if isExist {
                    print("[HLS Exist]\(url!)")
                    
                    if HLS.State(rawValue: hls!.state) == .downloaded {
                        avLayer = AVPlayerLayer()
                        
                        print("HLS Exists")
                        let item = manager.getLocalAsset(hls!)
                        
                        //                        let player = AVPlayer(playerItem: item)
                        //                        avLayer.player = player
                        //                        avLayer.frame = self.view.bounds
                        //                        self.view.layer.addSublayer(avLayer)
                        //                        avLayer.player?.play()
                        
//                        let player = AVPlayer(url: URL(string: "https://nepal.cinema-ghar.com/videos/1c82cc8ccb5b40639d2789a384dbcf39noises/1c82cc8ccb5b40639d2789a384dbcf39noises.m3u8?query=eyJ1c2VyX2lkIjoyMDc5OCwiY29udGVudF9pZCI6MzI4LCJkZXZpY2VfdHlwZSI6ImlvcyIsImRldmljZV9pZCI6IjAyRDYxRkM2LTFFRjQtNDI3Qi05OEYwLTJGMjUyRDE3NTg1MiIsImlwIjoiMjcuMzQuMjAuMjAwIiwidmlkZW8iOiIxYzgyY2M4Y2NiNWI0MDYzOWQyNzg5YTM4NGRiY2YzOW5vaXNlcyIsIm1lZGlhX3R5cGUiOiJjb250ZW50cyIsInR5cGUiOiJwbGF5IiwibWV0YSI6eyJoZWFkZXJzIjp7ImFjY2VwdC1lbmNvZGluZyI6WyJicjtxPTEuMCwgZ3ppcDtxPTAuOSwgZGVmbGF0ZTtxPTAuOCJdLCJ4LWRldmljZS1pZCI6WyIwMkQ2MUZDNi0xRUY0LTQyN0ItOThGMC0yRjI1MkQxNzU4NTIiXSwieC1kZXZpY2UtbW9kZWwiOlsiaVBob25lIl0sIngtdmVyc2lvbiI6WyIxLjEiXSwieC1kZXZpY2UtdHlwZSI6WyJpb3MiXSwidXNlci1hZ2VudCI6WyJDaW5lbWFHaGFyXC8zLjcuMiAoY29tLnNhZ2Fya2hhcmVsLmNpbmVtYWdoYXJoZDsgYnVpbGQ6NzA7IGlPUyAxNC42LjApIEFsYW1vZmlyZVwvNS40LjEiXSwiYWNjZXB0LWxhbmd1YWdlIjpbImVuLU5QO3E9MS4wIl0sImF1dGhvcml6YXRpb24iOlsiQmVhcmVyIGV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5leUpwYzNNaU9pSm9kSFJ3Y3pwY0wxd3ZjM1JoWjJsdVp5NWphVzVsYldFdFoyaGhjaTVqYjIxY0wyRndhVnd2WVhWMGFGd3ZiRzluYVc0aUxDSnBZWFFpT2pFMk5USTNPRGsxTkRBc0ltVjRjQ0k2TVRnME1qQXdOVFUwTUN3aWJtSm1Jam94TmpVeU56ZzVOVFF3TENKcWRHa2lPaUpWZWs1S1duWnNVblZVT0UxUE1GVm9JaXdpYzNWaUlqb3lNRGM1T0N3aWNISjJJam9pT0RkbE1HRm1NV1ZtT1daa01UVTRNVEptWkdWak9UY3hOVE5oTVRSbE1HSXdORGMxTkRaaFlTSXNJbVJsZG1salpWOXBaQ0k2SWpBeVJEWXhSa00yTFRGRlJqUXROREkzUWkwNU9FWXdMVEpHTWpVeVJERTNOVGcxTWlKOS5qa2ZPX0l3WUdxaUx2VVhSa1d4WVVfUTZac3BJNFRWLW9xZGVha2xiazUwIl0sImFjY2VwdCI6WyJhcHBsaWNhdGlvblwvanNvbiJdLCJob3N0IjpbInN0YWdpbmcuY2luZW1hLWdoYXIuY29tIl0sImNvbnRlbnQtbGVuZ3RoIjpbIiJdLCJjb250ZW50LXR5cGUiOlsiIl19fX0=")!)
//                        let playerViewController = AVPlayerViewController()
//                        playerViewController.player = player
//                        self.present(playerViewController, animated: true) {
//                            playerViewController.player!.play()
//                        }
                        
                        //                        let vc = DemoPlayerVC.instantiate(fromAppStoryboard: .MovieDetail)
                        //                        vc.offlineItem = item
                        //                        self.navigationController?.pushViewController(vc, animated: true)
                        
                                                let vc = CustomVideoViewController.instantiate(fromAppStoryboard: .CustomVideoPlayer)
                                                vc.offlineItem = item
                                                vc.playOffline = true
                                                self.navigationController?.pushViewController(vc, animated: true)
                        
                        //                        if cell.playerLayer.player == nil {
                        //                            let item = manager.getLocalAsset(hls!)
                        //                            let player = AVPlayer(playerItem: item)
                        //                            cell.playerLayer.player = player
                        //                            cell.playerLayer.frame = cell.playerView.bounds
                        //                        }
                        //                        cell.playerLayer.player?.play()
                    }
                    else {
                        print("[HLS Not Downloaded]\(url!)")
                    }
                }
                else {
                    print("[HLS Not Exist]\(url!)")
                }
            } catch {
                print("[ERROR]Repository problem")
            }
        }
    }
    @objc func gotoWeb(action: UIAlertAction){
        let url = URL(string: "https://cinema-ghar.com/" + (movieDetail?.slug ?? "") + "?" + "showPaymentModal=1")!
        if  UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    private func check(url: String?) -> Bool {
        return true
        //return urls.contains(url ?? "")
    }
    func getSelectedMovieLocation() -> [VideoData] {
        let downloadedmovies = getUserDefults.getDownloadedMoview()
        let exists = downloadedmovies.filter{$0.identifier == "\(movieDetail?.id ?? 0)" }
        return exists
    }
    func googleAdViewSetup() {
        let adRequest = GADRequest()
        
        adBannerView.load(GADRequest())
        googleAdView.addSubview(adBannerView)
        googleAdView.backgroundColor = .clear
    }
    func setupSubscribeView(){
        if checkIfDownloadedOrNot() == true {
            downloadLbl.text = "Downloaded"
            toplayofflineLbl.text = ""
        } else {
            downloadLbl.text = "Download"
            toplayofflineLbl.text = "to play offline"
        }
    }
    func checkIfDownloadedOrNot() -> Bool {
        let downloadedmovies = getUserDefults.getDownloadedMoview()
        let exists = downloadedmovies.filter{$0.identifier == "\(movieDetail?.id ?? 0)" }
        if exists.first?.state == .completed {
            return true
        } else {
            return false
        }
        
    }
    func renderView(){
        if let posterURL = movieDetail?.poster, let coverURL = movieDetail?.BackGroundImage , let rating = movieDetail?.rating, let Title = movieDetail?.title, let movieCasts = movieDetail?.cast, let movieDescription = movieDetail?.descriptionField  {
            movieimage.sd_setImage(with: URL(string: posterURL), placeholderImage: UIImage(named: "placeholder_cell"), options: [.scaleDownLargeImages], completed:nil)
            
            movieTitle.text = Title
            //descriptionLbl.setHTMLFromString(htmlText: movieDescription, hexCode: "#FFFFFF")
            descriptionLbl.text = movieDescription.htmlToString
//            descriptionLbl.backgroundColor = .clear
//            descriptionLbl.textColor = .white
           
            // castLabel.text = movieCasts
            ratingLbl.text = String(rating)
            genreLbl.text = movieDetail?.genre
            
            if movieDetail?.isComingSoon == true {
              
                mainStackView.isHidden = true
                mainStackHeight.constant = 0
               
            } else if movieDetail?.isBought == false{
                
                downloadBackView.isHidden = true
                mainStackHeight.constant = 52
            }
        }
        
        self.playButton.isEnabled = true
        self.playButton.isUserInteractionEnabled = true
        
        if let releaseYear = movieDetail?.releaseYear{
            //            releaseDateLbl.attributedText = makeMovieDetailLabel(preffix: "Release Year: ", suffix: releaseYear)
            releaseDateLbl.text = "Release Year: " + releaseYear
        }
        
        if let runTime = movieDetail?.duration {
            print(runTime)
            runtimeLbl.attributedText = makeMovieDetailLabel(preffix: "Runtime: ", suffix: runTime)
            runtimeLbl.text = "Runtime: " + runTime
        }
        
        
        
        if let isPaid = movieDetail?.paid, let isAddedToWishList = movieDetail?.isAddedToWishList{
            
            if isPaid { // paid movie
                
                
                if let isBought = self.movieDetail?.isBought {
                    if isBought {
                        payStackView.isHidden = true
                        mainStackHeight.constant = 52
                        downloadBackView.isHidden = false
                        //self.animatePlayButton()
                        //                        priceLabel.text = " PURCHASED "
                        //                        priceIndicatorBackgroundView.backgroundColor = UIColor(hex: "#08ABE8")
                        //
                        //                        redeemOutlet.isHidden = true
                        //                        subscribeOutlet.isHidden = true
                        
                    } else {
                        payStackView.isHidden = false
                        downloadBackView.isHidden = true
                        if (self.movieDetail?.isUnderSubscription) != nil {
                            //                            priceLabel.text = " Gold Content "
                            //                            priceIndicatorBackgroundView.isHidden = true
                            //                            priceIndicatorBackgroundView.backgroundColor = UIColor(hex: "#E59827")
                        }else{
                            //                            priceLabel.text = " PREMIUM "
                            //                            priceIndicatorBackgroundView.isHidden = true
                            //                            priceIndicatorBackgroundView.backgroundColor = UIColor(hex: "DF0018")
                        }
                        
                    }
                    
        
                }
            }
            else {
                //                priceLabel.text = " FREE "
                //                priceIndicatorBackgroundView.backgroundColor = UIColor(hex: "008000")
                //                expiryDateLabel.isHidden = true
                //
                //                redeemOutlet.isHidden = true
                //
                //                animatePlayButton()
                
            }
        }
        
        
        if let youtubeURL = self.movieDetail?.trailerUrl{
            if youtubeURL.isEmpty{
                reviewView.alpha  = 0.8
            }
            else {
                reviewView.alpha  = 1
                
            }
            
        }
    }
    
    func makeMovieDetailLabel(preffix: String, suffix:String)-> NSAttributedString {
        
        let formattedString = NSMutableAttributedString()
        if suffix == ""{
            formattedString
                .movieDetailLabelFormatter(preffix,  16, UIFont.Weight.medium, .white )
                .movieDetailLabelFormatter("N/A", 16,UIFont.Weight.semibold, .white)
            
        }
        else {
            formattedString
                .movieDetailLabelFormatter(preffix,  14, UIFont.Weight.regular, .white)
                .movieDetailLabelFormatter(suffix, 16, UIFont.Weight.medium, .white)
            
        }
        
        return formattedString
        
    }
    
    @objc func showProgress(isLoading: Bool){
        DispatchQueue.main.async {
            APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
            APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Preparing Payment...", presentingView: self.view)
        }
    }
    @objc func hideProgress(notification: NSNotification){
        print("sawsank: hiding progress")
        self.getMovieDetailData()
        
        DispatchQueue.main.async {
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: nil)
            guard let userInfo = notification.userInfo else {
                return
            }
            let alertDetailController = DOAlertController(title: "Info Detail", message: userInfo["message"] as? String, preferredStyle: .alert)
            alertDetailController.alertViewBgColor = UIColor.white
            alertDetailController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertDetailController.messageView.textAlignment = .left
            alertDetailController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertDetailController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "OK", style: .default, handler: { action in
                print("sawsank: OK Pressed")
                
                self.dismiss(animated: true, completion: nil)
            })
            // Add the action.
            alertDetailController.addAction(okAction)
            // Show alert
            self.present(alertDetailController, animated: true, completion: nil)
            
            
        }
//        let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
//        movieDetailScene.movieDetail = self.movieDetail
//        self.navigationController?.pushViewController(movieDetailScene, animated: true)
        
    }
    
    func addData() {
        castData = [CastCrewModel(image: "iconCast1", name: "Dalia Kevin"),CastCrewModel(image: "iconCast2", name: "Dalia Kevin"),CastCrewModel(image: "iconCast3", name: "Dalia Kevin")]
    }
    func collectionsetup() {
        collectionView.register(UINib(nibName: "CastCrewCVC", bundle: nil), forCellWithReuseIdentifier: "CastCrewCVC")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.reloadData()
    }
    @IBAction func shareTap(_ sender: Any) {
    }
    
    
    
    
    
}
//MARK: - COLLECTION DADTASOURCE
extension NewMovieDetailsVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let cast = movieDetail?.cast.components(separatedBy: ",")
        print(cast)
        return cast?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CastCrewCVC" , for: indexPath) as! CastCrewCVC
        let cast = movieDetail?.cast.components(separatedBy: ",")
        cell.actorName.text = cast?[indexPath.row]
        //        cell.actorImage.image = UIImage(named: castData[indexPath.row].image ?? "")
        return cell
    }
}
extension NewMovieDetailsVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 53)
    }
}

extension NewMovieDetailsVC {
    //MARK: - GET MOVIEW DETAIL DATA
    func getMovieDetailData(){
        print("sawsank: getting movie detail data")
        
        if (movieDetail != nil){
            contentID = String((movieDetail?.id)!)
        }
        print("sawsank: contentID: \(contentID)")
        
        APIHandler.shared.getMoviesDetail(contentID: contentID) { (status, movieDetail) in
            if(status){
                self.movieDetail = movieDetail
                self.renderView()
            }
        } failure: { (error) in
            print(error)
        }
    }
    func getSettings() {
        APIHandler.shared.getSettings(success: { (json) in
            print(json)
            self.redirectToHome = json["redirectToHome"].boolValue
            if self.redirectToHome == true {
                self.googleAdView.isHidden = true
            } else {
                self.googleAdView.isHidden = false
            }
        }, failure: { (message) in
            let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
                self.dismissVC()
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
        }) { (error) in
            let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
        }
    }
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK: - GOOGLE AD DELEGATE
extension NewMovieDetailsVC: GADBannerViewDelegate {
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("bannerViewDidReceiveAd")
    }
    
    private func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
        print("bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    func adViewDidRecordImpression(_ bannerView: GADBannerView) {
        print("bannerViewDidRecordImpression")
    }
    
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("bannerViewWillPresentScreen")
    }
    
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("bannerViewWillDIsmissScreen")
    }
    
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("bannerViewDidDismissScreen")
    }
}


