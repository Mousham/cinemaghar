//
//  MovieReviewsViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 7/2/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit

class MovieReviewsViewController: DesignableViewController, UINavigationBarDelegate {
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var reviewsArray = [Review]()
    var contentId: Int?
    
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var reviewsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back_arrow"), for: .normal)
        button.setTitle(" Back", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        button.sizeToFit()
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)


        self.navItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        navBar.clipsToBounds = false
        
        
        self.navBar.isTranslucent = true
        
        
        
        self.reviewsTableView.contentInset = UIEdgeInsetsMake(46, 0, 0, 0)
        
       self.reviewsTableView.register(UINib(nibName: "MovieReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "MovieReviewTableViewCell")
  
//        self.reviewsTableView.estimatedRowHeight = 1000.0;
        
//        reviewsTableView.delegate = self
//        reviewsTableView.dataSource = self
        
        reviewsTableView.rowHeight = UITableViewAutomaticDimension
        reviewsTableView.estimatedRowHeight = UITableViewAutomaticDimension
        navBar.delegate = self
        
        
        
        
        
        
        getReviews()
        
       
        

        
    }
    
    
    func getReviews(){
        indicator.startAnimating()
        
        APIHandler.shared.getReviews(contentId: self.contentId!, success: {(reviewsArray) in
            
            
            self.reviewsArray = reviewsArray
            self.reviewsTableView.delegate = self
            self.reviewsTableView.dataSource = self
            self.reviewsTableView.reloadData()
            self.indicator.stopAnimating()
            
        }, failure: {(failure) in
            
            self.indicator.stopAnimating()
            
            
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.getReviews()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                    self.getReviews()
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                self.getReviews()
                
            }
            
            
            
            
        })
        
        
        
        
    }
    
    
    
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
    
    
    
    
    
    @objc func dismissVC(){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   

}

extension MovieReviewsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reviewsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "MovieReviewTableViewCell", for: indexPath) as! MovieReviewTableViewCell
        cell.backgroundColor = .clear
        cell.loadReviewCell(review: self.reviewsArray[indexPath.row])
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: Int = 0

        if self.reviewsArray.count>0{
            
            tableView.backgroundView = nil
            numOfSection = 1
        }
        
        else {
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x:0, y:0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "This movie hasn't received any reviews yet."
            noDataLabel.textColor = UIColor.gray
            noDataLabel.numberOfLines = 0
            noDataLabel.font = UIFont.systemFont(ofSize: 17)
            noDataLabel.textAlignment = .center
          tableView.backgroundView = noDataLabel
            
        }
        
        
        return numOfSection
        
    }
  
    
    
    
    
}
