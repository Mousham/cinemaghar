//
//  QuestionVC.swift
//  CinemaGhar
//
//  Created by Midas on 07/07/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMobileAds
//import VegaScrollFlowLayout
enum QuizAction {
    case moveToNextQuestion
    case getOutOfQuiz
    case nothing
}
class QuestionVC: UIViewController {

    @IBOutlet weak var googleAdVie: UIView!
    @IBOutlet weak var totalCineCoinBtn: AnimatedButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var counter = 1
    var goToNext = false
    var questionsData = [QuestionModel]()
    var quizPoints = Int()
    let defaults = UserDefaults.standard
    var totalcinecoin = 0
//    lazy var adBannerView: GADBannerView = {
//        let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
//        adBannerView.adUnitID = Constant.googleBannerKey
//        adBannerView.delegate = self
//        adBannerView.rootViewController = self
//        return adBannerView
//    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        uisetup()
        loadAd()
        getQuestions()
    }
    func uisetup() {
        collectionView.register(UINib(nibName: "QuestionCVC", bundle: nil), forCellWithReuseIdentifier: "QuestionCVC")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isScrollEnabled = false
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
        layout.minimumLineSpacing = 20
        layout.itemSize = CGSize(width: SCREEN.WIDTH - 32, height: 414)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func loadAd() {
        lazy var adBannerView: GADBannerView = {
            let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
            adBannerView.adUnitID = Constant.googleBannerKey
            adBannerView.delegate = self
            adBannerView.rootViewController = self
            return adBannerView
        }()
       
        adBannerView.removeFromSuperview()
        adBannerView.load(GADRequest())
        googleAdVie.addSubview(adBannerView)
        googleAdVie.backgroundColor = .clear
        
    }
    @IBAction func backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func scrollToNextCell(){

        //get cell size
        let cellSize = view.frame.size

        //get current content Offset of the Collection view
        let contentOffset = collectionView.contentOffset;

        if collectionView.contentSize.width <= collectionView.contentOffset.x + cellSize.width
        {
            collectionView.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)

        } else {
           
            collectionView.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)

        }

    }
    @objc func submitTap() {
    }

}
//MARK: - COLLECTION VIEW DATASOURCE
extension QuestionVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return questionsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestionCVC", for: indexPath) as! QuestionCVC
        //cell.submitBtn.addTarget(self, action: #selector(submitTap), for: .touchUpInside)
        cell.delegate = self
        cell.questionData = self.questionsData
        cell.questionTitle.text = questionsData[indexPath.row].question
        cell.questionNumTitle.text = "Questions " + "\(indexPath.row + 1)" + "/" + "\(questionsData.count)"
        cell.questionIndex = indexPath.row
        cell.backView.layer.cornerRadius = 12
        cell.backView.clipsToBounds = true
        cell.backView.layer.masksToBounds = false
        cell.backView.layer.borderWidth = 1
        return cell
    }
    
    
}
//MARK: - COLLECTION VIEW DELEGATE
extension QuestionVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0
           UIView.animate(withDuration: 0.8) {
               cell.alpha = 1
               cell.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)

           }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 424)
    }
}
extension QuestionVC: QuestionsDelegate {
    func submitAction() {
        submitTap()
    }
    func moveToNextQuestion() {
        loadAd()
        counter = counter + 1
        print(counter)
        print(questionsData.count)
        if counter - 1 == questionsData.count {
            
            loadWithReward(from: "reward", token: "rewardad", amount: "2", isfromAppReview: false, rating: "", type: .getOutOfQuiz)

        } else {
            totalcinecoin += 1
            totalCineCoinBtn.setTitle("Toal Cinecoin " + String(totalcinecoin), for: .normal)
            self.collectionView.scrollToItem(at: [0,self.counter - 1], at: .centeredHorizontally, animated: true)
            //loadWithReward(from: "reward", token: "rewardad", amount: "2", isfromAppReview: false, rating: "", type: .moveToNextQuestion)
        }
        //collectionView.reloadData()
     
    }
    func getOutOfQuiz(points: Int) {
        let date = Date()
       let formater = DateFormatter()
    formater.dateFormat = "yyyy/MM/dd"
        let dateString = formater.string(from: date)
        print(dateString)
        defaults.set(dateString, forKey: Constant.disableOneDay)
        let d = defaults.set(dateString, forKey: Constant.disableOneDay)
        print(d)
        showPopUp(title: "Better luck next time", message: "You have won: \n" + String(totalcinecoin) + " Cinecoin", type: .getOutOfQuiz)
    }
    func showPopUp(title: String, message: String,type: QuizAction) {
        let alertController = DOAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.alertViewBgColor = UIColor.white
        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
        alertController.messageView.textAlignment = .left
        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
        alertController.titleTextColor = UIColor.red
        let okAction = DOAlertAction(title: "OK", style: .default, handler: { action in
            if type == .getOutOfQuiz {
                self.dismiss(animated: true) {
                    if self.totalcinecoin == 0 {
                        self.navigationController?.popViewController(animated: true)
                        return
                    } else {
                    self.loadWithReward(from: "reward", token: "cinequiz", amount: String(self.totalcinecoin), isfromAppReview: false, rating: "", type: .getOutOfQuiz)
                        self.navigationController?.popViewController(animated: true)
                    }
                  
                }
            } else if type == .nothing{
                print("dismiss")
            }else {
                self.collectionView.scrollToItem(at: [0,self.counter - 1], at: .centeredHorizontally, animated: true)
            }
            self.dismiss(animated: true, completion: nil)
        })
        // Add the action.
        alertController.addAction(okAction)
        // Show alert
        self.present(alertController, animated: true, completion: nil)
    }
}
extension QuestionVC {
    func getQuestions(){
        APIHandler.shared.getQuestions(success:{
            (status, movieSliderArr) in
            self.questionsData = movieSliderArr
            self.collectionView.reloadData()
        }, failure: {(failure ) in
            
            
            
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                    
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in

                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                
            }
            
            
            
            
        })
    }
    func loadWithReward(from: String,token: String,amount: String,isfromAppReview: Bool,rating: String, type: QuizAction) {
        APIHandler.shared.loadWalletW(from: from, token: token, amount: amount) { [weak self] (votingPrice) in
                guard let strongSelf = self else { return }
            print(type)
            if votingPrice.first?.code == 200 {
                if type == .getOutOfQuiz {
                    let date = Date()
                   let formater = DateFormatter()
                formater.dateFormat = "yyyy/MM/dd"
                    let dateString = formater.string(from: date)
                    print(dateString)
                    strongSelf.defaults.set(dateString, forKey: Constant.disableOneDay)
                    let d = strongSelf.defaults.set(dateString, forKey: Constant.disableOneDay)
                    print(d)
                }
               
                print(votingPrice)
                strongSelf.showPopUp(title: "Congratulations!", message: votingPrice.first?.message ?? "", type: type)
            }
            
            } failure: { (msg) in
                self.showPopUp(title: "Failure", message: msg, type: .nothing)
            } error: { (err) in
                print(err.localizedDescription)
            }
        
    }
}
extension QuestionVC: GADBannerViewDelegate {
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("bannerViewDidReceiveAd")
    }

    private func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
      print("bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    func adViewDidRecordImpression(_ bannerView: GADBannerView) {
      print("bannerViewDidRecordImpression")
    }

    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillPresentScreen")
    }

    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillDIsmissScreen")
    }

    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewDidDismissScreen")
    }
}

extension Array {
    mutating func remove(elementsAtIndices indicesToRemove: [Int]) -> [Element] {
        let removedElements = indicesToRemove.map { self[$0] }
        for indexToRemove in indicesToRemove.sorted(by: >) {
            remove(at: indexToRemove)
        }
        return removedElements
    }
}
