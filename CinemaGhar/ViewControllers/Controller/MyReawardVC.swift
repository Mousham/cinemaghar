//
//  MyReawardVC.swift
//  CinemaGhar
//
//  Created by Midas on 12/06/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import JKSteppedProgressBar
import SwiftConfettiView
import KDCircularProgress
import GoogleMobileAds
import MKMagneticProgress
import EzPopup
enum RewardEarnType {
    case dailyLogin
    case appreview
    case referEar
    case ansewrwin
}
struct RewardModel {
    var header = [HeaderModel]()
   
}
struct HeaderModel {
    var cellsData = [cellsModel]()
    var headerName = String()
    var type: RewardEarnType?
   
}
struct cellsModel {
    var image: String?
    var title: String?
    var headerName: String
    var type: RewardEarnType?
    var rewardPoints: String?
}

class MyReawardVC: UIViewController {
   
    @IBOutlet weak var progressPointLbl: UILabel!
    @IBOutlet weak var pointsLbl: UILabel!
    @IBOutlet weak var progressBar: MKMagneticProgress!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var naView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
   
    @IBOutlet weak var claimBtn: AnimatedButton!
    @IBOutlet weak var progressBackView: UIView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var coinsBackView: UIView!
    private var rewardedAd: GADRewardedAd?
    var rewardData = [Bool]()
    var datasource = RewardModel()
    var isRewardEarned = false
    var userRating: String?
    var rewardChallengeType: RewardEarnType?
    var inset: UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 30, right: 30)
    }
   
//    func configureTitleProgressBar() {
//        progressbar.insets = inset
//        progressbar.titles = ["✓", "✓", "✓",]
//
//    }
    let confettiView = SwiftConfettiView()
    var userPoints = 0.0
    let defaults = UserDefaults.standard
    var rating = 4.5
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        progressBarSetup()
       loadRewardedAd()
        getDataForReward()
        uisetup()
        addingImageToUiview()
        getProfile()
        userPoints = defaults.double(forKey: Constant.userProgress)
        //userPoints = 0.98
        print(userPoints)
        progressPointLbl.text = String(Int(userPoints * 100))
        DispatchQueue.main.async {
//            self.progressBar.setProgress(progress: CGFloat(self.userPoints), animated: true)
            if self.userPoints == 0 {
                self.progressBar.setProgress(progress: CGFloat(0.0), animated: true)
            } else {
              
                self.progressBar.setProgress(progress: self.userPoints, animated: true)
            }
          
        }
        
        //loginChallengeApi()
       
       
//        configureTitleProgressBar()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    func addingImageToUiview() {
        let imageName = "iconCoinBackimage"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        imageView.contentMode = .scaleAspectFill
        imageView.frame = CGRect(x: 0, y: 0, width: (SCREEN.WIDTH - 32), height: 87)
       
        self.coinsBackView.addSubview(imageView)
        //Imageview on Top of View
        self.view.bringSubview(toFront: imageView)
        coinsBackView.backgroundColor = AppColors.yellowColor
        coinsBackView.layer.cornerRadius = 8
    }
    func uisetup() {
        naView.backgroundColor = AppColors.darkbackgound
        tableView.register(UINib(nibName: "GoalsTVC", bundle: nil), forCellReuseIdentifier: "GoalsTVC")
        tableView.register(UINib(nibName: "RewardHeader", bundle: nil), forCellReuseIdentifier: "RewardHeader")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableHeaderView = headerView
        tableView.reloadData()
        tableView.backgroundColor = .clear
        
        collectionView.register(UINib(nibName: "RewardCVC", bundle: nil), forCellWithReuseIdentifier: "RewardCVC")
        collectionView.register(UINib(nibName: "FirstRewardCVC", bundle: nil), forCellWithReuseIdentifier: "FirstRewardCVC")
        collectionView.register(UINib(nibName: "LastRewardCVC", bundle: nil), forCellWithReuseIdentifier: "LastRewardCVC")
//        collectionView.dataSource = self
//        collectionView.delegate = self
        collectionView.reloadData()
        collectionView.backgroundColor = .clear
        
        progressBackView.backgroundColor = AppColors.darkbackgound
        progressBackView.layer.cornerRadius = 8
        claimBtn.backgroundColor = AppColors.yellowColor
        claimBtn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        
//        progressbar.currentTab = 0
    
        
    }
    func progressBarSetup() {
        progressBar.setProgress(progress: CGFloat(userPoints), animated: false)
//
//        progressBar.progressShapeColor = AppColors.yellowColor
//
//
//
//        progressBar.backgroundShapeColor = AppColors.darkbackgound
//
//
//
//
//        progressBar.titleColor = .clear
//
//
//
//
//        progressBar.percentColor = .clear
           
        
    }
    @IBAction func backtap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func confettiSetup() {
        confettiView.frame = self.view.frame
        self.view.addSubview(confettiView)
        confettiView.type = .confetti
        confettiView.intensity = 1
      //  confettiView.colors = [UIColor.redColor(), UIColor.greenColor(), UIColor.blueColor()]
       // claimBtn.addTarget(self, action: #selector(tapped), for: .touchUpInside)
        
    }
   
    @objc func tapped() {
        loadRewardedAd()
        show()
    }
   
    func loadRewardedAd() {
       let request = GADRequest()
        GADRewardedAd.load(withAdUnitID: Constant.googleRewardKey,
                          request: request,
                          completionHandler: { [self] ad, error in
         if let error = error {
           print("Failed to load rewarded ad with error: \(error.localizedDescription)")
           return
         }
         rewardedAd = ad
         print("Rewarded ad loaded.")
        rewardedAd?.fullScreenContentDelegate = self
       }
       )
     }
    
    func show() {
       
      if let ad = rewardedAd {
        ad.present(fromRootViewController: self) {
          let reward = ad.adReward
          print("Reward received with currency \(reward.amount), amount \(reward.amount.doubleValue)")
            self.isRewardEarned = true
            self.userPoints = self.userPoints + 0.02
            self.progressPointLbl.text = String(Int(self.userPoints * 100))
            self.defaults.set(self.userPoints, forKey: Constant.userProgress)
            DispatchQueue.main.async {
                self.progressBar.setProgress(progress: CGFloat(self.userPoints), animated: true)
            }
          // TODO: Reward the user.
            if self.userPoints == 1.0 {
                self.loadWithReward(from: "reward", token: "rewardchallenge", amount: "100", isfromAppReview: false, rating: "", claim100Point: true)
                self.userPoints = 0.0
                self.progressPointLbl.text = "0"
                self.progressBar.setProgress(progress: CGFloat(self.userPoints), animated: true)
                self.defaults.set(0, forKey: Constant.userProgress)
            } else {
                self.showPopup(title:
                                "Sucess!", message: "You have successfully earned 2 points.")
                return
               // self.loadWithReward(from: "reward", token: "rewardchallenge", amount: "2", isfromAppReview: false, rating: "", claim100Point: false)
            }
        }
      } else {
        print("Ad wasn't ready")
      }
    }
   
    func getDataForReward() {
        rewardData = [true,true,true,true,false,false,false]
        datasource = RewardModel(header: [HeaderModel(cellsData: [cellsModel(image: "iconDailyLogin", title: "Daily Login Bonus", headerName: "Goals",type: .dailyLogin,rewardPoints: "2"),cellsModel(image: "iconAppReview", title: "Write App Review", headerName: "",type: .appreview,rewardPoints: "5"),cellsModel(image: "iconReferEar", title: "Refer & Earn", headerName: "",type: .referEar,rewardPoints: "5")], headerName: "Daily"),HeaderModel(cellsData: [cellsModel(image: "iconQuiz", title: "Answer & Win", headerName: "",type:
                                                                                                                                                                                                                                                                                                                                                                                                                    .ansewrwin,rewardPoints: "100")], headerName: "Daily Quiz",type: .ansewrwin)])
    }
    @IBAction func btnTap(_ sender: Any) {
        loadRewardedAd()
        show()
//
//        let vc = QuestionVC.instantiate(fromAppStoryboard: .reward)
//        self.navigationController?.pushViewController(vc, animated: true)
//        confettiSetup()
//         confettiView.startConfetti()
    }
    //MARK: - SHOW REVIEW
        @objc func showPopupForReview() {
            let customAlertVC = ReviewVC.instantiate()
            let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: SCREEN.WIDTH - 100)
            customAlertVC.delegate = self
            popupVC.cornerRadius = 8
           present(popupVC, animated: true, completion: nil)
        }
    func showAppStoreReview(showAppstore: Bool) {
        let customAlertVC = AppstoreReviewVC.instantiate()
        customAlertVC.showAppStore = showAppstore
       customAlertVC.delegate = self
       let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: SCREEN.WIDTH - 100, popupHeight: 220)
       popupVC.cornerRadius = 8
        self.present(popupVC, animated: true, completion: nil)
    }
    
  
    
   
}
//MARK: - TABLE DATASOURCE
extension MyReawardVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return datasource.header.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.header[section].cellsData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GoalsTVC", for: indexPath) as! GoalsTVC
        print(indexPath.section,indexPath.row)
        cell.selectionStyle = .none
        print(datasource.header[indexPath.section].cellsData.count)
        cell.rewardChallengeType = datasource.header[indexPath.section].cellsData[indexPath.row].type
        cell.backView.backgroundColor = AppColors.darkbackgound
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        cell.iconimage.image = UIImage(named: datasource.header[indexPath.section].cellsData[indexPath.row].image ?? "")
        if indexPath.row == datasource.header[indexPath.section].cellsData.count - 1 {
            cell.backView.roundCorners([.bottomLeft,.bottomRight], radius: 8)
        }
        cell.pointsLbl.text = datasource.header[indexPath.section].cellsData[indexPath.row].rewardPoints
        cell.titleLbl.text = datasource.header[indexPath.section].cellsData[indexPath.row].title
        cell.delegate = self
        return cell
    }
    
    
    
}
//MARK: - TABLE DELEGATE
extension MyReawardVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected")
        if datasource.header[indexPath.section].cellsData[indexPath.row].title == "Write App Review" {
            showPopupForReview()
        }
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREEN.WIDTH, height: 44))
        headerView.backgroundColor = .clear
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "RewardHeader") as! RewardHeader
        headerCell.frame = headerView.bounds
        headerCell.headerTitle.text = datasource.header[section].headerName
        headerCell.backView.backgroundColor = AppColors.darkbackgound
        headerView.addSubview(headerCell)
       // headerCell.frame = CGRect(x: 0, y: 0, width: SCREEN.WIDTH, height: 44)
        return headerView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == datasource.header[indexPath.section].cellsData.count - 1 {
            return 65
        } else {
            return 63
        }
      
    }
}
//MARK: - COLLECTION DATASOURCE
extension MyReawardVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        return rewardData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print(indexPath.row,rewardData.count - 1)
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FirstRewardCVC", for: indexPath) as! FirstRewardCVC
            if rewardData[indexPath.row] == true {
                cell.pointBtn.layer.borderWidth  = 1.5
                cell.pointBtn.layer.borderColor = AppColors.lightGray.cgColor
                cell.pointBtn.setTitle("+10", for: .normal)
                cell.pointBtn.backgroundColor = AppColors.darkBrinjalColor
                cell.pointBtn.layer.borderColor = AppColors.lightGray.cgColor
                cell.iconTick.tintColor = AppColors.earnedTickColor
            } else {
                cell.pointBtn.layer.borderWidth  = 0
                cell.pointBtn.setBackgroundImage(UIImage(named: "iconEarnedpoint"), for: .normal)
                cell.iconTick.tintColor = AppColors.lightGray
            }
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            cell.secondLine.backgroundColor = AppColors.lightGray
            return cell
           
        }  else if indexPath.row == rewardData.count - 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LastRewardCVC", for: indexPath) as! LastRewardCVC
            
            if rewardData[indexPath.row] == true {
                cell.pointBtn.layer.borderWidth  = 1.5
                cell.pointBtn.layer.borderColor = AppColors.lightGray.cgColor
                cell.pointBtn.setTitle("+10", for: .normal)
                cell.pointBtn.backgroundColor = AppColors.darkBrinjalColor
                cell.pointBtn.layer.borderColor = AppColors.lightGray.cgColor
                cell.iconTick.tintColor = AppColors.earnedTickColor
            } else {
                cell.pointBtn.layer.borderWidth  = 0
                cell.pointBtn.setBackgroundImage(UIImage(named: "iconEarnedpoint"), for: .normal)
                cell.iconTick.tintColor = AppColors.lightGray
            }
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            cell.firstLine.backgroundColor = AppColors.lightGray
            return cell
           
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RewardCVC", for: indexPath) as! RewardCVC
            
            if rewardData[indexPath.row] == true {
                cell.pointBtn.layer.borderWidth = 1.5
                cell.pointBtn.layer.borderColor = AppColors.lightGray.cgColor
                cell.pointBtn.setTitle("+10", for: .normal)
                cell.pointBtn.backgroundColor = AppColors.darkBrinjalColor
                cell.pointBtn.layer.borderColor = AppColors.lightGray.cgColor
                cell.iconTick.tintColor = AppColors.earnedTickColor
               
            } else {
                cell.pointBtn.layer.borderWidth  = 0
                cell.pointBtn.setBackgroundImage(UIImage(named: "iconEarnedpoint"), for: .normal)
                cell.iconTick.tintColor = AppColors.lightGray
            }
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            cell.secondLine.backgroundColor = AppColors.lightGray
            cell.firstLine.backgroundColor = AppColors.lightGray
            return cell
          
        }
       
       
    }
    
    
}
//MARK: - COLLECTION DELEGATE
extension MyReawardVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width / 7, height: 69)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
extension MyReawardVC {
    //MARK: - LOAD WITH REWARD
    func loadWithReward(from: String,token: String,amount: String,isfromAppReview: Bool,rating: String, claim100Point: Bool) {
        APIHandler.shared.loadWalletW(from: from, token: token, amount: amount) { [weak self] (votingPrice) in
                guard let strongSelf = self else { return }
                print(votingPrice)
            print(rating)
            if isfromAppReview == true {
                if Double((rating) ) ?? 0 >= 4 {
                    strongSelf.okBtnTap(showappstore: true)
                } else {
                    strongSelf.showAppStoreReview(showAppstore: false)
                }
            } else if claim100Point == true {
                strongSelf.showPopup(title: "Success", message: "You have successfully loaded the wallet.")
            } else {
                strongSelf.showPopup(title: "Success", message: "You have successfully loaded the wallet.")
            }
            strongSelf.getProfile()
            //strongSelf.showAlert()
            } failure: { (msg) in
                self.showPopup(title: "Failure", message: msg)
            } error: { (err) in
                print(err.localizedDescription)
            }
        
    }
    func showPopup(title: String,message: String) {
        DispatchQueue.main.async {
        let alertDetailController = DOAlertController(title: title, message: message, preferredStyle: .alert)
        alertDetailController.alertViewBgColor = UIColor.white
        alertDetailController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
        alertDetailController.messageView.textAlignment = .left
        alertDetailController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
        alertDetailController.titleTextColor = UIColor.red
        let okAction = DOAlertAction(title: "OK", style: .default, handler: { action in
           // self.getProfile()
            print("sawsank: OK Pressed")
            if self.isRewardEarned == true {
                
                UIApplication.shared.windows.last?.rootViewController?.dismiss(animated: false, completion: nil)
                self.isRewardEarned = false
            }else {
                self.dismiss(animated: true, completion: nil)
            }
           // self.dismiss(animated: true, completion: nil)
          
          
        })
        // Add the action.
        alertDetailController.addAction(okAction)
        // Show alert
            if self.isRewardEarned == true {
//                UIApplication.shared.keyWindow?.rootViewController?.present(alertDetailController, animated: true, completion: {
//
//                })
                UIApplication.shared.windows.last?.rootViewController?.present(alertDetailController, animated: true, completion: nil)
              
               // UIApplication.shared.windows.first?.makeKeyAndVisible()
            } else {
                self.present(alertDetailController, animated: true, completion: nil)
            }
       
    }
    }
}
extension MyReawardVC {
    func loginChallengeApi() {
        
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "", presentingView: self.view)
        APIHandler.shared.loginChallenge() { [weak self] (votingPrice) in
            guard let strongSelf = self else { return }
            print(votingPrice)
          
            if votingPrice.code == 200 {
                APESuperHUD.removeHUD(animated: true, presentingView: strongSelf.view, completion: {
                    let alertController = DOAlertController(title: "Success!!!", message: "", preferredStyle: .alert)
                        alertController.alertViewBgColor = UIColor.white
                        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                        alertController.messageView.textAlignment = .left
                        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                        alertController.titleTextColor = UIColor.red
                        let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                            
                            strongSelf.dismiss(animated: true, completion: nil)
                            strongSelf.navigationController?.popViewController(animated: true)
                        })
                        // Add the action.
                        alertController.addAction(okAction)
                        // Show alert
                        strongSelf.present(alertController, animated: true, completion: nil)
                        
                        
                    
                    
                } )
            }
          
        } failure: { (msg) in
            print(msg)
        } error: { (err) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: err.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                
                
            } )
        }
       
        }
    func getProfile() {
        APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait...", presentingView: self.view)
        APIHandler.shared.getProfile(success: { (profile) in
            print("sawsank: Getting profile")
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                print("sawsank: Load Profile Removed HUD")
            })
            print(profile)
            self.pointsLbl.text = profile.walletBalance
//            self.profile = profile
//            self.userNameLabel.text = profile.name
//            self.walletAmountLabel.text = profile.walletBalance
//            self.cinemagharIDLabel.text = "CinemagharID: " + profile.phoneNumber
//            if(self.profile.image != ""){
//                self.setProfileImageToImageView(url: profile.image)
//            }

        }, failure: { (message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                   let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
                   alertController.alertViewBgColor = UIColor.white
                   alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                   alertController.messageView.textAlignment = .left
                   alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                   alertController.titleTextColor = UIColor.red
                   let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                       self.dismiss(animated: true, completion: nil)
                    self.dismissVC()
                   })
                   // Add the action.
                   alertController.addAction(okAction)
                   // Show alert
                   self.present(alertController, animated: true, completion: nil)
           } )
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
            } )
        }
    }
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
       
    }
    
    
//MARK: -CELLS DELEGATE
extension MyReawardVC: GoalsTVCDelegate {
    func rewardbtnTap(type: RewardEarnType) {
        switch type {
        case .dailyLogin:
            loadWithReward(from: "reward", token: "loginchallenge", amount: "2",isfromAppReview: false, rating: "", claim100Point: false)
        case .appreview:
            print("appreview")
            showPopupForReview()
        case .referEar:
            print("fer")
            showAlert(title: "Not available", message: "This content is not available for now.")
        case .ansewrwin:
            let date = Date()
           let formater = DateFormatter()
        formater.dateFormat = "yyyy/MM/dd"
            let dateString = formater.string(from: date)
            print(dateString)
            let currentDate = defaults.string(forKey: Constant.disableOneDay)
            print(currentDate,dateString)
            if currentDate == dateString {
                showPopup(title: "Sorry!!", message: "You have already played for today. Try again tomorrow.")
            } else {
                let vc = QuestionVC.instantiate(fromAppStoryboard: .reward)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
           // showAlert(title: "Not available", message: "This content is not available for now.")
           
        }
    }
}
extension MyReawardVC: AppStoreDelegate {
    func okBtnTap(showappstore: Bool) {
        if showappstore == true {
            if let url = URL(string: "itms-apps://apple.com/app/id1112415684?action=write-review") {
                UIApplication.shared.open(url)
            }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    

}
//MARK: - REWARD DELEGATE
extension MyReawardVC: ReviewDelegate {
    func continueTap(rating: String, reviewTxt: String) {
        self.userRating = rating
        print((Int(rating) ?? 0))
//        if (Int(rating) ?? 0) < 4 {
        if (rating as NSString).integerValue >= 4 {
            loadWithReward(from: "reward", token: "reviewapp", amount: "5",isfromAppReview: true, rating: rating, claim100Point: false)

        } else {
            let customAlertVC = FeedbackVC.instantiate()
            let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: SCREEN.WIDTH - 100)
            customAlertVC.delegate = self
            popupVC.cornerRadius = 8
           present(popupVC, animated: true, completion: nil)
        }
       
        
    }
}
//MARK: - FEEDBACK DELEGATE
extension MyReawardVC: FeedbackDelegate {
    func feedbackInfo(feedback: String) {
        loadWithReward(from: "reward", token: "reviewapp", amount: "5",isfromAppReview: false, rating: self.userRating ?? "", claim100Point: false)
    }
    
    
}
extension MyReawardVC: GADFullScreenContentDelegate {
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
       print("Ad did fail to present full screen content.")
     }

     /// Tells the delegate that the ad will present full screen content.
     func adWillPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
       print("Ad will present full screen content.")
        
     }

     /// Tells the delegate that the ad dismissed full screen content.
     func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
       print("Ad did dismiss full screen content.")
     }

}


