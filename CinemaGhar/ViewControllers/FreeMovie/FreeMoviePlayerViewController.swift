//
//  FreeMoviePlayerViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 5/24/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
//import GoogleCast
import GoogleMobileAds


class FreeMoviePlayerViewController: UIViewController {
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
//    private var sessionManager: GCKSessionManager!
    
    var interstitial: GADInterstitialAd!

    let appDelegate = (UIApplication.shared.delegate as? AppDelegate)

    @IBOutlet weak var overLayView: UIView!
    var youtubeURL: String?
     var statusBarState = false
    var mediaType: MediaType = .movie
    
    @IBOutlet weak var youtubePlayerView: YoutubePlayerView!
    
    var isAdDisplaaying = true
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
//        self.sessionManager = GCKCastContext.sharedInstance().sessionManager
      
        
    }
    

    
    override func viewDidLoad() {
        
        

        
//
//        interstitial = GADInterstitialAd(adUnitID: "ca-app-pub-4373633293802764/9489754391")
//
//        if self.mediaType == .series {
//        let request = GADRequest()
//        interstitial.load(request)
//        interstitial.delegate = self
//
//        }
        if self.mediaType == .series {
            let request = GADRequest()
                   GADInterstitialAd.load(withAdUnitID:"ca-app-pub-4373633293802764/9489754391",request: request,
                   completionHandler: { [self] ad, error in
                     if let error = error {
                        return
                        }
                        interstitial = ad
                        interstitial?.fullScreenContentDelegate = self
                       }
                   )
        }
       
        
        
        let playerVars: [String: Any] = [
            "controls": 1,
            "modestbranding": 1,
            "playsinline": 0,
            "rel": 0,
            "showinfo": 1,
            "autoplay": 1
        ]
        youtubePlayerView.delegate = self

        
        youtubePlayerView.loadWithVideoId(self.getYoutubeVideoID(youtubeUrl: self.youtubeURL!), with: playerVars)
        

//        let value = UIInterfaceOrientation.landscapeRight.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
//        GCKCastContext.sharedInstance().sessionManager.endSession()
        appDelegate?.isCastControlBarsEnabled = false
        
//        youtubePlayerView.delegate = self
        
        
        indicatorView.hidesWhenStopped = true
        
        var  rect = CGRect(x:15, y:30, width:30, height:30)
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
            rect = CGRect(x:20, y:40, width:30, height:30)
        }
        if UIDevice.current.userInterfaceIdiom == .pad {
              rect = CGRect(x:12, y:30, width:50, height:50)

        }
        
        let backButton = UIButton()
        backButton.layer.cornerRadius = rect.height/2
        backButton.backgroundColor = UIColor.white
        backButton.addShadow(offset: CGSize(width: 1, height: 1), color: .darkGray, radius: 2, opacity: 0.9)
        backButton.frame = rect
        backButton.setImage(UIImage(named: "icon_navBar_Back"), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(22,22,22,23)
        backButton.imageView?.tintColor = UIColor.black
        self.view.addSubview(backButton)
        backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
 
        NotificationCenter.default.addObserver(self, selector: #selector(self.doneButtonPressed), name: NSNotification.Name.UIWindowDidBecomeHidden, object: self.view.window)
        
        
        

    }
    
    @objc func doneButtonPressed (){
        if isAdDisplaaying == false{
        self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    

    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    
    
    func getYoutubeVideoID(youtubeUrl: String) -> String{
                
        if let  videoId = URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value{
            return videoId
        }
        else {
            return ""
        }
    }
    
    
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
   
    }
    
    
    
    


    
}

//extension FreeMoviePlayerViewController: GADInterstitialDelegate {
//
//    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
//        print("received")
//    }
//
//    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
//
////        player.pauseVideo()
//    }
//
//    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
//        print("closed")
//        isAdDisplaaying = false
//
//    }
//}
extension FreeMoviePlayerViewController: GADFullScreenContentDelegate {
    func adWillPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
      
    }
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
       print("Ad did fail to present full screen content.")
     }


     func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
       print("Ad did dismiss full screen content.")
         isAdDisplaaying = false
     }
}



extension FreeMoviePlayerViewController: YoutubePlayerViewDelegate {
    func playerViewDidBecomeReady(_ playerView: YoutubePlayerView) {
        print("Ready")
        playerView.play()
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
        indicatorView.stopAnimating()
    

    }
    
    func playerView(_ playerView: YoutubePlayerView, didChangedToState state: YoutubePlayerState) {
        print("Changed to state: \(state)")
    }
    
    func playerView(_ playerView: YoutubePlayerView, didChangeToQuality quality: YoutubePlaybackQuality) {
        print("Changed to quality: \(quality)")
    }
    
    func playerView(_ playerView: YoutubePlayerView, receivedError error: Error) {
        print("Error: \(error)")
    }
    
    func playerView(_ playerView: YoutubePlayerView, didPlayTime time: Float) {
        print("Play time: \(time)")
    }
    
    func playerViewPreferredInitialLoadingView(_ playerView: YoutubePlayerView) -> UIView? {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }
}

