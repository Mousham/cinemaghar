//
//  CineGoldView.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 9/24/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import SwiftUI

 @available(iOS 13.0.0, *)
struct CineGoldView: View {
    var body: some View {
        ZStack{
            Rectangle()
            .foregroundColor(Color.init(red: 37, green: 43, blue: 56))
                .edgesIgnoringSafeArea(.all)
        }
        
    }
}

 @available(iOS 13.0.0, *)
struct CineGoldView_Previews: PreviewProvider {
    static var previews: some View {
        CineGoldView()
    }
}
