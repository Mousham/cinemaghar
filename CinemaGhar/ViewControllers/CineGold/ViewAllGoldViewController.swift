//
//  ViewAllGoldViewController.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 10/13/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import Foundation

class ViewAllGoldViewController: UIViewController {
    private let spacing:CGFloat = 10
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var goldCollectionView: UICollectionView!
    
    var subscriptionArray = [Movie]() {
        didSet {
            self.goldCollectionView.reloadData()
        }
    }
    var nextPageId = 1
    var trendingArray = [Movie]() {
        didSet {
            self.goldCollectionView.reloadData()
        }
    }
    var type: HomeCategoryType?
    
    
    override func viewDidLoad() {
//        self.getSubscriptionData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("ReloadData"), object: nil)
        setupViews()
        
    }
    
    func setupViews(){
        //Back Button
        backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        //Collection View
        let nibName = UINib(nibName: "ViewAllGoldCollectionViewCell", bundle:nil)
        goldCollectionView.register(nibName, forCellWithReuseIdentifier: "ViewAllGoldCollectionViewCell")
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.minimumLineSpacing = spacing
        layout.minimumInteritemSpacing = spacing
        self.goldCollectionView.collectionViewLayout = layout
        self.goldCollectionView.delegate = self
        self.goldCollectionView.dataSource = self
        
        //For pagination
        self.goldCollectionView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        
        self.goldCollectionView.infiniteScrollIndicatorMargin = 50
        
       
        if type == .movies {
           // getSubscriptionData()
            self.goldCollectionView?.addInfiniteScroll {  (allMoviesCollectionView) -> Void in
                self.loadMoreData(pageId: String(self.nextPageId))
            }
             self.loadMoreData(pageId: String(self.nextPageId))
        } else if type == .shorts {
            getTrendingData()
        }
      
        
    }
    
    func loadMoreData(pageId: String){
            APIHandler.shared.getSubscriptionData(pageId: nextPageId, success: { (status, movies, nextPageLink) in
                print(status,nextPageLink)
                if status{
                    if nextPageLink != "null" {
                        print(nextPageLink)
                        self.nextPageId+=1
                        let photoCount = self.subscriptionArray.count

                        let (start, end) = (photoCount, movies.count + photoCount)
                        let indexPaths = (start..<end).map { return IndexPath(row: $0, section: 0) }
                        
                        self.subscriptionArray.append(contentsOf: movies)
                        self.goldCollectionView.performBatchUpdates({ () -> Void in
                                self.goldCollectionView.insertItems(at: indexPaths)
                            self.goldCollectionView.finishInfiniteScroll()
                        }, completion: { (finished) -> Void in
                            print("sawsank: finish infinite scroll")
                            // finish infinite scroll animations
                            self.goldCollectionView.finishInfiniteScroll()
                        });
                        
                    } else {
                        self.goldCollectionView.finishInfiniteScroll()
                    }
                    
                }else{
                    self.goldCollectionView.finishInfiniteScroll()
                }
                
            }, failure: {(failure) in
                print("sawsank: Failure")
                print("sawsank: \(failure.localizedDescription)")
            })
        
    }
    func getSubscriptionData(){
        APIHandler.shared.getSubscriptionData(success:{
            (status, subscriptionArr, nextPageId) in
            self.subscriptionArray = subscriptionArr
            print(self.subscriptionArray)
            for item in self.subscriptionArray {
                print(item.coverImage)
            }
        }, failure: {(failure ) in
            if failure._code == NSURLErrorNotConnectedToInternet {
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                }))
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                
            }

            
        })
   
        
    }
    //MARK: - TRENDING DATA API
    func getTrendingData(){
        APIHandler.shared.getShortMovies(success:{
            (status, shortArr) in
            
            self.trendingArray = shortArr
            print("Getting trending data...")
            print(self.trendingArray.count)
          
           
        }, failure: {(failure ) in
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                    
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in

                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
            }
        })
    }

    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension ViewAllGoldViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if type == .movies {
            return self.subscriptionArray.count
        } else if type == .shorts {
            return self.trendingArray.count
        } else {
            return 0
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewAllGoldCollectionViewCell", for: indexPath) as! ViewAllGoldCollectionViewCell
        if type == .movies {
            cell.setMovieInfos(movie: self.subscriptionArray[indexPath.row])
        } else if type == .shorts {
            cell.setMovieInfos(movie: self.trendingArray[indexPath.row])
        }
      
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow:CGFloat = 3
        let spacingBetweenCells:CGFloat = 4
        
        var totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        print("sawsank: \(UIDevice.current.screenType)")

        
        let width = ((collectionView.bounds.width - totalSpacing)/numberOfItemsPerRow) - 4
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
            
            return CGSize.init(width: width, height: 205)
            
        }else if UIDevice.current.screenType == .iPhones_6_6s_7_8 {
            return CGSize.init(width: width, height: 205)
        }else if UIDevice.current.screenType == .unknown {
            if(UIDevice.current.userInterfaceIdiom == .phone){
                let width = ((collectionView.bounds.width - totalSpacing)/3) - 4
                let height = collectionView.bounds.height * 0.33
                return CGSize.init(width: width, height: height)
            }else{
                let width = ((collectionView.bounds.width - totalSpacing)/3) - 4
                let height = collectionView.bounds.height * 0.35
                return CGSize.init(width: width, height: height)
            }
            
        } else if UIDevice.current.userInterfaceIdiom == .pad {
            totalSpacing = (2 * self.spacing) + ((6 - 1) * spacingBetweenCells)
            let width = ((collectionView.bounds.width - totalSpacing)/4) - 4
            return CGSize.init(width: width, height: 225)
        }
         else {
            return CGSize.init(width: width, height: 161)
            
        }
        


    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(subscriptionArray[indexPath.row].isBought)
       // let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
        let movieDetailScene = NewMovieDetailsVC.instantiate(fromAppStoryboard: .MovieDetail)
        movieDetailScene.movieDetail = subscriptionArray[indexPath.row]
        movieDetailScene.refreshCollectionViewDelegate = self
        self.navigationController?.pushViewController(movieDetailScene, animated: true)
    }
    
}

extension ViewAllGoldViewController: RefreshCollectionViews {
    func refreshCollectionViews() {
        self.goldCollectionView.reloadData()

    }
}

extension ViewAllGoldViewController {
    @objc func methodOfReceivedNotification(notification: Notification) {
        guard let userinfo = notification.userInfo else { return }
        for (index,item) in subscriptionArray.enumerated() {
            print(userinfo["movieid"])
            if item.id == userinfo["movieid"] as? Int{
                subscriptionArray[index].isBought = true
                subscriptionArray[index].paid = true
                
            }
        }
        
    }
}
