//
//  HomeVC.swift
//  CinemaGhar
//
//  Created by Midas on 21/07/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import FSPagerView
import PassKit
import GoogleMobileAds
import SwiftyJSON
import Alamofire
class HomeVC: UIViewController {
//    var newsImage = UIImage(named: "icons8-news")!
    var newsImage = UIImage(named: "iconMyAccount")!
    var myMoviesImage = UIImage(named: "iconTutorials")!
//    var myDownloadsImage = UIImage(named: "iconDownload1")!
    var myDownloadsImage = UIImage(named: "iconMyDownloads")!
    var settingsImage = UIImage(named: "icons8-settings")!
    var sessionManager = SessionManager()
    var tutorialLabel: UILabel!
    var circle: UIView!
    var arrowImageView: UIImageView!
    var tutorialImage = UIImage.animatedImage(with:  [UIImage(named: "video_tutorial_0")!, UIImage(named: "video_tutorial_1")!, UIImage(named: "video_tutorial_2")!, UIImage(named: "video_tutorial_3")!, UIImage(named: "video_tutorial_4")!, UIImage(named: "video_tutorial_3")!, UIImage(named: "video_tutorial_2")!, UIImage(named: "video_tutorial_1")!, UIImage(named: "video_tutorial_0")!], duration: 1)
    @IBOutlet weak var overLayView: UIView!
    @IBOutlet weak var googleAdView: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var buttonMenu: XXXRoundMenuButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var headerView: UIView!
    var categorSelected: HomeCategoryType? = .movies
    var hasTopNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
           // self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "HomeBannerCVC")
            self.pagerView.register(UINib(nibName:"HomeBannerCVC", bundle: Bundle.main), forCellWithReuseIdentifier: "cell")
        }
    }
    var votingData = [VotingModel]() {
        didSet {
            reloadDataShowAnimated()
//            tableView.reloadData()
        }
    }
    var movieSliderArray = [MovieSlider]() {
        didSet {
           
                self.pagerView.scrollToItem(at: 1, animated: true)
            
            self.pagerView.reloadData()
        }
    }
    var redirectToHome: Bool = false
    var subscriptionArray = [Movie]() {
        didSet {
          //  tableView.reloadData()
            reloadDataShowAnimated()
        }
    }
    var trendingArray = [Movie]() {
        didSet {
           // self.tableView.isHidden = false
            reloadDataShowAnimated()
           // self.tableView.reloadData()
        }
    }
    lazy var adBannerView: GADBannerView = {
        let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        adBannerView.adUnitID = Constant.googleBannerKey
        adBannerView.delegate = self
        adBannerView.rootViewController = self
        return adBannerView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
//        showNotification()
        loadUpApp()
        //for push notifcation handling
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlingPushNotificatoin(notification:)), name: Notification.Name("HandlingPushNotification"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("ReloadData"), object: nil)
        tableView.isHidden = true
        onViewDidLoad()
        getVotings()
        // Do any additional setup after loading the vie
        NotificationCenter.default.addObserver(self, selector: #selector(self.showProgress(isLoading:)), name: Notification.Name(rawValue: "IAPPaymentStatusLoading"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideProgress(notification:)), name: Notification.Name(rawValue: "IAPPaymentStatusNotLoading"), object: nil)
        tableSetup()
        pagerViewSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
       // tableView.reloadData()
       // self.reloadDataShowAnimated()
    }
    func loadUpApp(){
        NotificationCenter.default.addObserver(self, selector: #selector(SomeNotificationAct), name: Notification.Name(rawValue: "SomeNotification"), object: nil)
        registerFCMToken()
    }
    
    func viewSetup(){
//        switch UIDevice.current.userInterfaceIdiom {
//        case .phone:
//            sliderView.height(185)
//        case .pad:
//            sliderView.height(285)
//        case .unspecified:
//            break
//        case .tv:
//            break
//        case .carPlay:
//            break
//        case .mac:
//            break
//        }
    
        changeImages()
        topBarSetup()
        
    }
    func onViewDidLoad(){
        checkIfAppIsBlocked()
        getSettings()
        if(sessionManager.isLoggedIn()){
            getProfile()
        }
        IAPService.shared.getProducts()
        viewSetup()
        self.getSliderData()
        self.getSubscriptionData()
        self.getTrendingData()
        let adRequest = GADRequest()
        adBannerView.load(GADRequest())
        googleAdView.addSubview(adBannerView)
        googleAdView.backgroundColor = .clear
        
    }
    func pagerViewSetup() {
        pagerView.dataSource = self
        pagerView.delegate = self
        //pagerView.itemSize = CGSize(width: 229, height: 315)
        let transform = CGAffineTransform(scaleX: 0.6, y: 0.75)
        self.pagerView.itemSize = CGSize(width: 300, height: 390).applying(transform)
        self.pagerView.decelerationDistance = FSPagerView.automaticDistance
        pagerView.automaticSlidingInterval = 5
        pagerView.transformer = FSPagerViewTransformer(type: .linear)
    
        
        pagerView.reloadData()
    }
    func topBarSetup(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        overLayView.addGestureRecognizer(tap)
        
        self.view.addSubview(buttonMenu)
        buttonMenu.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
        buttonMenu.centerButtonSize = CGSize(width: 34, height: 34)
        buttonMenu.centerIconType = .customImage
        
        buttonMenu.centerIcon = UIImage(named: "iconMenu")
        buttonMenu.tintColor = UIColor.white
        buttonMenu.jumpOutButtonOnebyOne = false
        buttonMenu.mainColor = UIColor.clear
       // buttonMenu.centerIconType = .userDraw
        buttonMenu.translatesAutoresizingMaskIntoConstraints = false
        
        if(UIDevice.current.screenType == .unknown) || hasTopNotch == true{
            if(UIDevice.current.userInterfaceIdiom == .phone){
                buttonMenu.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: -120.0).isActive = true
                buttonMenu.topAnchor.constraint(equalTo: self.view.topAnchor, constant: -85).isActive = true
              
            }else{
                buttonMenu.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: -40.0).isActive = true
                buttonMenu.topAnchor.constraint(equalTo: self.view.topAnchor, constant: -90).isActive = true
            }
        }else{
            buttonMenu.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: -115.0).isActive = true
            buttonMenu.topAnchor.constraint(equalTo: self.view.topAnchor, constant: -108).isActive = true
        }
       
       
        buttonMenu.drawCenterButtonIconBlock = {(_ rect: CGRect, _ state: UIControlState) -> Void in
            
            if state == .normal {
                let rectanglePath = UIBezierPath(rect: CGRect(x: (rect.size.width - 15) / 2, y: rect.size.height / 2 - 7, width: 15, height: 2))
                UIColor.white.setFill()
                rectanglePath.fill()
                let rectangle2Path = UIBezierPath(rect: CGRect(x: (rect.size.width - 15) / 2, y: rect.size.height / 2, width: 20, height: 2))
                UIColor.white.setFill()
                rectangle2Path.fill()
                let rectangle3Path = UIBezierPath(rect: CGRect(x: (rect.size.width - 15) / 2, y: rect.size.height / 2 + 7, width: 15, height: 2))
                UIColor.white.setFill()
                rectangle3Path.fill()
            }
        }

        NotificationCenter.default.addObserver(self, selector: #selector(self.checkMenuStatus(notification:)), name: Notification.Name(rawValue: "menuStatus"), object: nil)
        
        
       
        buttonMenu.load(withIcons: [newsImage, myDownloadsImage,  tutorialImage!, settingsImage], startDegree: Float(.pi/1.94), layoutDegree: Float(-.pi/1.9), withIconDescription: [
            "My Account",
            "Downloads",
            "Tutorials",
            "Settings"
            ])
       
        buttonMenu.buttonClickBlock =  {(idx:NSInteger)-> Void in
            self.overLayView.isHidden = true
            self.buttonMenu.mainColor = UIColor.clear
            
            if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax || UIDevice.current.screenType == .unknown || self.hasTopNotch == true  {
                self.buttonMenu.topAnchor.constraint(equalTo: self.view.topAnchor, constant: -85).isActive = true
            
            }
            else {
                self.buttonMenu.topAnchor.constraint(equalTo: self.view.topAnchor, constant: -90).isActive = true
            }
            
            switch idx {
            case 0:
                if (self.sessionManager.isLoggedIn()){
                    self.buttonMenu.isOpened = false
                    //let accountVC = MyAccountViewController.instantiate(fromAppStoryboard: .MyAccount)
                    let accountVC = MyAccountVC.instantiate(fromAppStoryboard: .MyAccount)
                    self.navigationController?.pushViewController(accountVC, animated: true)

                }
                else{
                    let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                    self.navigationController?.pushViewController(loginVC, animated: true)
                }
                break
            case 1:
                
                if (self.sessionManager.isLoggedIn()){
//                    let offlineVC = OfflineDownloadsViewController.instantiate(fromAppStoryboard: .OfflineDownloads)
//                    self.navigationController?.pushViewController(offlineVC, animated: true )
//                    let theatreVC = TheatreViewController.instantiate(fromAppStoryboard: .Theatre)
//                    self.navigationController?.pushViewController(theatreVC, animated: true)
                    let vc = VideoListController.instantiate(fromAppStoryboard: .MovieDetail)
                    vc.dataProvider = VideoListDataProvider()
                    vc.showAllDownloads = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                    self.navigationController?.pushViewController(loginVC, animated: true)
                }
                break
            case 2:
                self.sessionManager.setIsTutorialFinished(status: true)
                self.changeImages()
                let tutorialsVC = TutorialsViewController.instantiate(fromAppStoryboard: .Tutorials)
                self.navigationController?.pushViewController(tutorialsVC, animated: true)
                
                break
                
                
            case 3:
               
               let settingsVC = SettingVC.instantiate(fromAppStoryboard: .Settings)
                self.navigationController?.pushViewController(settingsVC, animated: true)
               
                
                
                break
                
                
                
            default:
                print("no index")
            }
        };
        
     
//        let logoView = UIImageView()
//        logoView.frame = CGRect(x: 0, y: 0, width: 185, height: 24)
//        logoView.image = UIImage(named: "logo")
//        logoView.backgroundColor = .clear
//        self.view.addSubview(logoView)
//        logoView.translatesAutoresizingMaskIntoConstraints = false
//        logoView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0.0).isActive = true
//        logoView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 49).isActive = true
//        logoView.heightAnchor.constraint(equalToConstant: 20).isActive = true
//        //logoView.widthAnchor.constraint(equalToConstant: 140).isActive = true
//        logoView.leftAnchor.constraint(equalTo: newsButton.rightAnchor,constant: 8).isActive = true
//        logoView.leftAnchor.constraint(equalTo: se.rightAnchor,constant: 8).isActive = true
//        logoView.widthAnchor.constraint(equalToConstant: SCREEN.WIDTH - 225).isActive = true
       
        
     
      
       
        
       
       
        
      
        self.view.bringSubview(toFront: overLayView)
        self.view.bringSubview(toFront: buttonMenu)
    }
    @IBAction func newTap(_ sender: Any) {
        let newsVC = NewsFeedViewController.instantiate(fromAppStoryboard: .NewsFeed)
        self.navigationController?.pushViewController(newsVC, animated: true)
    }
    @IBAction func searchTap(_ sender: Any) {
        let searchVC = SearchViewController.instantiate(fromAppStoryboard: .Search)
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        
        self.overLayView.isHidden = true
        self.buttonMenu.isOpened = false
        self.buttonMenu.setSelected(false)
        self.buttonMenu.isSelected = false
        self.buttonMenu.mainColor = UIColor.clear
        self.buttonMenu.buttonClick(self.buttonMenu)
        changeImages()
        self.view.layoutIfNeeded()
    }
    @objc func checkMenuStatus(notification: NSNotification) {
        if let menuStatus = notification.userInfo?["isMenuOpen"] as? Bool {
            
            if menuStatus {
                overLayView.isHidden = false
                
                }
                self.view.layoutIfNeeded()
                
            }
        else {
          
            self.view.layoutIfNeeded()
            overLayView.isHidden = true
        }
        
    }
    func changeImages(){
        
        print("sawsank: Changing Images")
        print("sawsank: \(sessionManager.isTutorialFinished())")
        if(!sessionManager.isTutorialFinished()){
            print("sawsank: Changing Images with alpha")
            newsImage = newsImage.alpha(0.2)
            myMoviesImage = myMoviesImage.alpha(0.2)
            myDownloadsImage = myDownloadsImage.alpha(0.2)
            settingsImage = settingsImage.alpha(0.2)
            overlayTutorialSetup()
            self.buttonMenu.isSelected = true
            self.overLayView.isHidden = false
            sessionManager.setIsTutorialFinished(status: true)


        }else{
            print("sawsank: Changing Images without alpha")
//            tutorialImage = UIImage(named: "video_tutorial")
            tutorialImage = UIImage(named: "iconTutorials")
            newsImage = UIImage(named: "iconMyAccount")!
            myMoviesImage = UIImage(named: "theatre")!
            myDownloadsImage = UIImage(named: "iconMyDownloads")!
            settingsImage = UIImage(named: "iconSetting")!
            if(tutorialLabel != nil && circle != nil){
                tutorialLabel.isHidden = true
                circle.isHidden = true
                arrowImageView.isHidden = true
            }
            buttonMenu.load(withIcons: [newsImage, myDownloadsImage,  tutorialImage!, settingsImage], startDegree: Float(.pi/1.94), layoutDegree: Float(-.pi/1.9), withIconDescription: [
                "My Account",
                "Downloads",
                "Tutorials",
                "Settings"
                ])
            overLayView.isHidden = true
        }
    }
    func overlayTutorialSetup(){
        tutorialLabel = UILabel()
        tutorialLabel.text = "Here are tutorials in case you get confused."
        tutorialLabel.textColor = UIColor.white
        tutorialLabel.adjustsFontSizeToFitWidth = true
        tutorialLabel.numberOfLines = 2
        tutorialLabel.translatesAutoresizingMaskIntoConstraints = false
        
        circle = UIView()
        circle.frame = buttonMenu.frame
        circle.backgroundColor = UIColor.red
        circle.layer.cornerRadius = 150
        circle.translatesAutoresizingMaskIntoConstraints = false
        
        arrowImageView = UIImageView()
        arrowImageView.image = UIImage(named: "arrow")
        arrowImageView.translatesAutoresizingMaskIntoConstraints = false
        
        
        if(!sessionManager.isTutorialFinished()){
       
            overLayView.addSubview(circle)
            overLayView.addSubview(tutorialLabel)
            overLayView.addSubview(arrowImageView)
            
            tutorialLabel.centerXAnchor.constraint(equalTo: overLayView.centerXAnchor).isActive = true
            if(UIDevice.current.userInterfaceIdiom == .pad){
                tutorialLabel.centerYAnchor.constraint(equalTo: overLayView.centerYAnchor).isActive = true
            }else{
                tutorialLabel.centerYAnchor.constraint(equalTo: overLayView.centerYAnchor, constant: 20).isActive = true
            }
            
            tutorialLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
            tutorialLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
            
            circle.centerXAnchor.constraint(equalTo: self.buttonMenu.centerXAnchor).isActive = true
            circle.centerYAnchor.constraint(equalTo: self.buttonMenu.centerYAnchor).isActive = true
            circle.widthAnchor.constraint(equalTo: self.buttonMenu.widthAnchor).isActive = true
            circle.heightAnchor.constraint(equalTo: self.buttonMenu.heightAnchor).isActive = true
            
            arrowImageView.topAnchor.constraint(equalTo: self.buttonMenu.bottomAnchor, constant: -20).isActive = true
            
            arrowImageView.leftAnchor.constraint(equalTo: self.buttonMenu.rightAnchor, constant: -30).isActive = true
            arrowImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
            arrowImageView.widthAnchor.constraint(equalToConstant: 73).isActive = true
            
            //Arrow animation
            DispatchQueue.main.async {
                UIImageView.animate(withDuration: 1, delay: 0.0, options: [UIViewAnimationOptions.autoreverse, UIViewAnimationOptions.repeat], animations: {
                    self.arrowImageView.transform = CGAffineTransform.identity.scaledBy(x: 1.25, y: 1.25)
                 })
            }
            
        }else{
            tutorialLabel.isHidden = true
            circle.isHidden = true
            arrowImageView.isHidden = true
        }
    }
    

    func tableSetup() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerView
        tableView.register(UINib(nibName: "HomeVoteTVC", bundle: nil), forCellReuseIdentifier: "HomeVoteTVC")
        tableView.register(UINib(nibName: "HomeCategoriesTVC", bundle: nil), forCellReuseIdentifier: "HomeCategoriesTVC")
        tableView.register(UINib(nibName: "HomeMovieListTVC", bundle: nil), forCellReuseIdentifier: "HomeMovieListTVC")
        //tableView.reloadData()
        tableView.isHidden = false
       // tableView.reloadData()
      //  reloadDataShowAnimated()
    }
    @objc func moviesTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
    }
    @objc func shortsTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
    }
    @objc func seriessTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
    }
    @objc func viewAlltap() {
       
            let viewAllGoldVC = ViewAllGoldViewController.instantiate(fromAppStoryboard: .ViewAllGold)
        viewAllGoldVC.type = self.categorSelected
            self.navigationController?.pushViewController(viewAllGoldVC, animated: true)
        
    }

    @IBAction func rewardTap(_ sender: Any) {
        if (self.sessionManager.isLoggedIn()){
            let vc = MyReawardVC.instantiate(fromAppStoryboard: .reward)
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
       
    }
}
//MARK: TABLE DDATASOURCE
extension HomeVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeVoteTVC", for: indexPath) as! HomeVoteTVC
            cell.backView.layer.masksToBounds = false
            cell.titleLbl.text = votingData.first?.title
            cell.descLbl.text = votingData.first?.description
            cell.coverImage.sd_setImage(with: URL(string: votingData.first?.coverImage ?? ""), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCategoriesTVC", for: indexPath) as! HomeCategoriesTVC
//            let movieTap = UITapGestureRecognizer(target: self, action: #selector(self.moviesTap(_:)))
//            let shortTap = UITapGestureRecognizer(target: self, action: #selector(self.moviesTap(_:)))
//            let seriesTap = UITapGestureRecognizer(target: self, action: #selector(self.seriessTap(_:)))
//            cell.moviesBackView.addGestureRecognizer(movieTap)
//            cell.shortsBackView.addGestureRecognizer(shortTap)
//            cell.seriesBackView.addGestureRecognizer(seriesTap)
            if self.categorSelected == .movies {
               // cell.changeBackgroundColor(bview: cell.moviesBackView)
                cell.moviesBackView.backgroundColor = AppColors.yellowColor
            } else if self.categorSelected == .shorts {
                cell.shortsBackView.backgroundColor = AppColors.yellowColor
               // cell.changeBackgroundColor(bview: cell.shortsBackView)
            } else if self.categorSelected == .series {
                cell.changeBackgroundColor(bview: cell.seriesBackView)
            }
//            cell.moviesBackView.backgroundColor = AppColors.newDark
//            cell.shortsBackView.backgroundColor = AppColors.newDark
//            cell.seriesBackView.backgroundColor = AppColors.newDark
            cell.delegate = self
            cell.moviesBackView.layer.masksToBounds = false
            cell.shortsBackView.layer.masksToBounds = false
            cell.seriesBackView.layer.masksToBounds = false
            return cell

        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeMovieListTVC", for: indexPath) as! HomeMovieListTVC
            print(cell.collectionview.contentSize.height)
            if self.categorSelected == .series {
                cell.collectionViewHeight.constant = 360.0
                cell.titleLbl.text = "No Content Available."
                cell.leftView.isHidden = true
                cell.rightBtn.isHidden = true
                cell.titleLbl.textAlignment = .center
            } else if self.categorSelected == .movies {
                cell.collectionViewHeight.constant = 360
                cell.leftView.isHidden = false
                cell.rightBtn.isHidden = false
                cell.titleLbl.text = "Featured"
                cell.titleLbl.textAlignment = .left
            } else if self.categorSelected == .shorts {
                cell.leftView.isHidden = false
                cell.rightBtn.isHidden = false
                cell.titleLbl.text = "Comedy"
                cell.titleLbl.textAlignment = .left
            }
            cell.rightBtn.addTarget(self, action: #selector(viewAlltap), for: .touchUpInside)
            cell.subscriptionArray = self.subscriptionArray
            cell.trendingArray = self.trendingArray
            cell.index = indexPath.row
            cell.delegate = self
            cell.collectionview.reloadData()
           
            cell.type = self.categorSelected
            return cell
        }
//        else if indexPath.row == 3 {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeMovieListTVC", for: indexPath) as! HomeMovieListTVC
//            cell.titleLbl.text = "Shorts"
//            cell.index = indexPath.row
//            cell.delegate = self
//            cell.type = .shorts
//            cell.trendingArray = self.trendingArray
//            return cell
//        }
        else {
            return UITableViewCell()
        }
    }
    
    
}
//MARK: - TABLE DLELEGATE
extension HomeVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = UIStoryboard.votingStoryboard.instantiateAllContestantVC()

            vc.votingData = self.votingData[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if votingData.count > 0 {
            return UITableViewAutomaticDimension
        } else {
            if indexPath.row == 0 {
                return 0
            } else {
                return UITableViewAutomaticDimension
            }
        }
     
    }
}
//MARK: - PGAERVIEW DELEGATE
extension HomeVC: FSPagerViewDataSource,FSPagerViewDelegate {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        print(movieSliderArray.count)
        return movieSliderArray.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index) as! HomeBannerCVC
        if index == 0 {
            cell.backView.layer.cornerRadius = 12
            cell.backView.layer.masksToBounds = false
            cell.iconImage.sd_setImage(with: URL(string: movieSliderArray[index].image), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
            cell.iconImage.layer.cornerRadius = 12
            pagerView.scrollToItem(at: 1, animated: true)
            return cell
           
           
        } else {
           
        cell.backView.layer.cornerRadius = 12
        cell.backView.layer.masksToBounds = false
        cell.iconImage.sd_setImage(with: URL(string: movieSliderArray[index].image), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
        cell.iconImage.layer.cornerRadius = 12
        
        return cell
        }
       // return cell
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        if(movieSliderArray[index].type == "vote"){
            let moviesliderdeails = movieSliderArray[index]
            var votingdata = VotingModel(json: JSON())
            votingdata.id = moviesliderdeails.id
            print(moviesliderdeails)
//                let prabhuVoting = PrabhuVotingViewController.instantiate(fromAppStoryboard: .PrabhuVoting)
//                self.navigationController?.pushViewController(prabhuVoting, animated: true)
            let vc = UIStoryboard.votingStoryboard.instantiateAllContestantVC()
            vc.votingData = votingdata
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if(movieSliderArray[index].url != ""){
            let url = URL(string: movieSliderArray[index].url)!
            if  UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }else if(movieSliderArray[index].content){

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
               // let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
                let movieDetailScene = NewMovieDetailsVC.instantiate(fromAppStoryboard: .MovieDetail)
                movieDetailScene.movieDetail = self.movieSliderArray[index].movie
                movieDetailScene.refreshCollectionViewDelegate = self
                self.navigationController?.pushViewController(movieDetailScene, animated: true)
            }


        }
    }
    func pagerView(_ pagerView: FSPagerView, shouldSelectItemAt index: Int) -> Bool {
        if index == 1 {
            return true
        } else {
            return false
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            pagerView.scrollToItem(at: 1, animated: true)
        }
    }
   
}
extension HomeVC {
    //MARK: - VOTING API
    func getVotings() {
        APIHandler.shared.getVotingShows { [weak self](votings) in
            guard let strongSelf = self else { return }
            print(votings)
            strongSelf.votingData = votings
            if let _ = strongSelf.votingData.first {
              
            } else {
               
            }
            print(strongSelf.votingData)
           
//            self.tutorialsDataArr = tutorials
//            self.tutorialsTableView.reloadData()
        } failure: { (msg) in
            print(msg)
        } error: { (err) in
            print(err.localizedDescription)
        }
    }
    func checkIfAppIsBlocked(){

        APIHandler.shared.checkIfAppIsBlocked(success: {
            (status, message) in
            print("sawsank: \(message)")
            if(status){
                let controller = UIAlertController(title: "Notice", message: message, preferredStyle: .alert)
                let ok = UIAlertAction(title: "Okay", style: .cancel, handler: { (action) -> Void in
                    UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)

                })
                controller.addAction(ok)
                self.present(controller, animated: true, completion: nil)
            }
        }) { (failure) in
            
            let controller = UIAlertController(title: "Notice", message: failure.description, preferredStyle: .alert)
            let ok = UIAlertAction(title: "Okay", style: .cancel, handler: { (action) -> Void in
                UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)

            })
            controller.addAction(ok)
            self.present(controller, animated: true, completion: nil)
        } error: { (error) in
            print("Checking app block status: error: " + String(NSURLErrorNotConnectedToInternet))
            if error._code == 13 {
                
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                
//                alert.addAction(UIAlertAction(title: "Go To Offline", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
//                    let offlineVC = OfflineDownloadsViewController.instantiate(fromAppStoryboard: .OfflineDownloads)
//                    self.navigationController?.pushViewController(offlineVC, animated: true )
//                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.checkIfAppIsBlocked()
                }))
                alert.addAction(UIAlertAction(title: "Go to downloads", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    let vc = VideoListController.instantiate(fromAppStoryboard: .MovieDetail)
                    vc.showAllDownloads = true
                    vc.dataProvider = VideoListDataProvider()
                    self.navigationController?.pushViewController(vc, animated: true)
                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    @objc func showProgress(isLoading: Bool){
        DispatchQueue.main.async {
            APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
            APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Processing Payment...", presentingView: self.view)
        }
    }
    
    @objc func hideProgress(notification: NSNotification){
        DispatchQueue.main.async {
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: nil)
            guard let userInfo = notification.userInfo else {
                return
            }
            let alertController = DOAlertController(title: "Info", message: userInfo["message"] as? String, preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "OK", style: .default, handler: { action in
                self.getProfile()
                self.dismiss(animated: true, completion: nil)
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
            

        }
    }
    func getProfile() {
        APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait...", presentingView: self.view)
        
        
        APIHandler.shared.getProfile(success: { (profile) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                print("sawsank: Removed HUD")
            })
            
         
            
            

        }, failure: { (message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                   let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
                   alertController.alertViewBgColor = UIColor.white
                   alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                   alertController.messageView.textAlignment = .left
                   alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                   alertController.titleTextColor = UIColor.red
                   let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                       self.dismiss(animated: true, completion: nil)
                    self.dismissVC()
                   })
                   // Add the action.
                   alertController.addAction(okAction)
                   // Show alert
                   self.present(alertController, animated: true, completion: nil)
                   
                   
               
               
           } )
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                
                
            } )
        }
    }
    func getSettings() {
        APIHandler.shared.getSettings(success: { (json) in
            print(json)
            self.redirectToHome = json["redirectToHome"].boolValue
            if self.redirectToHome == true {
                self.googleAdView.isHidden = true
            } else {
                self.googleAdView.isHidden = false
            }
        }, failure: { (message) in
            let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
             self.dismissVC()
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
        }) { (error) in
            let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func getSliderData(){
        APIHandler.shared.getGoldSliderData(success:{
            (status, movieSliderArr) in
            
            self.movieSliderArray = movieSliderArr
            if movieSliderArr.count == 2 {
                self.movieSliderArray.append(movieSliderArr.first!)
            }
            self.pagerView.reloadData()
          
           
        }, failure: {(failure ) in
            
            
            
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                    
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in

                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
            }
        })
    }
    //MARK: - TRENDING DATA API
    func getTrendingData(){
        APIHandler.shared.getShortMovies(success:{
            (status, shortArr) in
            self.trendingArray = shortArr
            print("Getting trending data...")
            print(self.trendingArray.count)
        }, failure: {(failure ) in
            if failure._code == NSURLErrorNotConnectedToInternet {
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                    
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in

                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
            }
        })
    }
    //MARK: - SUBSCRIPTION DATA API
    func getSubscriptionData(){
        APIHandler.shared.getSubscriptionData(success:{
            (status, subscriptionArr, nextPageId) in
            self.subscriptionArray = subscriptionArr
            print(self.subscriptionArray)
            for item in self.subscriptionArray {
                print(item.coverImage)
            }
        }, failure: {(failure ) in
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                    
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in

                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                
            }

            
        })
   
        
    }
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK: - HOME CATEGORES DELEGATE
extension HomeVC: HomeCagegoresDelegate {
    func didselecCategories(type: HomeCategoriesTapType) {
        print(type)
        switch type {
        case .movies:
            self.categorSelected = .movies
          
//            let viewAllGoldVC = ViewAllGoldViewController.instantiate(fromAppStoryboard: .ViewAllGold)
//            self.navigationController?.pushViewController(viewAllGoldVC, animated: true)
        case .shorts:
            
            self.categorSelected = .shorts
           
//            let viewAllGoldVC = ViewAllGoldViewController.instantiate(fromAppStoryboard: .ViewAllGold)
//            self.navigationController?.pushViewController(viewAllGoldVC, animated: true)
        case .series:
            self.categorSelected = .series
//            let alert = UIAlertController(title: "", message: "No content available for now.", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
//                self.tableView.reloadData()
//                self.dismiss(animated: true, completion: nil)
//
//            }))
//            self.present(alert, animated: true, completion: nil)
//        case .all:
//            print("")
        default:
            break
        }
    tableView.reloadData()
   
    }
}
//MARK: - HOMELIST DELEGATE
extension HomeVC: HomeMovieListDelegate {
    func didTap(type: HomeCategoryType, indexpath: Int?) {
        print(indexpath)
        switch type {
        case .shorts:
            
           // let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
            let movieDetailScene = NewMovieDetailsVC.instantiate(fromAppStoryboard: .MovieDetail)
            movieDetailScene.movieDetail = trendingArray[indexpath ?? 0]
            movieDetailScene.refreshCollectionViewDelegate = self
            self.navigationController?.pushViewController(movieDetailScene, animated: true)
        case .movies:
           // let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
            let movieDetailScene = NewMovieDetailsVC.instantiate(fromAppStoryboard: .MovieDetail)
           movieDetailScene.movieDetail = subscriptionArray[indexpath ?? 0]
           movieDetailScene.refreshCollectionViewDelegate = self
           self.navigationController?.pushViewController(movieDetailScene, animated: true)

        default:
            break
        }
    }
    
   
}
extension HomeVC: RefreshCollectionViews {
    func refreshCollectionViews() {
        self.pagerView.reloadData()

    }
}
extension HomeVC: GADBannerViewDelegate {
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("bannerViewDidReceiveAd")
    }

    private func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
      print("bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    func adViewDidRecordImpression(_ bannerView: GADBannerView) {
      print("bannerViewDidRecordImpression")
    }

    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillPresentScreen")
    }

    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillDIsmissScreen")
    }

    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewDidDismissScreen")
    }
}
extension HomeVC {
  
  func reloadDataShowAnimated() {
    self.tableView.reloadData()
    
    let cells = tableView.visibleCells
    let tableHeight: CGFloat = tableView.bounds.size.height
    
    cells.forEach { cell in
        cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        self.headerView.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        self.footerView.transform = CGAffineTransform(translationX: 0, y: tableHeight)
    }
    
      for (index, cell) in cells.enumerated() {
          UIView.animate(withDuration: 1.2, delay: 0.04 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [],
        animations: {
              cell.transform = CGAffineTransform(translationX: 0, y: 0)
              self.headerView.transform = CGAffineTransform(translationX: 0, y: 0)
              self.footerView.transform = CGAffineTransform(translationX: 0, y: 0)
              self.tableView.isHidden = false
        }, completion: nil)
    }
  }
}
//MARK: - RELOAD AFTER CONTENT IS PURCHASED
extension HomeVC {
    @objc func methodOfReceivedNotification(notification: Notification) {
       //onViewDidLoad()
        self.getSubscriptionData()
        self.getTrendingData()
        
    }
    @objc func handlingPushNotificatoin(notification: Notification) {
        if "\(notification.userInfo?["type"] ?? "normal")" == "news" {
            let vc = NewsFeedViewController.instantiate(fromAppStoryboard: .NewsFeed)
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
       
        
    }
    func showNotification() {
        let center = UNUserNotificationCenter.current()
               //center.delegate = self
               center.requestAuthorization(options: [.badge,.sound,.alert]) { granted, error in
                   if error == nil {
                       print("User permission is granted : \(granted)")
                   }
             }
           //        Step-2 Create the notification content
                   let content = UNMutableNotificationContent()
                   content.title = "Download completed!"
        content.userInfo = ["type": "news"]
        content.categoryIdentifier = "news"
        content.body = ("Mausam") + " has been successfully downloaded."
              
               
           //        Step-3 Create the notification trigger
                   let date = Date().addingTimeInterval(1)
                   let dateComponent = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
                   let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponent, repeats: false)
               
               
               
           //       Step-4 Create a request
                   let uuid = UUID().uuidString
                   let request = UNNotificationRequest(identifier: uuid, content: content, trigger: trigger)
                   
               
           //      Step-5 Register with Notification Center
                   center.add(request) { error in
               
               
                   }
           }
    @objc func SomeNotificationAct(notification:Notification){
        
        print ("open another viewcontroller")
        
        if let myDict = notification.object as? [String: Any] {
           print(myDict)
            print(myDict["type"])
            print(myDict["id"])
          
            if let type = myDict["type"] as? String{
                let id = myDict["id"] as? String
                print(id)
                if(type == "news"){
                    let notificationsVC = NewsFeedViewController.instantiate(fromAppStoryboard: .NewsFeed)
                    self.navigationController?.pushViewController(notificationsVC, animated: true)
                }else if(type == "movies"){
                    let movieDetailVC = NewMovieDetailsVC.instantiate(fromAppStoryboard: .MovieDetail)
                    movieDetailVC.contentID = id ?? ""
                    self.navigationController?.pushViewController(movieDetailVC, animated: true)
                } else if(type == "series"){
                    let seriesDetail = SeriesDetailViewController.instantiate(fromAppStoryboard: .SeriesDetail)
                    seriesDetail.seriesID = id ?? ""
                    self.navigationController?.pushViewController(seriesDetail, animated: true)
                } else if (type == "downloads") {
                    let vc = VideoListController.instantiate(fromAppStoryboard: .MovieDetail)
                    vc.showAllDownloads = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }   else if (type == "statements") {
                    let vc = FinancialStatementViewController.instantiate(fromAppStoryboard: .FinancialStatement)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
    }
    func registerFCMToken(){
        if let token = UserDefaults.standard.string(forKey: "FirebaseToken"){
        if !UserDefaults.standard.bool(forKey: "isFCMTokenSentToServer"){
        APIHandler.shared.registerFCMToken(fcmToken: token, success: {(status) in
                    if status {
                        UserDefaults.standard.set(true, forKey: "isFCMTokenSentToServer")
                    }
                }, error: {(error) in
                   
                })
                
            }
            
            
        }
        
        
    }
}
