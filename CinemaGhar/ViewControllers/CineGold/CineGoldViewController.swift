//
//  CineGoldViewController.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 9/29/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import Foundation
import PassKit
import GoogleMobileAds


class CineGoldViewController: UIViewController{
    
  
    @IBOutlet weak var votingCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var googleAdView: UIView!
    @IBOutlet weak var votingCollectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var sliderView: KolodaView!
    @IBOutlet weak var subscriptionImageView1: UIImageView!
    @IBOutlet weak var subscription1Title: UILabel!
    @IBOutlet weak var subscriptionImageView2: UIImageView!
    @IBOutlet weak var subscription2Title: UILabel!
    @IBOutlet weak var subscriptionImageView3: UIImageView!
    @IBOutlet weak var subscription3Title: UILabel!
    @IBOutlet weak var subscriptionImageView4: UIImageView!
    @IBOutlet weak var subscription4Title: UILabel!
    @IBOutlet weak var viewAllMoviesButton: UIButton!
    @IBOutlet weak var trendingLabel: UILabel!
    @IBOutlet weak var trendingSlider: UICollectionView!
    @IBOutlet weak var buttonMenu: XXXRoundMenuButton!
    @IBOutlet weak var overLayView: UIView!

    //Round Button images
    var tutorialImage = UIImage.animatedImage(with:  [UIImage(named: "video_tutorial_0")!, UIImage(named: "video_tutorial_1")!, UIImage(named: "video_tutorial_2")!, UIImage(named: "video_tutorial_3")!, UIImage(named: "video_tutorial_4")!, UIImage(named: "video_tutorial_3")!, UIImage(named: "video_tutorial_2")!, UIImage(named: "video_tutorial_1")!, UIImage(named: "video_tutorial_0")!], duration: 1)
    var votingDataSource = ["iconTheVoice","iconNepalIdol"]
    var newsImage = UIImage(named: "icons8-news")!
    var myMoviesImage = UIImage(named: "theatre")!
    var myDownloadsImage = UIImage(named: "iconDownload1")!
    var settingsImage = UIImage(named: "icons8-settings")!
    
    @IBOutlet weak var subscribeOutlet: UIButton!
    
    var subscribeAlertController: UIAlertController!
    
    var redirectToHome: Bool = false
    
    
    @IBAction func subscribeAction(){
        let message = """
            Things to know: \n \n
            Subscription will be valid till 31st Dec of 2021 \n
            This subscription will only be valid on 2 devices.
            \n \n \n \n \n \n
        """
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
          
          let attributedMessageText = NSMutableAttributedString(
              string: message,
              attributes: [
                  NSAttributedString.Key.paragraphStyle: paragraphStyle,
                  NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0),
                NSAttributedString.Key.foregroundColor: UIColor.black
              ]
          )
        
        let attributedTitleText = NSMutableAttributedString(
            string: "Gold Subscription",
            attributes: [
              NSAttributedString.Key.foregroundColor: UIColor.black
              
            ]
        )
        
        
        
        subscribeAlertController = UIAlertController(title: "Gold Subscription", message: message, preferredStyle: .alert)
        
        //Customize view
        subscribeAlertController.view.tintColor = UIColor.init(hex:"#252B38")
        
        //Background color
        let subview :UIView = subscribeAlertController.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.subviews.first!.backgroundColor = UIColor.init(hex: "#E59827")
        
        // set attributed message here
        subscribeAlertController.setValue(attributedMessageText, forKey: "attributedMessage")
        subscribeAlertController.setValue(attributedTitleText, forKey: "attributedTitle")

        
        //CineCoin Outlets
        var paymentButton: PKPaymentButton!
        paymentButton.setTitle("Apple Pay", for: .normal)
        paymentButton = PKPaymentButton.init(paymentButtonType: .buy, paymentButtonStyle: .black)
        paymentButton.translatesAutoresizingMaskIntoConstraints = false
        subview.addSubview(paymentButton)
        paymentButton.leftAnchor.constraint(equalTo: subview.leftAnchor, constant: 16).isActive = true
        paymentButton.rightAnchor.constraint(equalTo: subview.rightAnchor, constant: -16).isActive = true
        paymentButton.bottomAnchor.constraint(equalTo: subview.bottomAnchor, constant: -100).isActive = true
        paymentButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //Please visit our site for more info.
        var websiteButton: UIButton!
        websiteButton = UIButton()
        if(redirectToHome){
            websiteButton.setTitle("Visit our website for more info", for: .normal)
        }else{
            websiteButton.setTitle("Pay from website", for: .normal)
        }
       
        websiteButton.backgroundColor = UIColor(hex: "#252B38")
        websiteButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        websiteButton.layer.cornerRadius = 16
        websiteButton.translatesAutoresizingMaskIntoConstraints = false
        subview.addSubview(websiteButton)
        websiteButton.leftAnchor.constraint(equalTo: subview.leftAnchor, constant: 16).isActive = true
        websiteButton.rightAnchor.constraint(equalTo: subview.rightAnchor, constant: -16).isActive = true
        websiteButton.bottomAnchor.constraint(equalTo: subview.bottomAnchor, constant: -50 ).isActive = true
        websiteButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        websiteButton.addTarget(self, action: #selector(gotoWeb(action:)), for: .touchUpInside)
        
        subscribeAlertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        paymentButton.addTarget(self, action: #selector(purchase(_:)), for: .touchUpInside)
                
        
        
        if(SessionManager().isLoggedIn()){
            self.present(subscribeAlertController, animated: true)
        }else{
            let signupVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(signupVC, animated: true)
        }
    }
    
    
    @objc func gotoWeb(action: UIAlertAction){
        let url = URL(string: CinemaGharAPI.homeURL)!
            if  UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
    }
    
    @objc func purchase(_ sender: UITapGestureRecognizer) {
        print("sawsank: Purchasing From apple")
        IAPService.shared.purchase(product: IAPProduct.cinemagharGoldSubscription)
        subscribeAlertController.dismiss(animated: true)
    }

    func gotoPayment(action: UIAlertAction){
        let paymentVC = PaymentViewController.instantiate(fromAppStoryboard: .Payment)
        self.navigationController?.pushViewController(paymentVC, animated: true)
    }
    
    var sessionManager = SessionManager()
    lazy var adBannerView: GADBannerView = {
        let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        adBannerView.adUnitID = "ca-app-pub-8501671653071605/1974659335"
        adBannerView.delegate = self
        adBannerView.rootViewController = self

        return adBannerView
    }()
    private let spacing:CGFloat = 10
    
    
    var movieSliderArray = [MovieSlider]()
    var subscriptionArray = [Movie]()
    var trendingArray = [Movie]()
    var votingData = [VotingModel]()
    override func viewDidLoad() {
        scrollView.delegate = self
       onViewDidLoad()
        getVotings()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showProgress(isLoading:)), name: Notification.Name(rawValue: "IAPPaymentStatusLoading"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideProgress(notification:)), name: Notification.Name(rawValue: "IAPPaymentStatusNotLoading"), object: nil)
       
    }
    
    func onViewDidLoad(){
        checkIfAppIsBlocked()
        getSettings()
        if(sessionManager.isLoggedIn()){
            getProfile()
        }
        IAPService.shared.getProducts()
        viewSetup()
        self.getSliderData()
        self.getSubscriptionData()
        self.getTrendingData()
//        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
//        bannerView.rootViewController = self
//        bannerView.load(GADRequest())
//        bannerView.delegate = self
//        googleAdView.addSubview(bannerView)
//        bannerView.frame = CGRect(x: 0, y: 0, width: googleAdView.frame.width, height: googleAdView.frame.height)
        let adRequest = GADRequest()
       // adRequest.testDevices = [ kGADSimulatorID, "2077ef9a63d2b398840261c8221a0c9b" ]
        adBannerView.load(GADRequest())
        googleAdView.addSubview(adBannerView)
        googleAdView.backgroundColor = .clear
    }
    
    
    
    @objc func showProgress(isLoading: Bool){
        DispatchQueue.main.async {
            APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
            APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Processing Payment...", presentingView: self.view)
        }
    }
    
    @objc func hideProgress(notification: NSNotification){
        DispatchQueue.main.async {
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: nil)
            guard let userInfo = notification.userInfo else {
                return
            }
            let alertController = DOAlertController(title: "Info", message: userInfo["message"] as? String, preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "OK", style: .default, handler: { action in
                self.getProfile()
                self.dismiss(animated: true, completion: nil)
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
            

        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkIfAppIsBlocked()
        self.trendingSlider.reloadData()
        
    }
    
    override func viewDidLayoutSubviews(){
        adBannerView.frame = CGRect(x: 0, y: 0, width: googleAdView.frame.width, height: googleAdView.frame.height)
        // ScrollView
       //e scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + 300)
    }
    
    
    
    func checkIfAppIsBlocked(){

        APIHandler.shared.checkIfAppIsBlocked(success: {
            (status, message) in
            print("sawsank: \(message)")
            if(status){
                let controller = UIAlertController(title: "Notice", message: message, preferredStyle: .alert)
                let ok = UIAlertAction(title: "Okay", style: .cancel, handler: { (action) -> Void in
                    UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)

                })
                controller.addAction(ok)
                self.present(controller, animated: true, completion: nil)
            }
        }) { (failure) in
            
            let controller = UIAlertController(title: "Notice", message: failure.description, preferredStyle: .alert)
            let ok = UIAlertAction(title: "Okay", style: .cancel, handler: { (action) -> Void in
                UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)

            })
            controller.addAction(ok)
            self.present(controller, animated: true, completion: nil)
        } error: { (error) in
            print("Checking app block status: error: " + String(NSURLErrorNotConnectedToInternet))
            if error._code == 13 {
                
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                
//                alert.addAction(UIAlertAction(title: "Go To Offline", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
//                    let offlineVC = OfflineDownloadsViewController.instantiate(fromAppStoryboard: .OfflineDownloads)
//                    self.navigationController?.pushViewController(offlineVC, animated: true )
//                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.checkIfAppIsBlocked()
                }))
                alert.addAction(UIAlertAction(title: "Go to downloads", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    let vc = VideoListController.instantiate(fromAppStoryboard: .MovieDetail)
                    vc.showAllDownloads = true
                    vc.dataProvider = VideoListDataProvider()
                    self.navigationController?.pushViewController(vc, animated: true)
                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    //MARK: - VOTING API
    func getVotings() {
        APIHandler.shared.getVotingShows { [weak self](votings) in
            guard let strongSelf = self else { return }
            print(votings)
            strongSelf.votingData = votings
            if let _ = strongSelf.votingData.first {
                strongSelf.votingCollectionView.isHidden = false
                strongSelf.votingCollectionView.reloadData()
            } else {
                strongSelf.votingCollectionView.isHidden = true
            }
            print(strongSelf.votingData)
           
//            self.tutorialsDataArr = tutorials
//            self.tutorialsTableView.reloadData()
        } failure: { (msg) in
            print(msg)
        } error: { (err) in
            print(err.localizedDescription)
        }
    }
    
    
    
    //Get Profile <Start>
    
    func getSettings() {
        APIHandler.shared.getSettings(success: { (json) in
            print(json)
            self.redirectToHome = json["redirectToHome"].boolValue
        }, failure: { (message) in
            let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
             self.dismissVC()
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
        }) { (error) in
            let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
            alertController.alertViewBgColor = UIColor.white
            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
            alertController.messageView.textAlignment = .left
            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
            alertController.titleTextColor = UIColor.red
            let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            })
            // Add the action.
            alertController.addAction(okAction)
            // Show alert
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func getProfile() {
        APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait...", presentingView: self.view)
        
        
        APIHandler.shared.getProfile(success: { (profile) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                print("sawsank: Removed HUD")
            })
            
            if (profile.isSubscribed){
                self.subscribeOutlet.heightAnchor.constraint(equalToConstant: 0).isActive = true
                self.subscribeOutlet.isHidden = true
            }
            self.subscribeOutlet.isHidden = true
            
            

        }, failure: { (message) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                   let alertController = DOAlertController(title: "Whoops!", message: message, preferredStyle: .alert)
                   alertController.alertViewBgColor = UIColor.white
                   alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                   alertController.messageView.textAlignment = .left
                   alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                   alertController.titleTextColor = UIColor.red
                   let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                       self.dismiss(animated: true, completion: nil)
                    self.dismissVC()
                   })
                   // Add the action.
                   alertController.addAction(okAction)
                   // Show alert
                   self.present(alertController, animated: true, completion: nil)
                   
                   
               
               
           } )
        }) { (error) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let alertController = DOAlertController(title: "Whoops!", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    let okAction = DOAlertAction(title: "ok", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                
                
            } )
        }
    }
    //Get Profile <End>
    
    
    func viewSetup(){
        
        //Subscribe Outlet
        subscribeOutlet.layer.borderWidth = 2
        subscribeOutlet.layer.borderColor = UIColor(hex: "#E59827").cgColor
        subscribeOutlet.layer.cornerRadius = 16
        subscribeOutlet.widthAnchor.constraint(equalToConstant: 125).isActive = true
    
        
        //Slider
        sliderView.dataSource = self
        sliderView.delegate = self
        sliderView.countOfVisibleCards = 3
        sliderView.isLoop = true
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            sliderView.height(185)
        case .pad:
            sliderView.height(285)
        case .unspecified:
            break
        case .tv:
            break
        case .carPlay:
            break
        case .mac:
            break
        }
        
        //Subsription Image1
        subscriptionImageView1.layer.cornerRadius = 20
        subscriptionImageView1.layer.masksToBounds = true
        subscriptionImageView1.contentMode = UIViewContentMode.scaleAspectFill
        
        //Subsription Image2
        subscriptionImageView2.layer.cornerRadius = 20
        subscriptionImageView2.layer.masksToBounds = true
        subscriptionImageView2.contentMode = UIViewContentMode.scaleAspectFill
        
        //Subsription Image3
        subscriptionImageView3.layer.cornerRadius = 20
        subscriptionImageView3.layer.masksToBounds = true
        subscriptionImageView3.contentMode = UIViewContentMode.scaleAspectFill
        
        //Subsription Image4
        subscriptionImageView4.layer.cornerRadius = 20
        subscriptionImageView4.layer.masksToBounds = true
        subscriptionImageView4.contentMode = UIViewContentMode.scaleAspectFill
        
        //View All Button
        viewAllMoviesButton.layer.cornerRadius = 20
        viewAllMoviesButton.addTarget(self, action: #selector(viewAllGold), for: .touchUpInside)
        
       
        
        //Trending Slider
        let nibName = UINib(nibName: "GoldTrendingCollectionViewCell", bundle:nil)
        trendingSlider.register(nibName, forCellWithReuseIdentifier: "GoldTrendingCollectionViewCell")
        votingCollectionView.register(UINib(nibName: "VotingCVC", bundle: nil), forCellWithReuseIdentifier: "VotingCVC")
        votingCollectionView.backgroundColor = .clear
        votingCollectionView.dataSource = self
        votingCollectionView.delegate = self

        trendingSlider.dataSource = self
        trendingSlider.delegate = self
        changeImages()
        topBarSetup()
        
    }
    //Tutorial contents
    var tutorialLabel: UILabel!
    var circle: UIView!
    var arrowImageView: UIImageView!
    func overlayTutorialSetup(){
        tutorialLabel = UILabel()
        tutorialLabel.text = "Here are tutorials in case you get confused."
        tutorialLabel.textColor = UIColor.white
        tutorialLabel.adjustsFontSizeToFitWidth = true
        tutorialLabel.numberOfLines = 2
        tutorialLabel.translatesAutoresizingMaskIntoConstraints = false
        
        circle = UIView()
        circle.frame = buttonMenu.frame
        circle.backgroundColor = UIColor.red
        circle.layer.cornerRadius = 150
        circle.translatesAutoresizingMaskIntoConstraints = false
        
        arrowImageView = UIImageView()
        arrowImageView.image = UIImage(named: "arrow")
        arrowImageView.translatesAutoresizingMaskIntoConstraints = false
        
        
        if(!sessionManager.isTutorialFinished()){
       
            overLayView.addSubview(circle)
            overLayView.addSubview(tutorialLabel)
            overLayView.addSubview(arrowImageView)
            
            tutorialLabel.centerXAnchor.constraint(equalTo: overLayView.centerXAnchor).isActive = true
            if(UIDevice.current.userInterfaceIdiom == .pad){
                tutorialLabel.centerYAnchor.constraint(equalTo: overLayView.centerYAnchor).isActive = true
            }else{
                tutorialLabel.centerYAnchor.constraint(equalTo: overLayView.centerYAnchor, constant: 20).isActive = true
            }
            
            tutorialLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
            tutorialLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
            
            circle.centerXAnchor.constraint(equalTo: self.buttonMenu.centerXAnchor).isActive = true
            circle.centerYAnchor.constraint(equalTo: self.buttonMenu.centerYAnchor).isActive = true
            circle.widthAnchor.constraint(equalTo: self.buttonMenu.widthAnchor).isActive = true
            circle.heightAnchor.constraint(equalTo: self.buttonMenu.heightAnchor).isActive = true
            
            arrowImageView.topAnchor.constraint(equalTo: self.buttonMenu.bottomAnchor, constant: -20).isActive = true
            
            arrowImageView.leftAnchor.constraint(equalTo: self.buttonMenu.rightAnchor, constant: -30).isActive = true
            arrowImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
            arrowImageView.widthAnchor.constraint(equalToConstant: 73).isActive = true
            
            //Arrow animation
            DispatchQueue.main.async {
                UIImageView.animate(withDuration: 1, delay: 0.0, options: [UIViewAnimationOptions.autoreverse, UIViewAnimationOptions.repeat], animations: {
                    self.arrowImageView.transform = CGAffineTransform.identity.scaledBy(x: 1.25, y: 1.25)
                 })
            }
            
        }else{
            tutorialLabel.isHidden = true
            circle.isHidden = true
            arrowImageView.isHidden = true
        }
    }
    
    func changeImages(){

        print("sawsank: Changing Images")
        print("sawsank: \(sessionManager.isTutorialFinished())")
        if(!sessionManager.isTutorialFinished()){
            print("sawsank: Changing Images with alpha")
            newsImage = newsImage.alpha(0.2)
            myMoviesImage = myMoviesImage.alpha(0.2)
            myDownloadsImage = myDownloadsImage.alpha(0.2)
            settingsImage = settingsImage.alpha(0.2)
            overlayTutorialSetup()
            self.buttonMenu.isSelected = true
            self.overLayView.isHidden = false
            sessionManager.setIsTutorialFinished(status: true)


        }else{
            print("sawsank: Changing Images without alpha")
            tutorialImage = UIImage(named: "video_tutorial")
            newsImage = UIImage(named: "icons8-news")!
            myMoviesImage = UIImage(named: "theatre")!
            myDownloadsImage = UIImage(named: "iconDownload1")!
            settingsImage = UIImage(named: "icons8-settings")!
            if(tutorialLabel != nil && circle != nil){
                tutorialLabel.isHidden = true
                circle.isHidden = true
                arrowImageView.isHidden = true
            }
            buttonMenu.load(withIcons: [newsImage, myDownloadsImage,  tutorialImage!, settingsImage], startDegree: Float(.pi/1.94), layoutDegree: Float(-.pi/1.9), withIconDescription: [
                "My Account",
                "Downloads",
                "Tutorials",
                "Settings"
                ])
        }
    }
    
    func topBarSetup(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        overLayView.addGestureRecognizer(tap)
        
        self.view.addSubview(buttonMenu)
        buttonMenu.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
        buttonMenu.centerButtonSize = CGSize(width: 30, height: 30)
        buttonMenu.tintColor = UIColor.white
        buttonMenu.jumpOutButtonOnebyOne = false
        buttonMenu.mainColor = UIColor.clear
        buttonMenu.centerIconType = .userDraw
        buttonMenu.translatesAutoresizingMaskIntoConstraints = false
        if(UIDevice.current.screenType == .unknown){
            if(UIDevice.current.userInterfaceIdiom == .phone){
                buttonMenu.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: -115.0).isActive = true
                buttonMenu.topAnchor.constraint(equalTo: self.view.topAnchor, constant: -90).isActive = true
            }else{
                buttonMenu.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: -40.0).isActive = true
                buttonMenu.topAnchor.constraint(equalTo: self.view.topAnchor, constant: -90).isActive = true
            }
        }else{
            buttonMenu.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: -115.0).isActive = true
            buttonMenu.topAnchor.constraint(equalTo: self.view.topAnchor, constant: -90).isActive = true
        }
        buttonMenu.drawCenterButtonIconBlock = {(_ rect: CGRect, _ state: UIControlState) -> Void in
            
            if state == .normal {
                let rectanglePath = UIBezierPath(rect: CGRect(x: (rect.size.width - 15) / 2, y: rect.size.height / 2 - 7, width: 15, height: 2))
                UIColor.white.setFill()
                rectanglePath.fill()
                let rectangle2Path = UIBezierPath(rect: CGRect(x: (rect.size.width - 15) / 2, y: rect.size.height / 2, width: 20, height: 2))
                UIColor.white.setFill()
                rectangle2Path.fill()
                let rectangle3Path = UIBezierPath(rect: CGRect(x: (rect.size.width - 15) / 2, y: rect.size.height / 2 + 7, width: 15, height: 2))
                UIColor.white.setFill()
                rectangle3Path.fill()
            }
            
            
            
            
        }
        

        
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkMenuStatus(notification:)), name: Notification.Name(rawValue: "menuStatus"), object: nil)
        
        
       
        buttonMenu.load(withIcons: [newsImage, myDownloadsImage,  tutorialImage!, settingsImage], startDegree: Float(.pi/1.94), layoutDegree: Float(-.pi/1.9), withIconDescription: [
            "My Account",
            "Downloads",
            "Tutorials",
            "Settings"
            ])
       
        buttonMenu.buttonClickBlock =  {(idx:NSInteger)-> Void in
            self.overLayView.isHidden = true
            self.buttonMenu.mainColor = UIColor.clear
            
            if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax || UIDevice.current.screenType == .unknown   {
                self.buttonMenu.topAnchor.constraint(equalTo: self.view.topAnchor, constant: -85).isActive = true
            }
            else {
                self.buttonMenu.topAnchor.constraint(equalTo: self.view.topAnchor, constant: -90).isActive = true
            }
            
            switch idx {
            case 0:
                if (self.sessionManager.isLoggedIn()){
                    self.buttonMenu.isOpened = false
                    let accountVC = MyAccountViewController.instantiate(fromAppStoryboard: .MyAccount)
                    self.navigationController?.pushViewController(accountVC, animated: true)

                }
                else{
                    let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                    self.navigationController?.pushViewController(loginVC, animated: true)
                }
                break
            case 1:
                
                if (self.sessionManager.isLoggedIn()){
//                    let offlineVC = OfflineDownloadsViewController.instantiate(fromAppStoryboard: .OfflineDownloads)
//                    self.navigationController?.pushViewController(offlineVC, animated: true )
//                    let theatreVC = TheatreViewController.instantiate(fromAppStoryboard: .Theatre)
//                    self.navigationController?.pushViewController(theatreVC, animated: true)
//                    let vc = VideoListController.instantiate(fromAppStoryboard: .MovieDetail)
//                    vc.dataProvider = VideoListDataProvider()
//                    vc.showAllDownloads = true
                    let vc = MyReawardVC.instantiate(fromAppStoryboard: .reward)
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                    self.navigationController?.pushViewController(loginVC, animated: true)
                }
                break
            case 2:
                self.sessionManager.setIsTutorialFinished(status: true)
                self.changeImages()
                let tutorialsVC = TutorialsViewController.instantiate(fromAppStoryboard: .Tutorials)
                self.navigationController?.pushViewController(tutorialsVC, animated: true)
                
                break
                
                
            case 3:
                let settingsVC = SettingsViewController.instantiate(fromAppStoryboard: .Settings)
                
                settingsVC.delegate = self
                
                self.navigationController?.pushViewController(settingsVC, animated: true)
                
                
                break
                
                
                
            default:
                print("no index")
            }
        };
        
        let newsButton = UIButton()
        newsButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        newsButton.setImage(UIImage(named: "icons8-news"), for: .normal)
        self.view.addSubview(newsButton)
        newsButton.translatesAutoresizingMaskIntoConstraints = false

        newsButton.leftAnchor.constraint(equalTo: buttonMenu.rightAnchor, constant: -105.0).isActive = true
        newsButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50.0).isActive = true
        newsButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        newsButton.addTarget(self, action: #selector(newsBtnPressed), for: .touchUpInside)
        
//        let logoView = UIImageView()
//        logoView.frame = CGRect(x: 0, y: 0, width: 185, height: 24)
//        logoView.image = UIImage(named: "logo")
//        logoView.backgroundColor = .clear
//        self.view.addSubview(logoView)
//        logoView.translatesAutoresizingMaskIntoConstraints = false
//        logoView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0.0).isActive = true
//        logoView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 49).isActive = true
//        logoView.heightAnchor.constraint(equalToConstant: 20).isActive = true
//        //logoView.widthAnchor.constraint(equalToConstant: 140).isActive = true
//        logoView.leftAnchor.constraint(equalTo: newsButton.rightAnchor,constant: 8).isActive = true
//        logoView.leftAnchor.constraint(equalTo: se.rightAnchor,constant: 8).isActive = true
//        logoView.widthAnchor.constraint(equalToConstant: SCREEN.WIDTH - 225).isActive = true
       
        
     
        let notificationsButton = UIButton()
        notificationsButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        notificationsButton.setImage(UIImage(named: "icons8-appointment_reminders"), for: .normal)
        self.view.addSubview(notificationsButton)
        notificationsButton.translatesAutoresizingMaskIntoConstraints = false
        if(UIDevice.current.screenType == .unknown){
            if(UIDevice.current.userInterfaceIdiom == .phone){
                notificationsButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16.0).isActive = true
            }else{
            notificationsButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -90.0).isActive = true
            }
            
        }else{
            notificationsButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16.0).isActive = true
            
        }
   
        notificationsButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50.0).isActive = true
        notificationsButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        notificationsButton.addTarget(self, action: #selector(notificationsBtnPressed), for: .touchUpInside)
        
        let searchButton = UIButton()
        searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        searchButton.setImage(UIImage(named: "icons8-search_filled"), for: .normal)
        self.view.addSubview(searchButton)
        searchButton.translatesAutoresizingMaskIntoConstraints = false
        searchButton.rightAnchor.constraint(equalTo: notificationsButton.leftAnchor, constant: -16).isActive = true
        searchButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        searchButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50.0).isActive = true
        searchButton.addTarget(self, action: #selector(searchBtnPressed), for: .touchUpInside)
        
       
        
        let logoView = UIImageView()
        logoView.frame = CGRect(x: 0, y: 0, width: 185, height: 24)
        logoView.image = UIImage(named: "logo")
        logoView.backgroundColor = .clear
        self.view.addSubview(logoView)
        logoView.translatesAutoresizingMaskIntoConstraints = false
        logoView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0.0).isActive = true
        logoView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50).isActive = true
        logoView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        //logoView.widthAnchor.constraint(equalToConstant: 140).isActive = true
        logoView.leftAnchor.constraint(equalTo: newsButton.rightAnchor,constant: 18).isActive = true
        logoView.rightAnchor.constraint(equalTo: searchButton.leftAnchor,constant: -10).isActive = true
        
       // logoView.widthAnchor.constraint(equalToConstant: SCREEN.WIDTH - 225).isActive = true
        self.view.bringSubview(toFront: overLayView)
        self.view.bringSubview(toFront: buttonMenu)
    }
    
   
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        
        self.overLayView.isHidden = true
        self.buttonMenu.isOpened = false
        self.buttonMenu.setSelected(false)
        self.buttonMenu.isSelected = false
        self.buttonMenu.mainColor = UIColor.clear
        self.buttonMenu.buttonClick(self.buttonMenu)
        changeImages()
        self.view.layoutIfNeeded()
    }
    
    @objc func newsBtnPressed(_ sender: Any) {
        
        let newsVC = NewsFeedViewController.instantiate(fromAppStoryboard: .NewsFeed)
        self.navigationController?.pushViewController(newsVC, animated: true)
        
    }
    
    @objc func searchBtnPressed(_ sender: Any) {
        
        let searchVC = SearchViewController.instantiate(fromAppStoryboard: .Search)
        self.navigationController?.pushViewController(searchVC, animated: true)
        
    }
    
    @objc func notificationsBtnPressed(_ sender: Any) {
        
        let notificationsVC = NotificationsViewController.instantiate(fromAppStoryboard: .Notifications)
        self.navigationController?.pushViewController(notificationsVC, animated: true)
        
    }
    
    @objc func checkMenuStatus(notification: NSNotification) {
        if let menuStatus = notification.userInfo?["isMenuOpen"] as? Bool {
            
            if menuStatus {
                overLayView.isHidden = false
                
                }
                self.view.layoutIfNeeded()
                
            }
        else {
          
            self.view.layoutIfNeeded()
            overLayView.isHidden = true
        }
        
    }
    
    
    
    func getSliderData(){
        
        
        APIHandler.shared.getGoldSliderData(success:{
            (status, movieSliderArr) in
            
            self.movieSliderArray = movieSliderArr
            
            self.sliderView.reloadData()

            

            
        }, failure: {(failure ) in
            
            
            
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                    
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in

                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                
            }
            
            
            
            
        })
        
        
        
        
        
        
        
        
    }
    
    func getTrendingData(){
        
        
        APIHandler.shared.getShortMovies(success:{
            (status, shortArr) in
            
            self.trendingArray = shortArr
            print("Getting trending data...")
            print(self.trendingArray.count)
            self.trendingSlider.reloadData()
            

            

            
        }, failure: {(failure ) in
            
            
            
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                    
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in

                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                
            }
            
            
            
            
        })
        
        
        
        
        
        
        
        
    }
    
    func getSubscriptionData(){
        
        
        APIHandler.shared.getSubscriptionData(success:{
            (status, subscriptionArr, nextPageId) in
            
            self.subscriptionArray = subscriptionArr
            print(self.subscriptionArray)
            for item in self.subscriptionArray {
                print(item.coverImage)
            }
            //Subscription Image 1
            if let movieCoverURL = self.subscriptionArray[0].coverImage {
                self.subscriptionImageView1.sd_setImage(with: URL(string: movieCoverURL), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
                
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.image1Tapped(gesture:)))
                // add it to the image view;
                self.subscriptionImageView1.addGestureRecognizer(tapGesture)
                // make sure imageView can be interacted with by user
                self.subscriptionImageView1.isUserInteractionEnabled = true
                
            }
            
            if let title1 = self.subscriptionArray[0].title {
                self.subscription1Title.text = title1
            }

            //Subscription Image 2
            if let movieCoverURL = self.subscriptionArray[1].poster {
                self.subscriptionImageView2.sd_setImage(with: URL(string: movieCoverURL), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
                
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.image2Tapped(gesture:)))
                // add it to the image view;
                self.subscriptionImageView2.addGestureRecognizer(tapGesture)
                // make sure imageView can be interacted with by user
                self.subscriptionImageView2.isUserInteractionEnabled = true
            }
            
            if let title2 = self.subscriptionArray[1].title {
                self.subscription2Title.text = title2
            }
            
            //Subscription Image 3
            if let movieCoverURL = self.subscriptionArray[2].poster {
                self.subscriptionImageView3.sd_setImage(with: URL(string: movieCoverURL), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
                
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.image3Tapped(gesture:)))
                // add it to the image view;
                self.subscriptionImageView3.addGestureRecognizer(tapGesture)
                // make sure imageView can be interacted with by user
                self.subscriptionImageView3.isUserInteractionEnabled = true
            }
            if let title3 = self.subscriptionArray[2].title {
                self.subscription3Title.text = title3
            }
            
            //Subscription Image 4
            if let movieCoverURL = self.subscriptionArray[3].coverImage {
                self.subscriptionImageView4.sd_setImage(with: URL(string: movieCoverURL), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
                
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.image4Tapped(gesture:)))
                // add it to the image view;
                self.subscriptionImageView4.addGestureRecognizer(tapGesture)
                // make sure imageView can be interacted with by user
                self.subscriptionImageView4.isUserInteractionEnabled = true
            }
            
            if let title4 = self.subscriptionArray[3].title {
                self.subscription4Title.text = title4
            }

            

            
        }, failure: {(failure ) in
            
            
            
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                    
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in

                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                
            }

            
        })
   
        
    }
    
    
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func viewAllGold(){
        let viewAllGoldVC = ViewAllGoldViewController.instantiate(fromAppStoryboard: .ViewAllGold)
        self.navigationController?.pushViewController(viewAllGoldVC, animated: true)
    }
    
    @objc func viewSubscriptionAlert(){
        let message = """
            Things to know: \n \n
            - Subscription will be valid till 31st Dec of 2021 \n
            - This subscription will only be valid on 2 registered devices.
        """
        
        let paragraphStyle = NSMutableParagraphStyle()
          paragraphStyle.alignment = NSTextAlignment.left
          
          let attributedMessageText = NSMutableAttributedString(
              string: message,
              attributes: [
                  NSAttributedString.Key.paragraphStyle: paragraphStyle,
                  NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0),
                NSAttributedString.Key.foregroundColor: UIColor.white
                
              ]
          )
        
        let attributedTitleText = NSMutableAttributedString(
            string: "Gold Subscription",
            attributes: [
              NSAttributedString.Key.foregroundColor: UIColor.white
              
            ]
        )
        
        
        
        let alert = UIAlertController(title: "Gold Subscription", message: message, preferredStyle: .alert)
        
        //Customize view
        alert.view.tintColor = UIColor.init(hex:"#F3BC31")
        
        //Background color
        let subview :UIView = alert.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.subviews.first!.backgroundColor = UIColor.init(hex: "#252B38")
        
        // set attributed message here
        alert.setValue(attributedMessageText, forKey: "attributedMessage")
        alert.setValue(attributedTitleText, forKey: "attributedTitle")

        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (action) in
            self.showPaymentDialog()
        }))

        if(self.sessionManager.isLoggedIn()){
            self.present(alert, animated: true)
        }else{
            let signupVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(signupVC, animated: true)
        }
    }
    
    func showPaymentDialog(){
        let message = ""
        
        let paragraphStyle = NSMutableParagraphStyle()
          paragraphStyle.alignment = NSTextAlignment.left
          
          let attributedMessageText = NSMutableAttributedString(
              string: message,
              attributes: [
                  NSAttributedString.Key.paragraphStyle: paragraphStyle,
                  NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0),
                NSAttributedString.Key.foregroundColor: UIColor.white
                
              ]
          )
        
        let attributedTitleText = NSMutableAttributedString(
            string: "Continue With",
            attributes: [
              NSAttributedString.Key.foregroundColor: UIColor.white
              
            ]
        )
        
        
        
        let alert = UIAlertController(title: "Continue With", message: message, preferredStyle: .alert)
        
        //Customize view
        alert.view.tintColor = UIColor.init(hex:"#F3BC31")
        
        //Background color
        let subview :UIView = alert.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.subviews.first!.backgroundColor = UIColor.init(hex: "#252B38")
        
        // set attributed message here
        alert.setValue(attributedMessageText, forKey: "attributedMessage")
        alert.setValue(attributedTitleText, forKey: "attributedTitle")

        alert.addAction(UIAlertAction(title: "CineCoin", style: .default, handler: continueSubscription))
        
        alert.addAction(UIAlertAction(title: "Voucher", style: .default, handler: continueVoucher))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))

        self.present(alert, animated: true)
    }
    
    func continueSubscription(action: UIAlertAction){
        let alert = UIAlertController(title: "Paying with CineCoin", message: "", preferredStyle: .alert)
        
        //Customize view
        alert.view.tintColor = UIColor.init(hex:"#F3BC31")
        
        let attributedTitleText = NSMutableAttributedString(
            string: "Paying with CineCoin",
            attributes: [
              NSAttributedString.Key.foregroundColor: UIColor.white
              
            ]
        )
        
        // set attributed message here
        alert.setValue(attributedTitleText, forKey: "attributedTitle")
        
        //Background color
        let subview :UIView = alert.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.subviews.first!.backgroundColor = UIColor.init(hex: "#252B38")

        alert.addTextField { (textField) in
            textField.placeholder = "Enter your pin"
            textField.isSecureTextEntry = true
        }
        let pinTextField = alert.textFields?.first
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { action in
            self.handlePayment(pinTextField: pinTextField!)
        }))
        if(self.sessionManager.isLoggedIn()){
            self.present(alert, animated: true)
        }else{
            let signupVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(signupVC, animated: true)
        }
    }
    
    func continueVoucher(action: UIAlertAction){
        let alert = UIAlertController(title: "Paying with Voucher", message: "", preferredStyle: .alert)
        
        //Customize view
        alert.view.tintColor = UIColor.init(hex:"#F3BC31")
        
        let attributedTitleText = NSMutableAttributedString(
            string: "Paying with Voucher",
            attributes: [
              NSAttributedString.Key.foregroundColor: UIColor.white
              
            ]
        )
        
        // set attributed message here
        alert.setValue(attributedTitleText, forKey: "attributedTitle")
        
        //Background color
        let subview :UIView = alert.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.subviews.first!.backgroundColor = UIColor.init(hex: "#252B38")

        alert.addTextField { (textField) in
            textField.placeholder = "Enter your coupon"
        }
        let tokenTextField = alert.textFields?.first
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { action in
            self.handleTokenPayment(tokenTextField: tokenTextField!)
        }))
        if(self.sessionManager.isLoggedIn()){
            self.present(alert, animated: true)
        }else{
            let signupVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(signupVC, animated: true)
        }
    }
    
    
    func handleTokenPayment(tokenTextField: UITextField){
        let token = tokenTextField.text!
        APIHandler.shared.tokenPayment(token: token) { (status, msg) in
            if(status){
                let alert = UIAlertController(title: "Success!", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                self.getSliderData()
                self.getTrendingData()
                self.getSubscriptionData()
                self.present(alert, animated: true)
            }else{
                let alert = UIAlertController(title: "Error!", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }
        } failure: { (msg) in
            let alert = UIAlertController(title: "Error!", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        } error: { (err) in
            let alert = UIAlertController(title: "Error!", message: err.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }

    }
    
    func handlePayment( pinTextField: UITextField){
        let pin = pinTextField.text!
        APIHandler.shared.walletVerifyPin(pin: pin, type: "subscription", id: "", season: "") { (status, token) in
            if(status){
                APIHandler.shared.validatePayment(paymentGateway: .wallet, mediaType: .subscription, contentId: 0, episodeID: 0, paymentSuccessToken: token, transactionID: "") { (status, url, msg) in
                    if(status){
                        let alert = UIAlertController(title: "Success!", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                        self.getSliderData()
                        self.getTrendingData()
                        self.getSubscriptionData()
                        self.present(alert, animated: true)
                    }else{
                        let alert = UIAlertController(title: "Error!", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                        self.present(alert, animated: true)
                    }
                } failure: { (err) in
                    let alert = UIAlertController(title: "Error!", message: err.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }

            }else{
                let alert = UIAlertController(title: "Error!", message: token, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }
        } failure: { (error) in
            let alert = UIAlertController(title: "Error!", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
        
    }
    
    @objc func image1Tapped(gesture: UIGestureRecognizer) {
        if (gesture.view as? UIImageView) != nil {
         let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
        movieDetailScene.movieDetail = subscriptionArray[0]
        movieDetailScene.refreshCollectionViewDelegate = self
        self.navigationController?.pushViewController(movieDetailScene, animated: true)

        }
    }
    
    @objc func image2Tapped(gesture: UIGestureRecognizer) {
        if (gesture.view as? UIImageView) != nil {
        let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
        movieDetailScene.movieDetail = subscriptionArray[1]
        movieDetailScene.refreshCollectionViewDelegate = self
        self.navigationController?.pushViewController(movieDetailScene, animated: true)


        }
    }
    
    @objc func image3Tapped(gesture: UIGestureRecognizer) {
        if (gesture.view as? UIImageView) != nil {
        let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
        movieDetailScene.movieDetail = subscriptionArray[2]
        movieDetailScene.refreshCollectionViewDelegate = self
        self.navigationController?.pushViewController(movieDetailScene, animated: true)


        }
    }
    
    @objc func image4Tapped(gesture: UIGestureRecognizer) {
        if (gesture.view as? UIImageView) != nil {
        let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
        movieDetailScene.movieDetail = subscriptionArray[3]
        movieDetailScene.refreshCollectionViewDelegate = self
        self.navigationController?.pushViewController(movieDetailScene, animated: true)


        }
    }
    

    
    
}


extension CineGoldViewController: RefreshCollectionViews {
    func refreshCollectionViews() {
        self.trendingSlider.reloadData()

    }
}



extension CineGoldViewController: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        //        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2, execute: {
        //            koloda.resetCurrentCardIndex()
        //
        //        })
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        
        if(movieSliderArray[index].type == "vote"){
                let prabhuVoting = PrabhuVotingViewController.instantiate(fromAppStoryboard: .PrabhuVoting)
                self.navigationController?.pushViewController(prabhuVoting, animated: true)
            
        }else if(movieSliderArray[index].url != ""){
            let url = URL(string: movieSliderArray[index].url)!
            if  UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }else if(movieSliderArray[index].content){

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
                movieDetailScene.movieDetail = self.movieSliderArray[index].movie
                movieDetailScene.refreshCollectionViewDelegate = self
                self.navigationController?.pushViewController(movieDetailScene, animated: true)
            }


        }
        
        
    }
    
    func kolodaShouldApplyAppearAnimation(_ koloda: KolodaView) -> Bool {
        return false
    }
    
    
    
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] {
        return [.left, .right]
    }
    
    
}

// MARK: KolodaViewDataSource

extension CineGoldViewController: KolodaViewDataSource {
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return movieSliderArray.count
    }
    
    
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .fast
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        
        let view = Bundle.main.loadNibNamed("GoldSliderSwipeCard", owner: self, options: nil)![0] as! GoldSliderSwipeCard
        view.loadKolodaView(sliderData: movieSliderArray[index])
        
        return view
        
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return Bundle.main.loadNibNamed("OverlayView", owner: self, options: nil)?[0] as? OverlayView
    }
    
    
    
    
}



extension CineGoldViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == votingCollectionView {
            return votingData.count
        } else {
        return trendingArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == votingCollectionView {
            if votingData.count > 0 {
                votingCollectionViewHeight.constant = (CGFloat(votingData.count) * 110.00)
            let cell = votingCollectionView.dequeueReusableCell(withReuseIdentifier: "VotingCVC", for: indexPath) as! VotingCVC
            cell.iconImage.sd_setImage(with: URL(string: votingData[indexPath.row].coverImage ?? ""), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages], completed:nil)
            cell.titleLbl.text = votingData[indexPath.row].title
            cell.contentView.backgroundColor = .clear

            cell.backView.backgroundColor = UIColor.init(hex: "#3C4352")
            cell.backView.clipsToBounds = true
            cell.backView.layer.masksToBounds = false
            return cell
            } else {
                let cell = votingCollectionView.dequeueReusableCell(withReuseIdentifier: "VotingCVC", for: indexPath) as! VotingCVC
                return cell
            }
        } else {
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GoldTrendingCollectionViewCell", for: indexPath) as! GoldTrendingCollectionViewCell
        cell.setMovieInfos(movie: self.trendingArray[indexPath.row])
            
        return cell
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == votingCollectionView {
            return CGSize(width: collectionView.bounds.size.width, height: 100)
        } else {
        return CGSize(width: 123, height: 191)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(trendingArray[indexPath.row].isBought)
        if collectionView == votingCollectionView {
//            let vc = UIStoryboard.votingStoryboard.instantiateVotingVC()
            let vc = UIStoryboard.votingStoryboard.instantiateAllContestantVC()

            vc.votingData = self.votingData[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
        let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
        movieDetailScene.movieDetail = trendingArray[indexPath.row]
        movieDetailScene.refreshCollectionViewDelegate = self
        self.navigationController?.pushViewController(movieDetailScene, animated: true)
        }
    }
        
}

extension UIImage {

    func alpha(_ value:CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

//extension CineGoldViewController: UIScrollViewDelegate {
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView.contentOffset.x != 0 {
//            scrollView.contentOffset.x = 0
//        }
//    }
//
//}
extension CineGoldViewController: GADBannerViewDelegate {
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("bannerViewDidReceiveAd")
    }

    private func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
      print("bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    func adViewDidRecordImpression(_ bannerView: GADBannerView) {
      print("bannerViewDidRecordImpression")
    }

    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillPresentScreen")
    }

    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillDIsmissScreen")
    }

    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewDidDismissScreen")
    }
}
