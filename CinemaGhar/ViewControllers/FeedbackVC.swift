//
//  FeedbackVC.swift
//  CinemaGhar
//
//  Created by Midas on 01/10/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
protocol FeedbackDelegate: class {
    func feedbackInfo(feedback: String)
}
class FeedbackVC: UIViewController {
    weak var delegate: FeedbackDelegate?
    @IBOutlet weak var feedBackField: UITextView!
    
    @IBOutlet weak var continueBtn: AnimatedButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        feedBackField.becomeFirstResponder()
    }
    static func instantiate() -> FeedbackVC {
        return (UIStoryboard(name: "reward", bundle: nil).instantiateViewController(withIdentifier: "FeedbackVC") as? FeedbackVC)!
    }
    @IBAction func continueTap(_ sender: Any) {
        delegate?.feedbackInfo(feedback: feedBackField.text ?? "")
        self.dismiss(animated: true)
    }
    


}
