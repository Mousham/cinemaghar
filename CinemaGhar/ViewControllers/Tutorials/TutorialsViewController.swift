//
//  TutorialsViewController.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 12/26/20.
//  Copyright © 2020 sunBi. All rights reserved.
//

import Foundation


class TutorialsViewController: UIViewController, UIGestureRecognizerDelegate  {
    
    @IBOutlet weak var tutorialsTableView: UITableView!
    var tutorialsDataArr = [Tutorials]()
    @IBOutlet weak var navItem: UINavigationItem!
    
    override func viewDidLoad() {
        tutorialsTableView.delegate = self
        tutorialsTableView.dataSource = self
        
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back_arrow"), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        button.sizeToFit()
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)

        
        self.navItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
        
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        getTutorials()
    }
    
    @objc func dismissVC(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func getTutorials(){
        APIHandler.shared.getTutorials { (tutorials) in
            self.tutorialsDataArr = tutorials
            self.tutorialsTableView.reloadData()
        } failure: { (msg) in
            print(msg)
        } error: { (err) in
            print(err.localizedDescription)
        }


    }
}

extension TutorialsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tutorialsDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let videoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TutorialsCell", for: indexPath) as! TutorialsCell
        
        videoTableViewCell.videoTitleLabel.text = self.tutorialsDataArr[indexPath.row].title
        
        videoTableViewCell.videoDescLabel.text = self.tutorialsDataArr[indexPath.row].desc
        
       
    
        
        if let imgUrl = self.tutorialsDataArr[indexPath.row].links.first {
            videoTableViewCell.videoThumbnailImageView.sd_setImage(with: URL(string: getYoutubeThumbNail(youtubeUrl: imgUrl.stringValue)! ), placeholderImage: UIImage(named: "placeholder_cell"), options: [.continueInBackground, .progressiveLoad])
        }
        
        return videoTableViewCell as TutorialsCell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let youtubePlayerVC = FreeMoviePlayerViewController.instantiate(fromAppStoryboard: .FreeMoviePlayer)
        if let youtubeURL = self.tutorialsDataArr[indexPath.row].links.first{
            youtubePlayerVC.youtubeURL = youtubeURL.stringValue
            self.navigationController?.pushViewController(youtubePlayerVC, animated: true)
        }
    }
    
    
    func getYoutubeThumbNail(youtubeUrl: String) -> String? {
        
        if let  videoId = URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value{
            return "https://img.youtube.com/vi/"+videoId+"/hqdefault.jpg"
            
        }
        else {
            return ""
        }
        
    }
}
