//
//  RechargeCardPopUpVC.swift
//  CinemaGhar
//
//  Created by Midas on 09/06/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
protocol RechargecardDelegate: class {
    func submitTap(cardNum: String?)
}

class RechargeCardPopUpVC: UIViewController {

    @IBOutlet weak var reviewFieldHeihgt: NSLayoutConstraint!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var submitBtn: AnimatedButton!
    @IBOutlet weak var rechargeTitle: UILabel!
    @IBOutlet weak var cardField: UITextField!
    @IBOutlet weak var fieldBackView: UIView!
    @IBOutlet weak var cancelbt: AnimatedButton!
    weak var delegate: RechargecardDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetup()
    }
    func uisetup() {
        reviewFieldHeihgt.constant = 0
        fieldBackView.layer.borderWidth = 0.75
        fieldBackView.layer.borderColor = UIColor.init(hex: "888888").cgColor
        fieldBackView.layer.cornerRadius = 4
        rechargeTitle.textColor = UIColor.init(hex: "888888")
        subtitle.textColor = UIColor.init(hex: "888888")
        cancelbt.backgroundColor = UIColor.init(hex: "888888")
        submitBtn.backgroundColor = UIColor.init(hex: "E83323")
    }
    
    static func instantiate() -> RechargeCardPopUpVC {
        return UIStoryboard(name: "MyAccount", bundle: nil).instantiateViewController(withIdentifier: "RechargeCardPopUpVC") as? RechargeCardPopUpVC ?? UIViewController() as! RechargeCardPopUpVC
    }
    @IBAction func cancelTap(_ sender: Any) {
        self.dismiss(animated: true)
    }
    @IBAction func submitTap(_ sender: Any) {
        delegate?.submitTap(cardNum: cardField.text ?? "")
        self.dismiss(animated: true, completion: nil)
    }
}
