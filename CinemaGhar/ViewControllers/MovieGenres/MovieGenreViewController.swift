//
//  MovieGenreViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 5/29/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import SDWebImage
import ViewAnimator
import Alamofire
import SwiftyJSON

class MovieGenreViewController: DesignableViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate, UICollectionViewDelegateFlowLayout, UINavigationBarDelegate{
    
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    
    private let spacing:CGFloat = 10

    @IBOutlet weak var navBar: UINavigationBar!
    
    private let animations = [AnimationType.identity]
    var height = CGFloat(0)
    
    
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.genreArray[self.selectedItemIndex].movies.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(10)
    }
    
    
    
    var nextPageURL:String = ""
    
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let numberOfItemsPerRow:CGFloat = 3
        let spacingBetweenCells:CGFloat = 10
        
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        
        let width = (collectionView.bounds.width - totalSpacing)/numberOfItemsPerRow
        
        
        
        
            //                layout.itemSize = CGSize(width: screenWidth/3-12, height: screenSize.height*0.38-80)

            
            return CGSize(width: width, height: width*1.6)


       
        
        
        
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieGenreCell", for: indexPath) as! MovieGenreCell
        cell.setMovieInfos(movie: self.genreArray[selectedItemIndex].movies[indexPath.row])
        return cell

    }

 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
        
        movieDetailScene.movieDetail =  genreArray[selectedItemIndex].movies[indexPath.row]
        self.navigationController?.pushViewController(movieDetailScene, animated: true)
        

        
    }
    
    var isFirstTime = true
    
    var shouldAnimate = true
  var curentIndex = 0
    
    var images : NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var movieCollectionView: UICollectionView!
    @IBOutlet weak var navItem: UINavigationItem!
    var genreArray = [Genre]()
    
     var genreCarouselView = iCarousel()
    var selectedItemIndex = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nibName = UINib(nibName: "MovieCell", bundle:nil)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        movieCollectionView.register(nibName, forCellWithReuseIdentifier: "MovieGenreCell")
        
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back_arrow"), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        button.sizeToFit()
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)
        self.navItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
        navBar.delegate = self
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        movieCollectionView.collectionViewLayout = layout
        
        
        
        
        genreCarouselView.type = iCarouselType.rotary

        movieCollectionView.parallaxHeader.view = genreCarouselView;
        
       genreCarouselView.bounces = true
        
        
        genreCarouselView.bounceDistance = CGFloat(4)
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            movieCollectionView.parallaxHeader.height = UIScreen.main.nativeBounds.height*0.2;
            height = UIScreen.main.nativeBounds.height*0.2
        }
        else {
            movieCollectionView.parallaxHeader.height = 230;
            height = 230
        }
        movieCollectionView.parallaxHeader.mode = .bottomFill;
        movieCollectionView.parallaxHeader.minimumHeight = 0;
        movieCollectionView.contentInset = UIEdgeInsetsMake(height+12, 0, 0, 0)
        
        movieCollectionView.infiniteScrollIndicatorView?.tintColor = UIColor.white
        
        movieCollectionView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        movieCollectionView.infiniteScrollIndicatorMargin = 50
        
        
        getAllGenres()

  
        
        

    }

    
  @objc func dismissVC(){
    self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getAllGenres(){
        indicatorView.startAnimating()
        APIHandler.shared.getAllGenres(success: {(movieGenreArray) in
            self.genreArray = movieGenreArray
                self.genreCarouselView.delegate = self
            self.genreCarouselView.dataSource = self
            self.genreCarouselView.reloadData()
            self.movieCollectionView.delegate = self
            self.movieCollectionView.dataSource = self
            self.indicatorView.stopAnimating()
            
            self.nextPageURL = self.genreArray[0].nextPageURL
            
            

            self.movieCollectionView?.addInfiniteScroll {  (movieCollectionView) -> Void in
                
                if self.nextPageURL != ""{
                    self.loadMoreDate()
                    
                }
                    
                else {
                    movieCollectionView.finishInfiniteScroll()
                    
                }
                
                
            }
            
            
            
        }, failure: {(failure) in
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.getAllGenres()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                    self.getAllGenres()
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                self.getAllGenres()
            }
            
        })
 
        
    }
    
    
    
    
    
    func loadMoreDate(){
        
        
        AF.request(nextPageURL, method:  .get , parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let value = response.value{
                    let json = JSON(value)
                    print(json)
                    if let moviesArray = json["data"].array {
                        
                        if moviesArray.count>0 {
                
                            let photoCount = self.genreArray[self.curentIndex].movies.count
                            
                            let (start, end) = (photoCount, moviesArray.count + photoCount)
                            let indexPaths = (start..<end).map { return IndexPath(row: $0, section: 0) }
                            
                            for i in 0..<moviesArray.count {
                                self.genreArray[self.curentIndex].movies.append(Movie(fromJson: moviesArray[i]))
                            }
                            

                            self.nextPageURL = json["links"]["next"].stringValue
                            
                            self.movieCollectionView.performBatchUpdates({ () -> Void in
                                self.movieCollectionView.insertItems(at: indexPaths)
                                
                            }, completion: { (finished) -> Void in
                                // finish infinite scroll animations
                                self.movieCollectionView.finishInfiniteScroll()
                            });
                            
                            
                        }
                        else {
                            
                            self.movieCollectionView.finishInfiniteScroll()
                        }
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                break
            }
            
        }
        
        
        
        
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    


}





extension MovieGenreViewController: iCarouselDelegate, iCarouselDataSource{
    func numberOfItems(in carousel: iCarousel) -> Int {
        return self.genreArray.count
    }

    
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {

        var itemView: UIImageView
        if (view == nil)
        {
            itemView = UIImageView(frame:CGRect(x:0, y:0, width:self.view.frame.width/1.4, height:height))
            itemView.contentMode = .scaleAspectFill
            itemView.layer.cornerRadius = 5
            itemView.clipsToBounds = true
        }
        else
        {
            itemView = view as! UIImageView;
        }

        if let genreImageURL = self.genreArray[index].icon{
        itemView.sd_setImage(with: URL(string: genreImageURL), placeholderImage: UIImage(named: "placeholder_carousel"), options: [.scaleDownLargeImages])
        
        }
      

        return itemView
    }
    
    
    
    func carouselDidEndDragging(_ carousel: iCarousel, willDecelerate decelerate: Bool) {
        
        if selectedItemIndex != carousel.currentItemIndex {
            carouselDidEndDecelerating(carousel)
        }
        
    }
    

    
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
        
        isFirstTime = false
        self.selectedItemIndex = index
        movieCollectionView.reloadData()
        
        if isFirstTime == false {
            
            if curentIndex != index {
            
            self.movieCollectionView.performBatchUpdates({
                UIView.animate(views: self.movieCollectionView.visibleCells,
                               animations: self.animations, completion: {
                })
            }, completion: nil)
            
            curentIndex = index
                self.nextPageURL = self.genreArray[curentIndex].nextPageURL

                
            }
            
            if let category = self.genreArray[index].name{
                self.navItem.title = category
            }
            
        }
    }
    
    func carouselDidEndDecelerating(_ carousel: iCarousel) {
        selectedItemIndex = carousel.currentItemIndex
            movieCollectionView.reloadData()
            self.movieCollectionView.performBatchUpdates({
                UIView.animate(views: self.movieCollectionView.visibleCells,
                               animations: self.animations, completion: {
                })
            }, completion: nil)
            curentIndex = selectedItemIndex
            self.nextPageURL = self.genreArray[curentIndex].nextPageURL


            if let category = self.genreArray[carousel.currentItemIndex].name{
            self.navItem.title = category
       
        }
        
        
    }
    }
    
    
    
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        print("scroll ended")
    
    
        
        
}

extension UIView {
    func slideX(x:CGFloat) {
        let yPosition = self.frame.origin.y
        let height = self.frame.height
        let width = self.frame.width
        UIView.animate(withDuration: 1.0, animations: {
            self.frame = CGRect(x: x, y: yPosition, width: width, height: height)
        })
    }
}


