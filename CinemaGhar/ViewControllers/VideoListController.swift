////
////  VideoListController.swift
////  VidLoaderExample
////
////  Created by Petre on 14.10.19.
////  Copyright © 2019 Petre. All rights reserved.
////
//
//import UIKit
//import AVKit
//
//class VideoListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
//
//    @IBOutlet weak var removeAllBtn: UIButton!
//    @IBOutlet weak var navView: UIView!
//    @IBOutlet weak var backBtn: AnimatedButton!
//    @IBOutlet weak var table: UITableView!
//
//    var dataProvider: VideoListDataProviding!
//    var movideDetail: Movie?
//    var videoUrl: String?
//    var showAllDownloads: Bool = false
//    var editPressed = false
//    private let fileManager: FileManager = .default
//    private let userDefaults: UserDefaults = .standard
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
////        backBtn.tintColor = AppColors.darkColor
//        removeAllBtn.backgroundColor = AppColors.darkRedColor
//        removeAllBtn.layer.cornerRadius = 12.5
//        backBtn.backgroundColor = .white
//        if showAllDownloads == true {
//            dataProvider.downloadvideoData(withd: VideoData(identifier: "offlineDownloads", title: "", imageName: "", state: .unknown, stringURL: "", location: URL(string: ""), coverImage: "",duration: "",year: ""))
//        } else {
//            dataProvider.downloadvideoData(withd: VideoData(identifier: "\(movideDetail?.id ?? 0)", title: movideDetail?.title ?? "", imageName: "", state: .unknown, stringURL: videoUrl ?? "", location: URL(string: ""), coverImage: movideDetail?.coverImage ?? "",duration: movideDetail?.duration ?? "", year: movideDetail?.releaseYear ?? ""))
//        }
//
//        setup()
//        //checkForDownloadList()
//
//
//    }
//    func checkForDownloadList() {
//        guard let data = userDefaults.value(forKey: "vid_loader_example_items") as? Data,
//              let items = try? JSONDecoder().decode([VideoData].self, from: data) else {
////                let items = generateDefaultItems()
////                save(items: items)
////                return items
//                  removeAllBtn.isHidden = false
//                  return
//        }
//        removeAllBtn.isHidden = true
//    }
//    @IBAction func backTap(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
//    }
//    @IBAction func editTap(_ sender: Any) {
////        editPressed = true
////        showEditFeature()
//        showAlert()
//    }
//    func showAlert() {
//        // create the alert
//              let alert = UIAlertController(title: "Are you sure?", message: "Do you want to delete all your downloads?", preferredStyle: UIAlertController.Style.alert)
//
//              // add an action (button)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {_ in
//            self.deleteAllDownloads()
//        }))
//        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {_ in
//            self.dismiss(animated: true, completion: {
//                self.dismiss(animated: true, completion: nil)
//            })
//        }))
//
//              // show the alert
//              self.present(alert, animated: true, completion: nil)
//    }
//
////MARK: - DELETE ALL VIDEOS
//    func deleteAllDownloads() {
////        guard let data = userDefaults.value(forKey: Constant.downloadsKey) as? Data,
////              let items = try? JSONDecoder().decode([VideoData].self, from: data) else {
////              return
////
////        }
////        for item in items {
////            if let itemlocation = item.location {
////                try? fileManager.removeItem(at: itemlocation)
////            }
////        }
////
////        userDefaults.set(Data(), forKey: Constant.downloadsKey)
//        dataProvider.editbtnPresed()
//       // table.reloadData()
//
//
//
//
//    }
//    // MARK: - UITableViewDataSource
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if dataProvider.items.count > 0 {
//             table.restore()
//            removeAllBtn.isHidden = false
//        } else {
//            removeAllBtn.isHidden = true
//            table.setEmptyMessage("No downloads yet")
//        }
//        return dataProvider.items.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: VideoCell.identifier, for: indexPath)
//
//
//        (cell as? VideoCell)?.setup(model: dataProvider.videoModel(row: indexPath.row))
//        (cell as? VideoCell)?.contentView.backgroundColor = AppColors.darkColor
//        (cell as? VideoCell)?.layer.backgroundColor = AppColors.darkColor.cgColor
//
//        return cell
//    }
//
//    // MARK: - UITableViewDelegate
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 113
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        guard let urlAsset = dataProvider.urlAsset(row: indexPath.row) else { return }
//
//       // startVideo(urlAsset: urlAsset)
//        let vc = CustomVideoViewController.instantiate(fromAppStoryboard: .CustomVideoPlayer)
//        vc.playOffline = true
//        vc.offlineItem = AVPlayerItem(asset: urlAsset)
//        self.navigationController?.pushViewController(vc, animated: true)
//
//    }
//
//    // MARK: - Private functions
//
//    private func startVideo(urlAsset: AVURLAsset) {
//        let item = AVPlayerItem(asset: urlAsset)
//        let player = AVPlayer(playerItem: item)
//        let controller = AVPlayerViewController()
//        controller.player = player
//        navigationController?.pushViewController(controller, animated: true)
//        player.play()
//    }
//
//    private func setup() {
//        navView.backgroundColor = AppColors.darkColor
//        backBtn.backgroundColor = AppColors.darkColor
//        table.backgroundColor = AppColors.darkColor
//        table.layer.backgroundColor = AppColors.darkColor.cgColor
//        table.delegate = self
//        table.dataSource = self
//        table.register(UINib(nibName: VideoCell.identifier, bundle: nil),
//                       forCellReuseIdentifier: VideoCell.identifier)
//        let footerView = UIView()
//        footerView.backgroundColor = AppColors.darkColor
//        table.tableFooterView = footerView
//
//        let videoListActions = VideoListActions(
//            reloadData: { [weak self] in self?.table.reloadData() },
//            showRemoveActionSheet: { [weak self] data in self?.showRemoveActionsSheet(with: data) },
//            showStopActionSheet: { [weak self] data in self?.showStopActionsSheet(with: data) },
//            showFailedActionSheet: { [weak self] data in self?.showFailedActionSheet(with: data) },
//            showRunningActions: { [weak self] data in self?.showRunningActions(with: data) },
//            showPausedActions: { [weak self] data in self?.showPausedActions(with: data) })
//        dataProvider.setup(videoListActions: videoListActions)
//    }
//
//    private func showRemoveActionsSheet(with data: VideoData) {
//        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { [weak self] _ in
//            self?.dataProvider.deleteVideo(with: data)
//            self?.table.reloadData()
//        }))
//        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
//        present(actionSheet, animated: true)
//    }
//
//    private func showStopActionsSheet(with data: VideoData) {
//        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        actionSheet.addAction(UIAlertAction(title: "Stop", style: .destructive, handler: { [weak self] _ in
//            self?.dataProvider.stopDownload(with: data)
//        }))
//        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
//        present(actionSheet, animated: true)
//    }
//
//    private func showFailedActionSheet(with data: VideoData) {
//        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { [weak self] _ in
//            self?.dataProvider.deleteVideo(with: data)
//            self?.table.reloadData()
//        }))
//        actionSheet.addAction(UIAlertAction(title: "Retry", style: .default, handler: { [weak self] _ in
//            self?.dataProvider.startDownload(with: data)
//        }))
//        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
//        present(actionSheet, animated: true)
//    }
//
//    private func showRunningActions(with data: VideoData) {
//        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        actionSheet.addAction(UIAlertAction(title: "Stop", style: .destructive, handler: { [weak self] _ in
//            self?.dataProvider.stopDownload(with: data)
//        }))
//        actionSheet.addAction(UIAlertAction(title: "Pause", style: .destructive, handler: { [weak self] _ in
//            self?.dataProvider.pauseDownload(with: data)
//        }))
//        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
//        present(actionSheet, animated: true)
//    }
//
//    private func showPausedActions(with data: VideoData) {
//        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        actionSheet.addAction(UIAlertAction(title: "Stop", style: .destructive, handler: { [weak self] _ in
//            self?.dataProvider.stopDownload(with: data)
//        }))
//        actionSheet.addAction(UIAlertAction(title: "Resume", style: .destructive, handler: { [weak self] _ in
//            self?.dataProvider.resumeDownload(with: data)
//        }))
//        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
//        present(actionSheet, animated: true)
//    }
//    @IBAction func btnTap(_ sender: Any) {
////        let vc = UIStoryboard.init(name: "VideoListController", bundle: Bundle.main).instantiateViewController(withIdentifier: "DemoVC") as? DemoVC
////        self.navigationController?.pushViewController(vc!, animated: true)
//    }
//}
//
//
//  VideoListController.swift
//  VidLoaderExample
//
//  Created by Petre on 14.10.19.
//  Copyright © 2019 Petre. All rights reserved.
//

import UIKit
import AVKit
import Alamofire

class VideoListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var removeAllBtn: UIButton!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var backBtn: AnimatedButton!
    @IBOutlet weak var table: UITableView!
    
    var dataProvider: VideoListDataProviding!
    var movideDetail: Movie?
    var videoUrl: String?
    var showAllDownloads: Bool = false
    var editPressed = false
    private let fileManager: FileManager = .default
    private let userDefaults: UserDefaults = .standard
    

    override func viewDidLoad() {
        super.viewDidLoad()
//        backBtn.tintColor = AppColors.darkColor
       // removeAllBtn.backgroundColor = AppColors.darkRedColor
        removeAllBtn.layer.cornerRadius = 12.5
        backBtn.backgroundColor = .white
        if showAllDownloads == true {
            dataProvider.downloadvideoData(withd: VideoData(identifier: "offlineDownloads", title: "", imageName: "", state: .unknown, stringURL: "", location: URL(string: ""), coverImage: "",duration: "",year: ""))
        } else {
            dataProvider.downloadvideoData(withd: VideoData(identifier: "\(movideDetail?.id ?? 0)", title: movideDetail?.title ?? "", imageName: "", state: .unknown, stringURL: videoUrl ?? "", location: URL(string: ""), coverImage: movideDetail?.coverImage ?? "",duration: movideDetail?.duration ?? "", year: movideDetail?.releaseYear ?? ""))
        }
       
        setup()
        //checkForDownloadList()
       
       
    }
    func checkForDownloadList() {
        guard let data = userDefaults.value(forKey: "vid_loader_example_items") as? Data,
              let items = try? JSONDecoder().decode([VideoData].self, from: data) else {
//                let items = generateDefaultItems()
//                save(items: items)
//                return items
                  removeAllBtn.isHidden = false
                  return
        }
        removeAllBtn.isHidden = true
    }
    @IBAction func backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func editTap(_ sender: Any) {
//        editPressed = true
//        showEditFeature()
        showAlert()
    }
    func showAlert() {
        // create the alert
              let alert = UIAlertController(title: "Are you sure?", message: "Do you want to delete all your downloads?", preferredStyle: UIAlertController.Style.alert)

              // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {_ in
            self.deleteAllDownloads()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {_ in
            self.dismiss(animated: true, completion: {
                self.dismiss(animated: true, completion: nil)
            })
        }))

              // show the alert
              self.present(alert, animated: true, completion: nil)
    }
    
//MARK: - DELETE ALL VIDEOS
    func deleteAllDownloads() {
//        guard let data = userDefaults.value(forKey: Constant.downloadsKey) as? Data,
//              let items = try? JSONDecoder().decode([VideoData].self, from: data) else {
//              return
//
//        }
//        for item in items {
//            if let itemlocation = item.location {
//                try? fileManager.removeItem(at: itemlocation)
//            }
//        }
//
//        userDefaults.set(Data(), forKey: Constant.downloadsKey)
        dataProvider.editbtnPresed()
       // table.reloadData()
        
        
       
        
    }
    // MARK: - UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataProvider.items.count > 0 {
             table.restore()
            removeAllBtn.isHidden = false
        } else {
            removeAllBtn.isHidden = true
            table.setEmptyMessage("No downloads yet")
        }
        return dataProvider.items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VideoCell.identifier, for: indexPath)
      
        
        (cell as? VideoCell)?.setup(model: dataProvider.videoModel(row: indexPath.row))
        (cell as? VideoCell)?.contentView.backgroundColor = AppColors.darkColor
        (cell as? VideoCell)?.layer.backgroundColor = AppColors.darkColor.cgColor

        return cell
    }

    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 113
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let urlAsset = dataProvider.urlAsset(row: indexPath.row) else { return }
        print(urlAsset)
       // startVideo(urlAsset: urlAsset)
        let vc = CustomVideoViewController.instantiate(fromAppStoryboard: .CustomVideoPlayer)
        vc.playOffline = true
        vc.offlineItem = AVPlayerItem(asset: urlAsset)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    // MARK: - Private functions

    private func startVideo(urlAsset: AVURLAsset) {
        let item = AVPlayerItem(asset: urlAsset)
        let player = AVPlayer(playerItem: item)
        let controller = AVPlayerViewController()
        controller.player = player
        navigationController?.pushViewController(controller, animated: true)
        player.play()
    }

    private func setup() {
        navView.backgroundColor = AppColors.darkColor
        backBtn.backgroundColor = AppColors.darkColor
        table.backgroundColor = AppColors.darkColor
        table.layer.backgroundColor = AppColors.darkColor.cgColor
        table.delegate = self
        table.dataSource = self
        table.register(UINib(nibName: VideoCell.identifier, bundle: nil),
                       forCellReuseIdentifier: VideoCell.identifier)
        let footerView = UIView()
        footerView.backgroundColor = AppColors.darkColor
        table.tableFooterView = footerView
        
        let videoListActions = VideoListActions(
            reloadData: { [weak self] in self?.table.reloadData() },
            showRemoveActionSheet: { [weak self] data in self?.showRemoveActionsSheet(with: data) },
            showStopActionSheet: { [weak self] data in self?.showStopActionsSheet(with: data) },
            showFailedActionSheet: { [weak self] data in self?.showFailedActionSheet(with: data) },
            showRunningActions: { [weak self] data in self?.showRunningActions(with: data) },
            showPausedActions: { [weak self] data in self?.showPausedActions(with: data) })
        dataProvider.setup(videoListActions: videoListActions)
    }

    private func showRemoveActionsSheet(with data: VideoData) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.deleteVideo(with: data)
            self?.table.reloadData()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(actionSheet, animated: true)
    }

    private func showStopActionsSheet(with data: VideoData) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Stop", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.stopDownload(with: data)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(actionSheet, animated: true)
    }
    
    private func showFailedActionSheet(with data: VideoData) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.deleteVideo(with: data)
            self?.table.reloadData()
        }))
        actionSheet.addAction(UIAlertAction(title: "Retry", style: .default, handler: { [weak self] _ in
            self?.dataProvider.startDownload(with: data)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(actionSheet, animated: true)
    }
    
    private func showRunningActions(with data: VideoData) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Stop", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.stopDownload(with: data)
        }))
        actionSheet.addAction(UIAlertAction(title: "Pause", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.pauseDownload(with: data)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(actionSheet, animated: true)
    }
    
    private func showPausedActions(with data: VideoData) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Stop", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.stopDownload(with: data)
        }))
        actionSheet.addAction(UIAlertAction(title: "Resume", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.resumeDownload(with: data)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(actionSheet, animated: true)
    }
    @IBAction func btnTap(_ sender: Any) {
//        let vc = UIStoryboard.init(name: "VideoListController", bundle: Bundle.main).instantiateViewController(withIdentifier: "DemoVC") as? DemoVC
//        self.navigationController?.pushViewController(vc!, animated: true)
    }
}

