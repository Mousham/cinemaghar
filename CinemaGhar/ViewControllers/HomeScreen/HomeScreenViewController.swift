//
//  HomeScreenViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 5/15/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import SDWebImage
import Spring
import Alamofire
import SwiftyJSON
import SwiftyGif
import SystemConfiguration
import FirebaseMessaging
//import Firebasemes


protocol RefreshCollectionViews {

    func refreshCollectionViews()
    
}


class HomeScreenViewController: DesignableViewController {
    @IBAction func notificationsBtnPressed(_ sender: Any) {
        
        let notificationsVC = NotificationsViewController.instantiate(fromAppStoryboard: .Notifications)
        self.navigationController?.pushViewController(notificationsVC, animated: true)
        
    }
    @IBAction func retryBtnPressed(_ sender: Any) {

            noConnectivityView.isHidden = true
//         DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2, execute: {
            self.getFeaturedMovies()

//         })
        
        
    }
    
    var sessionManager = SessionManager()
    @IBOutlet weak var latestMovieCollectionView: UICollectionView!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var noConnectivityView: UIView!
    
//    @IBOutlet weak var comedySeriesCollectionView: UICollectionView!
    @IBOutlet weak var latestMovieContainerView: CardView!
    @IBOutlet weak var seriesContainerView: CardView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBAction func searchBtnPressed(_ sender: Any) {
        let searchVC = SearchViewController.instantiate(fromAppStoryboard: .Search)
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    @IBAction func newsBtnPressed(_ sender: Any){
        let newsVC = NewsFeedViewController.instantiate(fromAppStoryboard: .NewsFeed)
        self.navigationController?.pushViewController(newsVC, animated: true)
    }

    var productIdentifiers = [String]()
    @IBOutlet weak var exclusiveMovieTopMargin: NSLayoutConstraint!
    @IBAction func seeAllBtnPressed(_ sender: Any) {
        guard let button = sender as? UIButton else {
            return
        }
        let allMovieVC = AllMoviesViewController.instantiate(fromAppStoryboard: .AllMovies)
        
        
        switch  button.tag {
        case 1:
            
//            allMovieVC.contentType = .movie
//            allMovieVC.moviesArr = self.exclusiceMovieArray
//            allMovieVC.movieType = .exclusive
//
//            allMovieVC.titleLabel = "Exclusive"
//
//            if self.exclusiceMovieArray.count>0{
//                self.navigationController?.pushViewController(allMovieVC, animated: true)
//
//            }
           // let goldVC = CineGoldViewController.instantiate(fromAppStoryboard: .CineGold)
            let goldVC = HomeVC.instantiate(fromAppStoryboard: .CineGold)
            self.navigationController?.pushViewController(goldVC, animated: true)
            
            
        case 2:
            
            allMovieVC.contentType = .movie
            allMovieVC.moviesArr = self.latestMovieArray
            allMovieVC.movieType = .latest
            allMovieVC.titleLabel = "Free"
            if self.latestMovieArray.count>0{
                self.navigationController?.pushViewController(allMovieVC, animated: true)
            }
            
            
            
        case 3:
            
            allMovieVC.contentType = .series
            allMovieVC.seriesArr = self.webSeriesArray
            
            allMovieVC.titleLabel = "Web Series/Shorts"
            
            if self.webSeriesArray.count>0{
                self.navigationController?.pushViewController(allMovieVC, animated: true)
                
            }
            
            print("3")
        default:
            print("none")
            return
        }
        
        
    }
    
    @IBOutlet weak var koldaViewTopMargin: NSLayoutConstraint!
    @IBOutlet weak var curveImage: UIImageView!
    
    @IBOutlet weak var exclusiveMovieContainerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var curveImageHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tvSeriesCollectionView: UICollectionView!
    @IBOutlet weak var roundMenuLeadingMargin: NSLayoutConstraint!
    @IBOutlet weak var roundMenuTopMargin: NSLayoutConstraint!
//    @IBOutlet weak var comedyContainerView: CardView!
    @IBOutlet weak var overLayView: UIView!
    @IBOutlet weak var roundMenuButton: XXXRoundMenuButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var exclusiveMovieContainerView: SpringView!
    @IBOutlet weak var koldaViewContainer: SpringView!
    @IBOutlet weak var exclusiveMovieCollectionView: UICollectionView!
    @IBOutlet weak var featuredMovieKoldaView: KolodaView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    var featuredMovieSliderCount:Int = 0
    private let spacing:CGFloat = 10
    
    
    var movieSliderArray = [MovieSlider]()
    var exclusiceMovieArray = [Movie]()
    var webSeriesArray = [Series]()
    
    var comedySeriesArray = [Series]()

    
    
    var latestMovieArray = [Movie]()
    let logoAnimationView = LogoAnimationView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkIfAppIsBlocked()
        if Messaging.messaging().fcmToken != nil {
            Messaging.messaging().subscribe(toTopic: "anonymous")
        }
        
//        view.addSubview(logoAnimationView)
//        logoAnimationView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
//        logoAnimationView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
//        logoAnimationView.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
//        logoAnimationView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 50).isActive = true
//        
//        logoAnimationView.translatesAutoresizingMaskIntoConstraints = false
//        
//        logoAnimationView.imageView.delegate = self
        
    }
    
    func loadUpApp(){
        NotificationCenter.default.addObserver(self, selector: #selector(SomeNotificationAct), name: Notification.Name(rawValue: "SomeNotification"), object: nil)
        registerFCMToken()

        
        customizeViews()
        
        
        self.getFeaturedMovies()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        logoAnimationView.imageView.startAnimatingGif()
    }
    
    @objc func SomeNotificationAct(notification:Notification){
        
        print ("open another viewcontroller")
        
        if let myDict = notification.object as? [String: Any] {
           
            if let type = myDict["type"] as? String, let id = myDict["id"] as? String {
                
                if(type == "news"){
                    
                    let notificationsVC = NotificationsViewController.instantiate(fromAppStoryboard: .Notifications)
                    
                    self.navigationController?.pushViewController(notificationsVC, animated: true)
                }else if(type == "movies"){
                    
                    let movieDetailVC = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
                    movieDetailVC.contentID = id
                    self.navigationController?.pushViewController(movieDetailVC, animated: true)
                } else if(type == "series"){
                    
                    let seriesDetail = SeriesDetailViewController.instantiate(fromAppStoryboard: .SeriesDetail)
                    seriesDetail.seriesID = id
                    self.navigationController?.pushViewController(seriesDetail, animated: true)
                } else if (type == "downloads") {
                    let vc = VideoListController.instantiate(fromAppStoryboard: .MovieDetail)
                    vc.showAllDownloads = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
    }
    
    
    
    func customizeViews(){
        
   
        self.navigationBar.backgroundColor = UIColor.clear
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.isTranslucent = true
        
        
        
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        overLayView.addGestureRecognizer(tap)
        
        featuredMovieKoldaView.dataSource = self
        featuredMovieKoldaView.delegate = self
        featuredMovieKoldaView.countOfVisibleCards = 3
        featuredMovieKoldaView.isLoop = true
        
        let nibName = UINib(nibName: "ExclusiveMovieCollectionViewCell", bundle:nil)
        let nibLatestMovie = UINib(nibName: "LatestMovieCollectionViewCell", bundle:nil)
        exclusiveMovieCollectionView.register(nibName, forCellWithReuseIdentifier: "ExclusiveMovieCollectionViewCell")
        latestMovieCollectionView.register(nibLatestMovie, forCellWithReuseIdentifier: "LatestMovieCollectionViewCell")
        tvSeriesCollectionView.register(UINib(nibName: "WebSeriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WebSeriesCollectionViewCell")
        
//        comedySeriesCollectionView.register(UINib(nibName: "ComedySeriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ComedySeriesCollectionViewCell")
        
        
        
        exclusiveMovieCollectionView.delegate = self
        exclusiveMovieCollectionView.dataSource = self
        latestMovieCollectionView.delegate = self
        latestMovieCollectionView.dataSource = self
        tvSeriesCollectionView.delegate = self
        tvSeriesCollectionView.dataSource = self
        
//        comedySeriesCollectionView.delegate = self
//        comedySeriesCollectionView.dataSource = self
        
        
        
        self.view.layoutIfNeeded()
        //
        exclusiveMovieContainerView.addDropShadow(offset: CGSize(width: 0, height: 5  ), color: UIColor.black   , radius: 2, opacity: 0.8)
        
        
        exclusiveMovieContainerView.layer.masksToBounds =  false
        exclusiveMovieContainerView.roundCorners([.bottomRight, .bottomLeft], [.layerMaxXMaxYCorner, .layerMinXMaxYCorner],  radius: 6)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkMenuStatus(notification:)), name: Notification.Name(rawValue: "menuStatus"), object: nil)
        
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            self.koldaViewTopMargin.constant = 25
            
            if UIScreen.main.nativeBounds.height >= 2732 {
                self.exclusiveMovieTopMargin.constant = 25
                
            }
                
            else if UIScreen.main.nativeBounds.height == 2224 {
                self.exclusiveMovieTopMargin.constant = 20
                
            }
                
            else if UIScreen.main.nativeBounds.height == 2048 {
                self.exclusiveMovieTopMargin.constant = 16
                
            }
            
        }
        
        
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
            self.exclusiveMovieContainerHeightConstraint.constant = self.exclusiveMovieContainerHeightConstraint.constant-36
            
        }
        
        
        
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.minimumLineSpacing = spacing
        layout.minimumInteritemSpacing = spacing
        layout.scrollDirection = .horizontal
        self.exclusiveMovieCollectionView.collectionViewLayout = layout
        
        
        let layout1 = UICollectionViewFlowLayout()
        layout1.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout1.minimumLineSpacing = spacing
        layout1.minimumInteritemSpacing = spacing
        layout1.scrollDirection = .horizontal
        self.latestMovieCollectionView.collectionViewLayout = layout1
        
        let layout2 = UICollectionViewFlowLayout()
        layout2.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout2.minimumLineSpacing = spacing
        layout2.minimumInteritemSpacing = spacing
        layout2.scrollDirection = .horizontal
        self.tvSeriesCollectionView.collectionViewLayout = layout2
        
        
        
//        let layout3 = UICollectionViewFlowLayout()
//        layout3.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
//        layout3.minimumLineSpacing = spacing
//        layout3.minimumInteritemSpacing = spacing
//        layout3.scrollDirection = .horizontal
//        self.comedySeriesCollectionView.collectionViewLayout = layout3
        
        
        // MARK: Round Menu Customization
        
        roundMenuButton.centerButtonSize = CGSize(width: 40, height: 40);
        roundMenuButton.tintColor = UIColor.white
        roundMenuButton.jumpOutButtonOnebyOne = true;
    
        roundMenuButton.centerIconType = .userDraw
        self.roundMenuButton.mainColor = UIColor.clear
        self.roundMenuLeadingMargin.constant = -125
        
        
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax || UIDevice.current.screenType == .unknown  {
            self.roundMenuTopMargin.constant = -85
        }else {
            self.roundMenuTopMargin.constant = -108
            
        }
        
        roundMenuButton.drawCenterButtonIconBlock = {(_ rect: CGRect, _ state: UIControlState) -> Void in
            
            if state == .normal {
                let rectanglePath = UIBezierPath(rect: CGRect(x: (rect.size.width - 15) / 2, y: rect.size.height / 2 - 7, width: 15, height: 2))
                UIColor.white.setFill()
                rectanglePath.fill()
                let rectangle2Path = UIBezierPath(rect: CGRect(x: (rect.size.width - 15) / 2, y: rect.size.height / 2, width: 20, height: 2))
                UIColor.white.setFill()
                rectangle2Path.fill()
                let rectangle3Path = UIBezierPath(rect: CGRect(x: (rect.size.width - 15) / 2, y: rect.size.height / 2 + 7, width: 15, height: 2))
                UIColor.white.setFill()
                rectangle3Path.fill()
            }
            
            
            
            
        }
        //UIImage(named: "icons8-settings")!
        roundMenuButton.load(withIcons: [UIImage(named: "icons8-news")!,UIImage(named: "theatre")!,UIImage(named: "genre")!, UIImage(named: "icons8-settings")!], startDegree: Float(.pi/1.94), layoutDegree: Float(-.pi/1.9), withIconDescription: [
            "My Account",
            "My Movies",
            "Genres",
            "Settings"
            ])
        
        
        roundMenuButton.buttonClickBlock =  {(idx:NSInteger)-> Void in
            self.overLayView.isHidden = true
            self.roundMenuButton.mainColor = UIColor.clear
            self.roundMenuLeadingMargin.constant = -125
            
            if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax || UIDevice.current.screenType == .unknown   {
                self.roundMenuTopMargin.constant = -85
            }
            else {
                self.roundMenuTopMargin.constant = -108
                
            }
            
            switch idx {
            case 0:
                if (self.sessionManager.isLoggedIn()){
//                    let accountVC = MyAccountViewController.instantiate(fromAppStoryboard: .MyAccount)
                    let accountVC = MyAccountVC.instantiate(fromAppStoryboard: .MyAccount)
                    self.navigationController?.pushViewController(accountVC, animated: true)

                }
                else{
                    let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                    self.navigationController?.pushViewController(loginVC, animated: true)
                }
                break
            case 1:
                
                if (self.sessionManager.isLoggedIn()){
                    let theatreVC = TheatreViewController.instantiate(fromAppStoryboard: .Theatre)
                    self.navigationController?.pushViewController(theatreVC, animated: true )
                }else{
                    let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                    self.navigationController?.pushViewController(loginVC, animated: true)
                }
                break
//            case 2:
//
//                if (self.sessionManager.isLoggedIn()){
//                    let accountVC = MyAccountViewController.instantiate(fromAppStoryboard: .MyAccount)
//                    self.navigationController?.pushViewController(accountVC, animated: true)}
//                else{
//                    let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
//                    self.navigationController?.pushViewController(loginVC, animated: true)
//                }
//                break
                
            case 2:
                let genreVC = MovieGenreViewController.instantiate(fromAppStoryboard: .MovieGenre)
                self.navigationController?.pushViewController(genreVC, animated: true)
                
                break
                
                
            case 3:
                let settingsVC = SettingsViewController.instantiate(fromAppStoryboard: .Settings)
                
                settingsVC.delegate = self
                
                self.navigationController?.pushViewController(settingsVC, animated: true)
                
                
                break
                
                
                
            default:
                print("no index")
            }
        };
        
        
        
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        
        
        self.overLayView.isHidden = true
        self.roundMenuButton.isOpened = false
        self.roundMenuButton.setSelected(false)
        roundMenuButton.isSelected = false
        self.roundMenuButton.mainColor = UIColor.clear
        roundMenuButton.buttonClick(self.roundMenuButton)
        self.roundMenuLeadingMargin.constant = -125
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax || UIDevice.current.screenType == .unknown   {
            self.roundMenuTopMargin.constant = -85
        }
        else {
            self.roundMenuTopMargin.constant = -108
        }
        self.view.layoutIfNeeded()
    }
    
    
    
    
    
    @objc func checkMenuStatus(notification: NSNotification) {
        if let menuStatus = notification.userInfo?["isMenuOpen"] as? Bool {
            
            if menuStatus {
                overLayView.isHidden = false
                self.roundMenuLeadingMargin.constant = -115
                
                if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax  || UIDevice.current.screenType == .unknown  {
                    self.roundMenuTopMargin.constant = -85
                }
                else {
                    self.roundMenuTopMargin.constant = -104
                    
                }
                self.view.layoutIfNeeded()
                
            }
            else {
                self.roundMenuLeadingMargin.constant = -125
                
                if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax || UIDevice.current.screenType == .unknown   {
                    self.roundMenuTopMargin.constant = -85
                }
                else {
                    self.roundMenuTopMargin.constant = -108
                    
                }
                self.view.layoutIfNeeded()
                overLayView.isHidden = true
            }
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func registerFCMToken(){
        
        if let token = UserDefaults.standard.string(forKey: "FirebaseToken"){
            
            if !UserDefaults.standard.bool(forKey: "isFCMTokenSentToServer"){
                
                
                APIHandler.shared.registerFCMToken(fcmToken: token, success: {(status) in
                    
                    
                    
                    if status {
                        UserDefaults.standard.set(true, forKey: "isFCMTokenSentToServer")
                        
                    }
                    
                    
                }, error: {(error) in
                    
                    
                    
                    
                    
                    
                })
                
            }
            
            
        }
        
        
    }
    
    
    
    
    @IBOutlet weak var latestMoviePlaceHolderView: UIStackView!
    @IBOutlet weak var exclusivePlaceHolderVIew: UIStackView!
    
    func checkIfAppIsBlocked(){
        loadingIndicator.startAnimating()
        self.loadUpApp()
        APIHandler.shared.checkIfAppIsBlocked(success: {
            (status, message) in
            print("sawsank: \(message)")
            if(status){
                self.loadingIndicator.stopAnimating()
                let controller = UIAlertController(title: "Notice", message: message, preferredStyle: .alert)
                let ok = UIAlertAction(title: "Okay", style: .cancel, handler: { (action) -> Void in
                    UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
                    self.loadUpApp()

                })
                controller.addAction(ok)
                self.present(controller, animated: true, completion: nil)
            }else{
                self.loadingIndicator.stopAnimating()
                self.loadUpApp()
            }
        }) { (failure) in
            print("Checking app block status: failure: " + failure)
        } error: { (error) in
            print("Checking app block status: error: " + error.localizedDescription)
        }
    }
    
    func getFeaturedMovies(){
        loadingIndicator.startAnimating()

        APIHandler.shared.getSliderData(success:{
            (status, movieSliderArr) in
            
            self.movieSliderArray = movieSliderArr
            
            self.featuredMovieKoldaView.reloadData()
            self.loadingIndicator.stopAnimating()

            
            self.getExclusiveMovies()

            
        }, failure: {(failure ) in
            
            self.loadingIndicator.stopAnimating()
            
            
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "Network Error", message: "Failed to connect to Cinemaghar. Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                    self.noConnectivityView.isHidden = false
                    
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.noConnectivityView.isHidden = true

                    self.getFeaturedMovies()
                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                    self.getFeaturedMovies()
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                self.getFeaturedMovies()
                
            }
            
            
            
            
        })
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
    func getLatestMovies(){
        APIHandler.shared.getMovies(movieType: .free, success: {(status, latestMovies, nextPage) in // latest movies
            self.latestMovieArray = latestMovies
            self.latestMovieCollectionView.reloadData()
            self.latestMoviePlaceHolderView.isHidden = true
            self.getWebSeries()
            
        
        }, failure: {(error)  in
            
            
            if (error._code == NSURLErrorNetworkConnectionLost || error._code == NSURLErrorTimedOut ){
                
                self.getLatestMovies()
                
            }
           
            
        }
        )
        
    }
    
    func getWebSeries(){
        
        APIHandler.shared.getWebSeries(success: {(webSeries) in
            self.webSeriesArray = webSeries
            
            
            self.tvSeriesCollectionView.reloadData()
            self.seriesContainerView.isHidden = false
            
            self.getComedySeries()
            
        }, failure: {(error) in
            
            if (error._code == NSURLErrorNetworkConnectionLost || error._code == NSURLErrorTimedOut ){
                
                self.getWebSeries()
                
            }
            
        })
    }
    
    
    
    
    
    func getComedySeries(){
        
        APIHandler.shared.getComedyWebSeries(success: {(webSeries) in
            self.comedySeriesArray = webSeries
            
            
//            self.comedySeriesCollectionView.reloadData()
//            self.comedyContainerView.isHidden = false
            
        }, failure: {(error) in
            
            if (error._code == NSURLErrorNetworkConnectionLost || error._code == NSURLErrorTimedOut ){
                
                self.getComedySeries()
                
            }
            
        })
        
    }
    
    
    
    func getExclusiveMovies(){
        exclusiveMovieContainerView.isHidden = false
        latestMovieContainerView.isHidden = false
        APIHandler.shared.getSubscriptionData(success: {(status, exclusiveMovies, nextPageid ) in // exclusive movies
            self.exclusiceMovieArray = exclusiveMovies
            self.exclusiveMovieCollectionView.reloadData()
            self.exclusivePlaceHolderVIew.isHidden = true

            
            self.getLatestMovies()

        
            
        }, failure: {(error) in
            
            
            
            if (error._code == NSURLErrorNetworkConnectionLost || error._code == NSURLErrorTimedOut ){
                
                self.getExclusiveMovies()
                
            }
            
            
            
            
        })
    }
    
    
    
    
    func isConnectedToNetwork() -> Bool {
        guard let flags = getFlags() else { return false }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    func getFlags() -> SCNetworkReachabilityFlags? {
        guard let reachability = ipv4Reachability() ?? ipv6Reachability() else {
            return nil
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(reachability, &flags) {
            return nil
        }
        return flags
    }
    
    func ipv6Reachability() -> SCNetworkReachability? {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }
    
    func ipv4Reachability() -> SCNetworkReachability? {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }
    
    
    
}










extension HomeScreenViewController: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        //        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2, execute: {
        //            koloda.resetCurrentCardIndex()
        //
        //        })
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        if(movieSliderArray[index].url != ""){
            let url = URL(string: movieSliderArray[index].url)!
            if  UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }else if(movieSliderArray[index].content){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
                movieDetailScene.movieDetail = self.movieSliderArray[index].movie
                movieDetailScene.refreshCollectionViewDelegate = self
                self.navigationController?.pushViewController(movieDetailScene, animated: true)
            }
            
            
        }
//        else {
//            
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
//                let moviePromoDetailScene = MoviePromoDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
//                moviePromoDetailScene.coverImageURL = self.movieSliderArray[index].image
//                moviePromoDetailScene.promoDetail = self.movieSliderArray[index].descriptionField
//                moviePromoDetailScene.promoTitle = self.movieSliderArray[index].title
//                self.navigationController?.pushViewController(moviePromoDetailScene, animated: true)
//            }
//            
//            
//        }
        
        
    }
    
    func kolodaShouldApplyAppearAnimation(_ koloda: KolodaView) -> Bool {
        return false
    }
    
    
    
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] {
        return [.left, .right]
    }
    
    
    
    
    
    
    
    
    
    
    
    
}

// MARK: KolodaViewDataSource

extension HomeScreenViewController: KolodaViewDataSource {
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return movieSliderArray.count
    }
    
    
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        
        let view = Bundle.main.loadNibNamed("FeaturedMovieSwipeCard", owner: self, options: nil)![0] as! FeaturedMovieSwipeCard
        view.loadKolodaView(sliderData: movieSliderArray[index])
        
        return view
        
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return Bundle.main.loadNibNamed("OverlayView", owner: self, options: nil)?[0] as? OverlayView
    }
    
    
    
    
}




extension HomeScreenViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == exclusiveMovieCollectionView){
            return exclusiceMovieArray.count
            
        }
        else if collectionView == tvSeriesCollectionView{
            
            return webSeriesArray.count
        }
            
//        else if collectionView == comedySeriesCollectionView {
//
//            return comedySeriesArray.count
//        }
            
        else {
            return latestMovieArray.count
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfItemsPerRow:CGFloat = 3
        let spacingBetweenCells:CGFloat = 0
        
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        
        let width = (collectionView.bounds.width - totalSpacing)/numberOfItemsPerRow
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
            
            return CGSize.init(width: width, height: floor(self.exclusiveMovieCollectionView.frame.height))
            
        }
            
        else {
            return CGSize.init(width: width, height: floor(self.exclusiveMovieCollectionView.frame.height))
            
        }
        
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let application = UIApplication.shared.delegate as! AppDelegate
        if application.isMoviePurchased {
            self.exclusiveMovieCollectionView.reloadData()
            self.latestMovieCollectionView.reloadData()
            application.isMoviePurchased = false
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
        let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
        //let goldVC = CineGoldViewController.instantiate(fromAppStoryboard: .CineGold)
        let goldVC = HomeVC.instantiate(fromAppStoryboard: .CineGold)
        
        if collectionView == latestMovieCollectionView{
            movieDetailScene.movieDetail = latestMovieArray[indexPath.row]
            movieDetailScene.refreshCollectionViewDelegate = self
            self.navigationController?.pushViewController(movieDetailScene, animated: true)
            
            
            
        }
            
        else if collectionView == tvSeriesCollectionView {
            let seriesDetailVC = SeriesDetailViewController.instantiate(fromAppStoryboard: .SeriesDetail)
            seriesDetailVC.series = self.webSeriesArray[indexPath.row]
            self.navigationController?.pushViewController(seriesDetailVC, animated: true)
            
        }
            
            
        else {
            
            self.navigationController?.pushViewController(goldVC, animated: true)
            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == exclusiveMovieCollectionView){
            
            let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExclusiveMovieCollectionViewCell", for: indexPath) as! ExclusiveMovieCollectionViewCell
            cell.setMovieInfos(movie: self.exclusiceMovieArray[indexPath.row])
            
            return cell
        }
            
        else if collectionView == tvSeriesCollectionView{
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WebSeriesCollectionViewCell", for: indexPath) as! WebSeriesCollectionViewCell
            cell.setWebSeriesInfo(webSeries: self.webSeriesArray[indexPath.row], labelColor: UIColor.black)
            
            return cell
            
            
        }
            
//        else if collectionView == comedySeriesCollectionView {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComedySeriesCollectionViewCell", for: indexPath) as! ComedySeriesCollectionViewCell
//            cell.setWebSeriesInfo(webSeries: self.comedySeriesArray[indexPath.row], labelColor: UIColor.black)
//            
//            return cell
//            
//        }
        else {
            let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LatestMovieCollectionViewCell", for: indexPath) as! LatestMovieCollectionViewCell
            
            cell.setMovieInfos(movie: latestMovieArray[indexPath.row])
            return cell
            
        }
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
}

extension UIColor {
    var hexString: String {
        let colorRef = cgColor.components
        let r = colorRef?[0] ?? 0
        let g = colorRef?[1] ?? 0
        let b = ((colorRef?.count ?? 0) > 2 ? colorRef?[2] : g) ?? 0
        let a = cgColor.alpha
        
        var color = String(
            format: "#%02lX%02lX%02lX",
            lroundf(Float(r * 255)),
            lroundf(Float(g * 255)),
            lroundf(Float(b * 255))
        )
        
        if a < 1 {
            color += String(format: "%02lX", lroundf(Float(a)))
        }
        
        return color
    }
}




extension HomeScreenViewController: RefreshCollectionViews {
    func refreshCollectionViews() {
      //  self.tvSeriesCollectionView.reloadData()
        self.exclusiveMovieCollectionView.reloadData()
        self.latestMovieCollectionView.reloadData()
    }
    
        
}


extension Array where Element: Equatable {
    mutating func remove(_ obj: Element) {
        self = self.filter { $0 != obj }
    }
}

extension HomeScreenViewController: SwiftyGifDelegate {
    func gifDidStop(sender: UIImageView) {
        logoAnimationView.isHidden = true
    }
}
