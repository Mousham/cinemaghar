//
//  WebViewController.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 3/27/21.
//  Copyright © 2021 sunBi. All rights reserved.
//

import Foundation
import UIKit
import WebKit
class WebViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    var webView: WKWebView!
    
    var redirectURL: String!
    var selectedVoteCount: String!
    
    var progressView: UIProgressView!
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.navigationDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        webView.uiDelegate = self
        view = webView
        
        //Web Bar to view on status bar
        let statusBar = UIView()
        statusBar.backgroundColor = UIColor(hex: "#252B38")
        statusBar.translatesAutoresizingMaskIntoConstraints = false
        webView.addSubview(statusBar)
        statusBar.topAnchor.constraint(equalTo: webView.topAnchor).isActive = true
        statusBar.leftAnchor.constraint(equalTo: webView.leftAnchor).isActive = true
        statusBar.rightAnchor.constraint(equalTo: webView.rightAnchor).isActive = true
        statusBar.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
        statusBar.widthAnchor.constraint(equalTo: webView.widthAnchor).isActive = true
        
        //Nav Bar
        let navBar = UIView()
        navBar.backgroundColor = UIColor.white
        navBar.translatesAutoresizingMaskIntoConstraints = false
        webView.addSubview(navBar)
        navBar.bottomAnchor.constraint(equalTo: webView.bottomAnchor).isActive = true
        navBar.leftAnchor.constraint(equalTo: webView.leftAnchor).isActive = true
        navBar.rightAnchor.constraint(equalTo: webView.rightAnchor).isActive = true
        navBar.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        navBar.widthAnchor.constraint(equalTo: webView.widthAnchor).isActive = true
        
        //Cancel
        let cancelButton = UIButton()
        cancelButton.setTitle("Back to App", for: .normal)
        cancelButton.backgroundColor = UIColor(hex: "#252B38")
        cancelButton.addTarget(self, action: #selector(dismissWebView), for: .touchUpInside)
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        navBar.addSubview(cancelButton)
        cancelButton.rightAnchor.constraint(equalTo: navBar.rightAnchor).isActive = true
        cancelButton.topAnchor.constraint(equalTo: navBar.topAnchor).isActive = true
        cancelButton.bottomAnchor.constraint(equalTo: navBar.bottomAnchor).isActive = true
        cancelButton.widthAnchor.constraint(equalTo: navBar.widthAnchor).isActive = true
        
        // Progress View
        progressView = UIProgressView()
        progressView.progressTintColor = UIColor.init(hex: "#F3BC31")
        progressView.translatesAutoresizingMaskIntoConstraints = false
        webView.addSubview(progressView)
        progressView.bottomAnchor.constraint(equalTo: navBar.topAnchor).isActive = true
        progressView.rightAnchor.constraint(equalTo: webView.rightAnchor).isActive = true
        progressView.leftAnchor.constraint(equalTo: webView.leftAnchor).isActive = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let urlString = redirectURL + "&voteCount=" + selectedVoteCount
        let myURL = URL(string:urlString)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
        
        //add observer to get estimated progress value
        self.webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil);
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            self.progressView.progress = Float(self.webView.estimatedProgress);
            if(self.webView.estimatedProgress < 1.0){
                self.progressView.isHidden = false
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.progressView.isHidden = true
                }
            }
        }
    }
    
    @objc func dismissWebView(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.documentElement.outerHTML.toString()") { (html, err) in
            print("sawsank: Loaded Page")
            let htmlString = html as! String
            if htmlString.contains("--reqsaved--") {
                self.dismissWebView()
            }
        }
    }
}

extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}
