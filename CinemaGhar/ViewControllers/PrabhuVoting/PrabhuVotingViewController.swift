//
//  PrabhuVotingViewController.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 3/23/21.
//  Copyright © 2021 sunBi. All rights reserved.
//

import Foundation
import CoreTelephony

class PrabhuVotingViewController: UIViewController {
    
    var votingPricesArr = [VotingPrices]()
    @IBOutlet weak var eventsTableView: UITableView!
    @IBOutlet weak var bannerImageView: UIImageView!
    
    var redirectUrl = ""

    
    @IBAction func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        print("Prabhu Voting")
        // Setup the Network Info and create a CTCarrier object
        let networkStatus = CTTelephonyNetworkInfo()
        if let info = networkStatus.serviceSubscriberCellularProviders,
           let carrier = info["0000000100000001"] {
            //work with carrier object
            print("sawsank: MNC = \(carrier.isoCountryCode)")
        }
        getLoginUrl()
        eventsTableView.delegate = self
        eventsTableView.dataSource = self
        getVotingPrices()
        eventsTableView.reloadData()
    }
    
    func getVotingPrices(){
        APIHandler.shared.getVotingPrices { (status, votingPrices, bannerImage) in
            if (status){
                self.votingPricesArr = votingPrices
                self.eventsTableView.reloadData()
                self.bannerImageView.layer.cornerRadius = 15
                self.bannerImageView.sd_setImage(with: URL(string: bannerImage), placeholderImage: UIImage(named: "placeholder_cell"), options: [.continueInBackground, .progressiveLoad])
                    
                
            }
            
        } failure: { (message) in
            print("sawsank: failure \(message)")
        } error: { (err) in
            print("sawsank: error \(err.localizedDescription)")
        }

    }
    
}

extension PrabhuVotingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return votingPricesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let votingPrice = votingPricesArr[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell") as! EventTableViewCell
        cell.setVotingPrice(votingPrice: votingPrice)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedVote = votingPricesArr[indexPath.row]
        let webViewController = WebViewController()
        webViewController.redirectURL = self.redirectUrl
        let voteName = selectedVote.name.split(separator: " ")
        let vote = voteName[0]
        webViewController.selectedVoteCount = String(vote)
        if(SessionManager().isLoggedIn()){
            self.navigationController?.pushViewController(webViewController, animated: true)
        }else{
            let signupVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(signupVC, animated: true)
        }
    }
    
    func getLoginUrl(){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait", presentingView: self.view)
        APIHandler.shared.getAutoLoginLink { (link) in
            self.redirectUrl = link
            APESuperHUD.removeHUD(animated: true, presentingView: self.view)
            print("sawsank: link \(link)")
        } failure: { (message) in
            print("sawsank: \(message)")
            APESuperHUD.removeHUD(animated: true, presentingView: self.view)
        } error: { (err) in
            print("sawsank: \(err.localizedDescription)")
            APESuperHUD.removeHUD(animated: true, presentingView: self.view)
        }
    }
}
