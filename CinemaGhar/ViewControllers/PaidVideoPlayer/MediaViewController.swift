// Copyright 2018 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import UIKit
import GoogleCast
import AirPlay
import AVFoundation
import MediaPlayer
/* The player state. */
enum PlaybackMode: Int {
    case none = 0
    case local
    case remote
}

let kPrefShowStreamTimeRemaining: String = "show_stream_time_remaining"
let appDelegate = UIApplication.shared.delegate as! AppDelegate

protocol RatingModalDelegate {
    func popViewController(updateRatingBar: Bool, rating: Double)
    func ratingComplete(title:String, message:String, rating: Double)
    
}


@objc(MediaViewController)
class MediaViewController: UIViewController,
LocalPlayerViewDelegate, RatingModalDelegate, GCKUIMediaControllerDelegate, GCKSessionManagerListener, GCKRequestDelegate, GCKRemoteMediaClientListener {
   
    @IBOutlet weak var userIdLabel: UILabel!
    
    @IBOutlet weak var navItem: UINavigationItem!
    var isAddedAirPlayBarButton = false
    var contentId = 0
    var videoTitle: String = ""


    @IBOutlet weak var airPlayStatusLabel: UILabel!
    @IBOutlet weak var navBar: UINavigationBar!
   
  @IBOutlet private var _localPlayerView: LocalPlayerView!
  private var sessionManager: GCKSessionManager!
  private var castSession: GCKCastSession?
    var hasUserRatedMovie = false
    
    var mediaType: MediaType = .movie


  private var castMediaController: GCKUIMediaController!
  private var volumeController: GCKUIDeviceVolumeController!
  private var streamPositionSliderMoving: Bool = false
  private var playbackMode = PlaybackMode.none
  private var showStreamTimeRemaining: Bool = false
  private var localPlaybackImplicitlyPaused: Bool = false
  private var actionSheet: ActionSheet?
  private var queueAdded: Bool = false
  private var gradient: CAGradientLayer!
  private var castButton: GCKUICastButton!
    
    let backButton = UIButton(type: .custom)

    
    let airPlayBtn = UIButton(type: .custom)

    
  /* Whether to reset the edges on disappearing. */
  var isResetEdgesOnDisappear: Bool = false
  // The media to play.
  var mediaInfo: GCKMediaInformation? {
    didSet {
      print("setMediaInfo")
    }
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    self.sessionManager = GCKCastContext.sharedInstance().sessionManager
    self.castMediaController = GCKUIMediaController()
    self.volumeController = GCKUIDeviceVolumeController()

  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    backButton.titleLabel?.textAlignment = .left
    backButton.setImage(UIImage(named: "back_arrow"), for: .normal)
    backButton.tintColor = .white
    backButton.setTitleColor(UIColor.gray, for: .highlighted)
    backButton.setTitleColor(UIColor .white, for: UIControlState.normal)
    backButton.setTitle(" Back", for: .normal)
    backButton.contentHorizontalAlignment = .left
    backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
    backButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .regular)
    navItem.leftBarButtonItem =  UIBarButtonItem(customView: backButton);
    navItem.title = self.videoTitle
    
    
    print("in MediaViewController viewDidLoad")
    appDelegate.myOrientation = .allButUpsideDown
//    navBar.backgroundColor = UIColor.clear
//    navBar.setBackgroundImage(UIImage(), for: .default)
//    navBar.shadowImage = UIImage()
//    navBar.isTranslucent = true
    

    let value = UIInterfaceOrientation.landscapeRight.rawValue
    UIDevice.current.setValue(value, forKey: "orientation")

    self._localPlayerView.delegate = self
    self.castButton = GCKUICastButton(frame: CGRect(x: CGFloat(0), y: CGFloat(0),
                                                    width: CGFloat(24), height: CGFloat(24)))
    self.castButton.tintColor = UIColor.white
    self.navItem.rightBarButtonItem = UIBarButtonItem(customView: self.castButton)
    
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(applicationDidEnterBackground),
                                           name: Notification.Name.UIApplicationDidEnterBackground,
                                           object: nil)
    
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(applicationWillEnterForeground),
                                           name: Notification.Name.UIApplicationWillEnterForeground,
                                           object: nil)
  
    NotificationCenter.default.addObserver(self, selector: #selector(self.castDeviceDidChange),
                                           name: NSNotification.Name.gckCastStateDidChange,
                                           object: GCKCastContext.sharedInstance())
    
    if  #available(iOS 11.0, *){
        if UIScreen.main.isCaptured {
            _localPlayerView.pause()
            showAlert()
        }
            
        else {
            NotificationCenter.default.addObserver(self, selector: #selector(screenCaptureChanged), name: Notification.Name.UIScreenCapturedDidChange, object: nil)
        }
    }
    
    NotificationCenter.default.addObserver(self, selector: #selector(checkOutputPortType), name: Notification.Name.AVAudioSessionRouteChange, object: nil)
    AirPlay.whenAvailable = { [weak self] in
        self?.updateUI()
    }
    
    AirPlay.whenUnavailable = { [weak self] in
        self?.updateUI()
    }
    
    AirPlay.whenRouteChanged = { [weak self] in
        self?.updateUI()
    }
  
    
    
    userIdLabel.text = UserDefaults.standard.string(forKey: AppConstants.uniqueId) ?? ""


    
    
    
  }
    
    @objc func applicationWillEnterForeground() {
        
    }
    
    @objc func applicationDidEnterBackground() {
        _localPlayerView.pause()
    }

    @objc func castDeviceDidChange(_ notification: Notification) {
    if GCKCastContext.sharedInstance().castState != .noDevicesAvailable {
      // You can present the instructions on how to use Google Cast on
      // the first time the user uses you app
      GCKCastContext.sharedInstance().presentCastInstructionsViewControllerOnce()
    }
  }
    
    
    @objc func screenCaptureChanged(){
        if #available(iOS 11.0, *) {
            //            print(UIScreen.main.isCaptured)
            if (UIScreen.main.isCaptured) {
                self._localPlayerView.pause()
                showAlert()
            }
            else {
                //  self.player.play()
                
            }
        }
        
    }
    
    
    @objc func checkOutputPortType() {
        let asRoute = AVAudioSession.sharedInstance().currentRoute
        for output in asRoute.outputs {
            if output.portType == AVAudioSessionPortHDMI {
                self.showAlert()
            }
        }
    }

    
    
    
    func showAlert(){
        
        let alertController = DOAlertController(title: "Warning!", message: "Screen recording is not allowed for copyright contents. The unauthorized copying, sharing or distribution of copyrighted material is strictly prohibited may subject those caught to legal action." , preferredStyle: .alert)
        alertController.alertViewBgColor = UIColor.white
        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 18)
        alertController.messageView.textAlignment = .left
        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 15)
        alertController.titleTextColor = UIColor.red
        let okAction = DOAlertAction(title: "Close", style: .destructive, handler: { action in
            if  #available(iOS 11.0, *){
                if UIScreen.main.isCaptured {
                    self.dismiss(animated: true, completion: {
                        self.showAlert()
                    })
                }
                else {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
        })
        // Add the action.
        alertController.addAction(okAction)
        // Show alert
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    
    
    
    
    
    
    @objc func didTapAirPlayBtn(sender: AnyObject){
        
        let rect = CGRect(x: -100, y: 0, width: 0, height: 0)
        let airplayVolume = MPVolumeView(frame: rect)
        airplayVolume.showsVolumeSlider = true
        
        self.view.addSubview(airplayVolume)
        for view: UIView in airplayVolume.subviews {
            if let button = view as? UIButton {
                button.sendActions(for: .touchUpInside)
                break
            }
        }
        airplayVolume.removeFromSuperview()
    }
    
    
    
    
    @objc func dismissVC(){
        appDelegate.myOrientation =  .portrait
        
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.0, execute: {
            if !self.hasUserRatedMovie {
                self.showRatingModalVC()
                
            }
                
            else {
                
                if AirPlay.isConnected {
                    
                    self._localPlayerView.mediaPlayer?.allowsExternalPlayback = false
                    
//                    self.player.avPlayer?.mediaPlayer = false
                }
                
                if self.mediaType == .movie {
                    
                    self.navigationController?.backToViewController(viewController: MovieDetailViewController.self)
                    
                }
                else {
                    
                    self.navigationController?.backToViewController(viewController: SeriesDetailViewController.self)
                    
                }
                
            }
            
            
            
        })
        
        
    }
    
    
    func showRatingModalVC(){
        _localPlayerView.stop()
        let ratingPopUpVC = MovieRatingViewController.instantiate(fromAppStoryboard: .MovieRating)
        ratingPopUpVC.modalPresentationStyle = .overCurrentContext
        ratingPopUpVC.delegate = self
        ratingPopUpVC.contentID = self.contentId
        self.present(ratingPopUpVC, animated: true, completion: nil)
        
    }
    
    
    
    
    func updateUI() {
      
        if AirPlay.isAvailable && !AirPlay.isConnected {

            if isAddedAirPlayBarButton == false {
                airPlayBtn.setImage(UIImage(named: "airplay"), for: .normal)
                airPlayBtn.tintColor = UIColor.white
                airPlayBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
                airPlayBtn.addTarget(self, action: #selector(didTapAirPlayBtn), for: .touchUpInside)
                airPlayBtn.setTitle("  ", for: .normal)
                let item1 = UIBarButtonItem(customView: airPlayBtn)
                item1.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0)
                self.navItem.rightBarButtonItems?.append(item1)
                self.isAddedAirPlayBarButton = true
                
            }
                
            else {
                airPlayBtn.setImage(UIImage(named: "airplay"), for: .normal)
                
            }
            
        }
        else {
            airPlayBtn.setImage(UIImage(), for: .normal)

            
        }
        
        
        
        if (AirPlay.isConnected) {
            
            _localPlayerView.pause()
            airPlayBtn.setImage(UIImage(named: "airplay_connected"), for: .normal)
            airPlayStatusLabel.font = UIFont.systemFont(ofSize: 20, weight: .light)
            airPlayStatusLabel.isHidden = false
            let audioSession = AVAudioSession.sharedInstance()
            let currentRoute: AVAudioSessionRouteDescription? = audioSession.currentRoute
            for outputPort: AVAudioSessionPortDescription? in currentRoute?.outputs ?? [AVAudioSessionPortDescription?]() {
                if (outputPort?.portType == AVAudioSessionPortAirPlay) {
                    if let airPlayDevice = outputPort?.portName {
                        airPlayStatusLabel.text = "Playing in \(airPlayDevice)"
                    }
                    
                }
            }
        }
            
        else {

            _localPlayerView.play()
            airPlayStatusLabel.isHidden = true
        }
        
        
        
    }
    
    deinit {
        UIApplication.shared.endReceivingRemoteControlEvents()
      
        AirPlay.stopMonitoring()
    }
    
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: nil) { (_) in
            UIView.setAnimationsEnabled(true)
        }
        UIView.setAnimationsEnabled(false)
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    
  override func viewWillAppear(_ animated: Bool) {
//    print("viewWillAppear; mediaInfo is \(String(describing: self.mediaInfo)), mode is \(self.playbackMode)")
    if (self.playbackMode == .local) && self.localPlaybackImplicitlyPaused {
        self._localPlayerView.play()
        self._localPlayerView.playButton.setImage(UIImage(named: "pause"), for: .normal)
        self.localPlaybackImplicitlyPaused = false
    }
    AirPlay.startMonitoring()

    appDelegate.isCastControlBarsEnabled = true
   
    
    // Do we need to switch modes? If we're in remote playback mode but no longer
    // have a session, then switch to local playback mode. If we're in local mode
    // but now have a session, then switch to remote playback mode.
    let hasConnectedSession: Bool = (self.sessionManager.hasConnectedSession())
    if hasConnectedSession && (self.playbackMode != .remote) {
      self.populateMediaInfo(true, playPosition: 0)
      self.switchToRemotePlayback()
    } else if (self.sessionManager.currentSession == nil) && (self.playbackMode != .local) {
      self.switchToLocalPlayback()
    }

    self.sessionManager.add(self)
    self.gradient = CAGradientLayer()
    self.gradient.colors = [(UIColor.clear.cgColor),
                            UIColor.darkGray.cgColor]
    self.gradient.startPoint = CGPoint(x: CGFloat(0), y: CGFloat(1))
    self.gradient.endPoint = CGPoint.zero
    let orientation: UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
    if UIInterfaceOrientationIsLandscape(orientation) {
      self.setNavigationBarStyle(.lpvNavBarTransparent)
    } else if self.isResetEdgesOnDisappear {
      self.setNavigationBarStyle(.lpvNavBarDefault)
    }

    NotificationCenter.default.addObserver(self, selector: #selector(self.deviceOrientationDidChange),
                                           name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    
    
    super.viewWillAppear(animated)
  }

  func setQueueButtonVisible(_ visible: Bool) {
//    if visible && !self.queueAdded {
//      var barItems = self.navigationItem.rightBarButtonItems
//      self.navigationItem.rightBarButtonItems = barItems
//      self.queueAdded = true
//    } else if !visible && self.queueAdded {
//      var barItems = self.navigationItem.rightBarButtonItems
//      let index = barItems?.index(of: self.queueButton) ?? -1
//      barItems?.remove(at: index)
//      self.navigationItem.rightBarButtonItems = barItems
//      self.queueAdded = false
//    }

  }

  override func viewWillDisappear(_ animated: Bool) {
    print("viewWillDisappear")
    self.setNavigationBarStyle(.lpvNavBarDefault)
    switch playbackMode {
    case .local:
      if self._localPlayerView.playerState == .playing || self._localPlayerView.playerState == .starting {
        self.localPlaybackImplicitlyPaused = true
        self._localPlayerView.pause()
      }
    default:
      // Do nothing.
      break
    }

//    self.sessionManager.remove(self)
    UIDevice.current.endGeneratingDeviceOrientationNotifications()
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    super.viewWillDisappear(animated)
  }
    
    
    
    func popViewController(updateRatingBar: Bool, rating: Double = 0){
        let orientation: UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
        if UIInterfaceOrientationIsLandscape(orientation){
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+4, execute: {
            if self.mediaType == .movie {
                
                self.navigationController?.backToViewController(viewController: MovieDetailViewController.self)
                
            }
            else {
                
                self.navigationController?.backToViewController(viewController: SeriesDetailViewController.self)
                
            }
        }
        )
        
    }
        
    
    
    func ratingComplete(title:String, message:String, rating: Double) {
        
        let alertController = DOAlertController(title: title, message: message , preferredStyle: .alert)
        alertController.alertViewBgColor = UIColor.white
        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 19)
        alertController.messageView.textAlignment = .left
        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 16)
        alertController.titleTextColor = UIColor.black
        let okAction = DOAlertAction(title: "Close", style: .default, handler: { action in
            alertController.dismiss(animated: true, completion: {
                if title == "Success!"{
                    self.popViewController(updateRatingBar: true, rating: rating)
                    
                }
                else {
                    self.popViewController(updateRatingBar: false)
                }
                
            })
        })
        // Add the action.
        alertController.addAction(okAction)
        // Show alert
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    
    
    
    

    @objc func deviceOrientationDidChange(_ notification: Notification) {
    print("Orientation changed.")
    let orientation: UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
    if UIInterfaceOrientationIsLandscape(orientation) {
      self.setNavigationBarStyle(.lpvNavBarTransparent)
    } else if !UIInterfaceOrientationIsLandscape(orientation) || !self._localPlayerView.isPlayingLocally {
      self.setNavigationBarStyle(.lpvNavBarDefault)
    }

    self._localPlayerView.orientationChanged()
  }

    @objc func didTapQueueButton(_ sender: Any) {
        appDelegate.isCastControlBarsEnabled = false
    self.performSegue(withIdentifier: "MediaQueueSegue", sender: self)
  }
  // MARK: - Mode switching

  func switchToLocalPlayback() {
    print("switchToLocalPlayback")
    if self.playbackMode == .local {
      return
    }
    
    var playPosition: TimeInterval = 0
    var paused: Bool = false
    var ended: Bool = false
    if self.playbackMode == .remote {
      playPosition = self.castMediaController.lastKnownStreamPosition
      paused = (self.castMediaController.lastKnownPlayerState == .paused)
      ended = (self.castMediaController.lastKnownPlayerState == .idle)
      print("last player state: \(self.castMediaController.lastKnownPlayerState), ended: \(ended)")
    }
    self.populateMediaInfo((!paused && !ended), playPosition: playPosition)
    
    self.castSession?.remoteMediaClient?.remove(self)
    self.castSession = nil
    self.playbackMode = .local
  }

  func populateMediaInfo(_ autoPlay: Bool, playPosition: TimeInterval) {
    print("populateMediaInfo")
//    self._titleLabel.text = self.mediaInfo?.metadata?.string(forKey: kGCKMetadataKeyTitle)
    var subtitle = self.mediaInfo?.metadata?.string(forKey: kGCKMetadataKeyArtist)
    if subtitle == nil {
      subtitle = self.mediaInfo?.metadata?.string(forKey: kGCKMetadataKeyStudio)
    }
//    self._subtitleLabel.text = subtitle

    self._localPlayerView.loadMedia(autoPlay: true, playPosition: playPosition)
  }

  func switchToRemotePlayback() {
//    print("switchToRemotePlayback; mediaInfo is \(String(describing: self.mediaInfo))")
    if self.playbackMode == .remote {
      return
    }
    if self.sessionManager.currentSession is GCKCastSession {
      self.castSession = (self.sessionManager.currentSession as? GCKCastSession)
    }
    // If we were playing locally, load the local media on the remote player
    if (self.playbackMode == .local) && (self._localPlayerView.playerState != .stopped) {
      print("loading media: \(String(describing: self.mediaInfo))")
      let paused: Bool = (self._localPlayerView.playerState == .paused)
      let builder = GCKMediaQueueItemBuilder()
      builder.mediaInformation = self.mediaInfo
      builder.autoplay = true
      builder.preloadTime = 0
      let item = builder.build()
      if let playPosition = self._localPlayerView.streamPosition {
        self.castSession?.remoteMediaClient?.queueLoad([item], start: 0, playPosition: playPosition,
                                                     repeatMode: .off, customData: nil)

      }
    }
    
    
    self._localPlayerView.stop()
    self._localPlayerView.showSplashScreen()
    setNavigationBarStyle(.lpvNavBarTransparent)
    self.castSession?.remoteMediaClient?.add(self)
    self.playbackMode = .remote
    
  }

  func clearMetadata() {
  
  }

  func showAlert(withTitle title: String, message: String) {
    let alert = UIAlertView(title: title, message: message, delegate: nil,
                            cancelButtonTitle: "OK", otherButtonTitles: "")
    alert.show()
  }
  // MARK: - Local playback UI actions

  func startAdjustingStreamPosition(_ sender: Any) {
    self.streamPositionSliderMoving = true
  }

  func finishAdjustingStreamPosition(_ sender: Any) {
    self.streamPositionSliderMoving = false
  }

  func togglePlayPause(_ sender: Any) {
    self._localPlayerView.togglePause()
  }
  // MARK: - GCKSessionManagerListener

  func sessionManager(_ sessionManager: GCKSessionManager, didStart session: GCKSession) {
    print("MediaViewController: sessionManager didStartSession \(session)")
    self.switchToRemotePlayback()
  }

  func sessionManager(_ sessionManager: GCKSessionManager, didResumeSession session: GCKSession) {
    print("MediaViewController: sessionManager didResumeSession \(session)")
    self.switchToRemotePlayback()
  }

  func sessionManager(_ sessionManager: GCKSessionManager, didEnd session: GCKSession, withError error: Error?) {
    print("session ended with error: \(String(describing: error))")
    let message = "The Casting session has ended.\n\(String(describing: error))"
//    if let window = appDelegate?.window {
//      Toast.displayMessage(message, for: 3, in: window)
//    }
    self.switchToLocalPlayback()
  }

  func sessionManager(_ sessionManager: GCKSessionManager, didFailToStartSessionWithError error: Error?) {
    if let error = error {
      self.showAlert(withTitle: "Failed to start a session", message: error.localizedDescription)
    }
    self.setQueueButtonVisible(false)
  }

  func sessionManager(_ sessionManager: GCKSessionManager,
                      didFailToResumeSession session: GCKSession, withError error: Error?) {
    if let window = UIApplication.shared.delegate?.window {
//      Toast.displayMessage("The Casting session could not be resumed.",
//                           for: 3, in: window)
    }
    self.setQueueButtonVisible(false)
    self.switchToLocalPlayback()
  }
  // MARK: - GCKRemoteMediaClientListener

  func remoteMediaClient(_ player: GCKRemoteMediaClient, didUpdate mediaStatus: GCKMediaStatus?) {
    self.mediaInfo = mediaStatus?.mediaInformation
  }
  // MARK: - LocalPlayerViewDelegate
  /* Signal the requested style for the view. */

  func setNavigationBarStyle(_ style: LPVNavBarStyle) {
    if style == .lpvNavBarDefault {
      print("setNavigationBarStyle: Default")
    } else if style == .lpvNavBarTransparent {
      print("setNavigationBarStyle: Transparent")
    } else {
      print("setNavigationBarStyle: Unknown - \(style)")
    }

    if style == .lpvNavBarDefault {
      self.edgesForExtendedLayout = .all
      self.hideNavigationBar(false)
      self.navBar.isTranslucent = true
      self.navBar.setBackgroundImage(UIImage(), for: .default)
      self.navBar.shadowImage = UIImage()
        
        self.navBar.backgroundColor = UIColor.clear
      UIApplication.shared.setStatusBarHidden(false, with: .fade)
      self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
      self.isResetEdgesOnDisappear = false
    } else if style == .lpvNavBarTransparent {
      self.edgesForExtendedLayout = .top
      self.navBar.isTranslucent = true
      // Gradient background
        self.gradient.frame = self.navBar.bounds
      UIGraphicsBeginImageContext(self.gradient.bounds.size)
      if let context = UIGraphicsGetCurrentContext() {
        self.gradient.render(in: context)
      }
        
        
        
      let gradientImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      self.navBar.setBackgroundImage(gradientImage, for: .default)
      self.navBar.shadowImage = UIImage()
        self.navBar.backgroundColor = UIColor.clear

      UIApplication.shared.setStatusBarHidden(true, with: .fade)
      // Disable the swipe gesture if we're fullscreen.
      self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
      self.isResetEdgesOnDisappear = true
    }

  }
  /* Request the navigation bar to be hidden or shown. */

  func hideNavigationBar(_ hide: Bool) {
    if hide {
      print("HIDE NavBar.")
    } else {
      print("SHOW NavBar.")
    }
    self.navBar.isHidden = hide
  }
  /* Play has been pressed in the LocalPlayerView. */

  func continueAfterPlayButtonClicked() -> Bool {
    let hasConnectedCastSession = GCKCastContext.sharedInstance().sessionManager.hasConnectedCastSession
    if (self.mediaInfo != nil) && hasConnectedCastSession() {

        print("show actionsheet")
      // Display an alert box to allow the user to add to queue or play
      // immediately.
        GCKCastContext.sharedInstance().presentDefaultExpandedMediaControls()

      return false
    }

    print(self.mediaInfo == nil)
    
    
    
    print("dont show actionsheet")

    return true
  }

    @objc func playSelectedItemRemotely() {
    self.loadSelectedItem(byAppending: false)
//    GCKCastContext.sharedInstance().presentDefaultExpandedMediaControls()
  }

    @objc func enqueueSelectedItemRemotely() {
    self.loadSelectedItem(byAppending: true)
//    let message = "Added \"\(self.mediaInfo?.metadata?.string(forKey: kGCKMetadataKeyTitle) ?? "")\" to queue."
    if let window = UIApplication.shared.delegate?.window {
//      Toast.displayMessage(message, for: 3, in: window)
    }
  }
  /**
   * Loads the currently selected item in the current cast media session.
   * @param appending If YES, the item is appended to the current queue if there
   * is one. If NO, or if
   * there is no queue, a new queue containing only the selected item is created.
   */

  func loadSelectedItem(byAppending appending: Bool) {
    print("enqueue item \(String(describing: self.mediaInfo))")
    if let remoteMediaClient = GCKCastContext.sharedInstance().sessionManager.currentCastSession?.remoteMediaClient {
      let builder = GCKMediaQueueItemBuilder()
      builder.mediaInformation = self.mediaInfo
      builder.autoplay = true
      builder.preloadTime = 0
      let item = builder.build()
      if ((remoteMediaClient.mediaStatus) != nil) && appending {
        let request = remoteMediaClient.queueInsert(item, beforeItemWithID: kGCKMediaQueueInvalidItemID)
        request.delegate = self
      } else {
        let repeatMode = remoteMediaClient.mediaStatus?.queueRepeatMode ?? .off
        let request = remoteMediaClient.queueLoad([item], start: 0, playPosition: 0,
                                                                            repeatMode: repeatMode, customData: nil)
        request.delegate = self
      }
    }
  }
  // MARK: - GCKRequestDelegate

  func requestDidComplete(_ request: GCKRequest) {
    print("request \(Int(request.requestID)) completed")
  }

  func request(_ request: GCKRequest, didFailWithError error: GCKError) {
    print("request \(Int(request.requestID)) failed with error \(error)")
  }
}
