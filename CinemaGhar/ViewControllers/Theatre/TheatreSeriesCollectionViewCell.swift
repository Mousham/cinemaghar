//
//  TheatreMovieCollectionViewCell.swift
//  CinemaGhar
//
//  Created by Sunil on 7/5/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit

class TheatreSeriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var seriesPosterImage: UIImageView!
    
    override func awakeFromNib() {
        seriesPosterImage.layer.cornerRadius = 5

    }
    
    
}
