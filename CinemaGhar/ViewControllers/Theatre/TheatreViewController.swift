//
//  TheatreViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 6/27/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import SDWebImage
import UIScrollView_InfiniteScroll

protocol RefreshTableViewDataDelegate{
    
    func refreshTheatreTableView()
}



class TheatreViewController: DesignableViewController, UIGestureRecognizerDelegate, RefreshTableViewDataDelegate, UINavigationBarDelegate  {
    @IBOutlet weak var theatreCollectionView: UICollectionView!
    
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBOutlet weak var loginPromptView: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var seriesPosterImage: UIImageView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBAction func loginBtnPressed(_ sender: Any) {
        
        let VC       = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
        VC.shouldDismissAfterLogin = true
        VC.shouldAllowSkip = false
        VC.isFromTheatre = false
        VC.delegate = self
        self.navigationController?.pushViewController(VC, animated: true)

    }
    var movieArray = [Movie]()
    var seriesArray = [Series]()
    
    @IBOutlet weak var navItem: UINavigationItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       


        
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back_arrow"), for: .normal)
        button.setTitle(" Back", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        button.sizeToFit()
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        segmentControl.addTarget(self, action: #selector(self.navigationSegmentedControlValueChanged(_:)), for: .valueChanged)
        self.navItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        // Do any additional setup after loading the view.
        
        let screenSize = UIScreen.main.bounds
        let  screenWidth = screenSize.width
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: screenWidth/3-12, height: screenWidth/1.95)
        
      
        //        layout.minimumInteritemSpacing = 12
        layout.minimumLineSpacing = 4
        theatreCollectionView.collectionViewLayout = layout
        navBar.delegate = self
        
        if SessionManager().isLoggedIn(){
            loginPromptView.isHidden = true

            getAllPurchases()
            
            
        }
        
        else {
            
            
                loginPromptView.isHidden = false
            
//            let alertController = DOAlertController(title: "Info!", message: "Hello there, You need to be logged in to access this section." , preferredStyle: .alert)
//            alertController.alertViewBgColor = UIColor.white
//            alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 16)
//            alertController.messageView.textAlignment = .left
//            alertController.messageFont = UIFont(name: "HelveticaNeue", size: 14)
//            alertController.titleTextColor = UIColor.red
//            
//            let okAction = DOAlertAction(title: "Login Now", style: .default, handler: { action in
//                // NotificationCenter.default.addObserver(self, selector: #selector(checkOutputPortType), name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)
//                alertController.dismiss(animated: true, completion: {
//                    let VC       = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
//                    VC.shouldDismissAfterLogin = true
//                    VC.isFromTheatre = true
//                    VC.delegate = self
//                    self.navigationController?.pushViewController(VC, animated: true)
//                })
//               
//                
//            })
//            
//            let cancelAction = DOAlertAction(title: "Not Now", style: .cancel, handler: { action in
//                // NotificationCenter.default.addObserver(self, selector: #selector(checkOutputPortType), name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)
//                alertController.dismiss(animated: true, completion: {
//                    self.navigationController?.popViewController(animated: true)
//                })
//                
//            })
//            // Add the action.
//            alertController.addAction(okAction)
//            alertController.addAction(cancelAction)
//            // Show alert
//            self.present(alertController, animated: true, completion: nil)
            
            
            
        }
        
        
    }
    
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
    
    @IBOutlet weak var messageLabel: UILabel!
    
    
    func getAllPurchases(){
        
        
        indicator.startAnimating()
        
        APIHandler.shared.getAllPurchasedItems(success: {(movieArray, seriesArray ) in
            
            self.movieArray = movieArray
            self.seriesArray = seriesArray
            
          
             if movieArray.isEmpty {
                self.messageLabel.text = "No Movies to display."

            }
             else {
                self.messageLabel.isHidden = true
            }
            self.loginPromptView.isHidden = true
            
            self.theatreCollectionView.delegate = self
            
            self.theatreCollectionView.dataSource = self
            self.theatreCollectionView.reloadData()
            
            self.indicator.stopAnimating()
            
            
            
            
            
        }, failure: {(failure) in
            
            
            if failure._code == NSURLErrorNotConnectedToInternet {
                
                let alert = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to internet and try again.", preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.getAllPurchases()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
                
            else if(failure._code == NSURLErrorTimedOut){
                let controller = UIAlertController(title: "Request Timed Out.", message: "Check Your Internet Connection and Try Again.", preferredStyle: .alert)
                let cancel = UIAlertAction(title: "ok", style: .default, handler: nil)
                let retry = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) -> Void in
                    self.getAllPurchases()
                })
                controller.addAction(cancel)
                controller.addAction(retry)
                self.present(controller, animated: true, completion: nil)
                
                
            }
                
            else if (failure._code == NSURLErrorNetworkConnectionLost){
                
                self.getAllPurchases()
                
            }
            
            
            
        })
        
        
    }
    
    
    
    
    
    @objc func navigationSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        self.theatreCollectionView.reloadData()
        print(sender.selectedSegmentIndex)
        if sender.selectedSegmentIndex == 0 {
            
            if self.movieArray.count == 0 {
                self.messageLabel.text = "Your Library does not contain any movie."
                self.messageLabel.isHidden  = false
                self.theatreCollectionView.isHidden = true
            }
            else {
                self.messageLabel.isHidden = true
                self.theatreCollectionView.isHidden = false

            }
            
//            leaderBoardTableView.addInfiniteScroll { (tableView) -> Void in
//
//                if(self.nextPageURL != "null"){
//
//                    self.loadMoreLeaderBoard(nextPageURL: self.nextPageURL)
//
//                }
//
//            }
            
            
        }
        else {
            
            if self.seriesArray.count == 0 {
                self.messageLabel.text = "Your Library does not contain any web series."
                self.messageLabel.isHidden  = false
                self.theatreCollectionView.isHidden = true


            }
            else {
                self.messageLabel.isHidden = true
                self.theatreCollectionView.isHidden = false

            }
            
        }
    }
    
    
    
    func refreshTheatreTableView() {
    
        getAllPurchases()
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    

   

}


extension TheatreViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.segmentControl.selectedSegmentIndex == 0 {
            
            return self.movieArray.count
        }
        else {
            
            return self.seriesArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        
        
        if self.segmentControl.selectedSegmentIndex == 0 {
            
            let movieCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TheatreMovieCollectionViewCell", for: indexPath) as! TheatreMovieCollectionViewCell
            
            if let posterImage = self.movieArray[indexPath.row].poster{
                
                 movieCell.moviePosterImage.sd_setImage(with: URL(string: posterImage ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad])
            }
            
            cell = movieCell

            
        }
        
        else {
            
            
            let seriesCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TheatreSeriesCollectionViewCell", for: indexPath) as! TheatreSeriesCollectionViewCell
            if let posterImage = self.seriesArray[indexPath.row].image{
                
                seriesCell.seriesPosterImage.sd_setImage(with: URL(string: posterImage ), placeholderImage: UIImage(), options: [.continueInBackground, .progressiveLoad])
                
                
            }
            
            cell = seriesCell
            
        }

        
        return cell
    }
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        var numOfSection: Int = 0
        if segmentControl.selectedSegmentIndex == 0 {
        
            if self.movieArray.count>0{
                collectionView.backgroundView = nil
                numOfSection = 1
            }
                
            else {
                
                self.loginPromptView.isHidden = true
                
                let noDataLabel: UILabel = UILabel(frame: CGRect(x:0, y:0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height))
                noDataLabel.text = "Your purchase list is empty."
                noDataLabel.textColor = UIColor.white
                noDataLabel.numberOfLines = 1
                noDataLabel.font = UIFont.systemFont(ofSize: 17)
                noDataLabel.textAlignment = .center
                collectionView.backgroundView = noDataLabel
                
            }
            
            
        }
        
        else if segmentControl.selectedSegmentIndex == 1 {
            
            if self.seriesArray.count>0{
                collectionView.backgroundView = nil
                numOfSection = 0
            }
                
            else {
                
                let view = UIView(frame: CGRect(x:0, y:0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height))
                let noDataLabel = UILabel()
                noDataLabel.text = ""
                noDataLabel.textColor = UIColor.white
                noDataLabel.numberOfLines = 0
               
                noDataLabel.font = UIFont.systemFont(ofSize: 17)
                noDataLabel.textAlignment = .center
                view.addSubview(noDataLabel)
                noDataLabel.snp.makeConstraints{(make) in
                    make.centerX.centerY.equalTo(view)
                    
                    
                    
                }
                collectionView.backgroundView = view
                
            }
            
            
            
            
        }
    return numOfSection
            
        
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if segmentControl.selectedSegmentIndex == 0 {
            
//            let movieDetailScene = MovieDetailViewController.instantiate(fromAppStoryboard: .MovieDetail)
            let movieDetailScene = NewMovieDetailsVC.instantiate(fromAppStoryboard: .MovieDetail)
            movieDetailScene.movieDetail = movieArray[indexPath.row]
            
            self.navigationController?.pushViewController(movieDetailScene, animated: true)

            
            
        }
        else {
            
            
            let seriesDetailVC = SeriesDetailViewController.instantiate(fromAppStoryboard: .SeriesDetail)
            seriesDetailVC.series = self.seriesArray[indexPath.row]
            self.navigationController?.pushViewController(seriesDetailVC, animated: true)
            
            
            
            
        }
        
    }
    
    
    
    
    
    
}
