//
//  TheatreMovieCollectionViewCell.swift
//  CinemaGhar
//
//  Created by Sunil on 7/5/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit

class TheatreMovieCollectionViewCell: UICollectionViewCell {
    
    
    
    
    @IBOutlet weak var moviePosterImage: UIImageView!
    
    
    override func awakeFromNib() {
        moviePosterImage.layer.cornerRadius = 3
         addDropShadow(offset: CGSize(width: 1, height: 0.3), color: .black, radius: 3, opacity: 0.5)
        

    }
    
}
