//
//  PassCodeVerificationViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 5/26/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import SVPinView

class PassCodeVerificationViewController: DesignableViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var verificationTitleLabel: UILabel!
    
    @IBOutlet weak var pinView: SVPinView!
    
    @IBOutlet weak var digit1TextField: UITextField!
    @IBOutlet weak var digit2TextField: UITextField!
    @IBOutlet weak var digit3TextField: UITextField!
    @IBOutlet weak var digit4TextField: UITextField!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var resendCodeOutlet: UIButton!
    @IBOutlet weak var countDownLabel: UILabel!
    @IBAction func resendCode(){
        resendCodeAPI()
    }
    var mediaType: MediaType = .movie
    
    var timer: Timer!
    var castPlayDelegate: CastPlayerDelegate?
    var count = 6
    @IBOutlet weak var codeTextField: UITextField!
    
    var phoneNumber: String = ""
    var userId: Int?

    
    @IBAction func verifyPassCodeBtnPressed(_ sender: Any) {
        if let text = codeTextField.text{
            if text.count<4{
                stackView.shake()
            }
            else{
                self.view.endEditing(true)
                APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Verifying Your Account...", presentingView: self.view)
                
                APIHandler.shared.verifyPassCode(phoneNumber: phoneNumber, passCode: self.codeTextField.text!, success: {(loginModel) in
                    APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                        SessionManager().setLogin(loginStatus: true)
                        UserDefaults.standard.set(loginModel.id, forKey: AppConstants.userId )
                        UserDefaults.standard.set(loginModel.accessToken, forKey: AppConstants.accessToken)
                        UserDefaults.standard.set(loginModel.name, forKey: AppConstants.userName)
                        UserDefaults.standard.set(loginModel.email, forKey: AppConstants.userEmail)
                        UserDefaults.standard.set(loginModel.uniqueId, forKey: AppConstants.uniqueId)
                        
                        let alertController = DOAlertController(title: "Success!!!", message: "You are registered successfully" , preferredStyle: .alert)
                        alertController.alertViewBgColor = UIColor.white
                        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                        alertController.messageView.textAlignment = .left
                        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                        alertController.titleTextColor = UIColor.red
                        let okAction = DOAlertAction(title: "Close", style: .default, handler: { action in
                            self.dismiss(animated: true, completion: nil)
                           // self.navigationController?.popToRootViewController(animated: true)
                            let vc = HomeVC.instantiate(fromAppStoryboard: .CineGold)
                            let navvc = UINavigationController(rootViewController: vc)
                            let appdelegate = UIApplication.shared.delegate as? AppDelegate
                            appdelegate?.changeRootViewController(with: navvc)
                            //self.changeRootViewController(with: navvc)
                        })
                        // Add the action.
                        alertController.addAction(okAction)
                        // Show alert
                        self.present(alertController, animated: true, completion: nil)
                    })
                }, failure: {(failureMessage) in

                    APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                    
                        
                        let alertController = DOAlertController(title: "Something went wrong!!!", message: failureMessage , preferredStyle: .alert)
                        alertController.alertViewBgColor = UIColor.white
                        alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                        alertController.messageView.textAlignment = .left
                        alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                        alertController.titleTextColor = UIColor.red
                        
                        let okAction = DOAlertAction(title: "Close", style: .default, handler: { action in
                            self.dismiss(animated: true, completion: nil)
                            self.codeTextField.becomeFirstResponder()

                            
                        })
                        // Add the action.
                        alertController.addAction(okAction)
                        // Show alert
                        self.present(alertController, animated: true, completion: nil)
                    
                    
                    
                    })
                    
               
                    
                    
                    
                }, error: {(error) in
                    
                    
                    
                    self.codeTextField.becomeFirstResponder()

                    
                    
                })
                
                
            }
        }
        
        
        
        
    }
  
    
    func initTimer(){
        count = 60
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(fireCountdownTimer), userInfo: nil, repeats: true)
    }
    
    @objc func fireCountdownTimer() {
        resendCodeOutlet.isEnabled = false
        resendCodeOutlet.isHidden = true
        countDownLabel.isHidden = false

        if(count > 0){
            count -= 1
            countDownLabel.text = "Resend Code in \(count)s"
        }else if(count == 0){
            resendCodeOutlet.isEnabled = true
            resendCodeOutlet.isHidden = false
            countDownLabel.isHidden = true
            timer.invalidate()
        }
    }
    
    func resendCodeAPI(){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please Wait....", presentingView: self.view)
        
        APIHandler.shared.resendPassword(phoneNumber: self.phoneNumber) { (status, msg) in
            
            if(status){
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                    
                    self.initTimer()
                    
                })
            }else{
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                    let alertController = DOAlertController(title: "Something went wrong!!!", message: msg , preferredStyle: .alert)
                    alertController.alertViewBgColor = UIColor.white
                    alertController.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)
                    alertController.messageView.textAlignment = .left
                    alertController.messageFont = UIFont(name: "HelveticaNeue", size: 13)
                    alertController.titleTextColor = UIColor.red
                    
                    let okAction = DOAlertAction(title: "Close", style: .default, handler: { action in
                        self.dismiss(animated: true, completion: nil)
                        
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.present(alertController, animated: true, completion: nil)                })
            }
        } failure: { (err) in
            print(err.localizedDescription)
        }
    }
  

    override func viewDidLoad() {
        super.viewDidLoad()
       // pinviewSetup()
        digit1TextField.textContentType = .oneTimeCode
        initTimer()
        self.navigationController?.isNavigationBarHidden = true
        
        var rect = CGRect(x:10, y:33, width:30, height:30)
        if UIDevice.current.screenType == .iPhone_XR || UIDevice.current.screenType == .iPhones_X_XS || UIDevice.current.screenType == .iPhone_XSMax   {
            rect = CGRect(x:10, y:50, width:30, height:30)
        }
        
        let backButton = UIButton() // back button
        backButton.backgroundColor = UIColor.white
        backButton.layer.cornerRadius = 15
        backButton.addShadow(offset: CGSize(width: 1, height: 1), color: .darkGray, radius: 2, opacity: 0.9)
        backButton.frame = rect
        backButton.setImage(UIImage(named: "icon_navBar_Back"), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(22,22,22,23)
        backButton.imageView?.tintColor = UIColor.darkGray
        self.view.addSubview(backButton)
        backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)


        codeTextField.isHidden = true
        codeTextField.delegate = self
        codeTextField.becomeFirstResponder()
        verificationTitleLabel.text = "We've just sent a 4 digit code to \n "+phoneNumber+". \n Enter it below and you're in."
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false


        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        digit1TextField.becomeFirstResponder()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    func pinviewSetup() {
        pinView.pinLength = 4
        pinView.secureCharacter = "\u{25CF}"
        pinView.interSpace = 15
        pinView.textColor = UIColor.black
        pinView.shouldSecureText = true
        pinView.style = .box

        pinView.borderLineColor = UIColor.black
        pinView.activeBorderLineColor = UIColor.lightGray
        pinView.borderLineThickness = 0.7
        pinView.activeBorderLineThickness = 3

        pinView.font = UIFont.systemFont(ofSize: 15)
        pinView.keyboardType = .phonePad
        pinView.keyboardAppearance = .default
        pinView.pinInputAccessoryView = UIView()
        pinView.fieldCornerRadius = 4
        pinView.becomeFirstResponderAtIndex = 0

    }

    
    @objc func dismissVC (){
       
        let alert = UIAlertController(title: "Exit Registration?", message: "Are you sure you want to exit the registration?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
           
            self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
  

}

extension PassCodeVerificationViewController: UITextFieldDelegate {
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let code = textField.text! + string
        
        switch code.count{
        case 1:
            digit1TextField.text = string
        case 2:
            digit2TextField.text = string
        case 3:
            digit3TextField.text = string
        case 4:
            digit4TextField.text = string
            print(digit4TextField.text)
        //submit your code here
        default: break
        }
        print(textField.text!.count + (string.count - range.length) <= 4)
        return textField.text!.count + (string.count - range.length) <= 4;
        

    }
    
}
