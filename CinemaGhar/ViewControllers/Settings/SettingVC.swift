//
//  SettingVC.swift
//  CinemaGhar
//
//  Created by Midas on 16/08/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import Alamofire
import SafariServices
import MessageUI
import UserNotifications
enum settingType: String {
    case pushNotification = "Push Notification"
    case about = "About"
    case contactus = "Contact Us"
    case privacyPolicy = "Privacy Policy"
    case changePin = "Change Pin"
    case logout = "Logout"
}
struct SettingModel {
    var image: String?
    var title: String?
    var type: settingType
    
}

class SettingVC: UIViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet var footerView: UIView!
    
    @IBOutlet weak var tablevIew: UITableView!
    var sessionManager = SessionManager()
    let defaults = UserDefaults.standard
    var dataSource = [SettingModel]() {
        didSet {
            tablevIew.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetup()
        adData()
    }
    func uisetup() {
        tablevIew.register(UINib(nibName: "SettingsTVC", bundle: nil), forCellReuseIdentifier: "SettingsTVC")
        tablevIew.dataSource = self
        tablevIew.delegate = self
        tablevIew.tableFooterView = footerView
        tablevIew.backgroundView?.backgroundColor = .clear
        tablevIew.backgroundColor = .clear
        
    }
    func adData() {
        if sessionManager.isLoggedIn() == true {
            dataSource = [SettingModel(image: "iconNotification", title: "Push Notification", type: .pushNotification),SettingModel(image: "iconAboutUs", title: "About", type: .about),SettingModel(image: "iconContactUs", title: "Contact Us", type: .contactus),SettingModel(image: "iconTerms", title: "Privacy Policy", type: .privacyPolicy),SettingModel(image: "iconChangePin", title: "Change Pin", type: .changePin),SettingModel(image: "iconLogOut", title: "Logout", type: .logout)]
        } else {
            dataSource = [SettingModel(image: "iconNotification", title: "Push Notification", type: .pushNotification),SettingModel(image: "iconAboutUs", title: "About", type: .about),SettingModel(image: "iconContactUs", title: "Contact Us", type: .contactus),SettingModel(image: "iconTerms", title: "Privacy Policy", type: .privacyPolicy),SettingModel(image: "iconLogOut", title: "SignIn", type: .logout)]
        }
      
    }
    
    @IBAction func backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func twitterTap(_ sender: Any) {
    }
    @IBAction func fbTap(_ sender: Any) {
        let url =  URL(string: "https://www.facebook.com/cinemaghargold")!
        
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func instaTap(_ sender: Any) {
        let url =  URL(string: "https://www.instagram.com/cinemaghar/")!
        
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func linkedInTap(_ sender: Any) {
    }
    

}
//MARK: - TABLE DATASOURCE
extension SettingVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell = tablevIew.dequeueReusableCell(withIdentifier: "SettingsTVC", for: indexPath) as! SettingsTVC
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
        if indexPath.row == 0 {
            cell.switchControl.addTarget(self, action: #selector(switchStateChanged(_:)), for: .touchUpInside)
            cell.switchControl.isHidden = false
            cell.iconNext.isHidden = true
            cell.backView.roundCorners([.topLeft,.topRight], radius: 16)
            cell.backView.layer.masksToBounds = false
            cell.borderView.isHidden = false
        } else if (dataSource.count - 1) == indexPath.row{
            cell.switchControl.isHidden = true
            cell.iconNext.isHidden = false
            cell.backView.roundCorners([.bottomLeft,.bottomRight], radius: 16)
            cell.backView.layer.masksToBounds = false
            cell.borderView.isHidden = true
        }else {
            cell.switchControl.isHidden = true
            cell.iconNext.isHidden = false
            cell.borderView.isHidden = false
        }
        cell.leftBtn.setImage(UIImage(named: dataSource[indexPath.row].image ?? ""), for: .normal)
        cell.titleLbl.text = dataSource[indexPath.row].title
        cell.selectionColor = .clear
        return cell
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
}
//MARK: - TABLE DELEGATE
extension SettingVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let type = dataSource[indexPath.row].type
        if type == .pushNotification {
            
        } else if type == .about {
            abouttap()
        } else if type == .contactus {
            contactTap()
        } else if type == .privacyPolicy {
            privacytap()
        } else if type == .changePin {
            changePintap()
        } else if type == .logout {
           logOutTap()
        }
    }
}
extension SettingVC {
    @objc func switchStateChanged(_ sender: Any) {
        guard let pushToggle = sender as? UISwitch else {
            return
        }
        
        
        let current = UNUserNotificationCenter.current()
        
        current.getNotificationSettings(completionHandler: { (settings) in
            if settings.authorizationStatus == .notDetermined {
                // Notification permission has not been asked yet, go for it!
                DispatchQueue.main.async {
                    pushToggle.setOn(false, animated: true)
                }
            } else if settings.authorizationStatus == .denied {
                // Notification permission was previously denied, go to settings & privacy to re-enable
                
                DispatchQueue.main.async {
                    pushToggle.setOn(false, animated: true)
                
                let alert = UIAlertController(title: "Notifications not enabled", message: "You seem to have disabled the notifications for this app. You can enable them under notifications section of your phone's settings menu.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                alert.addAction(UIAlertAction(title: "Open Settings", style: .default, handler: { (action: UIAlertAction) in
                    
                    DispatchQueue.main.async {
                        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                            return
                        }
                        
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in

                                })
                            } else {
                                UIApplication.shared.openURL(settingsUrl as URL)
                            }
                        }
                    }
                    
                    
                }))
                
                
                self.present(alert, animated: true, completion: nil)
                    
                    
            }
                
                
            } else if settings.authorizationStatus == .authorized {
                // Notification permission was already granted
                
                DispatchQueue.main.async {
                    if pushToggle.isOn{
                        UIApplication.shared.registerForRemoteNotifications()
                        UserDefaults.standard.set(true, forKey: "shouldReceiveNotification")
                        
                    }
                    else{
                        
                        UserDefaults.standard.set(false, forKey: "shouldReceiveNotification")
                        UIApplication.shared.unregisterForRemoteNotifications()
                        
                    }
                }
                
                
                
            }
        })

    }
    func contactTap() {
        let alertController = DOAlertController(title: "Contact Us", message: "Please choose what your subject of contact is below." , preferredStyle: .alert)
        
        alertController.alertViewBgColor = UIColor.white
                    
        let reportAction = DOAlertAction(title: "Report", style: .default, handler: {_ in
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setSubject("Report from Cinemaghar iOS App")

                mail.setToRecipients(["admin@cinemaghar.app"])
               

                mail.navigationBar.tintColor = Util.hexStringToUIColor(hex: "#243BA4")
                mail.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : Util.hexStringToUIColor(hex: "#243BA4")]

                self.dismiss(animated: true, completion: nil)
                self.present(mail, animated: true)

            } else {

                let alertController = DOAlertController(title: "Email app not configured!", message: "Please open email app on your iPhone and set it up with an email address." , preferredStyle: .alert)

                alertController.alertViewBgColor = UIColor.white

//                alertController.titleTextColor = UIColor.red

                let okAction = DOAlertAction(title: "Okay", style: .cancel, handler: {_ in
                    self.dismiss(animated: true, completion: nil)
                })
                // Add the action.
                alertController.addAction(okAction)
                // Show alert
                self.dismiss(animated: true, completion: nil)
                self.present(alertController, animated: true, completion: nil)

            }
        })
        let feedbackAction = DOAlertAction(title: "Feedback", style: .default, handler: {_ in
            if MFMailComposeViewController.canSendMail() {
                print("Sending mail")
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setSubject("Feedback from Cinemaghar iOS App")

                mail.setToRecipients(["hello@cinemaghar.app"])
               

                mail.navigationBar.tintColor = Util.hexStringToUIColor(hex: "#243BA4")
                mail.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : Util.hexStringToUIColor(hex: "#243BA4")]

                self.dismiss(animated: true, completion: nil)
                self.navigationController!.present(mail, animated: true)

            } else {

                let alertController = DOAlertController(title: "Email app not configured!", message: "Please open email app on your iPhone and set it up with an email address." , preferredStyle: .alert)

                alertController.alertViewBgColor = UIColor.white

//                alertController.titleTextColor = UIColor.red

                let okAction = DOAlertAction(title: "Okay", style: .cancel, handler: {_ in
                    self.dismiss(animated: true, completion: nil)
                })
                // Add the action.
                alertController.addAction(okAction)
                // Show alert
                self.dismiss(animated: true, completion: nil)
                self.present(alertController, animated: true, completion: nil)

            }
            
        })
        
        let whatsappAction = DOAlertAction(title: "Whatsapp", style: .default, handler: {_ in
            let phoneNumber =  "+9779616600666"
            let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
            if UIApplication.shared.canOpenURL(appURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                }
                else {
                    UIApplication.shared.openURL(appURL)
                }
            } else {
                // WhatsApp is not installed
                print("Whatsapp not installed")
            }
            self.dismiss(animated: true, completion: nil)
            
        })
        // Add the action.
        alertController.addAction(reportAction)
        alertController.addAction(feedbackAction)
        alertController.addAction(whatsappAction)
        // Show alert
        present(alertController, animated: true, completion: nil)
        
    }
    func privacytap() {
        let url =  URL(string: "https://cinema-ghar.com/privacy-policy")!
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    func logOutTap() {
        if sessionManager.isLoggedIn(){
            UserDefaults.standard.set(0.0, forKey: Constant.userProgress)
        APESuperHUD.showOrUpdateHUD( loadingIndicator: .standard, message: "Signing Out...", presentingView: self.view)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.5, execute: {
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                let defaults = UserDefaults.standard
                let dictionary = defaults.dictionaryRepresentation()
                dictionary.keys.forEach { key in
                    print(key)
                    defaults.removeObject(forKey: key)
                }
//                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                    let signUPVC = CineGoldViewController.instantiate(fromAppStoryboard: .CineGold)
                let signUPVC = HomeVC.instantiate(fromAppStoryboard: .CineGold)
//                    let nav = UINavigationController(rootViewController: signUPVC)
//                    nav.isNavigationBarHidden = true
//                    self.showViewControllerWith(newViewController: nav, usingAnimation: .ANIMATE_LEFT)
                self.navigationController?.pushViewController(signUPVC, animated: true)
                defaults.set("", forKey: Constant.disableOneDay)
                
            })
        })
        }
        else {
            let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
}
    func abouttap() {
        let url = URL(string: "https://cinema-ghar.com/")!
        if  UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    func changePintap() {
        let changePinVC = ChangePinViewController.instantiate(fromAppStoryboard: .ChangePin)
        self.navigationController?.pushViewController(changePinVC, animated: true)
    }
}
