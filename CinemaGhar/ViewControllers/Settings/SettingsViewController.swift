//
//  SettingsViewController.swift
//  CinemaGhar
//
//  Created by Sunil on 6/27/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI
import UserNotifications


class SettingsViewController: DesignableViewController, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var switchControl: UISwitch!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var changePin: UIView!
    @IBOutlet weak var logoutImageView: UIImageView!
    @IBOutlet weak var tutorials: UIView!
    @IBOutlet weak var facebook: UIImageView!
    @IBOutlet weak var instagram: UIImageView!
    @IBOutlet weak var website: UIImageView!
    @IBOutlet weak var callTo: UIImageView!

    var sessionManager = SessionManager()
    
    @objc func facebookClicked(_ sender: Any){
        let url =  URL(string: "https://www.facebook.com/cinemaghargold")!
        
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc func instagramClicked(_ sender: Any){
        let url =  URL(string: "https://www.instagram.com/cinemaghar/")!
        
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc func websiteClicked(_ sender: Any){
        let url =  URL(string: "https://cinema-ghar.com/")!
        
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc func callClicked(_ sender: Any){
        
        let url =  URL(string: "tel://+9779616600666")!
        
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
 
    
    
    @IBAction func switchStateChanged(_ sender: Any) {
        guard let pushToggle = sender as? UISwitch else {
            return
        }
        
        
        let current = UNUserNotificationCenter.current()
        
        current.getNotificationSettings(completionHandler: { (settings) in
            if settings.authorizationStatus == .notDetermined {
                // Notification permission has not been asked yet, go for it!
                DispatchQueue.main.async {
                    pushToggle.setOn(false, animated: true)
                }
            } else if settings.authorizationStatus == .denied {
                // Notification permission was previously denied, go to settings & privacy to re-enable
                
                DispatchQueue.main.async {
                    pushToggle.setOn(false, animated: true)
                
                let alert = UIAlertController(title: "Notifications not enabled", message: "You seem to have disabled the notifications for this app. You can enable them under notifications section of your phone's settings menu.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                alert.addAction(UIAlertAction(title: "Open Settings", style: .default, handler: { (action: UIAlertAction) in
                    
                    DispatchQueue.main.async {
                        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                            return
                        }
                        
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in

                                })
                            } else {
                                UIApplication.shared.openURL(settingsUrl as URL)
                            }
                        }
                    }
                    
                    
                }))
                
                
                self.present(alert, animated: true, completion: nil)
                    
                    
            }
                
                
            } else if settings.authorizationStatus == .authorized {
                // Notification permission was already granted
                
                DispatchQueue.main.async {
                    if pushToggle.isOn{
                        UIApplication.shared.registerForRemoteNotifications()
                        UserDefaults.standard.set(true, forKey: "shouldReceiveNotification")
                        
                    }
                    else{
                        
                        UserDefaults.standard.set(false, forKey: "shouldReceiveNotification")
                        UIApplication.shared.unregisterForRemoteNotifications()
                        
                    }
                }
                
                
                
            }
        })

        
       
        
        
    }
    
    
    var isRestoredPurchases: Bool = false
    
    @IBOutlet weak var logOutViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var logOutView: UIView!
    @IBOutlet weak var navBar: UINavigationBar!
    
    var delegate: RefreshCollectionViews?
    @IBAction func logoutBtnPressed(_ sender: Any) {
        
        
    }
    @IBAction func restorePurchasesBtnPressed(_ sender: Any) {
        APESuperHUD.showOrUpdateHUD( loadingIndicator: .standard, message: "Restoring Purchases...", presentingView: self.view)

        
        
    }
    @IBAction func settingsBtnPressed(_ sender: Any) {
        guard let button = sender as? UIButton else {
            return
        }
        if(button.tag == 1){
            let url = URL(string: "https://cinema-ghar.com/")!
            if  UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            
        }
            
        else if(button.tag == 2){
            
            let alertController = DOAlertController(title: "Contact Us", message: "Please choose what your subject of contact is below." , preferredStyle: .alert)
            
            alertController.alertViewBgColor = UIColor.white
                        
            let reportAction = DOAlertAction(title: "Report", style: .default, handler: {_ in
                if MFMailComposeViewController.canSendMail() {
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setSubject("Report from Cinemaghar iOS App")

                    mail.setToRecipients(["admin@cinemaghar.app"])
                   

                    mail.navigationBar.tintColor = Util.hexStringToUIColor(hex: "#243BA4")
                    mail.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : Util.hexStringToUIColor(hex: "#243BA4")]

                    self.dismiss(animated: true, completion: nil)
                    self.present(mail, animated: true)

                } else {

                    let alertController = DOAlertController(title: "Email app not configured!", message: "Please open email app on your iPhone and set it up with an email address." , preferredStyle: .alert)

                    alertController.alertViewBgColor = UIColor.white

    //                alertController.titleTextColor = UIColor.red

                    let okAction = DOAlertAction(title: "Okay", style: .cancel, handler: {_ in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.dismiss(animated: true, completion: nil)
                    self.present(alertController, animated: true, completion: nil)

                }
            })
            let feedbackAction = DOAlertAction(title: "Feedback", style: .default, handler: {_ in
                if MFMailComposeViewController.canSendMail() {
                    print("Sending mail")
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setSubject("Feedback from Cinemaghar iOS App")

                    mail.setToRecipients(["hello@cinemaghar.app"])
                   

                    mail.navigationBar.tintColor = Util.hexStringToUIColor(hex: "#243BA4")
                    mail.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : Util.hexStringToUIColor(hex: "#243BA4")]

                    self.dismiss(animated: true, completion: nil)
                    self.navigationController!.present(mail, animated: true)

                } else {

                    let alertController = DOAlertController(title: "Email app not configured!", message: "Please open email app on your iPhone and set it up with an email address." , preferredStyle: .alert)

                    alertController.alertViewBgColor = UIColor.white

    //                alertController.titleTextColor = UIColor.red

                    let okAction = DOAlertAction(title: "Okay", style: .cancel, handler: {_ in
                        self.dismiss(animated: true, completion: nil)
                    })
                    // Add the action.
                    alertController.addAction(okAction)
                    // Show alert
                    self.dismiss(animated: true, completion: nil)
                    self.present(alertController, animated: true, completion: nil)

                }
                
            })
            
            let whatsappAction = DOAlertAction(title: "Whatsapp", style: .default, handler: {_ in
                let phoneNumber =  "+9779616600666"
                let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
                if UIApplication.shared.canOpenURL(appURL) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                    }
                    else {
                        UIApplication.shared.openURL(appURL)
                    }
                } else {
                    // WhatsApp is not installed
                    print("Whatsapp not installed")
                }
                self.dismiss(animated: true, completion: nil)
                
            })
            // Add the action.
            alertController.addAction(reportAction)
            alertController.addAction(feedbackAction)
            alertController.addAction(whatsappAction)
            // Show alert
            present(alertController, animated: true, completion: nil)
            

            
        }
            
        else if(button.tag == 3){
            
            let url =  URL(string: "https://cinema-ghar.com/privacy-policy")!
            
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            
           
            
        }
            
        else if(button.tag == 4){
            
//            let imageDataDict:[String: Int] = ["statusCode": 300]
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "listenStatusCode"), object: nil, userInfo: imageDataDict)
//            logOutUser()
            
            if sessionManager.isLoggedIn(){
            
            APESuperHUD.showOrUpdateHUD( loadingIndicator: .standard, message: "Signing Out...", presentingView: self.view)
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.5, execute: {
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: {
                    let defaults = UserDefaults.standard
                    let dictionary = defaults.dictionaryRepresentation()
                    dictionary.keys.forEach { key in
                        print(key)
                        defaults.removeObject(forKey: key)
                    }
//                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    
                    
//                    let signUPVC = CineGoldViewController.instantiate(fromAppStoryboard: .CineGold)
                    let signUPVC = HomeVC.instantiate(fromAppStoryboard: .CineGold)
                    
                    
//                    let nav = UINavigationController(rootViewController: signUPVC)
//                    nav.isNavigationBarHidden = true
//                    self.showViewControllerWith(newViewController: nav, usingAnimation: .ANIMATE_LEFT)
                    self.navigationController?.pushViewController(signUPVC, animated: true)
                   
      
                })
                
            })
                
            }
            else {
                let loginVC = SignUpViewController.instantiate(fromAppStoryboard: .SignUp)
                self.navigationController?.pushViewController(loginVC, animated: true)
            }

            
            
            
            
        }
            
        else if (button.tag == 5){
//            let webVC = WebViewController(urlToLoad: "http://cinemagharapp.com/")
//            webVC.delegate = self
//            webVC.toolBarTintColor = UIColor.gray.withAlphaComponent(0.5)
//            webVC.progressTintColor = .blue
//            let navigationVC = UINavigationController(rootViewController: webVC)
//            
//            present(navigationVC, animated: true, completion: nil)
        }
        else if(button.tag == 6){
            
            let url = URL(string: "https://cinema-ghar.com")!
            
            if  UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            
         
            
        }
        
        
        
        
    }
    
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    enum AnimationType{
        case ANIMATE_RIGHT
        case ANIMATE_LEFT
        case ANIMATE_UP
        case ANIMATE_DOWN
    }
    // Create Function...
    
    func showViewControllerWith(newViewController:UIViewController, usingAnimation animationType:AnimationType)
    {
        
        let currentViewController = UIApplication.shared.delegate?.window??.rootViewController
        let width = currentViewController?.view.frame.size.width;
        let height = currentViewController?.view.frame.size.height;
        
        var previousFrame:CGRect?
        var nextFrame:CGRect?
        
        switch animationType
        {
        case .ANIMATE_LEFT:
            previousFrame = CGRect(x: width!-1, y: 0.0, width: width!, height: height!)
            nextFrame = CGRect(x: -width!, y: 0.0,  width: width!, height: height!);
        case .ANIMATE_RIGHT:
            previousFrame = CGRect(x: -width!+1, y: 0.0, width: width!, height: height!);
            nextFrame = CGRect(x: width!, y: 0.0, width: width!, height: height!);
        case .ANIMATE_UP:
            previousFrame = CGRect(x: 0.0, y: height!-1, width: width!, height: height!);
            nextFrame = CGRect(x: 0.0, y: -height!+1, width: width!, height: height!);
        case .ANIMATE_DOWN:
            previousFrame = CGRect(x: 0.0, y: -height!+1, width: width!, height: height!);
            nextFrame = CGRect(x: 0.0, y: height!-1, width: width!, height: height!);
        }
        
        newViewController.view.frame = previousFrame!
        UIApplication.shared.delegate?.window??.addSubview(newViewController.view)
        UIView.animate(withDuration: 0.33,
                                   animations: { () -> Void in
                                    newViewController.view.frame = (currentViewController?.view.frame)!
                                    currentViewController?.view.frame = nextFrame!
                                    
        })
        { (fihish:Bool) -> Void in
            UIApplication.shared.delegate?.window??.rootViewController = newViewController
        }
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if(UserDefaults.standard.object(forKey: "shouldReceiveNotification") == nil){
            self.switchControl.setOn(true, animated: false)
            
        }
            
        else {
            if(UserDefaults.standard.bool(forKey:"shouldReceiveNotification")){
                self.switchControl.setOn(true, animated: false)
                
            }
            else {
                self.switchControl.setOn(false, animated: false)
                
                
            }
            
        }
        
        
        
        changePin.isUserInteractionEnabled = true
        let gestureChangePin = UITapGestureRecognizer(target: self, action:  #selector (self.changePinAction (_:)))
        self.changePin.addGestureRecognizer(gestureChangePin)
        
        tutorials.isUserInteractionEnabled = true
        let gestureTutorials = UITapGestureRecognizer(target: self, action:  #selector (self.tutorials (_:)))
        self.tutorials.addGestureRecognizer(gestureTutorials)
        
        
    
        
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back_arrow"), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        button.sizeToFit()
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)
        self.navItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        APESuperHUD.appearance.backgroundBlurEffect = .dark
        APESuperHUD.appearance.titleFontSize = 20
        APESuperHUD.appearance.iconWidth = 48
        APESuperHUD.appearance.loadingActivityIndicatorColor = UIColor.darkText
        APESuperHUD.appearance.iconHeight = 48
        APESuperHUD.appearance.messageFontSize = 16
        APESuperHUD.appearance.textColor = UIColor.darkText
        
        APESuperHUD.appearance.messageFontName  = "Avenir-Medium"
        APESuperHUD.appearance.foregroundColor = .white
        
        
        if sessionManager.isLoggedIn() {
            
            loginLabel.text = "Log Out"
            changePin.isHidden = false
        }
        else {
//            logOutView.isHidden = true
//            logOutViewHeightConstraint.constant = 0
//            self.view.layoutIfNeeded()
            loginLabel.text = "Log In"
//            logoutImageView.image = 
            changePin.isHidden = true
        }
        

        // Do any additional setup after loading the view.
        let facebookTap = UITapGestureRecognizer(target: self, action: #selector(facebookClicked))
        facebook.isUserInteractionEnabled = true
        facebook.addGestureRecognizer(facebookTap)
        
        let instagramTap = UITapGestureRecognizer(target: self, action: #selector(instagramClicked))
        instagram.isUserInteractionEnabled = true
        instagram.addGestureRecognizer(instagramTap)
        
        let websiteTap = UITapGestureRecognizer(target: self, action: #selector(websiteClicked))
        website.isUserInteractionEnabled = true
        website.addGestureRecognizer(websiteTap)
        
        let callToTap = UITapGestureRecognizer(target: self, action: #selector(callClicked))
        callTo.isUserInteractionEnabled = true
        callTo.addGestureRecognizer(callToTap)
        
        
    }
    
   @objc func changePinAction(_ sender:UITapGestureRecognizer){
        let changePinVC = ChangePinViewController.instantiate(fromAppStoryboard: .ChangePin)
        self.navigationController?.pushViewController(changePinVC, animated: true)
   }
    
    @objc func tutorials(_ sender:UITapGestureRecognizer){
        let tutorialsVC = TutorialsViewController.instantiate(fromAppStoryboard: .Tutorials)
        self.navigationController?.pushViewController(tutorialsVC, animated: true)
    }

    
    @objc func dismissVC(){
        
        self.navigationController?.popViewController(animated: true)
        
        if isRestoredPurchases {
            if let reloadDelegate = self.delegate{
                
                reloadDelegate.refreshCollectionViews()
            }
            
        }
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}


