//
//  DownloadsVC.swift
//  CinemaGhar
//
//  Created by Midas on 02/06/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit
import VidLoader

class DownloadsVC: UIViewController {
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segmentView: UISegmentedControl!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var navTItle: UILabel!
    var downloadsData = [VideoData]()
    var showDonwloading = false
    private let vidLoaderHandler: VidLoaderHandler = .shared
    var downloadViedeoData = [VideoData]()
    
    var dataProvider: VideoListDataProviding!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        uisetup()
        loadData()
        dataProvider
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    func uisetup() {
        segmentView.selectedSegmentIndex = 0
        tableView.register(UINib(nibName: "DownloadsTVC", bundle: nil), forCellReuseIdentifier: "DownloadsTVC")
        tableView.register(UINib(nibName: "VideoCell", bundle: nil), forCellReuseIdentifier: "VideoCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    func loadData() {
        let videoListActions = VideoListActions(
            reloadData: { [weak self] in self?.tableView.reloadData() },
            showRemoveActionSheet: { [weak self] data in self?.showRemoveActionsSheet(with: data) },
            showStopActionSheet: { [weak self] data in self?.showStopActionsSheet(with: data) },
            showFailedActionSheet: { [weak self] data in self?.showFailedActionSheet(with: data) },
            showRunningActions: { [weak self] data in self?.showRunningActions(with: data) },
            showPausedActions: { [weak self] data in self?.showPausedActions(with: data) })
        dataProvider.setup(videoListActions: videoListActions)
        
        
        if showDonwloading == true {
            if let video = downloadsData.first {
                startDownload(with: video)
            }
            
        }
    }
    
    @IBAction func backtap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
//MARK: - START DOWNLOAD
    func startDownload(with data: VideoData) {
        guard let url = URL(string: data.stringURL) else { return }
       // guard let imageData = UIImageJPEGRepresentation(UIImage(named: data.imageName)!, 0.75) else { return }
        let downloadValues = DownloadValues(identifier: data.identifier,
                                            url: url,
                                            title: data.title,
                                            artworkData: Data(),
                                            minRequiredBitrate: 1, coverimage: data.coverImage ?? "")
        vidLoaderHandler.loader.download(downloadValues)
    }
    
}
extension DownloadsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return downloadsData.count
        return dataProvider.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath)
        //cell.backView.layer.masksToBounds = false
        (cell as? VideoCell)?.setup(model: dataProvider.videoModel(row: indexPath.row))
//        if showDonwloading == true {
//            cell.deleteBtn.isHidden = true
//            cell.retryBtn.isHidden = true
//            cell.failedLbl.isHidden = true
//            cell.progressBar.isHidden = false
//        } else {
//            cell.deleteBtn.isHidden = false
//            cell.retryBtn.isHidden = false
//            cell.failedLbl.isHidden = false
//            cell.progressBar.isHidden = true
//        }
        return cell
    }
    
    
}
extension DownloadsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
//MARK: - TABLE VIEW TAP ACTIONS
extension DownloadsVC {
    private func showRemoveActionsSheet(with data: VideoData) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.deleteVideo(with: data)
            self?.tableView.reloadData()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(actionSheet, animated: true)
    }

    private func showStopActionsSheet(with data: VideoData) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Stop", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.stopDownload(with: data)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(actionSheet, animated: true)
    }
    
    private func showFailedActionSheet(with data: VideoData) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.deleteVideo(with: data)
            self?.tableView.reloadData()
        }))
        actionSheet.addAction(UIAlertAction(title: "Retry", style: .default, handler: { [weak self] _ in
            self?.dataProvider.startDownload(with: data)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(actionSheet, animated: true)
    }
    
    private func showRunningActions(with data: VideoData) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Stop", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.stopDownload(with: data)
        }))
        actionSheet.addAction(UIAlertAction(title: "Pause", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.pauseDownload(with: data)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(actionSheet, animated: true)
    }
    
    private func showPausedActions(with data: VideoData) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Stop", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.stopDownload(with: data)
        }))
        actionSheet.addAction(UIAlertAction(title: "Resume", style: .destructive, handler: { [weak self] _ in
            self?.dataProvider.resumeDownload(with: data)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(actionSheet, animated: true)
    }
}
