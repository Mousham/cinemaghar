//
//  VotingRateTVC.swift
//  CinemaGhar
//
//  Created by Midas on 16/05/2022.
//  Copyright © 2022 sunBi. All rights reserved.
//

import UIKit

class VotingRateTVC: UITableViewCell {

    @IBOutlet weak var votingQtyLbl: UILabel!
    @IBOutlet weak var votingAmtLbl: UILabel!
    @IBOutlet weak var votingQtyView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var backView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uisetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func uisetup() {
        votingQtyView.layer.cornerRadius = 8
        nextBtn.layer.cornerRadius = 18
        nextBtn.backgroundColor = AppColors.redColor
        votingAmtLbl.textColor = AppColors.lightDarkColor
        votingQtyView.backgroundColor = AppColors.unSelectedColor
        votingQtyLbl.textColor = AppColors.orangeColor
        backView.backgroundColor = UIColor.init(hex: "#303744")
        backView.layer.cornerRadius = 12
    }
    
}
