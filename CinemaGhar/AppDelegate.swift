    //
//  AppDelegate.swift
//  CinemaGhar
//
//  Created by Sunil Gurung on 5/8/18.
//  Copyright © 2018 sunBi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import HLSDownloader
import SDWebImage
import GoogleCast
import AVKit
import GoogleMobileAds
import Firebase
//import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import FirebaseCore
import FirebaseFirestore
import FirebaseAuth

    
var backgrounDownloadTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
@UIApplicationMain
    class AppDelegate: UIResponder, UIApplicationDelegate {
    
    fileprivate var enableSDKLogging = false
    fileprivate var mediaNotificationsEnabled = false
    fileprivate var firstUserDefaultsSync = false
    fileprivate var useCastContainerViewController = false
    let gcmMessageIDKey = "gcm.message_id"
    let kReceiverAppID = kGCKDefaultMediaReceiverApplicationID
    let kDebugLoggingEnabled = true

    
    var isMoviePurchased = false
       
    var window: UIWindow?
        lazy var manager: HLSManager = {
            backgrounDownloadTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
                backgrounDownloadTask = UIBackgroundTaskInvalid
            })
            return DownloadHLSManager()
        }()
        
    var isCastControlBarsEnabled: Bool {
        get {
            if useCastContainerViewController {
                let castContainerVC = (window?.rootViewController as? GCKUICastContainerViewController)
                return castContainerVC!.miniMediaControlsItemEnabled
            } else {
                let rootContainerVC = (window?.rootViewController as? RootContainerViewController)
                return rootContainerVC!.miniMediaControlsViewEnabled
            }
        }
        set(notificationsEnabled) {
            if useCastContainerViewController {
                var castContainerVC: GCKUICastContainerViewController?
                castContainerVC = (window?.rootViewController as? GCKUICastContainerViewController)
                castContainerVC?.miniMediaControlsItemEnabled = notificationsEnabled
            } else {
                var rootContainerVC: RootContainerViewController?
                rootContainerVC = (window?.rootViewController as? RootContainerViewController)
                rootContainerVC?.miniMediaControlsViewEnabled = notificationsEnabled
            }
        }
    }
        

        let notificationDelegate = NotificationDelegate()
    


    var orientationLock = UIInterfaceOrientationMask.portrait
    var myOrientation: UIInterfaceOrientationMask = .portrait
        
        func changeRootViewController(with desiredViewController: UIViewController) {
            let snapshot: UIView = (self.window?.snapshotView(afterScreenUpdates: true))!
            desiredViewController.view.addSubview(snapshot)
            
            self.window?.rootViewController = desiredViewController
            UIView.animate(withDuration: 0.3, animations: {() in
                snapshot.layer.opacity = 0
                snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
            }, completion: { (_: Bool) in
                snapshot.removeFromSuperview()
            })
        }
            
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        let handled = DynamicLinks.dynamicLinks()
           .handleUniversalLink(userActivity.webpageURL!) { dynamiclink, error in
               print(dynamiclink)
             // ...
           }

         return handled
        return false
    }
        
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        window = UIWindow(frame: UIScreen.main.bounds)
//        window?.makeKeyAndVisible()
        //GADMobileAds.configure(withApplicationID: "ca-app-pub-4373633293802764~3690632803")
        GADMobileAds.sharedInstance().start(completionHandler: nil)

        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch let setCategoryError {
            print("Error setting audio category: \(setCategoryError.localizedDescription)")
        }
        
        useCastContainerViewController = false
        
        
       

        
//        let options = GCKCastOptions(receiverApplicationID: "110CB705")
        let options  =  GCKCastOptions(discoveryCriteria: GCKDiscoveryCriteria(applicationID: "110CB705"))
        options.physicalVolumeButtonsWillControlDeviceVolume = true
        GCKCastContext.setSharedInstanceWith(options)
        GCKCastContext.sharedInstance().useDefaultExpandedMediaControls = true

        GCKLogger.sharedInstance().delegate = self

        //cast ui customization

        let castStyle = GCKUIStyle.sharedInstance()
        castStyle.castViews.backgroundColor = Util.hexStringToUIColor(hex: "1E2736")
        castStyle.castViews.iconTintColor = UIColor.white
        castStyle.castViews.mediaControl.miniController.headingTextColor = UIColor.white
        castStyle.castViews.mediaControl.miniController.captionTextColor = UIColor.lightGray
        castStyle.castViews.deviceControl.deviceChooser.headingTextColor = UIColor.white
        castStyle.castViews.deviceControl.deviceChooser.captionTextColor = UIColor.white
        castStyle.castViews.mediaControl.captionTextColor = UIColor.lightGray
        castStyle.castViews.deviceControl.connectionController.headingTextColor = UIColor.white
        castStyle.castViews.deviceControl.connectionController.buttonTextColor = UIColor.red
        castStyle.castViews.deviceControl.connectionController.buttonTextFont = UIFont.systemFont(ofSize: 16, weight: .medium)
        castStyle.castViews.deviceControl.connectionController.captionTextColor = UIColor.white
        castStyle.castViews.mediaControl.trackSelector.headingTextColor = UIColor.white
        castStyle.castViews.mediaControl.trackSelector.bodyTextColor = .white
        

     

        IQKeyboardManager.shared.enable = true
 

            if useCastContainerViewController {
                let appStoryboard = UIStoryboard(name: "OnBoarding", bundle: nil)
                guard let navigationController = appStoryboard.instantiateViewController(withIdentifier: "MainNavigation")
                    as? UINavigationController else { return false }

                let castContainerVC = GCKCastContext.sharedInstance().createCastContainerController(for: navigationController)
                    as GCKUICastContainerViewController

                castContainerVC.miniMediaControlsItemEnabled = true
                window = UIWindow(frame: UIScreen.main.bounds)
                window?.rootViewController = castContainerVC
                window?.makeKeyAndVisible()
            } else {
                
                let rootContainerVC = (window?.rootViewController as? RootContainerViewController)
                rootContainerVC?.miniMediaControlsViewEnabled = true
                
            }

    
//        GCKCastContext.sharedInstance().sessionManager.add(self)
        
        
        
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                
                // Enable or disable features based on authorization.
                if granted  == true {
                    UserDefaults.standard.set(false, forKey: "shouldReceiveNotification")
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                } else {
                    UserDefaults.standard.set(false, forKey: "shouldReceiveNotification")

                }
            }
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        if(UserDefaults.standard.object(forKey: "shouldReceiveNotification") == nil){
            application.registerForRemoteNotifications()
            
        }
        else {
            if(UserDefaults.standard.bool(forKey:"shouldReceiveNotification")){
                application.registerForRemoteNotifications()
            }
            
        }
        
        
        FirebaseApp.configure()

        
        

        return true
    }
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
      let criteria = GCKDiscoveryCriteria(applicationID: kReceiverAppID)
      let options = GCKCastOptions(discoveryCriteria: criteria)
      GCKCastContext.setSharedInstanceWith(options)

      // Enable logger.
      GCKLogger.sharedInstance().delegate = self
        
      GCKCastContext.sharedInstance().useDefaultExpandedMediaControls = true
        

    }

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return myOrientation
        }
    
    
    func shouldShowIQToolbar(status:Bool){
        if status {
            IQKeyboardManager.shared.enable = true
            IQKeyboardManager.shared.enableAutoToolbar = false
            IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
            IQKeyboardManager.shared.shouldResignOnTouchOutside = true

        }

        else{
            IQKeyboardManager.shared.enable = false

        }
    }
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
        
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("DidReceiveRemoteNotification")
        print("Message ID: \(messageID)")
      }

    }
    
   
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        completionHandler(UIBackgroundFetchResult.newData)
        UserDefaults.standard.set(true, forKey: "Notifications")
        
        
        if application.applicationState == .inactive || application.applicationState == .background {
            
            print("Here printing user info")
            print(userInfo)
            let type = userInfo
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SomeNotification"), object:type)
            
            
            UserDefaults.standard.set(true, forKey: "Notifications")
            
            
            
        }
        
        
        
        
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("FetchCompletion")
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
//        print("ddd\(userInfo)")
        
        
        
        
        
    }
        
    
    


}
    
    extension AppDelegate: GCKSessionManagerListener {

        func sessionManager(_ sessionManager: GCKSessionManager, didEnd session: GCKSession, withError error: Error?) {
            if error == nil {
                print("finished")
                  //  showAlert(withTitle: "Session complete", message: "session completed")

            } else {
                let message = "Cast Session ended unexpectedly:\n\(error?.localizedDescription ?? "")"
                showAlert(withTitle: "Session error", message: message)
            }
        }

        func sessionManager(_ sessionManager: GCKSessionManager, didFailToStart session: GCKSession, withError error: Error) {
            let message = "Failed to start session:\n\(error.localizedDescription)"
            showAlert(withTitle: "Session error", message: message)
        }

        func showAlert(withTitle title: String, message: String) {
            // TODO: Pull this out into a class that either shows an AlertVeiw or a AlertController
            let alert = UIAlertView(title: title, message: message,
                                    delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "")
            alert.show()
        }

    }


    
    extension AppDelegate: GCKLoggerDelegate {
        
        func logMessage(_ message: String, at level: GCKLoggerLevel, fromFunction function: String, location: String) {
            print("Message from Chromecast = \(message)")
        }
    }

    
    @available(iOS 10, *)
    extension AppDelegate : UNUserNotificationCenterDelegate {
        
        // Receive displayed notifications for iOS 10 devices.
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    willPresent notification: UNNotification,
                                    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
            let userInfo = notification.request.content.userInfo
            // Print message ID.
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
            
            // Print full message.
            print("aaa\(userInfo)")
            
            // Change this to your preferred presentation option
            completionHandler([.alert, .badge, .sound])
        }
        
        
        
        //when we click on notificaiton
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    didReceive response: UNNotificationResponse,
                                    withCompletionHandler completionHandler: @escaping () -> Void) {
            let userInfo = response.notification.request.content.userInfo
            // Print message ID.
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
            
            // Print full message.
            print("\(userInfo)")
            print("\(userInfo["type"] ?? "normal")")
            print("\(userInfo["id"] ?? 0)")
            //
            //Code to be executed when you click notification
            let type = ["type":userInfo["type"] ?? "normal", "id":(userInfo["id"] ?? 0)]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SomeNotification"), object:type)
            
            
            //new
            if "\(userInfo["type"] ?? "normal")" == "downloads" {
             
               
                let vc = VideoListController.instantiate(fromAppStoryboard: .MovieDetail)
                vc.showAllDownloads = true
               
                if let nav = window?.rootViewController as? UINavigationController {
                    nav.pushViewController(vc, animated: true)
                }
                
            } else if "\(userInfo["type"] ?? "normal")" == "news" {
              
            
                
                
             
                
    
               // NotificationCenter.default.post(name: Notification.Name("HandlingPushNotification"), object: nil, userInfo: ["type":"news"])

                
                
                
            }
            completionHandler()
        }
        
        func configureNotification() {
            if #available(iOS 10.0, *) {
                let center = UNUserNotificationCenter.current()
                center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
                center.delegate = notificationDelegate
                let openAction = UNNotificationAction(identifier: "OpenNotification", title: NSLocalizedString("Abrir", comment: ""), options: UNNotificationActionOptions.foreground)
                let deafultCategory = UNNotificationCategory(identifier: "CustomSamplePush", actions: [openAction], intentIdentifiers: [], options: [])
                center.setNotificationCategories(Set([deafultCategory]))
            } else {
                UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            }
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    
 
