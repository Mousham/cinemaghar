//
//  IAPService.swift
//  CinemaGhar
//
//  Created by Shashank Gnawali on 2/18/21.
//  Copyright © 2021 Cinemaghar. All rights reserved.
//

import Foundation
import StoreKit

class IAPService: NSObject {
    
    private override init(){}
    static let shared = IAPService()
    
    var products = [SKProduct]()
    let paymentQueue = SKPaymentQueue.default()
    
    func getProducts(){
        print("sawsank: Getting products")
        NotificationCenter.default.post(name: NSNotification.Name("IAPPaymentStatusLoading"), object: nil)
        let products: Set = [IAPProduct.cinemagharGoldSubscription.rawValue, IAPProduct.coin100.rawValue, IAPProduct.coin200.rawValue, IAPProduct.coin300.rawValue, IAPProduct.coin500.rawValue, IAPProduct.coin1000.rawValue]
        let request = SKProductsRequest(productIdentifiers: products)
        request.delegate = self
        request.start()
        print("sawsank: Request Started")
        paymentQueue.add(self)
    }
    
    func purchase(product: IAPProduct){
//        for transaction in paymentQueue.transactions {
//            print(transaction)
//            paymentQueue.finishTransaction(transaction)
//        }
        print("sawsank: Purchasing...")
        NotificationCenter.default.post(name: NSNotification.Name("IAPPaymentStatusLoading"), object: nil)
        guard let productToPurchase = products.filter({ $0.productIdentifier == product.rawValue}).first else { return}
        print("sawsank: Product to purchase \(productToPurchase)")
        let payment = SKPayment(product: productToPurchase)
        
        paymentQueue.add(payment)
    }
    
    func restorePurchases(){
        
        paymentQueue.restoreCompletedTransactions()
    }
}

extension IAPService: SKProductsRequestDelegate{
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("sawsank: productsRequest")
        self.products = response.products
        print("sawsank: \(self.products)")
        for product in response.products {
            print(product.localizedTitle)
        }
        NotificationCenter.default.post(name: NSNotification.Name("IAPPaymentStatusNotLoading"), object: nil)
    }
    
    
}

extension IAPService: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            print(transaction.transactionState.status(), transaction.payment.productIdentifier)
            switch transaction.transactionState {
            case .purchasing: break
            case .purchased:
                print(transaction)
                getReceipt(transactionID: transaction.transactionIdentifier!, productID: transaction.payment.productIdentifier)
                queue.finishTransaction(transaction)
                break
            default:
                queue.finishTransaction(transaction)
                NotificationCenter.default.post(name: NSNotification.Name("IAPPaymentStatusNotLoading"), object: nil)
                break
            }
        }
        
    }
    
    
    func getReceipt(transactionID: String, productID: String){
        print("sawsank: Getting receipt")
        // Get the receipt if it's available
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {

            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                print(receiptData)

                let receiptString = receiptData.base64EncodedString(options: [])

                // Read receiptData
                processPayment(receiptString: receiptString, transactionID: transactionID, productID: productID)
            }
            catch { print("sawsank: Couldn't read receipt data with error: " + error.localizedDescription) }
        }
    }
    
    func processPayment(receiptString: String, transactionID: String, productID: String){
        print(productID)
        print("sawsank: Processing payment")
        let phone = UserDefaults.standard.string(forKey: "loadOthersWalletPhone")
//        if(productID.contains("coin")){
        //new method
        if globalisPurchasedFromApple == false {
            let coin = productID.split(separator: ".").last!
            print("sawsank: coin -> \(String(describing: coin))")
            APIHandler.shared.loadWallet(from: "appleInAppPurchase", token: receiptString, amount: String(describing: coin), phoneNumber: phone ?? "") { (msg) in
                print("sawsank:  success message -> \(msg)")
                NotificationCenter.default.post(name: NSNotification.Name("IAPPaymentStatusNotLoading"), object: nil, userInfo: ["message": msg])
                UserDefaults.standard.removeObject(forKey: "loadOthersWalletPhone")
            } failure: { (msg) in
                print("sawsank:  failure message -> \(msg)")
                NotificationCenter.default.post(name: NSNotification.Name("IAPPaymentStatusNotLoading"), object: nil, userInfo: ["message": msg])
                UserDefaults.standard.removeObject(forKey: "loadOthersWalletPhone")
            } error: { (err) in
                print("sawsank:  err -> \(err.localizedDescription)")
                NotificationCenter.default.post(name: NSNotification.Name("IAPPaymentStatusNotLoading"), object: nil, userInfo: ["message": err.localizedDescription])
                UserDefaults.standard.removeObject(forKey: "loadOthersWalletPhone")
            }

        }else{
            print(globalMovieDetail?.id)
            APIHandler.shared.validatePayment(paymentGateway: paymentGateway.appleIAP, mediaType: MediaType.movie, contentId: globalMovieDetail?.id ?? 0, episodeID: 0, paymentSuccessToken: receiptString, transactionID: transactionID) { (status, videoUrl, message) in
                if(status){
                    print("sawsank: Status -> \(status); message -> \(message)")
                    NotificationCenter.default.post(name: Notification.Name("ReloadData"), object: nil,userInfo: ["movieid": globalMovieDetail?.id ?? ""])
                }else{
                    print("sawsank: Status -> \(status); message -> \(message)")
                }
                NotificationCenter.default.post(name: NSNotification.Name("IAPPaymentStatusNotLoading"), object: nil, userInfo: ["message": message])

            } failure: { (err) in
                print("sawsank: Error \(err)")
                NotificationCenter.default.post(name: NSNotification.Name("IAPPaymentStatusNotLoading"), object: nil, userInfo: ["message": err.localizedDescription])

            }
            
        }
        
    }
    
}

extension SKPaymentTransactionState {
    
    func status() -> String {
        switch self {
        case .deferred:
            return "Deferred"
        case .failed:
            return "Failed"
        case .purchased:
            return "Purchased"
        case .purchasing:
            return "Purchasing"
        case .restored:
            return "Restored"
        }
    }
}


enum IAPProduct: String {
    case cinemagharGoldSubscription = "com.sagarkharel.cinemagharhd.consumablegold"
    case coin100 = "com.sagarkharel.cinemagharhd.vod.100"
    case coin200 = "com.sagarkharel.cinemagharhd.coin.200"
    case coin300 = "com.sagarkharel.cinemagharhd.coin.300"
    case coin500 = "com.sagarkharel.cinemagharhd.coin.500"
    case coin1000 = "com.sagarkharel.cinemagharhd.coin.1000"
}
